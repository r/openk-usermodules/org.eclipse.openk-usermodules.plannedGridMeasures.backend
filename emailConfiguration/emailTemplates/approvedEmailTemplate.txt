﻿To: approved1Recipient@test.de, approved2Recipient@test.de
CC: test1CCRecipient@test.de, test2CCRecipient@test.de
Subject: Die Maßnahme "$gridMeasureTitle$" mit Beginn: "$plannedStarttimeFirstSinglemeasure$" und Ende: $endtimeGridmeasure$ wurde in den Status Genehmigt geändert.

Body:
Sehr geehrte Damen und Herren,

die im Betreff genannte Maßnahme ist über folgenden Link erreichbar:

$directMeasureLink$

Mit freundlichen Grüßen

Ihre Admin-Meister-Team der PTA GmbH
