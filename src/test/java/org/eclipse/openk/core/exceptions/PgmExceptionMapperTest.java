/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.exceptions;


import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.apache.http.HttpStatus;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.common.util.ResourceLoaderBase;
import org.eclipse.openk.core.viewmodel.ErrorReturn;
import org.junit.Test;

public class PgmExceptionMapperTest extends ResourceLoaderBase {
    @Test
    public void testToJson() {
        String json = PgmExceptionMapper.toJson(new HttpStatusException(HttpStatus.SC_NOT_FOUND, "lalilu"));

        ErrorReturn er = JsonGeneratorBase.getGson().fromJson(json, ErrorReturn.class);
        assertEquals(404, er.getErrorCode());
        assertTrue(er.getErrorText().equals("lalilu"));
    }

    @Test
    public void testUnknownErrorToJson() {
        String json = PgmExceptionMapper.unknownErrorToJson();

        ErrorReturn er = JsonGeneratorBase.getGson().fromJson(json, ErrorReturn.class);
        assertEquals(500, er.getErrorCode());
    }

    @Test
    public void testGeneralOKJson() {
        String ok = PgmExceptionMapper.getGeneralOKJson();
        assertTrue("{\"ret\":\"OK\"}".equals(ok));
    }

    @Test
    public void testGeneralErrorJson() {
        String nok = PgmExceptionMapper.getGeneralErrorJson();
        assertTrue("{\"ret\":\"NOK\"}".equals(nok));
    }



}
