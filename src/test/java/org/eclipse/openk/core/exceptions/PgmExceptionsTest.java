/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.exceptions;

import static org.junit.Assert.assertEquals;

import org.eclipse.openk.core.viewmodel.ErrorReturn;
import org.junit.Test;

public class PgmExceptionsTest {

    @Test
    public void testPgmNestedExceptions() {
        ErrorReturn errorReturn = new ErrorReturn();
        errorReturn.setErrorText("this is an error");
        errorReturn.setErrorCode(404);
        assertEquals(404, new PgmNestedException(errorReturn).getHttpStatus());
        assertEquals(404, new PgmNestedException(errorReturn, new Throwable()).getHttpStatus());
        PgmNestedException exception = new PgmNestedException(errorReturn);
        assertEquals(404, exception.getHttpStatus());
    }

}
