/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.exceptions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HttpStatusExceptionTest {
    private final int httpStatus = 404;
    private final String httpText = "status";
    private final String httpPayload = "payload";

    @Test
    public void testHttpStatusException() {

        HttpStatusException hse = new HttpStatusException(httpStatus, httpText, new Throwable("testmessage"));

        assertEquals(new HttpStatusException(httpStatus).getHttpStatus(), httpStatus);
        assertEquals(new HttpStatusException(httpStatus, httpText).getHttpStatus(), httpStatus);
        assertEquals(new HttpStatusException(httpStatus, httpText, httpPayload).getPayload(), httpPayload);

    }
}
