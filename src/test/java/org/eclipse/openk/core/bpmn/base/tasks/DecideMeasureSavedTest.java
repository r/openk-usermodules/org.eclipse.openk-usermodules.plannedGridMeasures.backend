/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.base.tasks;

import static junit.framework.TestCase.assertEquals;

import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.DecideMeasureSaved;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class DecideMeasureSavedTest {

    @Test
    public void decideMethodNotNullIdTest() {
        DecideMeasureSaved dms = new DecideMeasureSaved();
        PlgmProcessSubject pps = null;
        try {
            pps = Whitebox.invokeConstructor(PlgmProcessSubject.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        GridMeasure gm = new GridMeasure();
        gm.setId(4);
        if (pps != null){
            pps.setGridMeasure(gm);
            DecisionTask.OutputPort r = dms.decide(pps);
            assertEquals(DecisionTask.OutputPort.YES, r);
        }

    }

    @Test
    public void decideMethodNullIdTest() {
        DecideMeasureSaved dms = new DecideMeasureSaved();
        PlgmProcessSubject pps = null;
        try {
            pps = Whitebox.invokeConstructor(PlgmProcessSubject.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        GridMeasure gm = new GridMeasure();
        gm.setId(null);
        if (pps != null){
            pps.setGridMeasure(gm);
            DecisionTask.OutputPort r = dms.decide(pps);
            assertEquals(DecisionTask.OutputPort.NO, r);
        }
    }

}
