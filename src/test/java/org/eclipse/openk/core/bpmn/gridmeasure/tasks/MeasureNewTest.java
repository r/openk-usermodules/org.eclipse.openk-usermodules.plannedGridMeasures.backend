/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import static junit.framework.TestCase.assertNotNull;

import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class MeasureNewTest {


    @Test
    public void prepareStorageTestNullFkRefStatus() throws Exception {
        GridMeasure gm = new GridMeasure();
        gm.setStatusId(null);
        MeasureNew mn  = new MeasureNew();
        PlgmProcessSubject subj = PlgmProcessSubject.fromGridMeasure(gm, "fd");
        Whitebox.invokeMethod(mn, "prepareStorage", gm);
        assertNotNull(gm.getStatusId());

        new MeasureNew().onEnterStep( subj );
    }

    @Test
    public void prepareStorageTestNotNullFkRefStatus() throws Exception {
        GridMeasure gm = new GridMeasure();
        gm.setStatusId(3);
        MeasureNew mn  = new MeasureNew();
        Whitebox.invokeMethod(mn, "prepareStorage", gm);
        assertNotNull(gm.getStatusId());
    }


}
