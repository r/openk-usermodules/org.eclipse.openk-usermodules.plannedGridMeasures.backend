/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.base.tasks;


import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.TestProcessSubject;
import org.junit.Test;

public class EndpointTaskTest {
    @Test
    public void testEndpoint_enterStep() throws ProcessException {
        TestProcessSubject subject = new TestProcessSubject();
        EndPointTask ep = new EndPointTask("Endpoint");
        ep.enterStep(subject);
    }

    @Test( expected = ProcessException.class)
    public void testEndpoint_connectTo() throws ProcessException {
        TestProcessSubject subject = new TestProcessSubject();
        EndPointTask ep = new EndPointTask("Endpoint");
        ep.connectOutputTo(null);
    }

    @Test ( expected = ProcessException.class)
    public void testEndpoint_leaveStep() throws ProcessException {
        TestProcessSubject subject = new TestProcessSubject();
        EndPointTask ep = new EndPointTask("Endpoint");
        ep.leaveStep(subject);
    }
}
