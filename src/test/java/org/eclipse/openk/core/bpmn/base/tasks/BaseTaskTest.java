/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.base.tasks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.core.bpmn.base.BaseTaskImpl;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.TestProcessSubject;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Test;

public class BaseTaskTest {
    private static final String DESC = "TestTask";


    @Test
    public void testBaseTask_NotConnected() throws ProcessException, HttpStatusException {
        BaseTaskImpl testtask1 = new BaseTaskImpl(DESC);

        TestProcessSubject subject = new TestProcessSubject();

        assertEquals(DESC, testtask1.getDescription());

        testtask1.enterStep(subject);
        assertTrue(testtask1.enterStepCalled);

        testtask1.leaveStep(subject);
        // fireOnconnectedOutput -> does nothing

    }

    @Test
    public void testBaseTask_Connected() throws ProcessException, HttpStatusException {
        BaseTaskImpl testtask1 = new BaseTaskImpl(DESC);
        BaseTaskImpl testtask2 = new BaseTaskImpl(DESC);

        TestProcessSubject subject = new TestProcessSubject();

        testtask1.connectOutputTo(testtask2);
        testtask1.leaveStep(subject);
        assertTrue(testtask2.enterStepCalled);



    }

}
