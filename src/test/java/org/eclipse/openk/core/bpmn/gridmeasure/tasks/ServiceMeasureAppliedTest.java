/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/



package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import org.eclipse.openk.PlannedGridMeasuresConfiguration;
import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.api.mail.EmailTemplatePaths;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.controller.EmailmanagerTest;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertNotNull;

public class ServiceMeasureAppliedTest {
    private static GreenMail mailServer;

    @BeforeClass
    public static void beforeAll() throws IOException {
        TestHelper.initDefaultBackendConfig();
        ServerSetup setup = new ServerSetup(EmailmanagerTest.port() , "localhost", ServerSetup.PROTOCOL_SMTP);
        setup.setServerStartupTimeout(3000);
        mailServer = new GreenMail(setup);
    }

    @Before
    public void prepareTests() throws Exception {
        // mock email-configuration
        PlannedGridMeasuresConfiguration.EmailConfiguration emailConfiguration = createEmailConfiguration();
        setTestTemplateEmail("emailConfigurationTest/emailTemplates/appliedEmailTemplateTest.txt");
        BackendConfig.getInstance().setEmailConfiguration(emailConfiguration);
        mailServer.start();
    }

    @After
    public void afterTest() {
        mailServer.stop();
    }

    @Test
    public void serviceMeasureAppliedTest() throws Exception{
        GridMeasure gm = new GridMeasure();
        PlgmProcessSubject subject = PlgmProcessSubject.fromGridMeasure(gm, "fd");

        ServiceMeasureApplied  ma = new ServiceMeasureApplied();

        assertNotNull(ma);
    }

    //@Ignore
    @Test
    public void serviceMeasureAppliedTestOnLeaveStep() throws Exception{
        GridMeasure gm = new GridMeasure();
        gm.setId(3);
        gm.setTitle("Yo Title");
        gm.setStatusId(PlgmProcessState.APPLIED.getStatusValue());
        java.time.LocalDateTime ldtdate = java.time.LocalDateTime.parse("2018-04-22T18:00:00");
        java.sql.Timestamp date = java.sql.Timestamp.valueOf(ldtdate);
        gm.setPlannedStarttimeFirstSinglemeasure(date);
        PlgmProcessSubject subject = PlgmProcessSubject.fromGridMeasure(gm, "fd");
        subject.setGridMeasure(gm);

        ServiceMeasureApplied  ma = new ServiceMeasureApplied();

        ma.onLeaveStep(subject);
    }

    @Test (expected = ProcessException.class)
    public void serviceMeasureAppliedTestOnLeaveStepExceptionIfMailServerNotStarted() throws Exception{
        mailServer.stop();
        ServiceMeasureApplied  ma = new ServiceMeasureApplied();
        ma.onLeaveStep(TestHelper.createProcessSubject(PlgmProcessState.APPLIED.getStatusValue()));
    }

    private void setTestTemplateEmail(String templatePath) {
        EmailTemplatePaths templatePaths = new EmailTemplatePaths();
        templatePaths.setAppliedEmailTemplate(templatePath);
        BackendConfig.getInstance().setEmailTemplatePaths(templatePaths);
    }

    private PlannedGridMeasuresConfiguration.EmailConfiguration createEmailConfiguration() throws IOException {
        PlannedGridMeasuresConfiguration.EmailConfiguration emailConfiguration = new PlannedGridMeasuresConfiguration.EmailConfiguration();
        emailConfiguration.setPort(EmailmanagerTest.port()+"");
        emailConfiguration.setSmtpHost("localhost");
        emailConfiguration.setSender("testCaseSender@test.de");
        return emailConfiguration;
    }
}
