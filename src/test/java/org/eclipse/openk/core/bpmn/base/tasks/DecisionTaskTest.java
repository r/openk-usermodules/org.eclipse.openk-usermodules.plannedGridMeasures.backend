/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.base.tasks;

import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.NO;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.PORT1;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.PORT2;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.PORT3;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.PORT4;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.PORT5;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.YES;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.core.bpmn.base.BaseTaskImpl;
import org.eclipse.openk.core.bpmn.base.DecisionTaskImpl;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.TestProcessSubject;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Test;
import org.powermock.reflect.Whitebox;


public class DecisionTaskTest {
    DecisionTaskImpl decTask;
    BaseTaskImpl taskYes;
    BaseTaskImpl taskNo;
    BaseTaskImpl[] taskPort = new BaseTaskImpl[5];

    private void setup() {
        final String dec = "DECISION1";
        decTask = new DecisionTaskImpl(dec);
        taskYes = new BaseTaskImpl("YES");
        taskNo = new BaseTaskImpl("NO");
        taskPort[0] = new BaseTaskImpl("Port1");
        taskPort[1] = new BaseTaskImpl("Port2");
        taskPort[2] = new BaseTaskImpl("Port3");
        taskPort[3]= new BaseTaskImpl("Port4");
        taskPort[4] = new BaseTaskImpl("Port5");

        decTask.connectOutputTo(YES, taskYes);
        decTask.connectOutputTo(NO, taskNo);
        decTask.connectOutputTo(PORT1, taskPort[0]);
        decTask.connectOutputTo(PORT2, taskPort[1]);
        decTask.connectOutputTo(PORT3, taskPort[2]);
        decTask.connectOutputTo(PORT4, taskPort[3]);
        decTask.connectOutputTo(PORT5, taskPort[4]);

    }

    @Test
    public void testPort_yes() throws ProcessException, HttpStatusException {
        setup();

        // test true port
        TestProcessSubject subject = new TestProcessSubject();
        subject.yes = true;
        decTask.enterStep(subject);
        assertTrue( taskYes.enterStepCalled );
        assertFalse( taskNo.enterStepCalled );
    }

    @Test
    public void testPort_no() throws ProcessException, HttpStatusException {
        setup();

        // test true port
        TestProcessSubject subject = new TestProcessSubject();
        subject.no = true;
        decTask.enterStep(subject);
        assertTrue( taskNo.enterStepCalled );
        assertFalse( taskYes.enterStepCalled );
    }

    public void testPort_portnumber( int port ) throws ProcessException, HttpStatusException {
        setup();

        TestProcessSubject subject = new TestProcessSubject();
        subject.decision = port;
        decTask.enterStep(subject);
    }

    @Test
    public void testPort_allports() throws ProcessException, HttpStatusException {
        for( int i=0; i<5; i++) {
            testPort_portnumber(i+1);
            assertTrue(taskPort[i].enterStepCalled);

            for( int j=0; j<5; j++) {
                if( j != i ) {
                    assertFalse(taskPort[j].enterStepCalled);
                }
            }
        }
    }

    @Test( expected = ProcessException.class )
    public void testPort_invalid() throws ProcessException, HttpStatusException {
        testPort_portnumber(999); // should throw exception
    }

    @Test( expected = ProcessException.class)
    public void test_onLeave_Exception() throws Exception {
        setup();
        TestProcessSubject subject = new TestProcessSubject();
        Whitebox.invokeMethod(decTask, "onLeaveStep", subject);
        // should raise exception
    }

    @Test( expected = ProcessException.class)
    public void test_outputUnconnected() throws ProcessException, HttpStatusException {
        TestProcessSubject subject = new TestProcessSubject();
        subject.yes = true;
        DecisionTaskImpl desc = new DecisionTaskImpl("unconnected Task");
        desc.enterStep(subject);
    }

}
