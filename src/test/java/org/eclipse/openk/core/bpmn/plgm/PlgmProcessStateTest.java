/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.plgm;

import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class PlgmProcessStateTest {
    @Test
    public void testAll() {
        assertEquals(PlgmProcessState.NEW, PlgmProcessState.fromValue(0) );
        assertEquals(PlgmProcessState.APPLIED, PlgmProcessState.fromValue(1) );
        assertEquals(PlgmProcessState.CANCELED, PlgmProcessState.fromValue(2) );
        assertEquals(PlgmProcessState.FOR_APPROVAL, PlgmProcessState.fromValue(3) );
        assertEquals(PlgmProcessState.APPROVED, PlgmProcessState.fromValue(4) );
        assertEquals(PlgmProcessState.REQUESTED, PlgmProcessState.fromValue(5) );
        assertEquals(PlgmProcessState.RELEASED, PlgmProcessState.fromValue(6) );
        assertEquals(PlgmProcessState.ACTIVE, PlgmProcessState.fromValue(7) );
        assertEquals(PlgmProcessState.IN_WORK, PlgmProcessState.fromValue(8) );
        assertEquals(PlgmProcessState.WORK_FINISHED, PlgmProcessState.fromValue(9) );
        assertEquals(PlgmProcessState.FINISHED, PlgmProcessState.fromValue(10) );
        assertEquals(PlgmProcessState.CLOSED, PlgmProcessState.fromValue(11) );
        assertEquals(PlgmProcessState.UNDEFINED_, PlgmProcessState.fromValue(-1) );

        assertEquals(PlgmProcessState.UNDEFINED_.getStatusValue(), -1);
    }

}
