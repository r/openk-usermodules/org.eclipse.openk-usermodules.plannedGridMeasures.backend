/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/



package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import static junit.framework.TestCase.assertNotNull;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import java.io.IOException;
import org.easymock.EasyMock;
import org.eclipse.openk.PlannedGridMeasuresConfiguration;
import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.api.mail.EmailTemplatePaths;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.controller.EmailmanagerTest;
import org.eclipse.openk.core.messagebroker.Producer;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ServiceMeasureClosedTest {

    private class ServiceMeasureClosedTestable extends ServiceMeasureClosed {

        public Producer testProducer;

        @Override
        public Producer createMessageQueueProducer() throws ProcessException {
            return testProducer;
        }
    }

    private static GreenMail mailServer;

    @BeforeClass
    public static void beforeAll() throws IOException {
        TestHelper.initDefaultBackendConfig();
        ServerSetup setup = new ServerSetup(EmailmanagerTest.port() , "localhost", ServerSetup.PROTOCOL_SMTP);
        setup.setServerStartupTimeout(3000);
        mailServer = new GreenMail(setup);
    }


    @Before
    public void prepareTests() throws Exception {
        // mock email-configuration
        PlannedGridMeasuresConfiguration.EmailConfiguration emailConfiguration = createEmailConfiguration();
        setTestTemplateEmail("emailConfigurationTest/emailTemplates/closedEmailTemplateTest.txt");
        BackendConfig.getInstance().setEmailConfiguration(emailConfiguration);
        mailServer.start();

    }

    @After
    public void afterTest() {
        mailServer.stop();
    }

    @Test
    public void serviceMeasureClosedTest() throws Exception{
        GridMeasure gm = new GridMeasure();
        PlgmProcessSubject subject = PlgmProcessSubject.fromGridMeasure(gm, "fd");

        ServiceMeasureClosed  ma = new ServiceMeasureClosed();

        assertNotNull(ma);
    }

    @Test
    public void serviceMeasureClosedTestOnLeaveStep() throws Exception{
        Producer producerMock = EasyMock.createNiceMock(Producer.class);
        producerMock.sendMessageAsJson(new GridMeasure(),"testRoutingKey");
        expectLastCall().andVoid().anyTimes();
        replay(producerMock);

        ServiceMeasureClosedTestable ma = new ServiceMeasureClosedTestable();
        ma.testProducer = producerMock;
        ma.onLeaveStep(TestHelper.createProcessSubject(PlgmProcessState.CLOSED.getStatusValue()));
        verify(producerMock);
    }

    @Test (expected = ProcessException.class)
    public void serviceMeasureSendMailTestOnLeaveStepProducerNull() throws Exception{
        Producer producerMock = EasyMock.createNiceMock(Producer.class);
        producerMock.sendMessageAsJson(new GridMeasure(),"testRoutingKey");
        expectLastCall().andVoid().anyTimes();
        replay(producerMock);

        ServiceMeasureClosedTestable ma = new ServiceMeasureClosedTestable();
        ma.testProducer = null;
        ma.onLeaveStep(TestHelper.createProcessSubject(PlgmProcessState.CLOSED.getStatusValue()));
        verify(producerMock);
    }

    @Test (expected = ProcessException.class)
    public void serviceMeasureClosedTestOnLeaveStepExceptionIfMailServerNotStarted() throws Exception{
        mailServer.stop();
        ServiceMeasureClosed  ma = new ServiceMeasureClosed();
        ma.onLeaveStep(TestHelper.createProcessSubject(PlgmProcessState.CLOSED.getStatusValue()));
    }

    private void setTestTemplateEmail(String templatePath) {
        EmailTemplatePaths templatePaths = new EmailTemplatePaths();
        templatePaths.setClosedEmailTemplate(templatePath);
        BackendConfig.getInstance().setEmailTemplatePaths(templatePaths);
    }

    private PlannedGridMeasuresConfiguration.EmailConfiguration createEmailConfiguration() throws IOException {
        PlannedGridMeasuresConfiguration.EmailConfiguration emailConfiguration = new PlannedGridMeasuresConfiguration.EmailConfiguration();
        emailConfiguration.setPort(EmailmanagerTest.port()+"");
        emailConfiguration.setSmtpHost("localhost");
        emailConfiguration.setSender("testCaseSender@test.de");
        return emailConfiguration;
    }
}
