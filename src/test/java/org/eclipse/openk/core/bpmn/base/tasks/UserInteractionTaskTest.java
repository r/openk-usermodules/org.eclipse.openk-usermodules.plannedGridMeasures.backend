/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.base.tasks;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.TestProcessSubject;
import org.eclipse.openk.core.bpmn.base.UserInteractionTaskImpl;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Test;

public class UserInteractionTaskTest {
    @Test
    public void testUserinteractionTask() throws ProcessException, HttpStatusException {
        UserInteractionTaskImpl task=new UserInteractionTaskImpl("UI Task");
        TestProcessSubject subject = new TestProcessSubject();
        task.enterStep( subject );
        assertTrue(task.enterStepCalled);
        assertFalse(task.leaveStepCalled);

        task.leaveStep( subject );
        assertTrue(task.leaveStepCalled);

    }

}
