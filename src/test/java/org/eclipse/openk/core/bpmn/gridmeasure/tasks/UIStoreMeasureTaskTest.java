/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.easymock.EasyMock;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.util.GridMeasureStorageHelper;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class UIStoreMeasureTaskTest {

    private GridMeasureStorageHelper gmsh;

    @Before
    public void prepare() throws ProcessException, HttpStatusException {
        gmsh = EasyMock.createMock(GridMeasureStorageHelper.class);
        gmsh.storeMeasureFromViewModel(anyObject());
        expectLastCall().andVoid().anyTimes();
        replay( gmsh );
        verify( gmsh );
    }

    @Test
    public void testStayInTask() throws Exception {
        UIStoreMeasureTask testTask = new UIStoreMeasureTask("desc", true) {

            @Override
            protected GridMeasureStorageHelper createGridMeasureStorageHelper() {
                return gmsh;
            }
        };

        GridMeasure gm = new GridMeasure();
        gm.setStatusId(PlgmProcessState.APPLIED.getStatusValue());

        PlgmProcessSubject sub = PlgmProcessSubject.fromGridMeasure(gm, "fd");
        sub.setStateInDb(PlgmProcessState.APPLIED);


        testTask.onEnterStep(sub);
        testTask.onLeaveStep(sub);
        Whitebox.invokeMethod(testTask, "onStayInTask", sub );
        assertTrue(Whitebox.invokeMethod(testTask, "isStayInThisTask", sub ));

        sub.setStateInDb(PlgmProcessState.FOR_APPROVAL);
        assertFalse(Whitebox.invokeMethod(testTask, "isStayInThisTask", sub ));
    }

    @Test
    public void testLeaveTask() throws Exception {
        UIStoreMeasureTask testTask = new UIStoreMeasureTask("desc", false) {

            @Override
            protected GridMeasureStorageHelper createGridMeasureStorageHelper() {
                return gmsh;
            }
        };

        GridMeasure gm = new GridMeasure();
        gm.setStatusId(PlgmProcessState.APPLIED.getStatusValue());

        PlgmProcessSubject sub = PlgmProcessSubject.fromGridMeasure(gm, "fd");
        sub.setStateInDb(PlgmProcessState.APPLIED);


        testTask.onEnterStep(sub);
        testTask.onLeaveStep(sub);
        assertFalse(Whitebox.invokeMethod(testTask, "isStayInThisTask", sub ));

        sub.setStateInDb(PlgmProcessState.FOR_APPROVAL);
        assertFalse(Whitebox.invokeMethod(testTask, "isStayInThisTask", sub ));
    }

    @Test
    public void testCreateHelper() throws Exception {
        UIStoreMeasureTask testTask = new UIStoreMeasureTask("desc", false);
        assertEquals( Whitebox.invokeMethod( testTask, "createGridMeasureStorageHelper").getClass(),
                GridMeasureStorageHelper.class);

    }


}
