/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.base;

import org.eclipse.openk.core.bpmn.base.tasks.EndPointTask;
import org.eclipse.openk.core.exceptions.HttpStatusException;

public class ProcessGridImpl extends ProcessGrid {
    public ProcessGridImpl() throws ProcessException, HttpStatusException {
        ProcessTask startTask = register(ProcessStateImpl.UITASK, new UserInteractionTaskImpl("UITASK"));
        ProcessTask serviceTask = register(ProcessStateImpl.SERVICE, new ServiceTaskImpl("SERVICETASK"));
        ProcessTask endpoint = register(ProcessStateImpl.ENDPOINT, new EndPointTask("ENDPOINT CHARLY"));

        startTask.connectOutputTo(serviceTask);
        serviceTask.connectOutputTo(endpoint);
    }

    public ProcessTask getService() throws ProcessException {
        return resolve( ProcessStateImpl.SERVICE);
    }
}
