/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.communication;

import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Before;
import org.junit.Test;

public class RestServiceWrapperTest {

    private RestServiceWrapper restServiceWrapper;
    private boolean useHttps = true;

    @Before
    public void init() {
        this.restServiceWrapper = new RestServiceWrapper("testURL", useHttps);
    }

    @Test(expected = HttpStatusException.class)
    public void testPerformGetRequest() throws HttpStatusException {
        restServiceWrapper.performGetRequest("testParam", "testToken");
    }

    @Test(expected = HttpStatusException.class)
    public void testPerformPostRequest() throws Exception {
        restServiceWrapper.performPostRequest("testParam", "testToken", "testData");
    }

}
