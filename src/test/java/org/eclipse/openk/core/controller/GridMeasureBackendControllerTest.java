/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import junit.framework.TestCase;
import org.apache.http.HttpStatus;
import org.easymock.EasyMock;
import org.eclipse.openk.PlannedGridMeasuresConfiguration.EmailConfiguration;
import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.api.*;
import org.eclipse.openk.api.mail.EmailTemplatePaths;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.ProcessGrid;
import org.eclipse.openk.core.bpmn.base.ProcessGrid.Recoverable;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmGrid;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.db.dao.*;
import org.eclipse.openk.db.model.*;
import org.eclipse.openk.resources.PlannedGridMeasuresResource;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;
import org.powermock.reflect.Whitebox;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static org.easymock.EasyMock.anyInt;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;


public class GridMeasureBackendControllerTest {

    public GridMeasureBackendController beMockGridMeasure;

    private TestableGridMeasureBackendController tbc;
    private String jwtString = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiJkOWIwZTk3OS1jMmYwLTQzYmYtYjM5Mi03ZTNiM2ZiNTZjNTUiLCJleHAiOjE1MjgxNzg4ODUsIm5iZiI6MCwiaWF0IjoxNTI4MTc4NTg1LCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImMyZTlkN2FlLTJiZmEtNDU3OC1iMDllLWY1ZGM1ZjA5YTg3OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIzZDA0YTAyZC01NGZkLTRkNTAtOGNmYS0wYzVjNGFmYTJkNDMiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLW5vcm1hbHVzZXIiLCJwbGFubmVkLXBvbGljaWVzLWFjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5lZC1wb2xpY2llcy1ub3JtYWx1c2VyIiwicGxhbm5lZC1wb2xpY2llcy1tZWFzdXJlYXBwbGljYW50Il19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwibmFtZSI6Ik90dG8gTWHDn25haG1lbi1BbnRyYWdzc3RlbGxlciIsInByZWZlcnJlZF91c2VybmFtZSI6Im90dG8iLCJnaXZlbl9uYW1lIjoiT3R0byIsImZhbWlseV9uYW1lIjoiTWHDn25haG1lbi1BbnRyYWdzc3RlbGxlciJ9.OQjxr18u3E0Eyzkaep32JAKhv1-sV-QLkBs4I7u5_Gf1rI2QIGqrp266mj_kVBVG0-hjNCOo1oaehh3PnrKEC4RDY6g39RDDlk2fh-WQw5IcasSSVyUuKLmSoePTzBrbEzaJWJC7ZjtO0I96kEbqBSHMWAa9bAcl4lqHjvBup1atACblJKAFbBltC_8BkKeNEVEkAY5r59EqSFh6Wzncuhqw8h7MDmFiHfhsNsvhlUJ1QiJ43qYoVMF6RSpbx4TVCdIyXDHhTmWXz5lyRt_YiHc4A4Ca9lx34FXCohswxwqtvisYiAp7AiIoMamHhZTXxTbPzOoCCMmihxNdiPK5wQ";

    @Before
    public void prepareTests() throws Exception {
        TestHelper.initDefaultBackendConfig();

        // mock email-configuration
        EmailConfiguration emailConfiguration = new EmailConfiguration();
        emailConfiguration.setPort("25");
        emailConfiguration.setSmtpHost("localhostTest");
        BackendConfig.getInstance().setEmailConfiguration(emailConfiguration);
        EmailTemplatePaths templatePaths = new EmailTemplatePaths();
        templatePaths.setAppliedEmailTemplate("appliedEmailPathToEmailTxt");
        BackendConfig.getInstance().setEmailTemplatePaths(templatePaths);

        beMockGridMeasure = PowerMock.createNiceMock(GridMeasureBackendController.class);
        tbc = new TestableGridMeasureBackendController();
        tbc.tblMeasureDocumentsDao = createTblMeasureDocumentsDao("noError");
        tbc.tblDocumentsDao = createTblDocumentsDao("noError", "fileNameExample.txt");
    }

    private class TestableGridMeasureBackendController extends GridMeasureBackendController {

        public TblGridMeasureDao tblGridMeasureDao;
        public TblSingleGridmeasureDao tblSingleGridmeasureDao;
        public TblStepsDao tblStepsDao;
        public TblMeasureDocumentsDao tblMeasureDocumentsDao;
        public TblDocumentsDao tblDocumentsDao;
        public TblLockDao tblLockDao;
        public HTblGridMeasureDao htblGridMeasureDao;
        public MailAddressCache mailAddressCache;

        @Override
        protected AutoCloseEntityManager createEm() {
            EntityManager em = getMockedEntityManager();

            return new AutoCloseEntityManager(em);
        }

        @Override
        protected TblGridMeasureDao createTblGridMeasureDao( EntityManager em ) {
            if( tblGridMeasureDao != null ) {
                return tblGridMeasureDao;
            }
            else {
                return super.createTblGridMeasureDao(em);
            }
        }

        @Override
        protected TblSingleGridmeasureDao createTblSingleGridmeasureDao( EntityManager em ) {
            if( tblSingleGridmeasureDao != null ) {
                return tblSingleGridmeasureDao;
            }
            else {
                return super.createTblSingleGridmeasureDao(em);
            }
        }

        @Override
        protected TblStepsDao createTblStepsDao( EntityManager em ) {
            if( tblStepsDao != null ) {
                return tblStepsDao;
            }
            else {
                return super.createTblStepsDao(em);
            }
        }

        @Override
        protected HTblGridMeasureDao createHistoricalTblGridMeasureDao( EntityManager em ) {
            if( htblGridMeasureDao != null ) {
                return htblGridMeasureDao;
            }
            else {
                return super.createHistoricalTblGridMeasureDao(em);
            }
        }

        @Override
        protected TblMeasureDocumentsDao createTblMeasureDocumentsDao( EntityManager em ) {
            if( tblMeasureDocumentsDao != null ) {
                return tblMeasureDocumentsDao;
            }
            else {
                return super.createTblMeasureDocumentsDao(em);
            }
        }

        @Override
        protected TblDocumentsDao createTblDocumentsDao ( EntityManager em ) {
            if( tblDocumentsDao != null ) {
                return tblDocumentsDao;
            }
            else {
                return super.createTblDocumentsDao(em);
            }
        }

        @Override
        protected TblLockDao createTblLockDao(EntityManager em) {
            if( tblLockDao != null ) {
                return tblLockDao;
            }
            else {
                return super.createTblLockDao(em);
            }
        }
        

    }

    private EntityManager getMockedEntityManager() {
        EntityTransaction et = EasyMock.createMock(EntityTransaction.class);
        et.begin();
        expectLastCall().andVoid().anyTimes();
        et.commit();
        expectLastCall().andVoid().anyTimes();
        et.rollback();
        expectLastCall().andVoid().anyTimes();

        EasyMock.replay(et);
        EasyMock.verify(et);

        EntityManager em = EasyMock.createMock(EntityManager.class);
        expect(em.getTransaction()).andReturn(et).anyTimes();

        expect(em.isOpen()).andReturn(false).anyTimes();

        em.close();
        expectLastCall().andVoid().anyTimes();

        EasyMock.replay(em);
        EasyMock.verify(em);
        return em;
    }

    private String payloadNormalUser = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiIwZGUyNTMyYi1mNTBkLTQxZTUtODQwYy1iNzZkOTYyZGM3MDYiLCJleHAiOjE1MjE2NDA0MTIsIm5iZiI6MCwiaWF0IjoxNTIxNjQwMTEyLCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImMyZTlkN2FlLTJiZmEtNDU3OC1iMDllLWY1ZGM1ZjA5YTg3OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJkOTE1MjY2MS03NTJlLTRiNmMtOWRiNi0wYjFmYzY3YTc4ZTciLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLW5vcm1hbHVzZXIiLCJ1bWFfYXV0aG9yaXphdGlvbiIsInBsYW5uZWQtcG9saWNpZXMtbm9ybWFsdXNlciJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInJvbGVzIjoiW3BsYW5uZWQtcG9saWNpZXMtbm9ybWFsdXNlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBlbG9nYm9vay1hY2Nlc3MsIGVsb2dib29rLW5vcm1hbHVzZXJdIiwibmFtZSI6Ik90dG8gTm9ybWFsdmVyYnJhdWNoZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJvdHRvIiwiZ2l2ZW5fbmFtZSI6Ik90dG8iLCJmYW1pbHlfbmFtZSI6Ik5vcm1hbHZlcmJyYXVjaGVyIn0.F-CcNatXaE0W4PhBNaSGUwddE_S05HqL6QuJLOWQo_n0_LdWGRw8iyF6RIbK8y06kH3gb79c6t1U0njWqE68pL9FQfP2t-_en-XlCwKzBdDXtq8e8xHyZ00P5zlE80du0A74qzh2g0HrE9fCgEaawk2_M6JxNFrToiwl6PGRS9RNRD_xgKVXn0XqG-I_qRKA-GdBTcLPjPD9bn4fAdlf3HWdVPUNclRI2ttUykFmpmRTAHKwI3cZOBrIMn4dasMn9k1xPP5IAGtCcgs6TF0YlnWvZgIhdT_5O4rmMtzZXjeuYsjpNnqz0LGA0GT6up90PZrTJF8rHtWU50wVgJSH9g";

    private RefVersionDao createRefVersionDao(String dBversion ) {
        RefVersion rv = new RefVersion();
        if( dBversion != null ) {
            rv.setVersion(dBversion);
        }
        else {
            rv = null;
        }
        RefVersionDao dao = EasyMock.createMock(RefVersionDao.class);
        expect( dao.getVersionInTx() ).andReturn(rv).anyTimes();
        replay( dao );
        verify( dao );
        return dao;
    }

    private TblGridMeasureDao createTblGridMeasureDao() {

        java.util.List<TblGridMeasure> mList = new ArrayList<TblGridMeasure>();

        Calendar gCal = new GregorianCalendar();
        gCal.add( Calendar.DAY_OF_MONTH, 1 );
        Date tomorrow = gCal.getTime();

        Calendar gCal2 = new GregorianCalendar();
        gCal2.add( Calendar.MONTH, 1 );
        Date nextMonth = gCal2.getTime();

        Calendar gCal3 = new GregorianCalendar();
        gCal3.add(Calendar.DATE, -1);
        Date yesterday = gCal3.getTime();

        TblGridMeasure mGm1 = new TblGridMeasure();
        mGm1.setId(1);
        mGm1.setIdDescriptive("20");
        mGm1.setTitle("Title1");
        mGm1.setFkRefGmStatus(0);
        mGm1.setPlannedStarttimeFirstSinglemeasure(tomorrow);
        //mGm1.setEmailAddresses("testuser1@test.com");
        mList.add(mGm1);

        TblGridMeasure mGm2 = new TblGridMeasure();
        mGm2.setId(2);
        mGm2.setIdDescriptive("21");
        mGm2.setTitle("Title2");
        mGm2.setFkRefGmStatus(1);
        mGm2.setPlannedStarttimeFirstSinglemeasure(nextMonth);
        mList.add(mGm2);

        TblGridMeasure mGm3 = new TblGridMeasure();
        mGm3.setId(3);
        mGm3.setIdDescriptive("22");
        mGm3.setTitle("Title3");
        mGm3.setFkRefGmStatus(0);
        mGm3.setPlannedStarttimeFirstSinglemeasure(yesterday);
        mList.add(mGm3);

        TblGridMeasure mGm4 = new TblGridMeasure();
        mGm4.setId(4);
        mGm4.setIdDescriptive("222");
        mGm4.setTitle("Title22");
        mGm4.setFkRefGmStatus(2); // canceled
        mGm4.setPlannedStarttimeFirstSinglemeasure(yesterday);
        mList.add(mGm4);

        TblGridMeasure mGm5 = new TblGridMeasure();
        mGm5.setId(5);
        mGm5.setIdDescriptive("221");
        mGm5.setTitle("Title11");
        mGm5.setFkRefGmStatus(11); //closed
        mGm5.setPlannedStarttimeFirstSinglemeasure(yesterday);
        mList.add(mGm5);

        List<String> mailAddressesList  = new ArrayList<>();
        mailAddressesList.add("testuser1@test.com, testuser2@test.com");
        mailAddressesList.add("testuser2@test.com, testuser3@test.com");
        mailAddressesList.add("testuser3@test.com, testuser4@test.com");

        List<String> userDepartmentsCreatedList  = new ArrayList<>();
        userDepartmentsCreatedList.add("Department abc");
        userDepartmentsCreatedList.add("Department def");
        userDepartmentsCreatedList.add("Department ghi");

        List<String> userDepartmentsModifiedList  = new ArrayList<>();
        userDepartmentsModifiedList.add("Department jkl");
        userDepartmentsModifiedList.add("Department mno");
        userDepartmentsModifiedList.add("Department pqr");


        TblGridMeasureDao dao = EasyMock.createMock(TblGridMeasureDao.class);
        expect( dao.findByIdInTx(anyObject(), anyInt())).andReturn(mList.get(0)).anyTimes();
        expect( dao.findById(anyObject(), anyInt())).andReturn(mList.get(0)).anyTimes();
        expect( dao.getGridMeasuresInTx()).andReturn(mList).anyTimes();
        expect(dao.getGridMeasuresByStatusId(anyInt())).andReturn(mList).anyTimes();
        expect(dao.getGridMeasuresExcludingStatusInTx(anyInt())).andReturn(mList).anyTimes();
        expect(dao.getGridMeasuresExcludingStatusInTx(anyInt(), anyInt())).andReturn(mList).anyTimes();

        expect(dao.getGridMeasuresByUserName(anyString())).andReturn(mList).anyTimes();
        expect(dao.getGridMeasuresByUserNameExcludingStatus(anyString(),anyInt())).andReturn(mList).anyTimes();
        expect(dao.getGridMeasuresByUserNameExcludingStatus(anyString(),anyInt(), anyInt())).andReturn(mList).anyTimes();

        expect(dao.getMailAddressesFromGridmeasures()).andReturn(mailAddressesList).anyTimes();

        expect(dao.getUserDepartmentsCreated()).andReturn(userDepartmentsCreatedList).anyTimes();
        expect(dao.getUserDepartmentsModified()).andReturn(userDepartmentsModifiedList).anyTimes();


        replay( dao );
        verify( dao );
        return dao;
    }



    private TblGridMeasureDao createTblGridMeasureDaoStatus() {

        java.util.List<TblGridMeasure> mList = new ArrayList<TblGridMeasure>();

        TblGridMeasure mGm1 = new TblGridMeasure();
        mGm1.setId(1);
        mGm1.setIdDescriptive("20");
        mGm1.setTitle("Title1");
        mGm1.setFkRefGmStatus(3);
        mList.add(mGm1);

        TblGridMeasureDao dao = EasyMock.createMock(TblGridMeasureDao.class);
        expect( dao.findByIdInTx(anyObject(), anyInt())).andReturn(mList.get(0)).anyTimes();
        expect( dao.findById(anyObject(), anyInt())).andReturn(mList.get(0)).anyTimes();
        expect( dao.getGridMeasuresInTx()).andReturn(mList).anyTimes();
        replay( dao );
        verify( dao );
        return dao;
    }

    private TblStepsDao createTblStepsDao() {

        java.util.List<TblSteps> stepList = new ArrayList<TblSteps>();

        TblSteps step1 = new TblSteps();
        step1.setId(1);
        step1.setTargetState("target");
        step1.setSwitchingObject("tool");
        step1.setSortorder(1);
        stepList.add(step1);

        TblSteps step2 = new TblSteps();
        step2.setId(2);
        step2.setTargetState("target 2");
        step2.setSwitchingObject("tool 2");
        step2.setSortorder(2);
        stepList.add(step2);

        TblStepsDao dao = EasyMock.createMock(TblStepsDao.class);
        expect( dao.findByIdInTx(anyObject(), anyInt())).andReturn(stepList.get(0)).anyTimes();
        expect( dao.findById(anyObject(), anyInt())).andReturn(stepList.get(0)).anyTimes();
        expect( dao.getStepsBySingleGmIdInTx(1)).andReturn(stepList).anyTimes();
        expect( dao.getStepsInTx()).andReturn(stepList).anyTimes();
        replay( dao );
        verify( dao );
        return dao;
    }

    private TblSingleGridmeasureDao createTblSingleGridmeasureDao() {

        java.util.List<TblSingleGridmeasure> mList = new ArrayList<TblSingleGridmeasure>();

        TblSingleGridmeasure mSgm1 = new TblSingleGridmeasure();
        mSgm1.setId(1);
        mSgm1.setTitle("Title1");
        mSgm1.setSortorder(1);
        mSgm1.setCimId("2");
        mList.add(mSgm1);

        List<String> userDepartments = new ArrayList<String>();
        userDepartments.add("Truppe1");
        userDepartments.add("Truppe2");
        userDepartments.add("Truppe3");

        List<String> networkControls = new ArrayList<String>();
        networkControls.add("Heinz XY");
        networkControls.add("Manne ZZ");
        networkControls.add("Heidi AB");

        TblSingleGridmeasureDao dao = EasyMock.createMock(TblSingleGridmeasureDao.class);
        expect( dao.findByIdInTx(anyObject(), anyInt())).andReturn(mList.get(0)).anyTimes();
        expect( dao.findById(anyObject(), anyInt())).andReturn(mList.get(0)).anyTimes();
        expect( dao.getSingleGridmeasuresByGmIdInTx(anyInt())).andReturn(mList).anyTimes();
        expect( dao.getSingleGridmeasuresInTx()).andReturn(mList).anyTimes();
        expect( dao.getUserDepartmentsResponsibleOnSite()).andReturn(userDepartments).anyTimes();
        expect( dao.getNetworkControls()).andReturn(userDepartments).anyTimes();

        replay( dao );
        verify( dao );
        return dao;
    }

    private HTblGridMeasureDao createHistoricalTblGridMeasureDao() {

        java.util.List<HTblGridMeasure> mList = new ArrayList<HTblGridMeasure>();

        Calendar gCal = new GregorianCalendar();
        gCal.add( Calendar.DAY_OF_MONTH, 1 );
        Date tomorrow = gCal.getTime();

        Calendar gCal2 = new GregorianCalendar();
        gCal2.add( Calendar.MONTH, 1 );
        Date nextMonth = gCal2.getTime();

        Calendar gCal3 = new GregorianCalendar();
        gCal3.add(Calendar.DATE, -1);
        Date yesterday = gCal2.getTime();

        HTblGridMeasure mGm1 = new HTblGridMeasure();
        mGm1.setId(1);
        mGm1.setIdDescriptive("20");
        mGm1.setTitle("Title1");
        mGm1.setFkRefGmStatus(0);
        mGm1.setPlannedStarttimeFirstSinglemeasure(tomorrow);
        mList.add(mGm1);

        HTblGridMeasure mGm2 = new HTblGridMeasure();
        mGm2.setId(2);
        mGm2.setIdDescriptive("21");
        mGm2.setTitle("Title2");
        mGm2.setFkRefGmStatus(1);
        mGm2.setPlannedStarttimeFirstSinglemeasure(nextMonth);
        mList.add(mGm2);

        HTblGridMeasure mGm3 = new HTblGridMeasure();
        mGm3.setId(3);
        mGm3.setIdDescriptive("22");
        mGm3.setTitle("Title3");
        mGm3.setFkRefGmStatus(3);
        mGm3.setPlannedStarttimeFirstSinglemeasure(yesterday);
        mList.add(mGm3);

        HTblGridMeasureDao dao = EasyMock.createMock(HTblGridMeasureDao.class);
        expect( dao.getHistoricalGridMeasuresByIdInTx(anyInt())).andReturn(mList).anyTimes();
        replay( dao );
        verify( dao );
        return dao;
    }

    private TblMeasureDocumentsDao createTblMeasureDocumentsDao(String error) throws Exception {
        TblMeasureDocumentsDao dao = getTblMeasureDocumentsDaoMockBasic();

        java.util.List<TblMeasureDocuments> allTblMeasureDocumentsByIdList = new ArrayList<>();
        java.util.List<TblMeasureDocuments> measureDocumentByDocumentIdList = new ArrayList<>();

        //TblDocuments
        TblDocuments tblDocuments1 = new TblDocuments();
        tblDocuments1.setId(1);
        tblDocuments1.setDocumentName("test.xls");

        TblDocuments tblDocuments2 = new TblDocuments();
        tblDocuments2.setId(2);
        tblDocuments2.setDocumentName("uploadTestFile.txt");

        TblDocuments tblDocuments3 = new TblDocuments();
        tblDocuments3.setId(3);
        tblDocuments3.setDocumentName("test.png");

        //TblMeasureDocuments
        TblMeasureDocuments mD1 = new TblMeasureDocuments();
        mD1.setId(1);
        mD1.setTblDocuments(tblDocuments1);
        mD1.setFkTblMeasure(1);

        TblMeasureDocuments mD2 = new TblMeasureDocuments();
        mD2.setId(2);
        mD2.setTblDocuments(tblDocuments2);
        mD2.setFkTblMeasure(1);

        TblMeasureDocuments mD3 = new TblMeasureDocuments();
        mD3.setId(3);
        mD3.setTblDocuments(tblDocuments3);
        mD3.setFkTblMeasure(2);

        //List
        allTblMeasureDocumentsByIdList.add(mD1);
        allTblMeasureDocumentsByIdList.add(mD2);
        allTblMeasureDocumentsByIdList.add(mD3);

        //List
        measureDocumentByDocumentIdList.add(mD1);

        switch (error){
            case "getMeasureDocumentByDocumentIdErrorHttp":
                expect( dao.getMeasureDocumentByDocumentId(anyInt())).andThrow(new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR)).anyTimes();
                break;
            case "getMeasureDocumentByDocumentIdError":
                expect( dao.getMeasureDocumentByDocumentId(anyInt())).andReturn(measureDocumentByDocumentIdList).anyTimes();
                expect( dao.mergeInTx(anyObject())).andThrow(new Exception("MockedException")).anyTimes();
                break;
            case "getAllTblMeasureDocumentsByIdError":
                expect( dao.getAllTblMeasureDocumentsById(anyInt())).andThrow(new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR)).anyTimes();
                break;
            case "storeInTxError":
                expect( dao.storeInTx(anyObject(TblMeasureDocuments.class))).andThrow(new Exception("MockedException")).anyTimes();
                break;
            default:
                expect( dao.getAllTblMeasureDocumentsById(anyInt())).andReturn(allTblMeasureDocumentsByIdList).anyTimes();
                expect( dao.getMeasureDocumentByDocumentId(anyInt())).andReturn(measureDocumentByDocumentIdList).anyTimes();
                expect( dao.storeInTx(anyObject(TblMeasureDocuments.class))).andReturn(mD1).anyTimes();
        }

        replay( dao );
        verify( dao );
        return dao;
    }

    private TblMeasureDocumentsDao getTblMeasureDocumentsDaoMockBasic() {
        TblMeasureDocumentsDao dao = EasyMock.createNiceMock(TblMeasureDocumentsDao.class);
        expect( dao.getEM()).andReturn(getMockedEntityManager()).anyTimes();
        return dao;
    }

    private TblDocumentsDao createTblDocumentsDao(String error, String fileName) throws Exception {
        TblDocumentsDao dao = getTblDocumentsDaoMockBasic();

        TblDocuments tblDocuments1 = new TblDocuments();
        tblDocuments1.setId(1);
        tblDocuments1.setDocumentName(fileName);
        tblDocuments1.setModUser("modUser");
        tblDocuments1.setCreateUser("createUser");

        switch (error){
            case "storeInTxError":
                expect( dao.storeInTx(anyObject(TblDocuments.class))).andThrow(new Exception("MockedException")).anyTimes();
                break;
            default:
                expect( dao.storeInTx(anyObject(TblDocuments.class))).andReturn(tblDocuments1).anyTimes();
                expect( dao.findByIdInTx(anyObject(), anyInt())).andReturn(tblDocuments1).anyTimes();
        }
        replay( dao );
        verify( dao );
        return dao;
    }

    private TblDocumentsDao getTblDocumentsDaoMockBasic() {
        TblDocumentsDao dao = EasyMock.createNiceMock(TblDocumentsDao.class);
        expect( dao.getEM()).andReturn(getMockedEntityManager()).anyTimes();
        return dao;
    }

    private TblLockDao createTblLockDaoBasic() throws HttpStatusException{
        TblLockDao mockedDao = EasyMock.createNiceMock(TblLockDao.class);
        expect( mockedDao.getEM()).andReturn(getMockedEntityManager()).anyTimes();

        return mockedDao;
    }

    private TblLockDao createTblLockDao() throws Exception{

        TblLockDao mockedDao = createTblLockDaoBasic();

        TblLock mLock = new TblLock();
        mLock.setId(1);
        mLock.setKey(2);
        mLock.setUsername("Testuser");
        mLock.setInfo("GridMeasure");

        expect( mockedDao.getLock(2, "GridMeasure")).andReturn(mLock).anyTimes();
        expect( mockedDao.storeInTx(anyObject(TblLock.class))).andReturn(mLock).anyTimes();

        replay( mockedDao );
        verify( mockedDao );

        return mockedDao;
    }

    private TblLockDao createTblLockDao_null() throws HttpStatusException{

        TblLockDao mockedDao = createTblLockDaoBasic();

        expect( mockedDao.getLock(anyInt(), anyString())).andReturn(null).anyTimes();
        replay( mockedDao );
        verify( mockedDao );

        return mockedDao;
    }

    private TblLockDao createTblLockDao_locked() throws HttpStatusException{

        TblLockDao mockedDao = createTblLockDaoBasic();

        TblLock mLock = new TblLock();
        mLock.setId(1);
        mLock.setKey(2);
        mLock.setUsername("ModUser");
        mLock.setInfo("Gridmeasure");

        expect( mockedDao.getLock(2, "Gridmeasure")).andReturn(mLock).anyTimes();
        //expect( mockedDao.storeInTx(anyObject(TblLock.class))).andReturn(mLock).anyTimes();

        replay( mockedDao );
        verify( mockedDao );

        return mockedDao;
    }


    @Test
    public void testGetVersionInfoImpl_ok() throws Exception {
        GridMeasureBackendController be = new GridMeasureBackendController();

        RefVersionDao dao = createRefVersionDao( "DB2" );
        VersionInfo vi = (VersionInfo)Whitebox.invokeMethod(be, "getVersionInfoImpl", dao, "BEVersion1");

        assertEquals(vi.getDbVersion(), "DB2");
        assertEquals(vi.getBackendVersion(), "BEVersion1");
    }

    @Test
    public void testGetVersionInfoImpl_dbnull() throws Exception {
        GridMeasureBackendController be = new GridMeasureBackendController();

        RefVersionDao dao = createRefVersionDao( null );
        VersionInfo vi = (VersionInfo)Whitebox.invokeMethod(be, "getVersionInfoImpl", dao, "BEVersion1");

        assertEquals(vi.getDbVersion(), "NO_DB");
        assertEquals(vi.getBackendVersion(), "BEVersion1");

    }
    
    @Test( expected = HttpStatusException.class )
    public void testGetGridMeasureAttachments_exception() throws Exception {
        tbc.tblMeasureDocumentsDao = createTblMeasureDocumentsDao("getAllTblMeasureDocumentsByIdError");
        tbc.getGridMeasureAttachments(1);
    }

    @Test
    public void testGetGridMeasureAttachments() throws Exception {
        TblMeasureDocumentsDao dao = createTblMeasureDocumentsDao("noError");
        tbc.tblMeasureDocumentsDao = dao;

        List<Document> gridMeasureAttachments = tbc.getGridMeasureAttachments(1);

        List<TblMeasureDocuments> measureDocuments = dao.getAllTblMeasureDocumentsById(1);
        assertEquals(gridMeasureAttachments.size(), measureDocuments.size());
        assertEquals(gridMeasureAttachments.get(0).getId(), new Integer(1));
        assertEquals(gridMeasureAttachments.get(0).getDocumentName(), "test.xls");
    }

    @Test( expected = HttpStatusException.class )
    public void testDeleteDocumentsError() throws Exception {
        prepareDeleteDocumentsTestError("getMeasureDocumentByDocumentIdError");
        tbc.deleteDocument("moduser", 1);
    }

    @Test(expected = HttpStatusException.class)
    public void testDeleteDocumentsErrorHttp() throws Exception {
        prepareDeleteDocumentsTestError("getMeasureDocumentByDocumentIdErrorHttp");
        tbc.deleteDocument("moduser", 1);
    }

    @Test
    public void testDeleteDocuments() throws Exception {
        int deletedDocumentId = tbc.deleteDocument("moduser", 1);
        assertEquals(deletedDocumentId, 1);

    }

    @Test
    public void testDownloadDocument() throws Exception {
        TblDocuments downloadDocument = tbc.downloadGridMeasureAttachment(1);

        assertEquals(downloadDocument.getId(),new Integer(1));
        assertEquals(downloadDocument.getDocumentName(),"fileNameExample.txt");

    }

    @Test( expected = HttpStatusException.class )
    public void testUploadDocuments_error() throws Exception {
        String uploadDocumentName = "uploadTestFile2.txt";
        prepareUploadDocumentsTestError(uploadDocumentName);
        int gridmeasuereId = 1;

        Document documentToUpload = new Document();
        documentToUpload.setDocumentName(uploadDocumentName);
        documentToUpload.setData("TestContent");

        tbc.uploadDocument(documentToUpload, "moduser", gridmeasuereId);
    }

    @Test
    public void testUploadDocumentsNewFile() throws Exception {
        String uploadDocumentName = "uploadTestFile2.txt";
        prepareUploadDocumentsTest(uploadDocumentName);
        int gridmeasuereId = 1;

        Document documentToUpload = new Document();
        documentToUpload.setDocumentName(uploadDocumentName);
        documentToUpload.setData("TestContent");

        Document document = tbc.uploadDocument(documentToUpload, "moduser", gridmeasuereId);

        assertNull(document.getId());
        assertEquals(document.getDocumentName(), uploadDocumentName);
    }

    @Test
    public void testUploadDocumentsUpdateFile() throws Exception {
        String uploadDocumentName = "uploadTestFile.txt";
        prepareUploadDocumentsTest(uploadDocumentName);
        int gridmeasuereId = 1;

        Document documentToUpload = new Document();
        documentToUpload.setDocumentName(uploadDocumentName);
        documentToUpload.setData("TestContent");

        Document document = tbc.uploadDocument(documentToUpload, "moduser", gridmeasuereId);

        assertEquals(document.getId(), new Integer(2));
        assertEquals(document.getDocumentName(), uploadDocumentName);

    }

    private InputStream getUploadTestFileInStream() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return classLoader.getResourceAsStream("uploadTestFile.txt");
    }

    private void prepareDeleteDocumentsTestError(String error) throws Exception {
        tbc.tblMeasureDocumentsDao = createTblMeasureDocumentsDao(error);
    }

    private void prepareUploadDocumentsTest(String uploadFileName) throws Exception {
        // BackendConfig.configure("portalBaseUrlMock", null);
        BackendConfig.getInstance().processAndSetWhiteListDocumenttypes("txt,pdf");
        tbc.tblDocumentsDao = createTblDocumentsDao("noError", uploadFileName);
    }

    private void prepareUploadDocumentsTestError(String uploadFileName) throws Exception {
        // BackendConfig.configure("portalBaseUrlMock", null);
        tbc.tblDocumentsDao = createTblDocumentsDao("storeInTxError", uploadFileName);
    }


    @Test
    public void testGetHistoricalStatusChangesById() throws Exception {
        HTblGridMeasureDao dao = createHistoricalTblGridMeasureDao();
        tbc.htblGridMeasureDao = dao;


        List<HGridMeasure> hgridMeasure = tbc.getHistoricalStatusChangesById(1);
        List<HTblGridMeasure> orgHgridMeasure = dao.getHistoricalGridMeasuresByIdInTx(1);

        assertEquals(hgridMeasure.get(0).getId(), orgHgridMeasure.get(0).getId());
        assertEquals(hgridMeasure.get(0).getTitle(), orgHgridMeasure.get(0).getTitle());
        assertEquals(hgridMeasure.get(0).getDescription(), orgHgridMeasure.get(0).getDescription());

    }

    @Test
    public void testGetGridMeasures() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        tbc.tblGridMeasureDao = dao;

        FilterObject filterObject = new FilterObject();
        filterObject.setOnlyUsersGMsDesired(false);
        filterObject.setCanceledStatusActive(false);
        filterObject.setClosedStatusActive(false);

        List<GridMeasure> retList = tbc.getGridMeasures("testhugo", filterObject);
        List<TblGridMeasure> orgList = dao.getGridMeasuresInTx();
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
        assertEquals(orgList.get(1).getAffectedResource(), retList.get(1).getAffectedResource());
        assertEquals(orgList.get(0).getTitle(), retList.get(0).getTitle());
    }

    @Test
    public void testGetGridMeasuresWithFilter() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        tbc.tblGridMeasureDao = dao;
        FilterObject filterObject = new FilterObject();
        filterObject.setOnlyUsersGMsDesired(false);
        filterObject.setCanceledStatusActive(true);
        filterObject.setClosedStatusActive(true);

        List<GridMeasure> retList = tbc.getGridMeasures("testhugo", filterObject);
        List<TblGridMeasure> orgList = dao.getGridMeasuresInTx();
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
        assertEquals(orgList.get(1).getAffectedResource(), retList.get(1).getAffectedResource());
        assertEquals(orgList.get(0).getTitle(), retList.get(0).getTitle());
    }

    @Test
    public void testGetGridMeasuresWithFilterWithoutClosed() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        tbc.tblGridMeasureDao = dao;
        FilterObject filterObject = new FilterObject();
        filterObject.setOnlyUsersGMsDesired(false);
        filterObject.setCanceledStatusActive(true);
        filterObject.setClosedStatusActive(false);

        List<GridMeasure> retList = tbc.getGridMeasures("testhugo",filterObject);
        List<TblGridMeasure> orgList = dao.getGridMeasuresExcludingStatusInTx(PlgmProcessState.CLOSED.getStatusValue());
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
    }

    @Test
    public void testGetGridMeasuresWithFilterWithoutCanceled() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        tbc.tblGridMeasureDao = dao;
        FilterObject filterObject = new FilterObject();
        filterObject.setOnlyUsersGMsDesired(false);
        filterObject.setCanceledStatusActive(false);
        filterObject.setClosedStatusActive(true);

        List<GridMeasure> retList = tbc.getGridMeasures("testhugo", filterObject);
        List<TblGridMeasure> orgList = dao.getGridMeasuresExcludingStatusInTx(PlgmProcessState.CANCELED.getStatusValue());
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
    }

    @Test
    public void testGetGridMeasuresWithFilterWithoutCanceledAndClosed() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        tbc.tblGridMeasureDao = dao;
        FilterObject filterObject = new FilterObject();
        filterObject.setOnlyUsersGMsDesired(false);
        filterObject.setCanceledStatusActive(false);
        filterObject.setClosedStatusActive(false);

        List<GridMeasure> retList = tbc.getGridMeasures("testhugo", filterObject);
        List<TblGridMeasure> orgList = dao.getGridMeasuresExcludingStatusInTx(PlgmProcessState.CANCELED.getStatusValue(), PlgmProcessState.CLOSED.getStatusValue());
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
    }

    @Test
    public void testGetGridMeasuresOnlyUsersGMs() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        tbc.tblGridMeasureDao = dao;
        FilterObject filterObject = new FilterObject();
        filterObject.setOnlyUsersGMsDesired(true);
        filterObject.setCanceledStatusActive(true);
        filterObject.setClosedStatusActive(true);

        List<GridMeasure> retList = tbc.getGridMeasures("testhugo", filterObject);
        List<TblGridMeasure> orgList = dao.getGridMeasuresByUserName("testhugo");
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
    }

    @Test
    public void testGetGridMeasuresOnlyUsersGMsWithoutClosed() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        tbc.tblGridMeasureDao = dao;
        FilterObject filterObject = new FilterObject();
        filterObject.setOnlyUsersGMsDesired(true);
        filterObject.setCanceledStatusActive(true);
        filterObject.setClosedStatusActive(false);

        List<GridMeasure> retList = tbc.getGridMeasures("testhugo", filterObject);
        List<TblGridMeasure> orgList = dao.getGridMeasuresByUserName("testhugo");
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
    }

    @Test
    public void testGetGridMeasuresOnlyUsersGMsWithoutCanceled() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        tbc.tblGridMeasureDao = dao;
        FilterObject filterObject = new FilterObject();
        filterObject.setOnlyUsersGMsDesired(true);
        filterObject.setCanceledStatusActive(false);
        filterObject.setClosedStatusActive(true);

        List<GridMeasure> retList = tbc.getGridMeasures("testhugo", filterObject);
        List<TblGridMeasure> orgList = dao.getGridMeasuresByUserName("testhugo");
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
    }

    @Test
    public void testGetGridMeasuresOnlyUsersGMsWithoutCanceledAndClosed() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        tbc.tblGridMeasureDao = dao;
        FilterObject filterObject = new FilterObject();
        filterObject.setOnlyUsersGMsDesired(true);
        filterObject.setCanceledStatusActive(false);
        filterObject.setClosedStatusActive(false);

        List<GridMeasure> retList = tbc.getGridMeasures("testhugo", filterObject);
        List<TblGridMeasure> orgList = dao.getGridMeasuresByUserName("testhugo");
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
    }

    @Test
    public void testGetGridMeasureById() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        TblSingleGridmeasureDao sgmDao = createTblSingleGridmeasureDao();
        TblStepsDao stpdao = createTblStepsDao();
        tbc.tblGridMeasureDao = dao;
        tbc.tblSingleGridmeasureDao = sgmDao;
        tbc.tblStepsDao = stpdao;

        GridMeasure gridMeasure = tbc.getGridMeasureById(1);
        TblGridMeasure gridMeasureCompare = dao.findByIdInTx(TblGridMeasure.class, 1);

        assertEquals(gridMeasure.getId(), gridMeasureCompare.getId());
        assertEquals(gridMeasure.getTitle(), gridMeasureCompare.getTitle());
        assertEquals(gridMeasure.getDescription(), gridMeasureCompare.getDescription());

    }

    @Test
    public void testStoreGridMeasure() throws Exception {
        GridMeasureBackendController be = new GridMeasureBackendController();

        TblGridMeasure mGm = new TblGridMeasure();
        mGm.setId(1);
        mGm.setIdDescriptive("20");
        mGm.setTitle("Title");

        TblGridMeasureDao daoMock = PowerMock.createNiceMock(TblGridMeasureDao.class);
        expect(daoMock.storeInTx(mGm)).andReturn(mGm);
    }


    @Test
    public void testGetAffectedResourcesDistinct() throws Exception {
        TestableGridMeasureBackendController be = new TestableGridMeasureBackendController();

        List<String> sList = new ArrayList<>(2);
        sList.add("Bruno");
        sList.add("Haferkamp");

        TblGridMeasureDao daoMock = PowerMock.createNiceMock(TblGridMeasureDao.class);
        expect(daoMock.getAffectedResourcesDistinct(PlgmProcessState.CANCELED.getStatusValue())).andReturn(sList);

        replay(daoMock);

        be.tblGridMeasureDao = daoMock;
        assertEquals( be.getAffectedResourcesDistinct().size(), 2 );
    }


    @Test
    public void testInvokeGetVersionInfo() {
        PlannedGridMeasuresResource plgmResources = new PlannedGridMeasuresResource();
        plgmResources.getVersionInfo();

        assertEquals(plgmResources.invokeRunnable(null, BaseWebService.SecureType.NONE, modusr -> new GridMeasureBackendController().getVersionInfo("version")).getStatus(), HttpStatus.SC_OK);
    }

    @Test
    public void testStoreGridMeasure_ok() throws ProcessException, HttpStatusException, Exception {
        GridMeasure gm = new GridMeasure();
        gm.setId(1);
        gm.setStatusId(0);
        PlgmProcessSubject sub = PlgmProcessSubject.fromGridMeasure(gm, "modUser");
        Recoverable<PlgmProcessSubject> mockedRecov
                = EasyMock.createMock(Recoverable.class);
        mockedRecov.start(anyObject());
        expectLastCall().andVoid().anyTimes();
        replay(mockedRecov);

        PlgmGrid mockedGrid = EasyMock.createNiceMock(PlgmGrid.class);
        expect(mockedGrid.recover((PlgmProcessSubject)anyObject())).andReturn(mockedRecov).anyTimes();

        replay(mockedGrid);
        verify(mockedGrid);

        TblLockDao mockedLockDao = createTblLockDao();
        TblGridMeasureDao mockedGMDao = createTblGridMeasureDao();
        TblSingleGridmeasureDao mockedSGmDao = createTblSingleGridmeasureDao();
        TblStepsDao mockStpDao = createTblStepsDao();

        //GridMeasureBackendController bc = new GridMeasureBackendController();
        TestableGridMeasureBackendController tbc = new TestableGridMeasureBackendController();
        tbc.tblLockDao = mockedLockDao;
        tbc.tblGridMeasureDao = mockedGMDao;
        tbc.tblSingleGridmeasureDao = mockedSGmDao;
        tbc.tblStepsDao = mockStpDao;
        ProcessGrid pg = Whitebox.getInternalState(GridMeasureBackendController.class, "processGrid");
        Whitebox.setInternalState(GridMeasureBackendController.class, "processGrid", mockedGrid);
        try {
            tbc.storeGridMeasure(jwtString,"modUser", gm);
        }
        finally {
            Whitebox.setInternalState(GridMeasureBackendController.class, pg);
        }

    }

    @Test( expected = HttpStatusException.class )
    public void testStoreGridMeasure_notAuthorized() throws ProcessException, HttpStatusException, Exception {
        GridMeasure gm = new GridMeasure();
        gm.setId(1);
        gm.setStatusId(3);
        PlgmProcessSubject sub = PlgmProcessSubject.fromGridMeasure(gm, "modUser");
        Recoverable<PlgmProcessSubject> mockedRecov
                = EasyMock.createMock(Recoverable.class);
        mockedRecov.start(anyObject());
        expectLastCall().andVoid().anyTimes();
        replay(mockedRecov);

        PlgmGrid mockedGrid = EasyMock.createNiceMock(PlgmGrid.class);
        expect(mockedGrid.recover((PlgmProcessSubject)anyObject())).andReturn(mockedRecov).anyTimes();

        replay(mockedGrid);
        verify(mockedGrid);

        TblLockDao mockedLockDao = createTblLockDao();
        TblGridMeasureDao mockedGMDao = createTblGridMeasureDaoStatus();

        TestableGridMeasureBackendController tbc = new TestableGridMeasureBackendController();
        tbc.tblLockDao = mockedLockDao;
        tbc.tblGridMeasureDao = mockedGMDao;
        ProcessGrid pg = Whitebox.getInternalState(GridMeasureBackendController.class, "processGrid");
        Whitebox.setInternalState(GridMeasureBackendController.class, "processGrid", mockedGrid);
        try {
            tbc.storeGridMeasure(jwtString,"modUser", gm);
        }
        finally {
            Whitebox.setInternalState(GridMeasureBackendController.class, pg);
        }
    }

    @Test( expected = HttpStatusException.class )
    public void testStoreGridMeasure_Locked() throws ProcessException, HttpStatusException, Exception {
        GridMeasure gm = new GridMeasure();
        gm.setId(2);
        gm.setStatusId(0);
        PlgmProcessSubject sub = PlgmProcessSubject.fromGridMeasure(gm, "modUser");
        Recoverable<PlgmProcessSubject> mockedRecov
                = EasyMock.createMock(Recoverable.class);
        mockedRecov.start(anyObject());
        expectLastCall().andVoid().anyTimes();
        replay(mockedRecov);

        PlgmGrid mockedGrid = EasyMock.createNiceMock(PlgmGrid.class);
        expect(mockedGrid.recover((PlgmProcessSubject)anyObject())).andReturn(mockedRecov).anyTimes();

        replay(mockedGrid);
        verify(mockedGrid);

        TblLockDao mockedLockDao = createTblLockDao_locked();
        TblGridMeasureDao mockedGMDao = createTblGridMeasureDaoStatus();

        TestableGridMeasureBackendController tbc = new TestableGridMeasureBackendController();
        tbc.tblLockDao = mockedLockDao;
        tbc.tblGridMeasureDao = mockedGMDao;
        ProcessGrid pg = Whitebox.getInternalState(GridMeasureBackendController.class, "processGrid");
        Whitebox.setInternalState(GridMeasureBackendController.class, "processGrid", mockedGrid);
        try {
            tbc.storeGridMeasure(jwtString,"modUser", gm);
        }
        finally {
            Whitebox.setInternalState(GridMeasureBackendController.class, pg);
        }

    }

    @Test( expected = HttpStatusException.class )
    public void testStoreGridMeasure_SimpleProcessException() throws ProcessException, HttpStatusException {
        testStoreGridMeasureBase_exception(new ProcessException(""));
    }

    @Test( expected = HttpStatusException.class )
    public void testStoreGridMeasure_NestedProcessException() throws ProcessException, HttpStatusException {
        try {
            testStoreGridMeasureBase_exception(new ProcessException("", new HttpStatusException(HttpStatus.SC_BAD_REQUEST)));
        } catch ( HttpStatusException e ) {
            assertEquals(HttpStatus.SC_BAD_REQUEST, e.getHttpStatus());
            throw e;
        }
    }

    protected void testStoreGridMeasureBase_exception(Exception exceptionToThrow) throws ProcessException, HttpStatusException {
        TblLockDao mockedLockDao;

        GridMeasure gm = new GridMeasure();
        gm.setStatusId(1);
        PlgmProcessSubject sub = PlgmProcessSubject.fromGridMeasure(gm, "modUser");
        Recoverable<PlgmProcessSubject> mockedRecov
                = EasyMock.createMock(Recoverable.class);
        mockedRecov.start(anyObject());
        expectLastCall().andThrow(exceptionToThrow).anyTimes();
        replay(mockedRecov);
        verify(mockedRecov);

        PlgmGrid mockedGrid = EasyMock.createNiceMock(PlgmGrid.class);
        expect(mockedGrid.recover((PlgmProcessSubject)anyObject())).andReturn(mockedRecov).anyTimes();

        replay(mockedGrid);
        verify(mockedGrid);

        try {
            mockedLockDao = createTblLockDao();
        }
        catch(Exception e){
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }

        TblGridMeasureDao mockedGMDao = createTblGridMeasureDao();

        //GridMeasureBackendController bc = new GridMeasureBackendController();
        TestableGridMeasureBackendController tbc = new TestableGridMeasureBackendController();
        tbc.tblLockDao = mockedLockDao;
        tbc.tblGridMeasureDao = mockedGMDao;
        ProcessGrid pg = Whitebox.getInternalState(GridMeasureBackendController.class, "processGrid");
        Whitebox.setInternalState(GridMeasureBackendController.class, "processGrid", mockedGrid);
        try {
            tbc.storeGridMeasure(jwtString, "modUser", gm);
        }
        finally {
            Whitebox.setInternalState(GridMeasureBackendController.class, "processGrid", pg);
        }
    }

    @Test
    public void testgGetCurrentReminders() throws Exception {
        List remindersList = new ArrayList();

        TblGridMeasureDao mockedGmDao = createTblGridMeasureDao();
        TestableGridMeasureBackendController tbc = new TestableGridMeasureBackendController();
        tbc.tblGridMeasureDao = mockedGmDao;

        remindersList = tbc.getCurrentReminders(jwtString);

        assertTrue(remindersList != null);
        assertTrue(!remindersList.isEmpty());
    }

    @Test
    public void testgGetExpiredReminders() throws Exception {
        List remindersList = new ArrayList();

        TblGridMeasureDao mockedGmDao = createTblGridMeasureDao();
        TestableGridMeasureBackendController tbc = new TestableGridMeasureBackendController();
        tbc.tblGridMeasureDao = mockedGmDao;

        remindersList = tbc.getExpiredReminders(jwtString);

        assertTrue(remindersList != null);
        assertTrue(!remindersList.isEmpty());
    }

    @Test
    public void testCheckLock_locked() throws Exception {
        TblLockDao mockedDao = createTblLockDao();

        tbc.tblLockDao = mockedDao;

        Lock vmLock = tbc.checkLock(2,"GridMeasure");

        assertTrue(vmLock != null);
        assertEquals(vmLock.getId(), (Integer)1);
        assertEquals(vmLock.getKey(), (Integer)2);
        assertEquals(vmLock.getInfo(), "GridMeasure");
    }

    @Test( expected = HttpStatusException.class )
    public void testCheckLock_notLocked() throws HttpStatusException {
        TblLockDao mockedDao = createTblLockDao_null();
        tbc.tblLockDao = mockedDao;
        Lock vmLock = tbc.checkLock(3,"GridMeasureX");
    }

    @Test
    public void testCreateLock_actualUser() throws Exception {
        TblLockDao mockedDao = createTblLockDao();

        tbc.tblLockDao = mockedDao;

        Lock vmLock = tbc.createLock("Testuser",2,"GridMeasure");

        assertTrue(vmLock != null);
        assertEquals(vmLock.getId(), (Integer)1);
        assertEquals(vmLock.getKey(), (Integer)2);
        assertEquals(vmLock.getInfo(), "GridMeasure");
    }

    @Test( expected = HttpStatusException.class )
    public void testCreateLock_locked() throws Exception {
        TblLockDao mockedDao = createTblLockDao();
        tbc.tblLockDao = mockedDao;
        Lock vmLock = tbc.createLock("TestuserX",2,"GridMeasure");
    }

    @Test
    public void testCreateLock_notLocked() throws Exception {
        TblLockDao mockedDao = createTblLockDao();
        tbc.tblLockDao = mockedDao;
        Lock vmLock = tbc.createLock("Testuser",5,"GridMeasureX");

        assertTrue(vmLock != null);
        assertEquals(vmLock.getId(), (Integer)1);
        assertEquals(vmLock.getKey(), (Integer)2);
        assertEquals(vmLock.getInfo(), "GridMeasure");
    }

    @Test
    public void testDeleteLock_notExisting() throws Exception {
        TblLockDao mockedDao = createTblLockDao();
        tbc.tblLockDao = mockedDao;
        Integer key = tbc.deleteLock("Testuser",5,"GridMeasureX", false);
        assertTrue( key == 5 );
    }

    @Test
    public void testDeleteLock() throws Exception {
        TblLockDao mockedDao = createTblLockDao();
        tbc.tblLockDao = mockedDao;
        Integer key = tbc.deleteLock("Testuser",2,"GridMeasure", false);

        assertTrue(key == 2);
    }

    @Test
    public void testGetMailAddressesFromGridmeasures(){

        TestableGridMeasureBackendController be = new TestableGridMeasureBackendController();

        List<String> mailAddressesList  = new ArrayList<>();
        mailAddressesList.add("testuser1@test.com, testuser2@test.com");
        mailAddressesList.add("testuser2@test.com, testuser3@test.com");
        mailAddressesList.add("testuser3@test.com, testuser4@test.com");

        MailAddressCache cacheMock = PowerMock.createNiceMock(MailAddressCache.class);

        expect(cacheMock.getMailAddresses()).andReturn(mailAddressesList);
        replay(cacheMock);

        be.mailAddressCache = cacheMock;
        List<String> retList = be.getMailAddressesFromGridmeasures();
        assertTrue(retList != null);
    }

    @Test
    public void testGetResponsiblesOnSiteFromSingleGridmeasures(){

        TestableGridMeasureBackendController be = new TestableGridMeasureBackendController();

        List<String> sList = new ArrayList<>(3);
        sList.add("Heini");
        sList.add("Martha");
        sList.add("Luis");

        TblSingleGridmeasureDao daoMock = PowerMock.createNiceMock(TblSingleGridmeasureDao.class);
        expect(daoMock.getResponsiblesOnSite()).andReturn(sList);

        replay(daoMock);

        be.tblSingleGridmeasureDao = daoMock;
        assertEquals( be.getResponsiblesOnSiteFromSingleGridmeasures().size(), 3 );
    }


    @Test
    public void testGetCalender() {

        TestableGridMeasureBackendController be = new TestableGridMeasureBackendController();

        TblGridMeasureDao mockedGmDao = createTblGridMeasureDao();
        be.tblGridMeasureDao = mockedGmDao;

        TblSingleGridmeasureDao mockedSgmDao = createTblSingleGridmeasureDao();
        be.tblSingleGridmeasureDao = mockedSgmDao;

        assertEquals( be.getCalender().size(),5);

    }

    @Test
    public void testGetUserDepartmentsResponsibleOnSite() throws Exception {
        TblSingleGridmeasureDao dao = createTblSingleGridmeasureDao();
        GridMeasureBackendControllerTest.TestableGridMeasureBackendController tbc = new GridMeasureBackendControllerTest.TestableGridMeasureBackendController();
        tbc.tblSingleGridmeasureDao = dao;

        List<String> retList = tbc.getUserDepartmentsResponsibleOnSite();
        List<String> orgList = dao.getUserDepartmentsResponsibleOnSite();
        TestCase.assertEquals(orgList.size(), retList.size());
    }

    @Test
    public void testGetUserDepartmentsCreated() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        GridMeasureBackendControllerTest.TestableGridMeasureBackendController tbc = new GridMeasureBackendControllerTest.TestableGridMeasureBackendController();
        tbc.tblGridMeasureDao = dao;

        List<String> retList = tbc.getUserDepartmentsCreated();
        List<String> orgList = dao.getUserDepartmentsCreated();
        TestCase.assertEquals(orgList.size(), retList.size());
    }

    @Test
    public void testGetUserDepartmentsModified() throws Exception {
        TblGridMeasureDao dao = createTblGridMeasureDao();
        GridMeasureBackendControllerTest.TestableGridMeasureBackendController tbc = new GridMeasureBackendControllerTest.TestableGridMeasureBackendController();
        tbc.tblGridMeasureDao = dao;

        List<String> retList = tbc.getUserDepartmentsModified();
        List<String> orgList = dao.getUserDepartmentsModified();
        TestCase.assertEquals(orgList.size(), retList.size());
    }

    @Test
    public void testGetNetworkControlsFromSingleGridmeasures() throws Exception {
        TblSingleGridmeasureDao dao = createTblSingleGridmeasureDao();
        GridMeasureBackendControllerTest.TestableGridMeasureBackendController tbc = new GridMeasureBackendControllerTest.TestableGridMeasureBackendController();
        tbc.tblSingleGridmeasureDao = dao;

        List<String> retList = tbc.getNetworkControlsFromSingleGridmeasures();
        List<String> orgList = dao.getNetworkControls();
        TestCase.assertEquals(orgList.size(), retList.size());
    }


}
