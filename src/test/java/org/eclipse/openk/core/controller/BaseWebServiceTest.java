/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.resources.PlannedGridMeasuresResource;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class BaseWebServiceTest {
	private String payloadNormalUser = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiIwZGUyNTMyYi1mNTBkLTQxZTUtODQwYy1iNzZkOTYyZGM3MDYiLCJleHAiOjE1MjE2NDA0MTIsIm5iZiI6MCwiaWF0IjoxNTIxNjQwMTEyLCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImMyZTlkN2FlLTJiZmEtNDU3OC1iMDllLWY1ZGM1ZjA5YTg3OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJkOTE1MjY2MS03NTJlLTRiNmMtOWRiNi0wYjFmYzY3YTc4ZTciLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLW5vcm1hbHVzZXIiLCJ1bWFfYXV0aG9yaXphdGlvbiIsInBsYW5uZWQtcG9saWNpZXMtbm9ybWFsdXNlciJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInJvbGVzIjoiW3BsYW5uZWQtcG9saWNpZXMtbm9ybWFsdXNlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBlbG9nYm9vay1hY2Nlc3MsIGVsb2dib29rLW5vcm1hbHVzZXJdIiwibmFtZSI6Ik90dG8gTm9ybWFsdmVyYnJhdWNoZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJvdHRvIiwiZ2l2ZW5fbmFtZSI6Ik90dG8iLCJmYW1pbHlfbmFtZSI6Ik5vcm1hbHZlcmJyYXVjaGVyIn0.F-CcNatXaE0W4PhBNaSGUwddE_S05HqL6QuJLOWQo_n0_LdWGRw8iyF6RIbK8y06kH3gb79c6t1U0njWqE68pL9FQfP2t-_en-XlCwKzBdDXtq8e8xHyZ00P5zlE80du0A74qzh2g0HrE9fCgEaawk2_M6JxNFrToiwl6PGRS9RNRD_xgKVXn0XqG-I_qRKA-GdBTcLPjPD9bn4fAdlf3HWdVPUNclRI2ttUykFmpmRTAHKwI3cZOBrIMn4dasMn9k1xPP5IAGtCcgs6TF0YlnWvZgIhdT_5O4rmMtzZXjeuYsjpNnqz0LGA0GT6up90PZrTJF8rHtWU50wVgJSH9g";
	private static final org.apache.log4j.Logger EMPTYLOGGER = org.apache.log4j.Logger
			.getLogger(BaseWebServiceTest.class.getName());

	private BaseWebService createBaseWebService() {
		return new BaseWebService(EMPTYLOGGER) {
			@Override
			protected void assertAndRefreshToken(String token, SecureType secureType) throws HttpStatusException {

			}

			@Override
			protected String getUserFromToken(String token) {
				return null;
			}
		};
	}

	private BaseWebService.ModifyingInvokable createInvokable( Object o, Exception e2Return ) {
		return (chuser) -> {
            if( e2Return == null ) {
                return o;
            }
            else {
                throw e2Return;
            }
        };
	}

	@Test
	public void testInvokeRunnableOK() {
		Response r = createBaseWebService().invokeRunnable(null, BaseWebService.SecureType.NONE, createInvokable("Test Ok", null));
		assertEquals(HttpStatus.OK_200, r.getStatus());
	}

	@Test
	public void testInvokeRunnableNotNoneSecuretype() throws HttpStatusException {
		Response r = createBaseWebService().invokeRunnable(payloadNormalUser, BaseWebService.SecureType.NORMAL, createInvokable("Test Ok", null));
		assertNotEquals("", new PlannedGridMeasuresResource().getUserFromToken(payloadNormalUser));
	}

	@Test
	public void testInvokeRunnable_GeneralException() {
		Response r = createBaseWebService().invokeRunnable(null, BaseWebService.SecureType.NONE, createInvokable( null,
				new Exception("hallodri")));
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR_500, r.getStatus());
	}

	@Test
	public void testGetVersionString() throws Exception, IOException {
		BaseWebService bws = createBaseWebService();
		String version = (String) Whitebox.invokeMethod(bws, "getVersionString");
		assertNotNull(version);
		assertNotEquals(version, "");
	}




}
