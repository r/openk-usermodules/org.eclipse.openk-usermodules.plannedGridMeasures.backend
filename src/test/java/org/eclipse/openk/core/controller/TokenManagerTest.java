/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.auth2.model.JwtPayload;
import org.eclipse.openk.auth2.util.JwtHelper;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TokenManagerTest {
	private TokenManager tokenManager;
	private String payloadNormalUser = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiIwZGUyNTMyYi1mNTBkLTQxZTUtODQwYy1iNzZkOTYyZGM3MDYiLCJleHAiOjE1MjE2NDA0MTIsIm5iZiI6MCwiaWF0IjoxNTIxNjQwMTEyLCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImMyZTlkN2FlLTJiZmEtNDU3OC1iMDllLWY1ZGM1ZjA5YTg3OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJkOTE1MjY2MS03NTJlLTRiNmMtOWRiNi0wYjFmYzY3YTc4ZTciLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLW5vcm1hbHVzZXIiLCJ1bWFfYXV0aG9yaXphdGlvbiIsInBsYW5uZWQtcG9saWNpZXMtbm9ybWFsdXNlciJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInJvbGVzIjoiW3BsYW5uZWQtcG9saWNpZXMtbm9ybWFsdXNlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBlbG9nYm9vay1hY2Nlc3MsIGVsb2dib29rLW5vcm1hbHVzZXJdIiwibmFtZSI6Ik90dG8gTm9ybWFsdmVyYnJhdWNoZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJvdHRvIiwiZ2l2ZW5fbmFtZSI6Ik90dG8iLCJmYW1pbHlfbmFtZSI6Ik5vcm1hbHZlcmJyYXVjaGVyIn0.F-CcNatXaE0W4PhBNaSGUwddE_S05HqL6QuJLOWQo_n0_LdWGRw8iyF6RIbK8y06kH3gb79c6t1U0njWqE68pL9FQfP2t-_en-XlCwKzBdDXtq8e8xHyZ00P5zlE80du0A74qzh2g0HrE9fCgEaawk2_M6JxNFrToiwl6PGRS9RNRD_xgKVXn0XqG-I_qRKA-GdBTcLPjPD9bn4fAdlf3HWdVPUNclRI2ttUykFmpmRTAHKwI3cZOBrIMn4dasMn9k1xPP5IAGtCcgs6TF0YlnWvZgIhdT_5O4rmMtzZXjeuYsjpNnqz0LGA0GT6up90PZrTJF8rHtWU50wVgJSH9g";
	private String payloadSuperUser = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiI2ZTU0YzFkNS02ZDk4LTRiYzUtOTk5NC0zNjdmZDM1YTgyODYiLCJleHAiOjE1MjE3MDk4NDgsIm5iZiI6MCwiaWF0IjoxNTIxNzA5NTQ4LCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6IjM1OWVmOWM5LTc3ZGYtNGEzZC1hOWM5LWY5NmQ4MzdkMmQ1NyIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI3NjdjMDI3Mi0wZjE4LTRkZTMtYmIxYS0yZTMyNDAyYjA1NDEiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLXN1cGVydXNlciIsImVsb2dib29rLW5vcm1hbHVzZXIiLCJ1bWFfYXV0aG9yaXphdGlvbiIsImZlZWRpbi1tYW5hZ2VtZW50LWFjY2VzcyIsInBsYW5uZWQtcG9saWNpZXMtYWNjZXNzIiwicGxhbm5pbmctYWNjZXNzIiwicGxhbm5lZC1wb2xpY2llcy1zdXBlcnVzZXIiLCJwbGFubmVkLXBvbGljaWVzLW5vcm1hbHVzZXIiXX0sInJlc291cmNlX2FjY2VzcyI6eyJyZWFsbS1tYW5hZ2VtZW50Ijp7InJvbGVzIjpbInZpZXctdXNlcnMiLCJxdWVyeS1ncm91cHMiLCJxdWVyeS11c2VycyJdfX0sInJvbGVzIjoiW2Vsb2dib29rLXN1cGVydXNlciwgZWxvZ2Jvb2stbm9ybWFsdXNlciwgdW1hX2F1dGhvcml6YXRpb24sIGZlZWRpbi1tYW5hZ2VtZW50LWFjY2VzcywgcGxhbm5lZC1wb2xpY2llcy1hY2Nlc3MsIHBsYW5uaW5nLWFjY2VzcywgcGxhbm5lZC1wb2xpY2llcy1ub3JtYWx1c2VyLCBlbG9nYm9vay1hY2Nlc3MsIGVsb2dib29rLW5vcm1hbHVzZXIsIHBsYW5uZWQtcG9saWNpZXMtc3VwZXJ1c2VyLCBwbGFubmVkLXBvbGljaWVzLW5vcm1hbHVzZXJdIiwibmFtZSI6IkFkbWluaXN0cmF0b3IiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiIsImdpdmVuX25hbWUiOiJBZG1pbmlzdHJhdG9yIiwiZmFtaWx5X25hbWUiOiIiLCJlbWFpbCI6IiJ9.XYeiSm_j79R_Z9BIZasQGY_7rLHjDQATZH92c0QJA-fLQ9GJJBS6qMh8PW29C-25vREdnV4LbdMQd0Ohoqyw86CktSNnqVnZMx-c6_6Y1H_txkKLz4P0T4OmoGSus5orxWAN7wwzBjEoNImdoBaCKD10cKXgpS2wD3kd9vjMhW0vaYD9DStzHJ-EOkQCAEesAJuRg8f1LyQ7s9rm4IUU5c9eOasfZUj2gSpKjcglxIiAgDFhC905B-KBC2KXEmDK-Z2hB9FEWsfzRN_HzXn2lR9BTI4ToEdIf-WcnkjiQ-nJ0SNFnr-GIK2eIoqphD7kkPnNxV4OYZAoT9MCPX07JA";

	// only elogbook user - jasper - he has not pgm access rights
	private String payloadRamboUser = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiIwMWVjYjdiOC01MDQ0LTQwZTEtYmI2Ny00ZmZhMjQwYWNkYzkiLCJleHAiOjE1MjE3MTIyNDAsIm5iZiI6MCwiaWF0IjoxNTIxNzExOTQwLCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6Ijc5YzQzNWFkLWQ3ZGQtNDRjMC1iMWQ1LTY1OWIwYmMyYzc3ZiIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJmYmU1NzFlOS00ODQ1LTQ0NjYtYWU4MC0yN2JiYmFhYTBlMzIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLW5vcm1hbHVzZXIiXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJyb2xlcyI6IltlbG9nYm9vay1hY2Nlc3MsIGVsb2dib29rLW5vcm1hbHVzZXJdIiwibmFtZSI6Ikphc3BlciBNYWdpY2lhbiIsInByZWZlcnJlZF91c2VybmFtZSI6Imphc3BlciIsImdpdmVuX25hbWUiOiJKYXNwZXIiLCJmYW1pbHlfbmFtZSI6Ik1hZ2ljaWFuIn0.s8M-dVtFWFHEkK5-mN6eUeQbPhqFdEI-Y5ovxUejBYi25-rzR4UGKrxdrOyDXBbAAHsZ9tAtDveh-lBh4xny3b82bdgMiYft3Wpsa_4EhweqM39dz1oROHVlUqFVTH2nmp-FXyvMpdVKj81sOgvGHD9YobgNuJiDWX6ohxS5tR3NU9CbDpUz81jd0gsYH0ZelKQjEinmmCnx2DdYKJPawSUMgTIhR_m_ea60ujwTN2dPTxU09HwtPlLTRSHf9I2uxsdxJrwaIIL-HNVsMBiAhXcRanEikMkW9YBBwgjWny87A1iwg8Y-tH06iFwc5qAYxAnX8eafaT0yOiWELQPXdg";

	JwtPayload jwtPayloadNormalUser = JwtHelper.getJwtPayload(payloadNormalUser);
	JwtPayload jwtPayloadSuperUser = JwtHelper.getJwtPayload(payloadSuperUser);
	JwtPayload jwtPayloadRamboUser = JwtHelper.getJwtPayload(payloadRamboUser);

    public TokenManagerTest() throws HttpStatusException {
    }

    @Before
	public void init() {
		TestHelper.initDefaultBackendConfig();
		this.tokenManager = TokenManager.getInstance();
	}


	@Test(expected = HttpStatusException.class)
	public void testLogout() throws HttpStatusException {
		tokenManager.logout(payloadNormalUser);
	}

	@Test(expected = HttpStatusException.class)
	public void testCheckAuth() throws HttpStatusException {
		tokenManager.checkAut(payloadNormalUser);
	}


	@Test
	public void testNormaluser() {
		assertTrue(jwtPayloadNormalUser.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_NORMALUSER));
	}

	@Test
	public void testSuperuser() {
		assertTrue(jwtPayloadSuperUser.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_SUPERUSER));
	}

	@Test
	public void testRambouser() {
		assertFalse(jwtPayloadRamboUser.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_NORMALUSER));
		assertFalse(jwtPayloadRamboUser.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_SUPERUSER));
	}

	@Test(expected = HttpStatusException.class)
	public void testPayloadisEmpty() throws HttpStatusException {
		tokenManager.checkAutLevel("", BaseWebService.SecureType.NORMAL);
	}

	// SecureType = NONE
	@Test
	public void testSecureTypeNone() throws HttpStatusException {
		tokenManager.checkAutLevel(payloadNormalUser, BaseWebService.SecureType.NONE);
	}

	// SecureType = NORMAL
	@Test(expected = HttpStatusException.class)
	public void testSecureTypeNormalAndRamboUser() throws HttpStatusException {
		tokenManager.checkAutLevel(payloadRamboUser, BaseWebService.SecureType.NORMAL);
	}

	@Test
	public void testSecureTypeNormalAndNormalUser() throws HttpStatusException {
		tokenManager.checkAutLevel(payloadNormalUser, BaseWebService.SecureType.NORMAL);
	}

	@Test
	public void testSecureTypeNormalAndSuperUser() throws HttpStatusException {
		tokenManager.checkAutLevel(payloadSuperUser, BaseWebService.SecureType.NORMAL);
	}

	// SecureType = HIGH
	@Test(expected = HttpStatusException.class)
	public void testSecureTypeHighAndNormalUSer() throws HttpStatusException {
		tokenManager.checkAutLevel(payloadNormalUser, BaseWebService.SecureType.HIGH);
	}

	@Test
	public void testSecureTypeHighAndSuperUSer() throws HttpStatusException {
		tokenManager.checkAutLevel(payloadSuperUser, BaseWebService.SecureType.HIGH);
	}


}
