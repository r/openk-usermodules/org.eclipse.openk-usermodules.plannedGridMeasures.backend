/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.easymock.EasyMock.*;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.apache.http.HttpStatus;
import org.easymock.EasyMock;
import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.api.*;
import org.eclipse.openk.common.mapper.generic.GenericApiToDbMapper;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.db.dao.*;
import org.eclipse.openk.db.model.*;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;

public class MasterDataBackendControllerTest {

    public MasterDataBackendController beMockController;
    private MasterDataBackendControllerTest.TestableMasterDataBackendController tbc;

    @Before
    public void prepareTests() throws Exception {
        TestHelper.initDefaultBackendConfig();

        //mock email-configuration
//        PlannedGridMeasuresConfiguration.EmailConfiguration emailConfiguration = new PlannedGridMeasuresConfiguration.EmailConfiguration();
//        emailConfiguration.setPort("25");
//        emailConfiguration.setSmtpHost("localhostTest");
//        BackendConfig.getInstance().setEmailConfiguration(emailConfiguration);
//        EmailTemplatePaths templatePaths = new EmailTemplatePaths();
//        templatePaths.setAppliedEmailTemplate("appliedEmailPathToEmailTxt");
        BackendConfig.getInstance().getBackendSettings().setEmailTemplateAddressesForNotification("appliedEmailTemplate,\n" +
                "forapprovalEmailTemplate,\n" +
                "approvedEmailTemplate,\n" +
                "requestedEmailTemplate,\n" +
                "releasedEmailTemplate,\n" +
                "activeEmailTemplate,\n" +
                "inworkEmailTemplate,\n" +
                "workfinishedEmailTemplate,\n" +
                "finishedEmailTemplate,\n" +
                "closedEmailTemplate,\n" +
                "cancelledEmailTemplate,\n" +
                "rejectedEmailTemplate");

        beMockController = PowerMock.createNiceMock(MasterDataBackendController.class);
        tbc = new MasterDataBackendControllerTest.TestableMasterDataBackendController();
//        tbc.tblMeasureDocumentsDao = createTblMeasureDocumentsDao("noError");
//        tbc.tblDocumentsDao = createTblDocumentsDao("noError", "fileNameExample.txt");
    }

    private class TestableMasterDataBackendController extends MasterDataBackendController {
        public RefGmStatusDao statusDao;
        public RefBranchDao branchDao;
        public RefBranchLevelDao branchLevelDao;
        public RefTerritoryDao territoryDao;
        public RefCostCenterDao costCenterDao;
        public TblUserSettingsDao userSettingsDao;

        @Override
        protected AutoCloseEntityManager createEm() {
            EntityTransaction et = EasyMock.createMock(EntityTransaction.class);
            et.begin();
            expectLastCall().andVoid().anyTimes();
            et.commit();
            expectLastCall().andVoid().anyTimes();
            et.rollback();
            expectLastCall().andVoid().anyTimes();

            EasyMock.replay(et);
            EasyMock.verify(et);

            EntityManager em = EasyMock.createMock(EntityManager.class);
            expect(em.getTransaction()).andReturn(et).anyTimes();

            expect(em.isOpen()).andReturn(false).anyTimes();


            em.close();
            expectLastCall().andVoid().anyTimes();

            EasyMock.replay(em);
            EasyMock.verify(em);

            return new AutoCloseEntityManager(em);
        }

        @Override
        protected RefBranchDao createRefBranchDao( EntityManager em ) {
            if( branchDao != null ) {
                return branchDao;
            }
            else {
                return super.createRefBranchDao(em);
            }
        }

        @Override
        protected RefBranchLevelDao createRefBranchLevelDao( EntityManager em ) {
            if( branchLevelDao != null ) {
                return branchLevelDao;
            }
            else {
                return super.createRefBranchLevelDao(em);
            }
        }

        @Override
        protected RefGmStatusDao createRefGmStatusDao( EntityManager em ) {
            if( statusDao != null ) {
                return statusDao;
            }
            else {
                return super.createRefGmStatusDao(em);
            }
        }


        @Override
        protected RefCostCenterDao createRefCostCenterDao( EntityManager em ) {
            if( costCenterDao != null ) {
                return costCenterDao;
            }
            else {
                return super.createRefCostCenterDao(em);
            }
        }

        @Override
        protected RefTerritoryDao createRefTerritoryDao( EntityManager em ) {
            if( territoryDao != null ) {
                return territoryDao;
            }
            else {
                return super.createRefTerritoryDao(em);
            }
        }

        @Override
        protected TblUserSettingsDao createRefUserSettingsDao( EntityManager em ) {
            if( userSettingsDao != null ) {
                return userSettingsDao;
            }
            else {
                return super.createRefUserSettingsDao(em);
            }
        }


        protected TblUserSettingsDao createRefUserSettingsDao_store( EntityManager em ) {
            if( userSettingsDao != null ) {
                return userSettingsDao;
            }
            else {
                return super.createRefUserSettingsDao(em);
            }
        }
    }

    private RefBranchDao createRefBranchDao() {

        java.util.List<RefBranch> mList = new ArrayList<RefBranch>();

        RefBranch refBranch1 = new RefBranch();
        refBranch1.setId(1);
        refBranch1.setDescription("Description1");
        refBranch1.setName("Testname1");
        mList.add(refBranch1);

        RefBranch refBranch2 = new RefBranch();
        refBranch2.setId(2);
        refBranch2.setDescription("Description2");
        refBranch2.setName("Testname2");
        mList.add(refBranch2);

        RefBranch refBranch3 = new RefBranch();
        refBranch3.setId(3);
        refBranch3.setDescription("Description3");
        refBranch3.setName("Testname3");
        mList.add(refBranch3);

        RefBranchDao dao = EasyMock.createMock(RefBranchDao.class);
        expect( dao.getBranchesInTx() ).andReturn(mList).anyTimes();
        replay( dao );
        verify( dao );
        return dao;
    }

    private RefBranchLevelDao createRefBranchLevelDao() {

        java.util.List<RefBranchLevel> mList = new ArrayList<RefBranchLevel>();

        RefBranchLevel refBranchLevel1 = new RefBranchLevel();
        refBranchLevel1.setId(1);
        refBranchLevel1.setDescription("Description1");
        refBranchLevel1.setName("Testname1");
        mList.add(refBranchLevel1);

        RefBranchLevel refBranchLevel2 = new RefBranchLevel();
        refBranchLevel2.setId(2);
        refBranchLevel2.setDescription("Description2");
        refBranchLevel2.setName("Testname2");
        mList.add(refBranchLevel2);

        RefBranchLevel refBranchLevel3 = new RefBranchLevel();
        refBranchLevel3.setId(3);
        refBranchLevel3.setDescription("Description3");
        refBranchLevel3.setName("Testname3");
        mList.add(refBranchLevel3);

        RefBranchLevelDao dao = EasyMock.createMock(RefBranchLevelDao.class);
        expect( dao.getBranchLevelsByBranchId(anyInt()) ).andReturn(mList).anyTimes();
        replay( dao );
        verify( dao );
        return dao;
    }

    private RefGmStatusDao createRefGmStatusDao() {

        java.util.List<RefGmStatus> mList = new ArrayList<RefGmStatus>();

        RefGmStatus refGmStatus1 = new RefGmStatus();
        refGmStatus1.setId(1);
        refGmStatus1.setName("Testname1");
        mList.add(refGmStatus1);

        RefGmStatus refGmStatus2 = new RefGmStatus();
        refGmStatus2.setId(2);
        refGmStatus2.setName("Testname2");
        mList.add(refGmStatus2);
        mList.add(refGmStatus2);

        RefGmStatus refGmStatus3 = new RefGmStatus();
        refGmStatus3.setId(3);
        refGmStatus3.setName("Testname3");
        mList.add(refGmStatus3);

        RefGmStatusDao dao = EasyMock.createMock(RefGmStatusDao.class);
        expect( dao.getGmStatusInTx() ).andReturn(mList).anyTimes();
        replay( dao );
        verify( dao );
        return dao;
    }

    private RefCostCenterDao createRefCostCenterDao() {

        java.util.List<RefCostCenter> mList = new ArrayList<RefCostCenter>();

        RefCostCenter refCostCenter1 = new RefCostCenter();
        refCostCenter1.setId(1);
        refCostCenter1.setName("Testname1");
        mList.add(refCostCenter1);

        RefCostCenter refCostCenter2 = new RefCostCenter();
        refCostCenter2.setId(2);
        refCostCenter2.setName("Testname2");
        mList.add(refCostCenter2);

        RefCostCenter refCostCenter3 = new RefCostCenter();
        refCostCenter3.setId(3);
        refCostCenter3.setName("Testname3");
        mList.add(refCostCenter3);

        RefCostCenterDao dao = EasyMock.createMock(RefCostCenterDao.class);
        expect( dao.getCostCentersInTx() ).andReturn(mList).anyTimes();
        replay( dao );
        verify( dao );
        return dao;
    }


    private RefTerritoryDao createRefTerritoryDao() {

        java.util.List<RefTerritory> mList = new ArrayList<RefTerritory>();

        RefTerritory refTerritory1 = new RefTerritory();
        refTerritory1.setId(1);
        refTerritory1.setName("Nord");
        mList.add(refTerritory1);

        RefTerritory refTerritory2 = new RefTerritory();
        refTerritory2.setId(2);
        refTerritory2.setName("Ost");
        mList.add(refTerritory2);

        RefTerritory refTerritory3 = new RefTerritory();
        refTerritory3.setId(3);
        refTerritory3.setName("Süd");
        mList.add(refTerritory3);

        RefTerritoryDao dao = EasyMock.createMock(RefTerritoryDao.class);
        expect( dao.getTerritoriesInTx() ).andReturn(mList).anyTimes();
        replay( dao );
        verify( dao );
        return dao;
    }

    private TblUserSettingsDao createTblUserSettingsDao(String error) throws HttpStatusException{

        TblUserSettings tblUserSettings = new TblUserSettings();
        tblUserSettings.setId(1);
        tblUserSettings.setUsername("Testname1");
        tblUserSettings.setSettingType("zzz");
        tblUserSettings.setValue("abc");

        TblUserSettingsDao dao = EasyMock.createMock(TblUserSettingsDao.class);

        switch (error) {
            case "HttpStatusException":
                expect(dao.getUserSettingsBySettingType(anyString(),anyString())).andThrow(new HttpStatusException(HttpStatus.SC_NOT_FOUND)).anyTimes();
                break;
            default:
                expect( dao.getUserSettingsBySettingType("Testname1", "zzz") ).andReturn(tblUserSettings).anyTimes();
        }
        replay( dao );
        verify( dao );

        return dao;
    }

    private TblUserSettingsDao createTblUserSettingsDao_store(AutoCloseEntityManager em) throws HttpStatusException{

        TblUserSettings tblUserSettings = new TblUserSettings();
        tblUserSettings.setId(null);
        tblUserSettings.setUsername("Testname2");
        tblUserSettings.setSettingType("aaa");
        tblUserSettings.setValue("xyz");

        TblUserSettingsDao dao = EasyMock.createMock(TblUserSettingsDao.class);

        expect( dao.getEM()).andReturn(em).anyTimes();
        expect( dao.getUserSettingsBySettingType(anyString(), anyString()) ).andReturn(tblUserSettings).anyTimes();
        expect( dao.createUserSettingsInDb(anyObject(), anyObject()) ).andReturn(tblUserSettings).anyTimes();
        replay( dao );
        verify( dao );

        return dao;
    }

    @Test
    public void testGetEmailAddressesFromTemplates() throws Exception {
        MasterDataBackendControllerTest.TestableMasterDataBackendController tbc = new MasterDataBackendControllerTest.TestableMasterDataBackendController();
        List<String>recipientsList = tbc.getEmailAddressesFromTemplates();
        assertTrue(recipientsList != null);
    }

    @Test
    public void testGetBranches() throws Exception {
        RefBranchDao dao = createRefBranchDao();
        MasterDataBackendControllerTest.TestableMasterDataBackendController tbc = new MasterDataBackendControllerTest.TestableMasterDataBackendController();
        tbc.branchDao = dao;

        List<Branch> retList = tbc.getBranches();
        List<RefBranch> orgList = dao.getBranchesInTx();
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
        assertEquals(orgList.get(1).getDescription(), retList.get(1).getDescription());
        assertEquals(orgList.get(0).getName(), retList.get(0).getName());
    }

    @Test
    public void testGetBranchLevelsByBranch() throws Exception {
        RefBranchLevelDao dao = createRefBranchLevelDao(); //MockDao
        MasterDataBackendControllerTest.TestableMasterDataBackendController tbc = new MasterDataBackendControllerTest.TestableMasterDataBackendController();
        tbc.branchLevelDao = dao; //MockDao

        List<BranchLevel> retList = tbc.getBranchLevelsByBranch(3); //Aufruf Methode der Testklasse
        List<RefBranchLevel> orgList = dao.getBranchLevelsByBranchId(3); //Mock Models
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
        assertEquals(orgList.get(1).getDescription(), retList.get(1).getDescription());
        assertEquals(orgList.get(0).getName(), retList.get(0).getName());
    }

    @Test
    public void testGetGmStatus() throws Exception {
        RefGmStatusDao dao = createRefGmStatusDao();
        MasterDataBackendControllerTest.TestableMasterDataBackendController tbc = new MasterDataBackendControllerTest.TestableMasterDataBackendController();
        tbc.statusDao = dao;

        List<GmStatus> retList = tbc.getGmStatus();
        List<RefGmStatus> orgList = dao.getGmStatusInTx();
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
        assertEquals(orgList.get(1).getName(), retList.get(1).getName());
    }

    @Test
    public void testGetCostCenters() throws Exception {
        RefCostCenterDao dao = createRefCostCenterDao();
        MasterDataBackendControllerTest.TestableMasterDataBackendController tbc = new MasterDataBackendControllerTest.TestableMasterDataBackendController();
        tbc.costCenterDao = dao;

        List<CostCenter> retList = tbc.getCostCenters();
        List<RefCostCenter> orgList = dao.getCostCentersInTx();
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
        assertEquals(orgList.get(1).getName(), retList.get(1).getName());
    }


    @Test
    public void testGetTerritories() throws Exception {
        RefTerritoryDao dao = createRefTerritoryDao();
        MasterDataBackendControllerTest.TestableMasterDataBackendController tbc = new MasterDataBackendControllerTest.TestableMasterDataBackendController();
        tbc.territoryDao = dao;

        List<Territory> retList = tbc.getTerritories();
        List<RefTerritory> orgList = dao.getTerritoriesInTx();
        assertEquals(orgList.size(), retList.size());
        assertEquals(orgList.get(2).getId(), retList.get(2).getId());
        assertEquals(orgList.get(1).getName(), retList.get(1).getName());
    }

    @Test
    public void testGetUserSettings() throws Exception {
        TblUserSettingsDao dao = createTblUserSettingsDao("");
        MasterDataBackendControllerTest.TestableMasterDataBackendController tbc = new MasterDataBackendControllerTest.TestableMasterDataBackendController();
        tbc.userSettingsDao = dao;

        UserSettings userSettings = tbc.getUserSettings("Testname1", "zzz");
        TblUserSettings tblUserSettings = dao.getUserSettingsBySettingType("Testname1", "zzz");

        assertEquals(userSettings.getUsername(), tblUserSettings.getUsername());
        assertEquals(userSettings.getSettingType(), tblUserSettings.getSettingType());
        assertEquals(userSettings.getValue(), tblUserSettings.getValue());
    }

    @Test( expected = HttpStatusException.class )
    public void testGetUserSettings_HttpStatusException() throws HttpStatusException {
        TblUserSettingsDao dao = createTblUserSettingsDao("HttpStatusException");
        MasterDataBackendControllerTest.TestableMasterDataBackendController tbc = new MasterDataBackendControllerTest.TestableMasterDataBackendController();
        tbc.userSettingsDao = dao;

        UserSettings userSettings = tbc.getUserSettings("Testname1", "zzz");
    }


    @Test
    public void testStoreUserSettings() throws HttpStatusException {

        TblUserSettings tblUserSettings = new TblUserSettings();
        tblUserSettings.setId(null);
        tblUserSettings.setUsername("Testname2");
        tblUserSettings.setSettingType("aaa");
        tblUserSettings.setValue("xyz");

        MasterDataBackendControllerTest.TestableMasterDataBackendController tbc = new MasterDataBackendControllerTest.TestableMasterDataBackendController();
        AutoCloseEntityManager emMock = tbc.createEm();
        TblUserSettingsDao dao = createTblUserSettingsDao_store(emMock);

        tbc.userSettingsDao = dao;

        GenericApiToDbMapper mapper = new GenericApiToDbMapper();
        UserSettings vmUs = mapper.mapToViewModel(UserSettings.class, tblUserSettings);

        UserSettings userSettings = tbc.storeUserSettings("Testname2", vmUs);

        assertEquals(userSettings.getUsername(), tblUserSettings.getUsername());
        assertEquals(userSettings.getSettingType(), tblUserSettings.getSettingType());
        assertEquals(userSettings.getValue(), tblUserSettings.getValue());
    }
}
