/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetup;
import org.apache.commons.lang3.time.DateUtils;
import org.eclipse.openk.PlannedGridMeasuresConfiguration.EmailConfiguration;
import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.api.mail.EmailTemplatePaths;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;


public class EmailmanagerTest {
    private static GreenMail mailServer;
    private static int emailTestPort;

    /**
     * Allocate free port.
     * @return Found port.
     * @throws IOException In case of error.
     */
    public static int port() throws IOException {
        if (emailTestPort == 0){
            try (ServerSocket socket = new ServerSocket(0)) {
                emailTestPort = socket.getLocalPort();
                return emailTestPort;
            }
        }
        return emailTestPort;
    }

    @BeforeClass
    public static void beforeAll() throws IOException {
        ServerSetup setup = new ServerSetup(EmailmanagerTest.port() , "localhost", ServerSetup.PROTOCOL_SMTP);
        setup.setServerStartupTimeout(3000);
        mailServer = new GreenMail(setup);
    }

    @Before
    public void prepareTests() throws Exception {
        TestHelper.initDefaultBackendConfig();
        // mock email-configuration
        EmailConfiguration emailConfiguration = createEmailConfiguration();
        BackendConfig.getInstance().setEmailConfiguration(emailConfiguration);


        setTestTemplateEmail("emailConfigurationTest/emailTemplates/appliedEmailTemplateTest.txt");
        mailServer.start();

    }
    @After
    public void afterTest() {
        mailServer.stop();
    }

    private EmailConfiguration createEmailConfiguration() {
        EmailConfiguration emailConfiguration = new EmailConfiguration();
        emailConfiguration.setPort(emailTestPort+"");
        emailConfiguration.setSmtpHost("localhost");
        emailConfiguration.setSender("testCaseSender@test.de");
        return emailConfiguration;
    }


    @Test
    public void testSendMail_ok() throws Exception {

        Date date = DateUtils.parseDate("01.01.2018 18:00:00", "dd.MM.yyyy HH:mm:ss");
        GridMeasure gridMeasure = new GridMeasure();
        String gridMeasureTitle = "TestTitle";
        gridMeasure.setTitle(gridMeasureTitle);
        gridMeasure.setPlannedStarttimeFirstSinglemeasure(date);

        PlgmProcessSubject plgmProcessSubject = PlgmProcessSubject.fromGridMeasure(gridMeasure, "testChangeUser");


        EmailManager emailManager = new PgmEmail(plgmProcessSubject, BackendConfig.getInstance().getEmailTemplatePaths().getAppliedEmailTemplate(), false);
        emailManager.sendEmail();
        String subject = emailManager.subject;
        Message[] messages = mailServer.getReceivedMessages();

        assertEquals(messages.length, 4);
        assertEquals(subject, messages[0].getSubject());
        assertTrue(messages[0].getSubject().contains(gridMeasureTitle));
        assertTrue(messages[0].getSubject().contains(gridMeasureTitle));
        assertTrue(GreenMailUtil.getBody(messages[0]).trim().contains("TEST"));
    }

    @Test
    public void testSendMail_withRecipientsFromGM_ok() throws Exception {

        Date date = DateUtils.parseDate("01.01.2018 18:00:00", "dd.MM.yyyy HH:mm:ss");
        GridMeasure gridMeasure = new GridMeasure();
        String gridMeasureTitle = "testSendMail_withRecipientsFromGM";
        gridMeasure.setTitle(gridMeasureTitle);
        gridMeasure.setPlannedStarttimeFirstSinglemeasure(date);
        gridMeasure.setEmailAddresses("UnitTestMailingListRecipient@test.de; UnitTest2MailingListRecipient@test.de");

        PlgmProcessSubject plgmProcessSubject = PlgmProcessSubject.fromGridMeasure(gridMeasure, "testChangeUser");


        EmailManager emailManager = new PgmEmail(plgmProcessSubject, BackendConfig.getInstance().getEmailTemplatePaths().getAppliedEmailTemplate(), true);
        emailManager.sendEmail();
        String subject = emailManager.subject;
        Message[] messages = mailServer.getReceivedMessages();

        assertEquals(messages.length, 6);
        assertEquals(subject, messages[0].getSubject());
        assertTrue(messages[0].getSubject().contains(gridMeasureTitle));
        assertTrue(messages[0].getSubject().contains(gridMeasureTitle));
        assertTrue(GreenMailUtil.getBody(messages[0]).trim().contains("TEST"));
    }



    @Test (expected = MessagingException.class)
    public void testSendMail_nok() throws Exception {

        mailServer.stop();
        Date date = DateUtils.parseDate("01.01.2018 18:00:00", "dd.MM.yyyy HH:mm:ss");
        GridMeasure gridMeasure = new GridMeasure();
        String gridMeasureTitle = "TestTitle";
        gridMeasure.setTitle(gridMeasureTitle);
        gridMeasure.setPlannedStarttimeFirstSinglemeasure(date);

        PlgmProcessSubject plgmProcessSubject = PlgmProcessSubject.fromGridMeasure(gridMeasure, "testChangeUser");

        EmailManager emailManager = new PgmEmail(plgmProcessSubject, BackendConfig.getInstance().getEmailTemplatePaths().getAppliedEmailTemplate(), false);
        emailManager.sendEmail();
    }

    @Test (expected = MessagingException.class)
    public void testSendMail_invalidRecipient() throws Exception {
        setTestTemplateEmail("emailConfigurationTest/emailTemplates/emailTemplateTestFehlerRecipient.txt");

        GridMeasure gridMeasure = new GridMeasure();
        PlgmProcessSubject plgmProcessSubject = PlgmProcessSubject.fromGridMeasure(gridMeasure, "testChangeUser");

        EmailManager emailManager = new PgmEmail(plgmProcessSubject, BackendConfig.getInstance().getEmailTemplatePaths().getAppliedEmailTemplate(), false);
        emailManager.sendEmail();
    }

    @Test (expected = MessagingException.class)
    public void testSendMail_invalidSender() throws Exception {
        setTestTemplateEmail("emailConfigurationTest/emailTemplates/emailTemplateTestFehlerRecipient.txt");
        EmailConfiguration emailConfiguration = createEmailConfiguration();
        emailConfiguration.setSender("testCaseSendertest.de");
        BackendConfig.getInstance().setEmailConfiguration(emailConfiguration);

        GridMeasure gridMeasure = new GridMeasure();
        PlgmProcessSubject plgmProcessSubject = PlgmProcessSubject.fromGridMeasure(gridMeasure, "testChangeUser");

        EmailManager emailManager = new PgmEmail(plgmProcessSubject, BackendConfig.getInstance().getEmailTemplatePaths().getAppliedEmailTemplate(), false);
        emailManager.sendEmail();
    }


    private void setTestTemplateEmail(String templatePath) {
        EmailTemplatePaths templatePaths = new EmailTemplatePaths();
        templatePaths.setAppliedEmailTemplate(templatePath);
        BackendConfig.getInstance().setEmailTemplatePaths(templatePaths);
    }
}
