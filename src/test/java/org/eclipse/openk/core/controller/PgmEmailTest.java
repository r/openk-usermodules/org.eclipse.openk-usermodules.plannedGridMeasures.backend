/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.easymock.EasyMock;
import org.eclipse.openk.PlannedGridMeasuresConfiguration;
import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.communication.RestServiceWrapper;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Ignore;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import javax.mail.MessagingException;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertTrue;

public class PgmEmailTest {

    private static class PgmEmailTestable extends PgmEmail {
        public RestServiceWrapper restServiceWrapper;

        public PgmEmailTestable( PlgmProcessSubject model ) throws MessagingException {
            super( model, "", false);
        }

        @Override
        protected void init() {}

    }

    private String getModulConfigCorrectly() {
        return "[\n" +
                "  {\n" +
                "    \"name\": \"Betriebstagebuch\",\n" +
                "    \"cols\": 1,\n" +
                "    \"rows\": 1,\n" +
                "    \"color\": \"#ffffff\",\n" +
                "    \"link\": \"http://localhost:8088/elogbookFE\",\n" +
                "    \"pictureLink\": \"https://www.openkonsequenz.de/medien/cache/main_image/m_logbuch_2443636.jpg\",\n" +
                "    \"requiredRole\": \"elogbook-access\"\n" +
                "\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\": \"Bereitschaftsplanung\",\n" +
                "    \"cols\": 1,\n" +
                "    \"rows\": 1,\n" +
                "    \"color\": \"#ffffff\",\n" +
                "    \"link\": \"https://www.openkonsequenz.de/anwender/11-geplante-projekte/94-bereitschaftsplan\",\n" +
                "    \"pictureLink\": \"https://www.openkonsequenz.de/medien/cache/main_image/l_bereitschaftsplan_57882047.jpg\",\n" +
                "    \"requiredRole\": \"planning-access\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\": \"Einspeisemanagement\",\n" +
                "    \"cols\": 1,\n" +
                "    \"rows\": 1,\n" +
                "    \"color\": \"#ffffff\",\n" +
                "    \"link\": \"https://www.openkonsequenz.de/anwender/11-geplante-projekte/20-eisman\",\n" +
                "    \"pictureLink\": \"https://www.openkonsequenz.de/medien/cache/main_image/l_ok_module6.jpg\",\n" +
                "    \"requiredRole\": \"feedin-management-access\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\": \"Geplante Maßnahmen\",\n" +
                "    \"cols\": 1,\n" +
                "    \"rows\": 1,\n" +
                "    \"color\": \"#ffffff\",\n" +
                "    \"link\": \"https://www.openkonsequenz.de/anwender/11-geplante-projekte/64-geplante-netzmassnahme\",\n" +
                "    \"pictureLink\": \"https://www.openkonsequenz.de//components/com_flexicontent/librairies/phpthumb/phpThumb.php?src=/medien/cache/main_image/l_ok_module5.jpg\",\n" +
                "    \"requiredRole\": \"planned-policies-access\"\n" +
                "  }\n" +
                "]";
    }

    private String getModulConfigEmtpy() {
        return "[{}]";
    }

    private RestServiceWrapper getRestWrapperMock(String moduleConfig) throws HttpStatusException {
        RestServiceWrapper wrapper = EasyMock.createMock(RestServiceWrapper.class);
        EasyMock.expect(wrapper.performGetRequest(anyObject(), anyObject())).andReturn(moduleConfig).anyTimes();
        //verify(wrapper);
        replay(wrapper);

        return wrapper;
    }

    @Ignore
    @Test
    public void testGetDirectMeasureLink() throws Exception {
        PlannedGridMeasuresConfiguration.EmailConfiguration emaily = new PlannedGridMeasuresConfiguration.EmailConfiguration();
        emaily.setPort("1000");
        emaily.setSender("Sendros@sandbank.se");
        emaily.setSmtpHost("Horst");

        TestHelper.initDefaultBackendConfig();
        BackendConfig.getInstance().setEmailConfiguration(emaily);
        PlgmProcessSubject model = PlgmProcessSubject.fromGridMeasure(null, "_fd");
        model.setJwt("Bear Haferkamp");
        PgmEmailTestable pgmEmail = new PgmEmailTestable(model);
        pgmEmail.restServiceWrapper = getRestWrapperMock(getModulConfigCorrectly());
        String url = Whitebox.invokeMethod(pgmEmail, "getDirectMeasureLink", (Integer)666);
        assertTrue( url.toUpperCase().contains("HTTP") && url.contains("666") );

        pgmEmail.restServiceWrapper = getRestWrapperMock(getModulConfigEmtpy());
        String urlEmpty = Whitebox.invokeMethod(pgmEmail, "getDirectMeasureLink", (Integer)666);
        assertTrue( urlEmpty.isEmpty() );
    }
}
