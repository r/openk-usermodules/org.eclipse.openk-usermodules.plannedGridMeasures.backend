/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;


import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class TblDocumentsTest {

    @Test
    public void testGettersAndSetters() {

        TblDocuments tblDocuments = new TblDocuments();

        tblDocuments.setId(3);
        assertEquals((Integer)3, tblDocuments.getId());

        tblDocuments.setDocumentName("docname");
        assertEquals("docname", tblDocuments.getDocumentName());

        tblDocuments.setDocument(new byte[64]);
        assertEquals(64, tblDocuments.getDocument().length);

        java.time.LocalDateTime ldtCreated = java.time.LocalDateTime.parse("2018-04-20T16:00:00");
        java.sql.Timestamp tsCreated = java.sql.Timestamp.valueOf(ldtCreated);
        tblDocuments.setCreateDate(tsCreated);
        org.junit.Assert.assertEquals(tsCreated, tblDocuments.getCreateDate());

        tblDocuments.setCreateUser("Carl Creator");
        assertEquals("Carl Creator", tblDocuments.getCreateUser());

        java.time.LocalDateTime ldtModified = java.time.LocalDateTime.parse("2018-04-21T17:00:00");
        java.sql.Timestamp tsModified = java.sql.Timestamp.valueOf(ldtModified);
        tblDocuments.setModDate(tsModified);
        org.junit.Assert.assertEquals(tsModified, tblDocuments.getModDate());

        tblDocuments.setModUser("Max Modifier");
        assertEquals("Max Modifier", tblDocuments.getModUser());

    }
}
