/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;


import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TblLockTest {

    @Test
    public void testGettersAndSetters() {

        TblLock tblLock = new TblLock();

        tblLock.setId(3);
        assertEquals((Integer)3, tblLock.getId());

        tblLock.setKey(2);
        assertEquals((Integer)2, tblLock.getKey());

        tblLock.setUsername("Tommy Tester");
        assertEquals("Tommy Tester", tblLock.getUsername());

        tblLock.setInfo("Information");
        assertEquals("Information", tblLock.getInfo());

        java.time.LocalDateTime ldtCreated = java.time.LocalDateTime.parse("2018-04-20T16:00:00");
        java.sql.Timestamp tsCreated = java.sql.Timestamp.valueOf(ldtCreated);
        tblLock.setCreateDate(tsCreated);
        org.junit.Assert.assertEquals(tsCreated, tblLock.getCreateDate());

        tblLock.setCreateUser("Carl Creator");
        assertEquals("Carl Creator", tblLock.getCreateUser());

        java.time.LocalDateTime ldtModified = java.time.LocalDateTime.parse("2018-04-21T17:00:00");
        java.sql.Timestamp tsModified = java.sql.Timestamp.valueOf(ldtModified);
        tblLock.setModDate(tsModified);
        org.junit.Assert.assertEquals(tsModified, tblLock.getModDate());

        tblLock.setModUser("Max Modifier");
        assertEquals("Max Modifier", tblLock.getModUser());

    }
}
