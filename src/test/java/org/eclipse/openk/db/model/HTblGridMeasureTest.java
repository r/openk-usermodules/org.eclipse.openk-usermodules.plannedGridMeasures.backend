/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class HTblGridMeasureTest {



    @Test
    public void testGettersAndSetters() {

        HTblGridMeasure hTblGridMeasure = new HTblGridMeasure();

        hTblGridMeasure.setHId(1);
        org.junit.Assert.assertEquals((Integer)1, hTblGridMeasure.getHId());

        hTblGridMeasure.sethAction(1);
        org.junit.Assert.assertEquals((Integer)1, hTblGridMeasure.gethAction());

        hTblGridMeasure.sethUser("user");
        org.junit.Assert.assertEquals("user", hTblGridMeasure.gethUser());

        java.time.LocalDateTime ldthdate = java.time.LocalDateTime.parse("2018-04-22T18:00:00");
        java.sql.Timestamp hdate = java.sql.Timestamp.valueOf(ldthdate);
        hTblGridMeasure.sethDate(hdate);
        org.junit.Assert.assertEquals(hdate, hTblGridMeasure.gethDate());

        hTblGridMeasure.setId(1);
        org.junit.Assert.assertEquals((Integer)1, hTblGridMeasure.getId());

        hTblGridMeasure.setIdDescriptive("20");
        assertEquals("20", hTblGridMeasure.getIdDescriptive());

        hTblGridMeasure.setTitle("Title");
        assertEquals("Title", hTblGridMeasure.getTitle());

        hTblGridMeasure.setAffectedResource("Affected resource");
        assertEquals("Affected resource", hTblGridMeasure.getAffectedResource());

        hTblGridMeasure.setRemark("Remark");
        assertEquals("Remark", hTblGridMeasure.getRemark());

        hTblGridMeasure.setFkRefGmStatus(8);
        assertEquals((Integer)8, hTblGridMeasure.getFkRefGmStatus());

        hTblGridMeasure.setSwitchingObject("Transformator");
        assertEquals("Transformator", hTblGridMeasure.getSwitchingObject());

        hTblGridMeasure.setCostCenter("costa punta");
        assertEquals("costa punta", hTblGridMeasure.getCostCenter());

        hTblGridMeasure.setApprovalBy("Abel Approver");
        assertEquals("Abel Approver", hTblGridMeasure.getApprovalBy());

        hTblGridMeasure.setAreaOfSwitching("Northwest");
        assertEquals("Northwest", hTblGridMeasure.getAreaOfSwitching());

        hTblGridMeasure.setAppointmentRepetition("weekly");
        assertEquals("weekly", hTblGridMeasure.getAppointmentRepetition());

        java.time.LocalDateTime ldtAppointmentStartdate = java.time.LocalDateTime.parse("2018-04-22T18:00:00");
        java.sql.Timestamp tsAppointmentStartdate = java.sql.Timestamp.valueOf(ldtAppointmentStartdate);
        hTblGridMeasure.setAppointmentStartdate(tsAppointmentStartdate);
        org.junit.Assert.assertEquals(tsAppointmentStartdate, hTblGridMeasure.getAppointmentStartdate());

        hTblGridMeasure.setAppointmentNumberOf(5);
        assertEquals((Integer)5, hTblGridMeasure.getAppointmentNumberOf());

        java.time.LocalDateTime ldtPlannedStarttimeFirstSinglemeasure = java.time.LocalDateTime.parse("2018-04-24T20:00:00");
        java.sql.Timestamp tsPlannedStarttimeFirstSinglemeasure = java.sql.Timestamp.valueOf(ldtPlannedStarttimeFirstSinglemeasure);
        hTblGridMeasure.setPlannedStarttimeFirstSinglemeasure(tsPlannedStarttimeFirstSinglemeasure);
        org.junit.Assert.assertEquals(tsPlannedStarttimeFirstSinglemeasure, hTblGridMeasure.getPlannedStarttimeFirstSinglemeasure());

        java.time.LocalDateTime ldtEndtimeGridmeasure = java.time.LocalDateTime.parse("2018-04-30T03:00:00");
        java.sql.Timestamp tsEndtimeGridmeasure = java.sql.Timestamp.valueOf(ldtEndtimeGridmeasure);
        hTblGridMeasure.setEndtimeGridmeasure(tsEndtimeGridmeasure);
        org.junit.Assert.assertEquals(tsEndtimeGridmeasure, hTblGridMeasure.getEndtimeGridmeasure());

        String tsTimeOfReallocation = "2018-04-19T08:00:00";
        hTblGridMeasure.setTimeOfReallocation(tsTimeOfReallocation);
        org.junit.Assert.assertEquals(tsTimeOfReallocation, hTblGridMeasure.getTimeOfReallocation());

        hTblGridMeasure.setDescription("This is a description");
        assertEquals("This is a description", hTblGridMeasure.getDescription());

        hTblGridMeasure.setFkRefBranch(3);
        assertEquals((Integer)3, hTblGridMeasure.getFkRefBranch());

        hTblGridMeasure.setFkRefBranchLevel(2);
        assertEquals((Integer)2, hTblGridMeasure.getFkRefBranchLevel());

        hTblGridMeasure.setCreateUser("Create User");
        assertEquals("Create User", hTblGridMeasure.getCreateUser());

        hTblGridMeasure.setCreateUserDepartment("Create User Department");
        assertEquals("Create User Department", hTblGridMeasure.getCreateUserDepartment());

        java.time.LocalDateTime ldtCreate = java.time.LocalDateTime.parse("2018-03-20T15:00:00");
        java.sql.Timestamp tsCreate = java.sql.Timestamp.valueOf(ldtCreate);

        hTblGridMeasure.setCreateDate(tsCreate);
        org.junit.Assert.assertEquals(tsCreate, hTblGridMeasure.getCreateDate());

        hTblGridMeasure.setModUser("Modifiing User");
        assertEquals("Modifiing User", hTblGridMeasure.getModUser());

        hTblGridMeasure.setModUserDepartment("Modifiing User Department");
        assertEquals("Modifiing User Department", hTblGridMeasure.getModUserDepartment());

        java.time.LocalDateTime ldtMod = java.time.LocalDateTime.parse("2018-03-21T16:00:00");
        java.sql.Timestamp tsMod = java.sql.Timestamp.valueOf(ldtMod);

        hTblGridMeasure.setModDate(tsMod);
        org.junit.Assert.assertEquals(tsMod, hTblGridMeasure.getModDate());
    }


}
