/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;


import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class RefCostCenterTest {

    @Test
    public void testGettersAndSetters() {

        RefCostCenter refCostCenter = new RefCostCenter();

        refCostCenter.setId(1);
        assertEquals((Integer)1, refCostCenter.getId());

        refCostCenter.setName("CostCenterName");
        assertEquals("CostCenterName", refCostCenter.getName());

        refCostCenter.setDescription("Cost Center Description");
        assertEquals("Cost Center Description", refCostCenter.getDescription());

        java.time.LocalDateTime ldtCreated = java.time.LocalDateTime.parse("2018-04-16T16:00:00");
        java.sql.Timestamp tsCreated = java.sql.Timestamp.valueOf(ldtCreated);
        refCostCenter.setCreateDate(tsCreated);
        org.junit.Assert.assertEquals(tsCreated, refCostCenter.getCreateDate());

        refCostCenter.setCreateUser("Carlina Creator");
        assertEquals("Carlina Creator", refCostCenter.getCreateUser());

        java.time.LocalDateTime ldtModified = java.time.LocalDateTime.parse("2018-04-16T17:00:00");
        java.sql.Timestamp tsModified = java.sql.Timestamp.valueOf(ldtModified);
        refCostCenter.setModDate(tsModified);
        org.junit.Assert.assertEquals(tsModified, refCostCenter.getModDate());

        refCostCenter.setModUser("Mirko Modifier");
        assertEquals("Mirko Modifier", refCostCenter.getModUser());

    }
}
