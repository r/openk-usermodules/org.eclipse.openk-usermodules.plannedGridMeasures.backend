/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RefUserSettingsTest {

    @Test
    public void testGettersAndSetters() {

        TblUserSettings tblUserSettings = new TblUserSettings();

        tblUserSettings.setId(4);
        assertEquals((Integer)4, tblUserSettings.getId());

        tblUserSettings.setUsername("Username");
        assertEquals("Username", tblUserSettings.getUsername());

        tblUserSettings.setSettingType("xyz");
        assertEquals("xyz", tblUserSettings.getSettingType());

        tblUserSettings.setValue("{\n" +
                "  \"aliceblue\": \"#f0f8ff\",\n" +
                "  \"antiquewhite\": \"#faebd7\",\n" +
                "  \"aqua\": \"#00ffff\",\n" +
                "  \"aquamarine\": \"#7fffd4\",\n" +
                "  \"azure\": \"#f0ffff\",\n" +
                "  \"beige\": \"#f5f5dc\",\n" +
                "  \"bisque\": \"#ffe4c4\",\n" +
                "  \"black\": \"#000000\",\n" +
                "  \"blanchedalmond\": \"#ffebcd\",\n" +
                "  \"blue\": \"#0000ff\",\n" +
                "  \"blueviolet\": \"#8a2be2\",\n" +
                "  \"brown\": \"#a52a2a\",\n" +
                "}\n");

        assertEquals("{\n" +
                "  \"aliceblue\": \"#f0f8ff\",\n" +
                "  \"antiquewhite\": \"#faebd7\",\n" +
                "  \"aqua\": \"#00ffff\",\n" +
                "  \"aquamarine\": \"#7fffd4\",\n" +
                "  \"azure\": \"#f0ffff\",\n" +
                "  \"beige\": \"#f5f5dc\",\n" +
                "  \"bisque\": \"#ffe4c4\",\n" +
                "  \"black\": \"#000000\",\n" +
                "  \"blanchedalmond\": \"#ffebcd\",\n" +
                "  \"blue\": \"#0000ff\",\n" +
                "  \"blueviolet\": \"#8a2be2\",\n" +
                "  \"brown\": \"#a52a2a\",\n" +
                "}\n", tblUserSettings.getValue());

        java.time.LocalDateTime ldtCreated = java.time.LocalDateTime.parse("2018-04-24T06:00:00");
        java.sql.Timestamp tsCreated = java.sql.Timestamp.valueOf(ldtCreated);
        tblUserSettings.setCreateDate(tsCreated);
        org.junit.Assert.assertEquals(tsCreated, tblUserSettings.getCreateDate());

        tblUserSettings.setCreateUser("Coco Creator");
        assertEquals("Coco Creator", tblUserSettings.getCreateUser());

        java.time.LocalDateTime ldtModified = java.time.LocalDateTime.parse("2018-04-21T07:00:00");
        java.sql.Timestamp tsModified = java.sql.Timestamp.valueOf(ldtModified);
        tblUserSettings.setModDate(tsModified);
        org.junit.Assert.assertEquals(tsModified, tblUserSettings.getModDate());

        tblUserSettings.setModUser("Minnie Modifier");
        assertEquals("Minnie Modifier", tblUserSettings.getModUser());

    }
}
