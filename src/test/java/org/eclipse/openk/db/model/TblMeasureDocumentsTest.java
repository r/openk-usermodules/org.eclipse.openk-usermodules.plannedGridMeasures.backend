/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;


import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class TblMeasureDocumentsTest {

    @Test
    public void testGettersAndSetters() {

        TblMeasureDocuments tblMeasureDocuments = new TblMeasureDocuments();
        TblDocuments tblDocuments = new TblDocuments();
        tblDocuments.setId(7);

        tblMeasureDocuments.setId(3);
        assertEquals((Integer)3, tblMeasureDocuments.getId());

        tblMeasureDocuments.setFkTblMeasure(5);
        assertEquals((Integer)5, tblMeasureDocuments.getFkTblMeasure());

        tblMeasureDocuments.setTblDocuments(tblDocuments);
        assertEquals((Integer)7, tblMeasureDocuments.getTblDocuments().getId());

        java.time.LocalDateTime ldtCreated = java.time.LocalDateTime.parse("2018-04-20T16:00:00");
        java.sql.Timestamp tsCreated = java.sql.Timestamp.valueOf(ldtCreated);
        tblMeasureDocuments.setCreateDate(tsCreated);
        org.junit.Assert.assertEquals(tsCreated, tblMeasureDocuments.getCreateDate());

        tblMeasureDocuments.setCreateUser("Carl Creator");
        assertEquals("Carl Creator", tblMeasureDocuments.getCreateUser());

        java.time.LocalDateTime ldtModified = java.time.LocalDateTime.parse("2018-04-21T17:00:00");
        java.sql.Timestamp tsModified = java.sql.Timestamp.valueOf(ldtModified);
        tblMeasureDocuments.setModDate(tsModified);
        org.junit.Assert.assertEquals(tsModified, tblMeasureDocuments.getModDate());

        tblMeasureDocuments.setModUser("Max Modifier");
        assertEquals("Max Modifier", tblMeasureDocuments.getModUser());

    }
}
