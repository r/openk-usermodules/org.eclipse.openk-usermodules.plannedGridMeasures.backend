/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class RefVersionTest {

    @Test
    public void testGettersAndSetters() {

        RefVersion refVersion = new RefVersion();

        refVersion.setId(1);
        assertEquals((Integer)1, refVersion.getId());

        refVersion.setVersion("version54");
        assertEquals("version54", refVersion.getVersion());

    }
}
