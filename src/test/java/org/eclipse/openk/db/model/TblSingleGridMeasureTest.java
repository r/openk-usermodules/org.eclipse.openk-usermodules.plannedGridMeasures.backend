/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TblSingleGridMeasureTest {



    @Test
    public void testGettersAndSetters() {

        TblSingleGridmeasure tblSingleGridmeasure = new TblSingleGridmeasure();

        tblSingleGridmeasure.setId(1);
        org.junit.Assert.assertEquals((Integer)1, tblSingleGridmeasure.getId());

        tblSingleGridmeasure.setSortorder(2);
        assertEquals((Integer)2, tblSingleGridmeasure.getSortorder());

        tblSingleGridmeasure.setTitle("title");
        assertEquals("title", tblSingleGridmeasure.getTitle());

        tblSingleGridmeasure.setSwitchingObject("tool");
        assertEquals("tool", tblSingleGridmeasure.getSwitchingObject());

        tblSingleGridmeasure.setCimId("cim id");
        assertEquals("cim id", tblSingleGridmeasure.getCimId());

        tblSingleGridmeasure.setCimName("cim");
        assertEquals("cim", tblSingleGridmeasure.getCimName());

        tblSingleGridmeasure.setCimDescription("cim");
        assertEquals("cim", tblSingleGridmeasure.getCimDescription());

        tblSingleGridmeasure.setDescription("des");
        assertEquals("des", tblSingleGridmeasure.getDescription());

        tblSingleGridmeasure.setResponsibleOnSiteName("Rick Responsible");
        assertEquals("Rick Responsible", tblSingleGridmeasure.getResponsibleOnSiteName());

        tblSingleGridmeasure.setResponsibleOnSiteDepartment("Department ZZ");
        assertEquals("Department ZZ", tblSingleGridmeasure.getResponsibleOnSiteDepartment());

        tblSingleGridmeasure.setNetworkControl("Adam");
        assertEquals("Adam", tblSingleGridmeasure.getNetworkControl());

        tblSingleGridmeasure.setFkTblGridmeasure(4);
        assertEquals((Integer)4, tblSingleGridmeasure.getFkTblGridmeasure());


        java.time.LocalDateTime ldtStarttimeSinglemeasure = java.time.LocalDateTime.parse("2018-04-28T01:00:00");
        java.sql.Timestamp tsStarttimeSinglemeasure = java.sql.Timestamp.valueOf(ldtStarttimeSinglemeasure);
        tblSingleGridmeasure.setPlannedStarttimeSinglemeasure(tsStarttimeSinglemeasure);
        org.junit.Assert.assertEquals(tsStarttimeSinglemeasure, tblSingleGridmeasure.getPlannedStarttimeSinglemeasure());

        java.time.LocalDateTime ldtEndtimeSinglemeasure = java.time.LocalDateTime.parse("2018-04-29T02:00:00");
        java.sql.Timestamp tsEndtimeSinglemeasure = java.sql.Timestamp.valueOf(ldtEndtimeSinglemeasure);
        tblSingleGridmeasure.setPlannedEndtimeSinglemeasure(tsEndtimeSinglemeasure);
        org.junit.Assert.assertEquals(tsEndtimeSinglemeasure, tblSingleGridmeasure.getPlannedEndtimeSinglemeasure());



        tblSingleGridmeasure.setCreateUser("Create User");
        assertEquals("Create User", tblSingleGridmeasure.getCreateUser());

        java.time.LocalDateTime ldtCreate = java.time.LocalDateTime.parse("2018-03-20T15:00:00");
        java.sql.Timestamp tsCreate = java.sql.Timestamp.valueOf(ldtCreate);

        tblSingleGridmeasure.setCreateDate(tsCreate);
        org.junit.Assert.assertEquals(tsCreate, tblSingleGridmeasure.getCreateDate());

        tblSingleGridmeasure.setModUser("Modifiing User");
        assertEquals("Modifiing User", tblSingleGridmeasure.getModUser());

        java.time.LocalDateTime ldtMod = java.time.LocalDateTime.parse("2018-03-21T16:00:00");
        java.sql.Timestamp tsMod = java.sql.Timestamp.valueOf(ldtMod);

        tblSingleGridmeasure.setModDate(tsMod);
        org.junit.Assert.assertEquals(tsMod, tblSingleGridmeasure.getModDate());
    }


}
