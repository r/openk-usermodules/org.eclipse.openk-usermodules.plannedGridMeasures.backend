/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;


import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class RefBranchTest {

    @Test
    public void testGettersAndSetters() {

        RefBranch refBranch = new RefBranch();

        refBranch.setId(3);
        assertEquals((Integer)3, refBranch.getId());

        refBranch.setName("Branchname");
        assertEquals("Branchname", refBranch.getName());

        refBranch.setDescription("Description");
        assertEquals("Description", refBranch.getDescription());

        refBranch.setColorCode("ColorCode");
        assertEquals("ColorCode", refBranch.getColorCode());

        java.time.LocalDateTime ldtCreated = java.time.LocalDateTime.parse("2018-04-20T16:00:00");
        java.sql.Timestamp tsCreated = java.sql.Timestamp.valueOf(ldtCreated);
        refBranch.setCreateDate(tsCreated);
        org.junit.Assert.assertEquals(tsCreated, refBranch.getCreateDate());

        refBranch.setCreateUser("Carl Creator");
        assertEquals("Carl Creator", refBranch.getCreateUser());

        java.time.LocalDateTime ldtModified = java.time.LocalDateTime.parse("2018-04-21T17:00:00");
        java.sql.Timestamp tsModified = java.sql.Timestamp.valueOf(ldtModified);
        refBranch.setModDate(tsModified);
        org.junit.Assert.assertEquals(tsModified, refBranch.getModDate());

        refBranch.setModUser("Max Modifier");
        assertEquals("Max Modifier", refBranch.getModUser());

    }
}
