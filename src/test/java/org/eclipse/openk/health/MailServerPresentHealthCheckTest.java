/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.health;

import com.codahale.metrics.health.HealthCheck;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import org.eclipse.openk.PlannedGridMeasuresConfiguration;
import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.api.mail.EmailTemplatePaths;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.controller.EmailmanagerTest;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;

public class MailServerPresentHealthCheckTest {

    private static GreenMail mailServer;
    String templatePath = "emailConfigurationTest/emailTemplates/closedEmailTemplateTest.txt";

    class MockHC extends MailServerPresentHealthCheck {

    }

    @BeforeClass
    public static void beforeAll() throws IOException {
        ServerSetup setup = new ServerSetup(EmailmanagerTest.port() , "localhost", ServerSetup.PROTOCOL_SMTP);
        setup.setServerStartupTimeout(3000);
        mailServer = new GreenMail(setup);
    }

    @Before
    public void prepareTests() throws Exception {
        TestHelper.initDefaultBackendConfig();
        // mock email-configuration
        PlannedGridMeasuresConfiguration.EmailConfiguration emailConfiguration = createEmailConfiguration();

        setTestTemplateEmail(templatePath);
        BackendConfig.getInstance().setEmailConfiguration(emailConfiguration);
        mailServer.start();

    }

    @After
    public void afterTest() {
        mailServer.stop();
    }

    @Test
    public void checkTest() throws Exception{
        MockHC mailSPHC = new MockHC();

        HealthCheck.Result rlt = mailSPHC.check();
        assertEquals(rlt.isHealthy(), true);
    }

    private void setTestTemplateEmail(String templatePath) {
        EmailTemplatePaths templatePaths = new EmailTemplatePaths();
        templatePaths.setClosedEmailTemplate(templatePath);
        BackendConfig.getInstance().setEmailTemplatePaths(templatePaths);
    }

    private PlannedGridMeasuresConfiguration.EmailConfiguration createEmailConfiguration() throws IOException {
        PlannedGridMeasuresConfiguration.EmailConfiguration emailConfiguration = new PlannedGridMeasuresConfiguration.EmailConfiguration();
        emailConfiguration.setPort(EmailmanagerTest.port()+"");
        emailConfiguration.setSmtpHost("localhost");
        emailConfiguration.setSender("testCaseSender@test.de");
        return emailConfiguration;
    }
}
