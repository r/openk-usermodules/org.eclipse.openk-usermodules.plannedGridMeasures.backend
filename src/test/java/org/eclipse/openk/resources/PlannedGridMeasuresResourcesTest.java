/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.resources;

import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.controller.BaseWebService;
import org.eclipse.openk.core.controller.TokenManager;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

public class PlannedGridMeasuresResourcesTest {

    private String payloadNormalUser = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiIwZGUyNTMyYi1mNTBkLTQxZTUtODQwYy1iNzZkOTYyZGM3MDYiLCJleHAiOjE1MjE2NDA0MTIsIm5iZiI6MCwiaWF0IjoxNTIxNjQwMTEyLCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImMyZTlkN2FlLTJiZmEtNDU3OC1iMDllLWY1ZGM1ZjA5YTg3OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJkOTE1MjY2MS03NTJlLTRiNmMtOWRiNi0wYjFmYzY3YTc4ZTciLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLW5vcm1hbHVzZXIiLCJ1bWFfYXV0aG9yaXphdGlvbiIsInBsYW5uZWQtcG9saWNpZXMtbm9ybWFsdXNlciJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInJvbGVzIjoiW3BsYW5uZWQtcG9saWNpZXMtbm9ybWFsdXNlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBlbG9nYm9vay1hY2Nlc3MsIGVsb2dib29rLW5vcm1hbHVzZXJdIiwibmFtZSI6Ik90dG8gTm9ybWFsdmVyYnJhdWNoZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJvdHRvIiwiZ2l2ZW5fbmFtZSI6Ik90dG8iLCJmYW1pbHlfbmFtZSI6Ik5vcm1hbHZlcmJyYXVjaGVyIn0.F-CcNatXaE0W4PhBNaSGUwddE_S05HqL6QuJLOWQo_n0_LdWGRw8iyF6RIbK8y06kH3gb79c6t1U0njWqE68pL9FQfP2t-_en-XlCwKzBdDXtq8e8xHyZ00P5zlE80du0A74qzh2g0HrE9fCgEaawk2_M6JxNFrToiwl6PGRS9RNRD_xgKVXn0XqG-I_qRKA-GdBTcLPjPD9bn4fAdlf3HWdVPUNclRI2ttUykFmpmRTAHKwI3cZOBrIMn4dasMn9k1xPP5IAGtCcgs6TF0YlnWvZgIhdT_5O4rmMtzZXjeuYsjpNnqz0LGA0GT6up90PZrTJF8rHtWU50wVgJSH9g";
    GridMeasure gm = new GridMeasure();

    @Before
    public void createGridMeasure() {
        TestHelper.initDefaultBackendConfig();
        TokenManager.getInstance();

        gm.setId(4);
        gm.setTitle("title");
        gm.setAffectedResource("ar");
        gm.setStatusId(3);
        gm.setDescriptiveId("4");
        gm.setCreateUser("rambo");
        gm.setCreateUserDepartment("oben");
        gm.setRemark("a remark");
    }

    @Test
    public void testGetVersionInfo() {
        Response r = new PlannedGridMeasuresResource().getVersionInfo();
        assertNotNull( r );
    }

    @Test
    public void testGetGridMeasures() {
        Response r = new PlannedGridMeasuresResource().getGridMeasures("token", "");
        assertNotNull( r );
    }

    @Test
    public void testStoreGridMeasures() {
        Response r = new PlannedGridMeasuresResource().storeGridMeasure("token", "");
        assertNotNull( r );
    }

    @Test
    public void testGetGridMeasureById() {
        Response r = new PlannedGridMeasuresResource().getGridMeasure("token", "4");
        assertNotNull( r );
    }

    @Test
    public void testGetHistoricalStatusChangesById() {
        Response r = new PlannedGridMeasuresResource().getHistoricalStatusChanges("token", "4");
        assertNotNull( r );
    }

    @Test
    public void testGetCurrentReminders() {
        Response r = new PlannedGridMeasuresResource().getCurrentReminders("token");
        assertNotNull( r );
    }

    @Test
    public void testGetExpiredReminders() {
        Response r = new PlannedGridMeasuresResource().getExpiredReminders("token");
        assertNotNull( r );
    }

    @Test
    public void testUploadGridMeasureAttachments() {
        Response r = new PlannedGridMeasuresResource().uploadGridMeasureAttachments("token", "1", "{\n" +
                "    \"id\": 7}");
        assertNotNull( r );
    }

    @Test
    public void testGetGridMeasureAttachments() {
        Response r = new PlannedGridMeasuresResource().getGridMeasureAttachments("1", "token");
        assertNotNull( r );
    }

    @Test
    public void testDownloadGridMeasureAttachments() {
        Response r = new PlannedGridMeasuresResource().downloadGridMeasureAttachment("1", "token");
        assertNotNull( r );
    }

    @Test
    public void testDeleteLock() {
        Response r = new PlannedGridMeasuresResource().deleteLock("1", "GridMeasure", "token", Globals.FORCE_DELETE_LOCK);
        assertNotNull( r );
    }

    @Test
    public void testCreateLock() {
        Response r = new PlannedGridMeasuresResource().createLock("token", "{}");
        assertNotNull( r );
    }

    @Test
    public void testCheckLock() {
        Response r = new PlannedGridMeasuresResource().checkLock("1", "test", "token");
        assertNotNull( r );
    }

    @Test
    public void deleteGridMeasureAttechment() {
        Response r = new PlannedGridMeasuresResource().deleteGridMeasureAttachment("1", "token");
        assertNotNull( r );
    }

    @Test
    public void testGetRoleAccessDefinition() {
        Response r = new PlannedGridMeasuresResource().getRoleAccessDefinition("token");
        assertNotNull( r );
    }

    @Test
    public void testLogout() {
        Response r = new PlannedGridMeasuresResource().logout(null);
        assertNotNull( r );
    }

    @Test
    public void testLET_ME_IN() throws HttpStatusException {
        new PlannedGridMeasuresResource().assertAndRefreshToken("LET_ME_IN", BaseWebService.SecureType.NORMAL);
        assertEquals("default_backdoor_user", new PlannedGridMeasuresResource().getUserFromToken("LET_ME_IN"));
    }

    @Test
    public void testGetUserFromTokenNoramlToken() throws HttpStatusException {
        assertEquals("otto", new PlannedGridMeasuresResource().getUserFromToken(payloadNormalUser));
    }

    @Test(expected = HttpStatusException.class)
    public void testAssertAndRefreshTokenNoramlToken() throws HttpStatusException {
        new PlannedGridMeasuresResource().assertAndRefreshToken(payloadNormalUser, BaseWebService.SecureType.NORMAL);
    }


    @Test
    public void testGetAffectedResources() throws HttpStatusException {
        Response r = new PlannedGridMeasuresResource().getAffectedResources("" );
        assertNotNull( r );
    }

    @Test
    public void testGetMailAddressesFromGridmeasures()  {
        Response r = new PlannedGridMeasuresResource().getMailAddressesFromGridmeasures("LET_ME_IN");
        assertNotNull( r );
    }

    @Test
    public void testGetResponsiblesOnSiteFromGridmeasures()  {
        Response r = new PlannedGridMeasuresResource().getResponsiblesOnSiteFromSingleGridmeasures("LET_ME_IN");
        assertNotNull( r );
    }

    @Test
    public void testGetUserDepartmentsResponsibleOnSite()  {
        Response r = new PlannedGridMeasuresResource().getUserDepartmentsResponsibleOnSite("LET_ME_IN");
        assertNotNull( r );
    }

    @Test
    public void testGetUserDepartmentsCreated()  {
        Response r = new PlannedGridMeasuresResource().getUserDepartmentsCreated("LET_ME_IN");
        assertNotNull( r );
    }

    @Test
    public void testGetUserDepartmentsModified()  {
        Response r = new PlannedGridMeasuresResource().getUserDepartmentsModified("LET_ME_IN");
        assertNotNull( r );
    }

    @Test
    public void testGetNetworkControlsFromSingleGridmeasures()  {
        Response r = new PlannedGridMeasuresResource().getNetworkControlsFromSingleGridmeasures("LET_ME_IN");
        assertNotNull( r );
    }

    @Test
    public void testGetCalender()  {
        Response r = new PlannedGridMeasuresResource().getCalender("LET_ME_IN");
        assertNotNull( r );
    }

}
