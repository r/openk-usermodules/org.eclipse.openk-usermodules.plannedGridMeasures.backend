/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.viewmodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.common.util.ResourceLoaderBase;
import org.eclipse.openk.core.viewmodel.GeneralReturnItem;
import org.junit.Test;

public class GeneralReturnItemTest extends ResourceLoaderBase {
	// IMPORTANT TEST!!!
	// Make sure, our Interface produces a DEFINED Json!
	// Changes in the interface will HOPEFULLY crash here!!!

	@Test
	public void TestStructureAgainstJson() {
		String json = super.loadStringFromResource("testGeneralReturnItem.json");
		GeneralReturnItem gRRet = JsonGeneratorBase.getGson().fromJson(json, GeneralReturnItem.class);
		assertEquals("It works!", gRRet.getRet());
	}

	@Test
	public void TestSetters() {
		GeneralReturnItem gri = new GeneralReturnItem("firstStrike");
		assertTrue("firstStrike".equals(gri.getRet()));
		gri.setRet("Retour");
		assertTrue("Retour".equals(gri.getRet()));

	}

}
