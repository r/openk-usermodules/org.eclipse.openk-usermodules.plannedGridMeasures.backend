/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.viewmodel;

import static org.eclipse.openk.common.JsonGeneratorBase.getGson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.eclipse.openk.common.util.ResourceLoaderBase;
import org.eclipse.openk.core.viewmodel.ErrorReturn;
import org.junit.Test;

public class ErrorReturnTest extends ResourceLoaderBase {

    @Test
    public void TestStructureAgainstJson() {
        String json = super.loadStringFromResource("testErrorReturn.json");
        ErrorReturn errRet = getGson().fromJson(json, ErrorReturn.class);
        assertFalse(errRet.getErrorText().isEmpty());
        assertEquals(999, errRet.getErrorCode());
    }

    @Test
    public void TestSetters() {
        ErrorReturn errRet = new ErrorReturn();
        errRet.setErrorCode(1);
        errRet.setErrorText("bla bla");
    }

}
