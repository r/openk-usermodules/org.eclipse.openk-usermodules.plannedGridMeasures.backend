/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Test;

public class BranchTest {

    @Test
    public void testGetId()throws IOException {
        Branch branch = new Branch();
        branch.setId(5);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(branch);

        Branch branch2 = om.readValue(jsonString, Branch.class);

        assertEquals(branch.getId(), branch2.getId());
    }

    @Test
    public void testGetName()throws IOException {
        Branch branch = new Branch();
        branch.setName("Branchname");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(branch);

        Branch branch2 = om.readValue(jsonString, Branch.class);

        assertEquals(branch.getName(), branch2.getName());
    }

    @Test
    public void testGetDescription()throws IOException {
        Branch branch = new Branch();
        branch.setDescription("Description");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(branch);

        Branch branch2 = om.readValue(jsonString, Branch.class);

        assertEquals(branch.getDescription(), branch2.getDescription());
    }

    @Test
    public void testGetColorCode()throws IOException {
        Branch branch = new Branch();
        branch.setColorCode("ColorCode");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(branch);

        Branch branch2 = om.readValue(jsonString, Branch.class);

        assertEquals(branch.getColorCode(), branch2.getColorCode());
    }
}
