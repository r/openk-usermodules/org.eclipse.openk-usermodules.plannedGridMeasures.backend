/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Test;

public class BranchLevelTest {

    @Test
    public void testGetId()throws IOException {
        BranchLevel branchLevel = new BranchLevel();
        branchLevel.setId(7);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(branchLevel);

        BranchLevel branchLevel2 = om.readValue(jsonString, BranchLevel.class);

        assertEquals(branchLevel.getId(), branchLevel2.getId());
    }

    @Test
    public void testGetName()throws IOException {
        BranchLevel branchLevel = new BranchLevel();
        branchLevel.setName("BranchLevelName");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(branchLevel);

        BranchLevel branchLevel2 = om.readValue(jsonString, BranchLevel.class);

        assertEquals(branchLevel.getName(), branchLevel2.getName());
    }

    @Test
    public void testGetDescription()throws IOException {
        BranchLevel branchLevel = new BranchLevel();
        branchLevel.setDescription("Branch Level Description");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(branchLevel);

        BranchLevel branchLevel2 = om.readValue(jsonString, BranchLevel.class);

        assertEquals(branchLevel.getDescription(), branchLevel2.getDescription());
    }

    @Test
    public void testGetBranchId()throws IOException {
        BranchLevel branchLevel = new BranchLevel();
        branchLevel.setBranchId(5);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(branchLevel);

        BranchLevel branchLevel2 = om.readValue(jsonString, BranchLevel.class);

        assertEquals(branchLevel.getBranchId(), branchLevel2.getBranchId());
    }
}
