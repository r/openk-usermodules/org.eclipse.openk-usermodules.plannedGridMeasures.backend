/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;
import org.junit.Test;

public class GridmeasureTest {

    @Test
    public void testGetDescriptiveId()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setDescriptiveId("4");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getDescriptiveId(), gm2.getDescriptiveId());
    }

    @Test
    public void testGetTitle()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setTitle("Test");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getTitle(), gm2.getTitle());
    }

    @Test
    public void testGetAffected_resource()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setAffectedResource("Test");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getAffectedResource(), gm2.getAffectedResource());
    }

    @Test
    public void testGetRemark()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setAffectedResource("Test");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getRemark(), gm2.getRemark());
    }

    @Test
    public void testGetEmail_a()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setAffectedResource("Test");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getRemark(), gm2.getRemark());
    }

    @Test
    public void testGetStatusId()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setStatusId(3);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getStatusId(), gm2.getStatusId());
    }

    @Test
    public void testGetSwitchingObject()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setSwitchingObject("Transformator");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getSwitchingObject(), gm2.getSwitchingObject());
    }

    @Test
    public void testGetCostCenter()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setCostCenter("Centre di costa");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getCostCenter(), gm2.getCostCenter());
    }

    @Test
    public void testGetApprovalBy()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setApprovalBy("Adam Approver");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getApprovalBy(), gm2.getApprovalBy());
    }

    @Test
    public void testGetAreaOfSwitching()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setAreaOfSwitching("Southeast");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getAreaOfSwitching(), gm2.getAreaOfSwitching());
    }

    @Test
    public void testGetAppointmentRepetition()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setAppointmentRepetition("daily");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getAppointmentRepetition(), gm2.getAppointmentRepetition());
    }

    @Test
    public void testGetAppointmentStartdate()throws IOException {
        GridMeasure gm = new GridMeasure();

        java.util.Date dateAppointmentStartdate = new Date(System.currentTimeMillis());
        gm.setAppointmentStartdate(dateAppointmentStartdate);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getAppointmentStartdate(), gm2.getAppointmentStartdate());
    }

    @Test
    public void testGetAppointmentNumberOf()throws IOException {
        GridMeasure gm = new GridMeasure();

        gm.setAppointmentNumberOf(5);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getAppointmentNumberOf(), gm2.getAppointmentNumberOf());
    }

    @Test
    public void testGetPlannedStarttimeFirstSinglemeasure()throws IOException {
        GridMeasure gm = new GridMeasure();

        java.util.Date datePlannedStarttimeFirstSinglemeasure = new Date(System.currentTimeMillis());
        gm.setPlannedStarttimeFirstSinglemeasure(datePlannedStarttimeFirstSinglemeasure);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getPlannedStarttimeFirstSinglemeasure(), gm2.getPlannedStarttimeFirstSinglemeasure());
    }

    @Test
    public void testGetEndtimeGridmeasure()throws IOException {
        GridMeasure gm = new GridMeasure();

        java.util.Date dateEndtimeGridmeasure = new Date(System.currentTimeMillis());
        gm.setEndtimeGridmeasure(dateEndtimeGridmeasure);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getEndtimeGridmeasure(), gm2.getEndtimeGridmeasure());
    }

    @Test
    public void testGetTimeOfReallocation()throws IOException {
        GridMeasure gm = new GridMeasure();

        String dateTimeOfReallocation = "2018-04-19T08:00:00";
        gm.setTimeOfReallocation(dateTimeOfReallocation);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getTimeOfReallocation(), gm2.getTimeOfReallocation());
    }

    @Test
    public void testGetDescription()throws IOException {
        GridMeasure gm = new GridMeasure();

        gm.setDescription("This is a description");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getDescription(), gm2.getDescription());
    }


    @Test
    public void testGetBranch()throws IOException {
        GridMeasure gm = new GridMeasure();

        gm.setBranchId(2);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getBranchId(), gm2.getBranchId());
    }

    @Test
    public void testGetLevel()throws IOException {
        GridMeasure gm = new GridMeasure();

        gm.setBranchLevelId(3);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getBranchLevelId(), gm2.getBranchLevelId());
    }

    @Test
    public void testGetCreate_user_department()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setAffectedResource("Test");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getCreateUserDepartment(), gm2.getCreateUserDepartment());
    }

    @Test
    public void testGetModUserDepartment()throws IOException {
        GridMeasure gm = new GridMeasure();
        gm.setModUserDepartment("Department YY");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(gm);

        GridMeasure gm2 = om.readValue(jsonString, GridMeasure.class);

        assertEquals(gm.getModUserDepartment(), gm2.getModUserDepartment());
    }

}






