/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Test;

public class LockTest {

    @Test
    public void testGetId()throws IOException {
        Lock lock = new Lock();
        lock.setId(3);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(lock);

        Lock lock2 = om.readValue(jsonString, Lock.class);

        assertEquals(lock.getId(), lock2.getId());
    }

    @Test
    public void testGetKey()throws IOException {
        Lock lock = new Lock();
        lock.setKey(4);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(lock);

        Lock lock2 = om.readValue(jsonString, Lock.class);

        assertEquals(lock.getKey(), lock2.getKey());
    }

    @Test
    public void testGetUsername()throws IOException {
        Lock lock = new Lock();
        lock.setUsername("Tom Tester");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(lock);

        Lock lock2 = om.readValue(jsonString, Lock.class);

        assertEquals(lock.getUsername(), lock2.getUsername());
    }

    @Test
    public void testGetInfo()throws IOException {
        Lock lock = new Lock();
        lock.setInfo("locked");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(lock);

        Lock lock2 = om.readValue(jsonString, Lock.class);

        assertEquals(lock.getUsername(), lock2.getUsername());
    }


}

