/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class RoleAccessDefinitionApiTest {

    @Test
    public void testEditRoleGetName()throws IOException {
        RoleAccessDefinitionApi.EditRole ra = new RoleAccessDefinitionApi.EditRole ();
        ra.setName("name");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(ra);

        RoleAccessDefinitionApi.EditRole ra2 = om.readValue(jsonString, RoleAccessDefinitionApi.EditRole.class);

        assertEquals(ra.getName(), ra2.getName());
    }

    @Test
    public void testEditRoleGridMeasureStatusIds()throws IOException {
        RoleAccessDefinitionApi.EditRole ra = new RoleAccessDefinitionApi.EditRole ();
        int[] intArray = {1,2};
        ra.setGridMeasureStatusIds(intArray);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(ra);

        RoleAccessDefinitionApi.EditRole ra2 = om.readValue(jsonString, RoleAccessDefinitionApi.EditRole.class);

        assertEquals(ra.getGridMeasureStatusIds().length, ra2.getGridMeasureStatusIds().length);
    }

    ///////

    @Test
    public void testControlGridMeasureStatusId()throws IOException {
        RoleAccessDefinitionApi.Control ra = new RoleAccessDefinitionApi.Control ();
        ra.setGridMeasureStatusId(1);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(ra);

        RoleAccessDefinitionApi.Control ra2 = om.readValue(jsonString, RoleAccessDefinitionApi.Control.class);

        assertEquals(ra.getGridMeasureStatusId(), ra2.getGridMeasureStatusId());
    }

    @Test
    public void testControlActiveButtons()throws IOException {
        RoleAccessDefinitionApi.Control ra = new RoleAccessDefinitionApi.Control ();
        String[] btnArray = {"cancel", "save"};
        ra.setActiveButtons(btnArray);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(ra);

        RoleAccessDefinitionApi.Control ra2 = om.readValue(jsonString, RoleAccessDefinitionApi.Control.class);

        assertEquals(ra.getActiveButtons().length, ra2.getActiveButtons().length);
    }

    @Test
    public void testControlInactiveFields()throws IOException {
        RoleAccessDefinitionApi.Control ra = new RoleAccessDefinitionApi.Control ();
        String[] fieldsArray = {"id", "user"};
        ra.setInactiveFields(fieldsArray);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(ra);

        RoleAccessDefinitionApi.Control ra2 = om.readValue(jsonString, RoleAccessDefinitionApi.Control.class);

        assertEquals(ra.getInactiveFields().length, ra2.getInactiveFields().length);
    }

    @Test
    public void testGetEditRoles()throws IOException {
        RoleAccessDefinitionApi ra = new RoleAccessDefinitionApi ();
        RoleAccessDefinitionApi.EditRole[] editRolesArray = {new RoleAccessDefinitionApi.EditRole()};
        ra.setEditRoles(editRolesArray);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(ra);

        RoleAccessDefinitionApi ra2 = om.readValue(jsonString, RoleAccessDefinitionApi.class);

        assertEquals(ra.getEditRoles().length, ra2.getEditRoles().length);
    }

    @Test
    public void testGetControls()throws IOException {
        RoleAccessDefinitionApi ra = new RoleAccessDefinitionApi ();
        RoleAccessDefinitionApi.Control[] controlsArray = {new RoleAccessDefinitionApi.Control()};
        ra.setControls(controlsArray);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(ra);

        RoleAccessDefinitionApi ra2 = om.readValue(jsonString, RoleAccessDefinitionApi.class);

        assertEquals(ra.getControls().length, ra2.getControls().length);
    }


    @Test
    public void testGetStornoSection()throws IOException {
        RoleAccessDefinitionApi ra = new RoleAccessDefinitionApi ();
        RoleAccessDefinitionApi.StornoSection stornoSections = new RoleAccessDefinitionApi.StornoSection();
        String[] stornoRoles = {"a role", "a super role"};
        stornoSections.setStornoRoles(stornoRoles);
        ra.setStornoSection(stornoSections);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(ra);

        RoleAccessDefinitionApi ra2 = om.readValue(jsonString, RoleAccessDefinitionApi.class);

        assertEquals(ra.getStornoSection().getStornoRoles().length, ra2.getStornoSection().getStornoRoles().length);
    }

    @Test
    public void testGetDuplicateSection()throws IOException {
        RoleAccessDefinitionApi ra = new RoleAccessDefinitionApi ();
        RoleAccessDefinitionApi.DuplicateSection duplicateSection = new RoleAccessDefinitionApi.DuplicateSection();
        String[] duplicateRoles = {"a role", "a super role"};
        duplicateSection.setDuplicateRoles(duplicateRoles);
        ra.setDuplicateSection(duplicateSection);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(ra);

        RoleAccessDefinitionApi ra2 = om.readValue(jsonString, RoleAccessDefinitionApi.class);

        assertEquals(ra.getDuplicateSection().getDuplicateRoles().length, ra2.getDuplicateSection().getDuplicateRoles().length);
    }

}
