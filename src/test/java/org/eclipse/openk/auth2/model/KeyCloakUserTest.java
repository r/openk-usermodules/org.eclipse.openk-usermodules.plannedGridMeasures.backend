/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.auth2.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class KeyCloakUserTest {

    @Test
    public void testGettersAndSetters(){

        KeyCloakUser kcUser = new KeyCloakUser();

        kcUser.setId("7");
        assertEquals("7", kcUser.getId());

        kcUser.setCreatedTimestamp(9);
        assertEquals(9, kcUser.getCreatedTimestamp());

        kcUser.setUsername("testUsername");
        assertEquals("testUsername", kcUser.getUsername());

        kcUser.setEnabled(true);
        assertEquals(true, kcUser.getEnabled());

        kcUser.setTotp(true);
        assertEquals(true, kcUser.getTotp());

        kcUser.setEmailVerified(true);
        assertEquals(true, kcUser.getEmailVerified());

        kcUser.setFirstName("firstName");
        assertEquals("firstName", kcUser.getFirstName());

        kcUser.setLastName("lastName");
        assertEquals("lastName", kcUser.getLastName());

        List rolesList = new ArrayList<String>();
        kcUser.setRealmRoles(rolesList);
        assertEquals(rolesList, kcUser.getRealmRoles());

        List dctList = new ArrayList<String>();
        kcUser.setDisableableCredentialTypes(dctList);
        assertEquals(dctList, kcUser.getDisableableCredentialTypes());

        List raList = new ArrayList<String>();
        kcUser.setRequiredActions(raList);
        assertEquals(raList, kcUser.getRequiredActions());

        KeyCloakUserAccess kcuAccess = new KeyCloakUserAccess();
        kcUser.setAccess(kcuAccess);
        assertEquals(kcuAccess, kcUser.getAccess());


    }
}
