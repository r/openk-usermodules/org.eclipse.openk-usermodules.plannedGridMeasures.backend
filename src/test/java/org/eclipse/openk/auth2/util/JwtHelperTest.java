/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.auth2.util;

import static org.junit.Assert.assertNotNull;

import org.eclipse.openk.auth2.model.JwtPayload;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Test;

public class JwtHelperTest {

    JwtPayload pl;

    @Test
    public void testGetJwtPayload_Base64URL() {
        String base64urlToken = "eyJqdGkiOiI3NDVmOTc4Zi00OTYwLTRlO" +
                "WYtYmFiMS1jNjdkZjMzODEwYjQiLCJleH" +
                "AiOjE1MTYwMTAxNDksIm5iZiI6MCwiaWF" +
                "0IjoxNTE2MDA5ODQ5LCJpc3MiOiJodHRw" +
                "Oi8vbG9jYWxob3N0OjgwOTAvYXV0aC9yZ" +
                "WFsbXMvTVZWTmV0emUiLCJhdWQiOiJlbG" +
                "9nYm9vay1iYWNrZW5kIiwic3ViIjoiNTk" +
                "5ZWY3NzMtNjRmYS00MzM2LWIwNDktMmEz" +
                "Njc4NTEzZTU4IiwidHlwIjoiQmVhcmVyI" +
                "iwiYXpwIjoiZWxvZ2Jvb2stYmFja2VuZC" +
                "IsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9" +
                "zdGF0ZSI6ImE2OGE4MzNkLTRiNTItNDgw" +
                "My05MDU4LTRkNjJhNmI3NzI3MCIsImFjc" +
                "iI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOl" +
                "siKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9" +
                "sZXMiOlsiZWxvZ2Jvb2stYWNjZXNzIiwi" +
                "ZWxvZ2Jvb2stbm9ybWFsdXNlciIsInVtY" +
                "V9hdXRob3JpemF0aW9uIl19LCJyZXNvdX" +
                "JjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJ" +
                "yb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIs" +
                "Im1hbmFnZS1hY2NvdW50LWxpbmtzIiwid" +
                "mlldy1wcm9maWxlIl19fSwicm9sZXMiOi" +
                "JbdW1hX2F1dGhvcml6YXRpb24sIGVsb2d" +
                "ib29rLWFjY2VzcywgZWxvZ2Jvb2stbm9y" +
                "bWFsdXNlciwgb2ZmbGluZV9hY2Nlc3NdI" +
                "iwibmFtZSI6IlV3ZSByb8OfIiwicHJlZm" +
                "VycmVkX3VzZXJuYW1lIjoib3Blbmt0ZXN" +
                "0IiwiZ2l2ZW5fbmFtZSI6IlV3ZSIsImZh" +
                "bWlseV9uYW1lIjoicm_DnyJ9";
        base64urlToken = "AA." + base64urlToken + ".CC";
        // this payload contains a char with is valid in Base64URL but NOT in Base64.
        try {
            pl = JwtHelper.getJwtPayload(base64urlToken);
        }
        catch (HttpStatusException sEx){
            pl = null;
        }

            assertNotNull( pl ); }

    public void testGetJwtPayload_notOk(){

    }
}
