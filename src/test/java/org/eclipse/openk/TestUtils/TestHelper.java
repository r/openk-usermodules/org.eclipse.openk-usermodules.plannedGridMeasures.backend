/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.TestUtils;

import java.io.IOException;
import org.eclipse.openk.PlannedGridMeasuresConfiguration;
import org.eclipse.openk.PlannedGridMeasuresConfiguration.RabbitmqConfiguration;
import org.eclipse.openk.api.BackendSettings;
import org.eclipse.openk.api.BackendSettingsTest;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.api.mail.EmailTemplatePaths;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.controller.EmailmanagerTest;

public class TestHelper {

  public static void initDefaultBackendConfig(){
    BackendConfig.resetBackendConfigUnitTests();

    RabbitmqConfiguration rabbitmqConfiguration = new RabbitmqConfiguration();
    rabbitmqConfiguration.setUser("admin");
    rabbitmqConfiguration.setPassword("admin");
    rabbitmqConfiguration.setExchangeName("openk-pgm-exchange");
    rabbitmqConfiguration.setHost("MockHost");
    rabbitmqConfiguration.setPort("5672");

    BackendSettings bs = BackendSettingsTest.getBackendSettings();
    PlannedGridMeasuresConfiguration measuresConfiguration = new PlannedGridMeasuresConfiguration();
    measuresConfiguration.setRabbitmqConfiguration(rabbitmqConfiguration);
    measuresConfiguration.setPortalBaseURL("portalBaseUrlMock");
    BackendConfig.configure(measuresConfiguration, bs);
  }

  public static GridMeasure createGridMeasure(int statusValue) {
    GridMeasure gm = new GridMeasure();
    gm.setId(3);
    gm.setTitle("Yo Title");
    gm.setStatusId(statusValue);
    java.time.LocalDateTime ldtdate = java.time.LocalDateTime.parse("2018-04-22T18:00:00");
    java.sql.Timestamp date = java.sql.Timestamp.valueOf(ldtdate);
    gm.setPlannedStarttimeFirstSinglemeasure(date);
    return gm;
  }

  public static PlgmProcessSubject createProcessSubject(int statusValue){
    GridMeasure gm = createGridMeasure(statusValue);
    PlgmProcessSubject subject = PlgmProcessSubject.fromGridMeasure(gm, "fd");
    subject.setGridMeasure(gm);
    return subject;
  }

}
