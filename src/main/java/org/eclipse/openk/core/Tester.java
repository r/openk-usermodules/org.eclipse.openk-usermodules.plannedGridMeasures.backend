/*
 * *****************************************************************************
 *  Copyright © 2018 PTA GmbH.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *
 *      http://www.eclipse.org/legal/epl-v10.html
 *
 * *****************************************************************************
 */

package org.eclipse.openk.core;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.eclipse.openk.common.util.DateUtils;

public class Tester {

  private static final String DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss";
  private static final String DATEFORMAT2 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
  private static final String DATEFORMAT3 = "yyMMdd";

  public static void main(String[] args) throws Exception {
    test3();
  }

  private static void test3() {
    List<String> list = new ArrayList<>();
    list.add("behold");
    list.add("bend");
    list.add("bet");
    list.add("bear");
    list.add("beat");
    list.add("become");
    list.add("begin");

    String s = list.stream().filter(it -> it.contains("bea")).findFirst().orElse(null);
    String s2 = list.stream().filter(it -> it.contains("bea")).findFirst().get();
    //List<String> matches = list.stream().filter(it -> it.contains("bea")).collect(Collectors.toList());

    System.out.println(s); // [bear, beat]

  }

  private static void test1() throws ParseException {
    DateFormat dateFormat3 = new SimpleDateFormat("yyMMdd");
    DateFormat dateFormat = new SimpleDateFormat(DATEFORMAT, Locale.GERMANY);      //This is the format I need
    // dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    // dateFormat.setTimeZone(TimeZone.getTimeZone("ECT"));

    Date date = new Date();
    String format = dateFormat3.format(date);
    System.out.println("format3: " + format);
    // Date date2 = dateFormat.parse("");

    Date date1 = DateUtils.parseStringToDate(null);
    Date date2 = new Date();
    Date date3 = org.apache.commons.lang3.time.DateUtils.addDays(date2, 3);
    System.out.println(date3);

    boolean sameDay = org.apache.commons.lang3.time.DateUtils.isSameDay(date, date3);
    System.out.println(sameDay);

    System.out.println(DateUtils.asLocalDate(date3));


  }

  private static void test2() {
    int number = 123;
    String numberAsString = String.format ("%05d", number);
    System.out.println(numberAsString);

  }

  private static String createDescriptiveId(int counter, Date date){
    DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
    String counterAsString = String.format ("%04d", counter);
    return dateFormat.format(date)+ counterAsString;
  }



}
