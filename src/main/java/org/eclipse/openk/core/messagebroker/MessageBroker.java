/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.core.messagebroker;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import org.apache.log4j.Logger;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.controller.BackendConfig;

public abstract class MessageBroker implements AutoCloseable{

  private static final Logger LOGGER = Logger.getLogger(MessageBroker.class.getName());

  protected Channel channel;
  protected Connection connection;

  public MessageBroker() throws ProcessException {
    //Create a connection factory
    ConnectionFactory factory = new ConnectionFactory();

    //hostname of your rabbitmq server
    factory.setHost(BackendConfig.getInstance().getRabbitmqConfiguration().getHost());
    factory.setPassword(BackendConfig.getInstance().getRabbitmqConfiguration().getPassword());
    factory.setUsername(BackendConfig.getInstance().getRabbitmqConfiguration().getUser());
    factory.setPort(Integer.parseInt(BackendConfig.getInstance().getRabbitmqConfiguration().getPort()));

    try {
      //getting a connection
      connection = factory.newConnection();

      //creating a channel
      channel = connection.createChannel();
    } catch (IOException | TimeoutException e) {
      LOGGER.error("Error in MessageBroker Constructor (RabbitMQ)", e);
      throw new ProcessException("Error in MessageBroker Constructor (RabbitMQ)");
    }
  }

  @Override
  public void close() throws Exception {
    this.channel.close();
    this.connection.close();
  }
}
