/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.ProcessState;
import org.eclipse.openk.core.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.exceptions.HttpStatusException;


public class DecideMeasureApplied extends DecisionTask<PlgmProcessSubject> {
    private static Logger logger = Logger.getLogger(DecideMeasureApplied.class.getName()); // NOSONAR
    // we leave this logger in place, even if it's not called ... (at the moment)

    public DecideMeasureApplied() {
        super("Decision: Soll Massnahme beantragt werden?");
    }

    @Override
    public OutputPort decide(PlgmProcessSubject model) throws ProcessException {
        ProcessState newState = PlgmProcessState.fromValue(model.getGridMeasure().getStatusId());
        String loggerOutput1 = "Decide: ";

        if( newState == PlgmProcessState.APPLIED ) {
            logger.debug(loggerOutput1 + getDescription()+"\" -> Firing Port 1");
            return OutputPort.PORT1;
        }
        else if( newState == PlgmProcessState.NEW ) {
            logger.debug(loggerOutput1 + getDescription()+"\" -> Firing Port 2");
            return OutputPort.PORT2;
        }
        else if( newState == PlgmProcessState.CANCELED ) {
            logger.debug(loggerOutput1 + getDescription()+"\" -> Firing Port 3");
            return OutputPort.PORT3;
        }
        else {
            throw new ProcessException(this.getDescription()+": Invalid status request:" + newState,
                    new HttpStatusException(HttpStatus.UNPROCESSABLE_ENTITY_422));
        }
    }

}
