/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.ProcessState;
import org.eclipse.openk.core.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.exceptions.HttpStatusException;

public class DecideMeasureNotNeededOrRequested extends DecisionTask<PlgmProcessSubject> {
    private static Logger logger = Logger.getLogger(DecideMeasureNotNeededOrRequested.class.getName()); // NOSONAR
    // we leave this logger in place, even if it's not called ... (at the moment)

    public DecideMeasureNotNeededOrRequested() {
        super("Decision: Maßnahme nicht mehr nötig (oder angefordert)?");
    }

    @Override
    public OutputPort decide(PlgmProcessSubject model) throws ProcessException {
        ProcessState newState = PlgmProcessState.fromValue(model.getGridMeasure().getStatusId());
        String loggerOutput1 = "Decide: ";

        if( newState == PlgmProcessState.REQUESTED ) {
            logger.debug(loggerOutput1 + getDescription()+"\" -> Firing Port NO");
            return OutputPort.NO;
        }
        else if( newState == PlgmProcessState.CANCELED ) {
            logger.debug(loggerOutput1 + getDescription()+"\" -> Firing Port YES");
            return OutputPort.YES;
        }
        else {
            throw new ProcessException(this.getDescription()+": Invalid status request:" + newState,
                    new HttpStatusException(HttpStatus.UNPROCESSABLE_ENTITY_422));
        }
    }
}
