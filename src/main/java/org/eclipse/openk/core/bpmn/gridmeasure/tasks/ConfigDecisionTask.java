/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.apache.log4j.Logger;
import org.eclipse.openk.api.BackendSettings;
import org.eclipse.openk.common.util.SmallReflectionHelper;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.controller.BackendConfig;

public class ConfigDecisionTask extends DecisionTask<PlgmProcessSubject> {
    private static Logger logger = Logger.getLogger(DecideAnotherSinglemeasure.class.getName()); // NOSONAR
    // we leave this logger in place, even if it's not called ... (at the moment)

    private String configParamName;

    public ConfigDecisionTask(String backendSettingParamName) {
        super("ConfigDecisionTask: Param="+backendSettingParamName);
        this.configParamName = backendSettingParamName;
    }

    @Override
    public OutputPort decide(PlgmProcessSubject model) throws ProcessException {
        BackendSettings.BpmnGridConfig bs = BackendConfig.getInstance().getBackendSettings().getBpmnGridConfig();
        try {
            String loggerOutput1 = "Decide: ";
            OutputPort port = SmallReflectionHelper.checkBoolField(bs, this.configParamName)? OutputPort.YES : OutputPort.NO;
            logger.debug(loggerOutput1 + getDescription() + "\" -> Firing Port: " + port.toString());
            return port;
        } catch (Exception e) {
            String errText = "Invalid configuration for grid: " + configParamName + " is an unknown configParam";
            logger.fatal(errText, e);
            throw new ProcessException(errText);
        }
    }
}
