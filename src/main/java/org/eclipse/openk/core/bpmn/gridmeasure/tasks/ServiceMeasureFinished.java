/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import javax.mail.MessagingException;
import org.apache.log4j.Logger;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.tasks.ServiceTask;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.controller.EmailManager;
import org.eclipse.openk.core.controller.PgmEmail;
import org.eclipse.openk.core.messagebroker.Producer;

public class ServiceMeasureFinished extends ServiceTask<PlgmProcessSubject> {
    private static final Logger LOGGER = Logger.getLogger(ServiceMeasureFinished.class.getName());
    public ServiceMeasureFinished() {
        super("Service task 'Beendet'");
    }

    @Override
    protected void onLeaveStep(PlgmProcessSubject model) throws ProcessException {
        try {
            EmailManager emailManager = new PgmEmail(model, BackendConfig.getInstance().getEmailTemplatePaths().getFinishedEmailTemplate(), false);
            emailManager.sendEmail();
        } catch (MessagingException e) {
            LOGGER.error("Error in ServiceMeasureFinished onLeaveStep", e);
            throw new ProcessException("Error in ServiceMeasureFinished onLeaveStep (EmailManager)");
        }
        LOGGER.debug(">>execute: Send Mail on 'Beendet'");

        try (Producer prod = createMessageQueueProducer()) {
            prod.sendMessageAsJson(model.getGridMeasure(), "finished");
        } catch (Exception e) {
            LOGGER.error("Error in ServiceMeasureFinished onLeaveStep (RabbitMQ)", e);
            throw new ProcessException("Error in ServiceMeasureFinished onLeaveStep (RabbitMQ)");
        }
        LOGGER.debug(">>execute: basicPublish RabbitMQ");

    }


}
