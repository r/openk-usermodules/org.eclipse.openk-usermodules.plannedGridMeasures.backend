/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.api.SingleGridmeasure;
import org.eclipse.openk.api.Steps;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.common.mapper.GridMeasureMapper;
import org.eclipse.openk.common.mapper.generic.GenericApiToDbMapper;
import org.eclipse.openk.common.util.DateUtils;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.db.dao.AutoCloseEntityManager;
import org.eclipse.openk.db.dao.EntityHelper;
import org.eclipse.openk.db.dao.TblGridMeasureDao;
import org.eclipse.openk.db.dao.TblIdCounterDao;
import org.eclipse.openk.db.dao.TblSingleGridmeasureDao;
import org.eclipse.openk.db.dao.TblStepsDao;
import org.eclipse.openk.db.model.TblGridMeasure;
import org.eclipse.openk.db.model.TblIdCounter;
import org.eclipse.openk.db.model.TblSingleGridmeasure;
import org.eclipse.openk.db.model.TblSteps;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class GridMeasureStorageHelper {
    private static final Logger logger = Logger.getLogger(GridMeasureStorageHelper.class);
    public static final String CANNOT_UPDATE_AN_ENTITY_THAT_DOES_NOT_EXIST_ID = "Cannot update an entity that does not exist. ID=";
    public static final String CANNOT_STORE_EMPTY_GRID_MEASURE = "Cannot store empty gridMeasure";

    private static final Object lock = new Object();

    protected GridMeasureStorageHelper(){}

    public static GridMeasureStorageHelper getHelper() {
        return new GridMeasureStorageHelper();
    }

    public void storeMeasureFromViewModel(PlgmProcessSubject model) throws ProcessException, HttpStatusException {

        TblGridMeasure changedGM;

        try (AutoCloseEntityManager em = new AutoCloseEntityManager(createEntityManager())) {
            GridMeasureMapper gmMapper = new GridMeasureMapper();
            GridMeasure vmGm = model.getGridMeasure();
            TblGridMeasure tblGm = gmMapper.mapFromVModel(vmGm);
            List<SingleGridmeasure> listSg = vmGm.getListSingleGridmeasures();

            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
            gmDao.getEM().getTransaction().begin();
            changedGM = storeMeasureImplInTx(tblGm, model.getChangeUser(), gmDao, em);

            TblSingleGridmeasureDao sgmDao = createTblSingleGridmeasureDao(em);
            List<SingleGridmeasure> newSingleGms = new ArrayList<>();

            if (listSg != null && !listSg.isEmpty()) {
                sortSingleGridMeasuresByDate(vmGm);
                processExistingSingleGridMeasureList(model, changedGM.getId(), em, listSg, sgmDao, newSingleGms);

            }
            else{
                //Anlegen einer Leermassnahme
                TblSingleGridmeasure tblSg = new TblSingleGridmeasure();
                fillEmptyTblSingleGridmeasure(changedGM.getId(), tblGm.getModUser(), tblSg);
                TblSingleGridmeasure tblSgDB = createSinglemeasureInDb(tblSg, sgmDao);
                newSingleGms.add(new GenericApiToDbMapper().mapToViewModel(SingleGridmeasure.class, tblSgDB));
            }

            GridMeasure vmGmDB = gmMapper.mapToVModel(changedGM);
            vmGmDB.setListSingleGridmeasures(newSingleGms);
            model.setGridMeasure(vmGmDB);

            gmDao.getEM().getTransaction().commit();
        }
    }

    private GridMeasure sortSingleGridMeasuresByDate(GridMeasure gridMeasure) {
        List<SingleGridmeasure> singleGridmeasures = gridMeasure.getListSingleGridmeasures();

        Collections.sort(singleGridmeasures, new Comparator<SingleGridmeasure>() {
            public int compare(SingleGridmeasure sgm1, SingleGridmeasure sgm2) {
                if (sgm1.getPlannedStarttimeSinglemeasure() == null || sgm2.getPlannedStarttimeSinglemeasure() == null)
                    return 0;
                return sgm1.getPlannedStarttimeSinglemeasure().compareTo(sgm2.getPlannedStarttimeSinglemeasure());
            }
        });
        for (SingleGridmeasure sgm: singleGridmeasures) {
            sgm.setSortorder(singleGridmeasures.indexOf(sgm) + 1);
        }

        gridMeasure.setListSingleGridmeasures(singleGridmeasures);
        return gridMeasure;
    }

    private void processExistingSingleGridMeasureList(PlgmProcessSubject model,
                                                      Integer changedGmId,
                                                      AutoCloseEntityManager em,
                                                      List<SingleGridmeasure> listSg,
                                                      TblSingleGridmeasureDao sgmDao,
                                                      List<SingleGridmeasure> newSingleGms) throws HttpStatusException {
        List<Steps> newSteps = new ArrayList<>();
        TblStepsDao stpDao = createTblStepsDao(em);

        //Insert, Update or Delete of SingleGridmeasures
        GridMeasureMapper gridMeasureMapper = new GridMeasureMapper();
        for (SingleGridmeasure sg : listSg) {
            TblSingleGridmeasure mSg = new GenericApiToDbMapper().mapFromViewModel(TblSingleGridmeasure.class, sg);
            gridMeasureMapper.adjustPowerSystemResourceMapFromViewModel(sg, mSg);
            if (sg.isDelete() && listSg.size() > 1) {
                deleteSingleGridmeasure(mSg, sg, sgmDao, stpDao);
            }
            else{
                mSg.setFkTblGridmeasure(changedGmId);
                TblSingleGridmeasure tblSgDB = storeSingleGm(mSg, model.getChangeUser(), sgmDao);
                newSingleGms.add(new GenericApiToDbMapper().mapToViewModel(SingleGridmeasure.class, tblSgDB));

                List<Steps> listStp = sg.getListSteps();

                if (listStp != null) {
                    processStepList(model, newSteps, stpDao, tblSgDB, listStp);
                    sg.setListSteps(newSteps);
                }
            }
        }
    }

    private void processStepList(PlgmProcessSubject model,
                                 List<Steps> newSteps,
                                 TblStepsDao stpDao,
                                 TblSingleGridmeasure tblSgDB,
                                 List<Steps> listStp) throws HttpStatusException {
        for (Steps stp: listStp) {
            TblSteps mStp = new GenericApiToDbMapper().mapFromViewModel(TblSteps.class, stp);
            if (stp.isDelete() && listStp.size() > 1) {
                deleteSteps(mStp, stpDao);
            }
            else {
                mStp.setFkTblSingleGridmeasure(tblSgDB.getId());
                TblSteps tblStpDB = storeSteps(mStp, model.getChangeUser(), stpDao);
                newSteps.add(new GenericApiToDbMapper().mapToViewModel(Steps.class, tblStpDB));
            }
        }
    }

    private void fillEmptyTblSingleGridmeasure(Integer changedGmId, String modUser, TblSingleGridmeasure tblSg)  {
        tblSg.setFkTblGridmeasure(changedGmId);
        tblSg.setSortorder(1);
        tblSg.setTitle("Leermassnahme");
        tblSg.setCreateDate(Date.from(Instant.now()));
        tblSg.setCreateUser(modUser);
        tblSg.setModDate(Date.from(Instant.now()));
        tblSg.setModUser(modUser);
    }

    public TblGridMeasure setMeasureStatusAndStore(Integer measureId, String changeUser, PlgmProcessState newStatus) throws ProcessException {
        try (AutoCloseEntityManager em = new AutoCloseEntityManager(createEntityManager())) {
            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);

            TblGridMeasure tblGM = gmDao.findByIdInTx(TblGridMeasure.class, measureId);
            tblGM.setFkRefGmStatus(newStatus.getStatusValue());

            return storeMeasureImpl(tblGM, changeUser, gmDao, em);
        }
    }

    protected TblGridMeasureDao createTblGridMeasureDao(EntityManager em) {
        return new TblGridMeasureDao(em);
    }

    protected TblIdCounterDao createTblIdCounterDao(EntityManager em) {
        return new TblIdCounterDao(em);
    }

    protected TblSingleGridmeasureDao createTblSingleGridmeasureDao (EntityManager em) {
        return new TblSingleGridmeasureDao(em);
    }

    protected TblStepsDao createTblStepsDao (EntityManager em) {
        return new TblStepsDao(em);
    }


    protected EntityManager createEntityManager() {
        // this method has been extracted for test purposes
        return EntityHelper.getEMF().createEntityManager();
    }


    private TblGridMeasure storeMeasureImpl(TblGridMeasure tblGm, String changeUser
            , TblGridMeasureDao gmDao, EntityManager em) throws ProcessException {

        gmDao.getEM().getTransaction().begin();
        TblGridMeasure tblGmDB = storeMeasureImplInTx(tblGm, changeUser, gmDao, em);
        gmDao.getEM().getTransaction().commit();
        return tblGmDB;
    }

    private TblGridMeasure storeMeasureImplInTx(TblGridMeasure tblGm, String changeUser
            , TblGridMeasureDao gmDao, EntityManager em) throws ProcessException {

        tblGm.setModUser(changeUser);
        tblGm.setModDate(Date.from(Instant.now()));
        if( tblGm.getCreateDate() == null ) {
            tblGm.setCreateDate(tblGm.getModDate());
        }
        TblGridMeasure tblGmDB = null;
        if( tblGm.getId() == null ) {
            tblGmDB = createMeasureInDb(tblGm, gmDao, em);
        }
        else {
            if( gmDao.findByIdInTx(TblGridMeasure.class, tblGm.getId()) != null) {
                // entity should exist before it is updated
                tblGmDB = updateMeasureInDb(tblGm, gmDao);
            }
            else {
                String errText = CANNOT_UPDATE_AN_ENTITY_THAT_DOES_NOT_EXIST_ID +tblGm.getId();
                logger.error(errText);
                throw new ProcessException( errText, new HttpStatusException(HttpStatus.SC_NOT_FOUND, errText) );
            }
        }

        return tblGmDB;
    }



    private TblGridMeasure createMeasureInDb(TblGridMeasure tblGm, TblGridMeasureDao gmDao, EntityManager em) throws ProcessException {
        tblGm.setIdDescriptive(getNextDescriptiveId(em, tblGm));
        tblGm.setCreateDate(Date.from(Instant.now()));
        tblGm.setCreateUser(tblGm.getModUser());

        return updateMeasureInDb(tblGm, gmDao);
    }

    private TblGridMeasure updateMeasureInDb(TblGridMeasure tblGm, TblGridMeasureDao gmDao) throws ProcessException {
        try {
            return gmDao.storeInTx(tblGm);

        } catch (Exception e) {
            String errorText = tblGm != null ?
                    "Error storing gridMeasureEntity:" + JsonGeneratorBase.getGson().toJson(tblGm) :
                    CANNOT_STORE_EMPTY_GRID_MEASURE;

            logger.error(errorText, e);
            throw new ProcessException("Error in process:",
                    new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR));
        }
    }

    private TblSingleGridmeasure storeSingleGm(TblSingleGridmeasure newTblSg, String changeUser, TblSingleGridmeasureDao sgmDao) throws HttpStatusException{
        newTblSg.setModUser(changeUser);
        newTblSg.setModDate(Date.from(Instant.now()));

        TblSingleGridmeasure ret = null;
        if( newTblSg.getId() == null ) {
            newTblSg.setCreateUser(changeUser);
            newTblSg.setCreateDate(Date.from(Instant.now()));

            ret = createSinglemeasureInDb(newTblSg, sgmDao);
        }
        else {
            TblSingleGridmeasure aktSg = sgmDao.findByIdInTx(TblSingleGridmeasure.class, newTblSg.getId());
            if( aktSg != null) {
                // entity should exist before it is updated
                newTblSg.setCreateUser(aktSg.getCreateUser());
                newTblSg.setCreateDate(aktSg.getCreateDate());

                ret = updateSingleGridmeasureInDb(newTblSg, sgmDao);
            }
            else {
                String errText = CANNOT_UPDATE_AN_ENTITY_THAT_DOES_NOT_EXIST_ID+newTblSg.getId();
                logger.error(errText);
                throw new HttpStatusException(HttpStatus.SC_NOT_FOUND, errText) ;
            }
        }
        return ret;
    }

    private TblSingleGridmeasure createSinglemeasureInDb(TblSingleGridmeasure tblSgm, TblSingleGridmeasureDao sgmDao) throws HttpStatusException{
        return updateSingleGridmeasureInDb(tblSgm, sgmDao);
    }

    private TblSingleGridmeasure updateSingleGridmeasureInDb(TblSingleGridmeasure tblSgm, TblSingleGridmeasureDao sgmDao) throws HttpStatusException{
        try {
            return sgmDao.storeInTx(tblSgm);

        } catch (Exception e) {
            String errorText = tblSgm != null ?
                    "Error storing singleGridmeasureEntity:" + JsonGeneratorBase.getGson().toJson(tblSgm) :
                    CANNOT_STORE_EMPTY_GRID_MEASURE;

            logger.error(errorText, e);
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private void deleteSingleGridmeasure(TblSingleGridmeasure tblSgm, SingleGridmeasure sg, TblSingleGridmeasureDao sgmDao, TblStepsDao stpDao)throws HttpStatusException{
        GenericApiToDbMapper mapper = new GenericApiToDbMapper();
        TblSingleGridmeasure tblSgmDB = null;

        try {

            //delete steps first
            List<Steps> listSteps = sg.getListSteps();
            if (listSteps != null && !listSteps.isEmpty()) {
                for (Steps stp : listSteps) {
                    TblSteps mStp = mapper.mapFromViewModel(TblSteps.class, stp);
                    deleteSteps(mStp, stpDao);
                }
            }

            //check entity is not new
            if(tblSgm.getId() != null){
                tblSgmDB = sgmDao.findByIdInTx(TblSingleGridmeasure.class, tblSgm.getId());
            }
            //check can be found in DB
            if(tblSgmDB != null) {
                sgmDao.removeInTx(tblSgmDB, tblSgmDB.getId());
            }

        } catch (Exception e) {
            String errorText = "Error deleting singleGridmeasureEntity:" + JsonGeneratorBase.getGson().toJson(tblSgm);
            logger.error(errorText, e);
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private TblSteps storeSteps(TblSteps newTblStp, String changeUser, TblStepsDao stpDao) throws HttpStatusException{
        newTblStp.setModUser(changeUser);
        newTblStp.setModDate(Date.from(Instant.now()));

        TblSteps ret = null;
        if( newTblStp.getId() == null ) {
            newTblStp.setCreateUser(changeUser);
            newTblStp.setCreateDate(Date.from(Instant.now()));

            ret = createStepsInDb(newTblStp, stpDao);
        }
        else {
            TblSteps aktSg = stpDao.findByIdInTx(TblSteps.class, newTblStp.getId());
            if( aktSg != null) {
                // entity should exist before it is updated
                newTblStp.setCreateUser(aktSg.getCreateUser());
                newTblStp.setCreateDate(aktSg.getCreateDate());

                ret = updateStepsInDb(newTblStp, stpDao);
            }
            else {
                String errText = CANNOT_UPDATE_AN_ENTITY_THAT_DOES_NOT_EXIST_ID+newTblStp.getId();
                logger.error(errText);
                throw new HttpStatusException(HttpStatus.SC_NOT_FOUND, errText) ;
            }
        }
        return ret;
    }

    private TblSteps createStepsInDb(TblSteps tblStp, TblStepsDao stpDao) throws HttpStatusException{
        return updateStepsInDb(tblStp, stpDao);
    }

    private TblSteps updateStepsInDb(TblSteps tblStp, TblStepsDao stpDao) throws HttpStatusException{
        try {
            return stpDao.storeInTx(tblStp);

        } catch (Exception e) {
            String errorText = tblStp != null ?
                    "Error storing stepsEntity:" + JsonGeneratorBase.getGson().toJson(tblStp) :
                    CANNOT_STORE_EMPTY_GRID_MEASURE;

            logger.error(errorText, e);
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private void deleteSteps(TblSteps tblStp, TblStepsDao stpDao)throws HttpStatusException{
        TblSteps tblStpDB = null;

        try {
            //check entity is not new
            if(tblStp.getId() != null){
                tblStpDB = stpDao.findByIdInTx(TblSteps.class, tblStp.getId());
            }
            //check can be found in DB
            if(tblStpDB != null) {
                stpDao.removeInTx(tblStpDB, tblStpDB.getId());
            }

        } catch (Exception e) {
            String errorText = "Error deleting stepsEntity:" + JsonGeneratorBase.getGson().toJson(tblStp);
            logger.error(errorText, e);
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    // Returns descriptive ID in in form of 180730-0001
    private String getNextDescriptiveId(EntityManager em, TblGridMeasure tblGm) throws ProcessException {
        synchronized (lock) {
            try {
                TblIdCounterDao idCounterDao = createTblIdCounterDao(em);
                TblIdCounter idCounter = idCounterDao.getIdCounterForCounterType(Globals.COUNTERTYPE_GRIDMEASURE_ID);
                if (idCounter == null){
                    idCounter = createNewTblCounter(tblGm);
                } else {
                    updateTblCounter(idCounter, tblGm);
                }

                idCounterDao.storeInTx(idCounter);
                return createDescriptiveId(idCounter);
            } catch (Exception e) {
                logger.error("Error in getNextGridmeasureId with counterType:" + Globals.COUNTERTYPE_GRIDMEASURE_ID , e);
                throw new ProcessException("Error in process:",
                    new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR));
            }
        }
    }

    private void updateTblCounter(TblIdCounter idCounter, TblGridMeasure tblGm) {
        Integer counter = idCounter.getCounter();
        Date lastModifiedDate = DateUtils.parseStringToDate(idCounter.getModifiedDate());
        assert lastModifiedDate != null;
        LocalDate localDateLastModified = DateUtils.asLocalDate(lastModifiedDate);
        LocalDate localDateNow = LocalDate.now();

        if (localDateNow.isEqual(localDateLastModified)) {
            counter++;
            idCounter.setCounter(counter);
        } else {
            idCounter.setCounter(1);
            idCounter.setModifiedDate(DateUtils.formatDateToString(new Date()));
        }
        idCounter.setModDate(new Date());
        idCounter.setModUser(tblGm.getModUser());
    }

    private TblIdCounter createNewTblCounter(TblGridMeasure tblGm) {
        TblIdCounter idCounter;
        idCounter = new TblIdCounter();
        idCounter.setCounter(1);
        idCounter.setCounterType(Globals.COUNTERTYPE_GRIDMEASURE_ID);
        Date now = new Date();
        idCounter.setModifiedDate(DateUtils.formatDateToString(now));
        idCounter.setCreateDate(now);
        idCounter.setCreateUser(tblGm.getModUser());
        return idCounter;
    }

    private static String createDescriptiveId(TblIdCounter idCounter) {
        DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
        String counterAsString = String.format ("%04d", idCounter.getCounter());
        Date date = DateUtils.parseStringToDate(idCounter.getModifiedDate());
        return dateFormat.format(date)+ "-" + counterAsString;
    }

}
