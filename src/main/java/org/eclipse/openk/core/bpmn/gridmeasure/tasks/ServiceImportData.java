/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.ProcessSubject;
import org.eclipse.openk.core.bpmn.base.tasks.ServiceTask;


public class ServiceImportData extends ServiceTask {
    private static final Logger logger= Logger.getLogger(ServiceImportData.class.getName());
    public ServiceImportData() {
        super("Service task 'Daten aus Leitsystem importieren'");
    }

    @Override
    protected void onLeaveStep(ProcessSubject model) throws ProcessException {
        logger.debug(">>execute: Daten aus Leitsystem importieren");
    }


}
