/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import javax.mail.MessagingException;
import org.apache.log4j.Logger;
import org.eclipse.openk.api.mail.EmailTemplatePaths;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.tasks.ServiceTask;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.controller.EmailManager;
import org.eclipse.openk.core.controller.PgmEmail;
import org.eclipse.openk.core.messagebroker.Producer;


public class ServiceMeasureApproved extends ServiceTask<PlgmProcessSubject> {
    private static final Logger LOGGER = Logger.getLogger(ServiceMeasureApproved.class.getName());

    public ServiceMeasureApproved() {
        super("Service task 'genehmigt'");
    }

    @Override
    protected void onLeaveStep(PlgmProcessSubject model) throws ProcessException {

        try {
            EmailTemplatePaths paths = BackendConfig.getInstance().getEmailTemplatePaths();
            String template = paths.getApprovedEmailTemplate();
            EmailManager emailManager = new PgmEmail(model, template, true);
            emailManager.sendEmail();
        } catch (MessagingException e) {
            LOGGER.error("Error in ServiceMeasureApproved onLeaveStep", e);
            throw new ProcessException("Error in ServiceMeasureApproved onLeaveStep (EmailManager)");
        }
        LOGGER.debug(">>execute: Send Mail on 'genehmigt'");

        try (Producer prod = createMessageQueueProducer()) {
            prod.sendMessageAsJson(model.getGridMeasure(), "approved");
        } catch (Exception e) {
            LOGGER.error("Error in ServiceMeasureApproved onLeaveStep (RabbitMQ)", e);
            throw new ProcessException("Error in ServiceMeasureApproved onLeaveStep (RabbitMQ)");
        }
        LOGGER.debug(">>execute: basicPublish RabbitMQ");
    }

}
