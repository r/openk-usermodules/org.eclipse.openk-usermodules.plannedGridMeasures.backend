/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.tasks.UserInteractionTask;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.util.GridMeasureStorageHelper;
import org.eclipse.openk.core.exceptions.HttpStatusException;


public class MeasureForApproval extends UserInteractionTask<PlgmProcessSubject> {
    private static final Logger logger = Logger.getLogger(MeasureForApproval.class); // NOSONAR
    // we leave this logger in place, even if it's not called ... (at the moment)

    public MeasureForApproval() {
        super("State FOR_APPROVAL: Edit and approve GridMeasure");
    }


    @Override
    protected void onLeaveStep(PlgmProcessSubject model) throws ProcessException, HttpStatusException {
        GridMeasureStorageHelper.getHelper().storeMeasureFromViewModel(model);
    }

    @Override
    protected void onEnterStep(PlgmProcessSubject model) throws ProcessException {
        // nothing to do here
    }
}
