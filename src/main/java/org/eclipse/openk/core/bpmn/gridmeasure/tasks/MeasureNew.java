/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.apache.log4j.Logger;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.tasks.UserInteractionTask;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.util.GridMeasureStorageHelper;
import org.eclipse.openk.core.exceptions.HttpStatusException;


public class MeasureNew extends UserInteractionTask<PlgmProcessSubject> {
    private static final Logger logger = Logger.getLogger(MeasureNew.class); // NOSONAR
    // we leave this logger in place, even if it's not called ... (at the moment)

    public MeasureNew() {
        super("State NEW: Create GridMeasure");
    }

    protected void prepareStorage(GridMeasure gm) {
        if( gm.getStatusId() == null ) {
            gm.setStatusId(PlgmProcessState.NEW.getStatusValue());
        }
    }


    @Override
    protected void onLeaveStep(PlgmProcessSubject model) throws ProcessException, HttpStatusException {
        prepareStorage(model.getGridMeasure());
        GridMeasureStorageHelper.getHelper().storeMeasureFromViewModel(model);
    }

    @Override
    protected void onEnterStep(PlgmProcessSubject model) throws ProcessException {
        // nothing to do here
    }
}
