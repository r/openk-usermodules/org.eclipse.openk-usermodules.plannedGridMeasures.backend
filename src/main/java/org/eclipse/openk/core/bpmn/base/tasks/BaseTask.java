/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.base.tasks;


import org.apache.log4j.Logger;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.ProcessSubject;
import org.eclipse.openk.core.bpmn.base.ProcessTask;
import org.eclipse.openk.core.exceptions.HttpStatusException;


public abstract class BaseTask<T extends ProcessSubject> implements ProcessTask {
    private static final Logger logger = Logger.getLogger(BaseTask.class.getName());
    private ProcessTask outputStep;
    private String description;

    protected BaseTask(String description ) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void leaveStep(ProcessSubject model) throws ProcessException, HttpStatusException {
        logger.debug("On Leave: \""+description+"\"");
        onLeaveStep( (T)model );
        if(outputStep==null) {
            logger.debug(description+": firing unconnected Output!");
        }
        else {
            logger.debug("Left step: \""+description+"\"");
            outputStep.enterStep(model);
        }
    }

    @Override
    public void enterStep(ProcessSubject model ) throws ProcessException, HttpStatusException {
        logger.debug("Enter: \""+description+"\"");
        onEnterStep((T)model);
    }

    @Override
    public void recover(ProcessSubject model) throws ProcessException, HttpStatusException {
        logger.debug("Recover: \""+description+"\"");
        onRecover((T)model);
    }


    protected abstract void onLeaveStep(T model) throws ProcessException, HttpStatusException;
    protected abstract void onEnterStep(T model) throws ProcessException, HttpStatusException;
    protected abstract void onRecover(T model) throws ProcessException, HttpStatusException;

    @Override
    public void connectOutputTo(ProcessTask step) throws ProcessException {
        this.outputStep = step;
    }

}
