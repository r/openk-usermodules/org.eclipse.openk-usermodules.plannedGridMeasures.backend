/*
 * *****************************************************************************
 *  Copyright © 2018 PTA GmbH.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *
 *      http://www.eclipse.org/legal/epl-v10.html
 *
 * *****************************************************************************
 */

package org.eclipse.openk.core;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.AMQP.BasicProperties.Builder;
import com.rabbitmq.client.AMQP.Queue.DeclareOk;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.MessageProperties;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import org.eclipse.openk.PlannedGridMeasuresConfiguration;
import org.eclipse.openk.PlannedGridMeasuresConfiguration.RabbitmqConfiguration;
import org.eclipse.openk.api.BackendSettings;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.messagebroker.Producer;

public class RabbitMqTester {

  private final static String QUEUE_NAME = "pgm-applied-queue";
  private final static String ROUTING_KEY = "applied";
  private final static String EXCHANGE_NAME = "openk-pgm-exchange-test";

  public static void main(String[] args) throws Exception {
    prepareBackendSettings();
    //prepareRabbitMq();
    send2();



    // consumeExchange();
    //consume();



  }

  private  static void prepareBackendSettings() {
    RabbitmqConfiguration rabbitmqConfiguration = new RabbitmqConfiguration();
    rabbitmqConfiguration.setUser("admin");
    rabbitmqConfiguration.setPassword("admin");
    rabbitmqConfiguration.setExchangeName("openk-pgm-exchange-test");
    // rabbitmqConfiguration.setHost("172.18.22.160");
    rabbitmqConfiguration.setHost("localhost");
    rabbitmqConfiguration.setPort("5672");
    rabbitmqConfiguration.setUnroutedMessagesExchangeName("unrouted-messages-test-ex");
    rabbitmqConfiguration.setUnroutedMessagesQueueName("unrouted-messages-test-q");

    rabbitmqConfiguration.setQueueNames("pgm-applie-queue");

    BackendSettings backendSettings = new BackendSettings();
    PlannedGridMeasuresConfiguration measuresConfiguration = new PlannedGridMeasuresConfiguration();
    measuresConfiguration.setPortalBaseURL("portalBaseUrlMock");
    measuresConfiguration.setRabbitmqConfiguration(rabbitmqConfiguration);
    BackendConfig.configure(measuresConfiguration, backendSettings);
  }

  private static void send2() throws Exception {
    GridMeasure gm = new GridMeasure();
    gm.setTitle("Tester33");
    try (Producer prod = new Producer()) {
      prod.sendMessageAsJson(gm, "applied");
    }

  }

  private static void prepareRabbitMq(){
    ConnectionFactory factory = new ConnectionFactory();
    // factory.setHost("172.18.22.160");
    factory.setHost("localhost");
    factory.setPassword("admin");
    factory.setUsername("admin");
    try (Connection connection = factory.newConnection();
        Channel channel = connection.createChannel()) {

      // durable = true
      channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT, true);

      Map<String, Object> args = new HashMap<>();
      args.put("x-queue-mode", "lazy");

      List<String> queueNameList = Arrays.asList("pgm-canceled-queue", "pgm-applied-queue", "pgm-approved-queue");

      for (String queueName : queueNameList) {
        channel.queueDeclare(queueName, true, false, false, args);
        String routingKey = queueName.split("-")[1];
        channel.queueBind(queueName, EXCHANGE_NAME, routingKey);
      }

    } catch (TimeoutException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void send() {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("172.18.22.160");
    factory.setPassword("admin");
    factory.setUsername("admin");
    try (Connection connection = factory.newConnection();
          Channel channel = connection.createChannel()) {

      Map<String, Object> args = new HashMap<String, Object>();
      args.put("x-queue-mode", "lazy");

      channel.queueDeclare(QUEUE_NAME, true, false, false, args);
      String message = "Hello World!333233";
      // channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
      BasicProperties basicProperties = new Builder().contentType("text/plain").deliveryMode(2).priority(0).userId("bob").build();
      // channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
      channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
      System.out.println(" [x] Sent '" + message + "'");

    } catch (TimeoutException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  private static void consumeExchange() {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("172.18.22.160");
    factory.setPassword("admin");
    factory.setUsername("admin");
    try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {

      // channel.queueDeclare(QUEUE_NAME, false, false, false, null);
      System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

      Consumer consumer = new DefaultConsumer(channel) {
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
          String message = new String(body, "UTF-8");
          System.out.println(" [x] Received '" + envelope.getRoutingKey() + "':'" + message + "'");
        }
      };
      channel.basicConsume(QUEUE_NAME, true, consumer);

    } catch (TimeoutException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void consumeDirect() {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("172.18.22.160");
    factory.setPassword("admin");
    factory.setUsername("admin");
    try (Connection connection = factory.newConnection();
        Channel channel = connection.createChannel()) {

      // channel.queueDeclare(QUEUE_NAME, false, false, false, null);
      System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

      Consumer consumer = new DefaultConsumer(channel) {
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
            throws IOException {
          String message = new String(body, "UTF-8");
          System.out.println(" [x] Received '" + message + "'");
        }
      };
      channel.basicConsume(QUEUE_NAME, true, consumer);

    } catch (TimeoutException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }




  }




}
