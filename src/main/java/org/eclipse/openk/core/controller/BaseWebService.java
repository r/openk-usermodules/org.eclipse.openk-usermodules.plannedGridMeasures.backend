/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import java.io.IOException;
import java.util.*;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.core.exceptions.HttpStatusException;

public abstract class BaseWebService {
    public enum SecureType {NONE, NORMAL, HIGH}
    private final Map<String, Long> currentTimeMeasures = new HashMap<>();

    @FunctionalInterface
    public interface ModifyingInvokable<T> {
        T invoke( String changeUser) throws  Exception; // NOSONAR
    }
    private final Logger logger;

    protected BaseWebService(Logger logger) {
        this.logger = logger;
    }

    protected abstract void assertAndRefreshToken(String token, SecureType secureType) throws HttpStatusException;
    protected abstract String getUserFromToken(String token) throws HttpStatusException;

    protected Response invokeRunnable(String token, SecureType secureType, ModifyingInvokable invokable) {
        try (AutoCloseable ignored = perform(invokable.getClass().getName() + ".Invoke()")) { // NOSONAR
            String changeUserName = "";
            if (secureType != SecureType.NONE) {
                assertAndRefreshToken(token, secureType);
                changeUserName = getUserFromToken(token);
            }
            Object o = invokable.invoke(changeUserName);
            return Response.ok(JsonGeneratorBase.getGson().toJson(o)).build();
        } catch( HttpStatusException hse ) {
            logger.error( "Caught internal exception:", hse);
            return Response.status(hse.getHttpStatus()).entity(JsonGeneratorBase.getGson().toJson(hse.getPayload())).build();
        } catch (Exception e) {
            logger.error("Caught unexpected Exception:", e);
            return Response.status(HttpStatus.INTERNAL_SERVER_ERROR_500).build();
        }
    }
    protected String getVersionString() {
        try {
            // determine static VersionInfo
            final Properties properties = new Properties();

            properties.load(BaseWebService.class.getClassLoader().getResourceAsStream("project.properties"));

            String beversion = properties.getProperty("backend.version");
            if( beversion.contains("$")) {
                beversion = "LOCAL-DEV";
            }
            return beversion;
        } catch (IOException e) {
            logger.error("Exception reading the properties file", e);
            throw new RuntimeException("Exception during start up"); // NOSONAR
        }
    }

    private void startProcessing(String methodName) {
        Long lStartingTime = System.currentTimeMillis();
        String lookupName = buildLookupId(methodName);
        currentTimeMeasures.put(lookupName, lStartingTime);

        logger.info(this.getClass().getName() + "." + lookupName + ": Start processing...");
    }

    protected AutoCloseable perform(final String methodName) {
        startProcessing(methodName);
        return () -> endProcessing(methodName);
    }

    private String buildLookupId(String func) {
        return func + "@@" + Thread.currentThread().getId();
    }

    private void endProcessing(String methodName) {
        String lookupName = buildLookupId(methodName);
        if (currentTimeMeasures.containsKey(lookupName)) {
            Long lStartingTime = currentTimeMeasures.get(lookupName);
            currentTimeMeasures.remove(lookupName);
            logger.info(
                    this.getClass().getName() + "." + lookupName + ": Finished processing in " +
                            (System.currentTimeMillis() - lStartingTime) + " ms");
        }

    }

}
