/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.eclipse.openk.api.RoleAccessDefinitionApi;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.common.util.ResourceLoaderBase;

public enum RoleAccessDefinitions {
    INSTANCE;

    private boolean isDirty = true;
    public interface RoleAccessManipulator {
        void apply(RoleAccessDefinitionApi roleAccessDefObj );
    }

    private Object synchLock = new Object();
    private  RoleAccessDefinitionApi roleAccessDefinition;


    public void setDirty() {
        synchronized (synchLock) {
            isDirty = true;
        }
    }

    public RoleAccessDefinitionApi getRoleAccessDefinition( RoleAccessManipulator manipulator ) {
        synchronized (synchLock) {
            if( isDirty && manipulator != null ) {
                load();
                manipulator.apply(this.roleAccessDefinition);
            }
            isDirty = false;
            return roleAccessDefinition;
        }
    }

    public void load() {
        synchronized (synchLock) {
            ResourceLoaderBase loaderBase = new ResourceLoaderBase();
            String roles = loaderBase.loadFromPath("roleAccessConfiguration/role-access-definition.json");
            roleAccessDefinition = JsonGeneratorBase.getGson().fromJson(roles, RoleAccessDefinitionApi.class);
        }
    }
}
