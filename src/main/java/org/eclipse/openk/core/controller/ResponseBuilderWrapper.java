/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import java.io.UnsupportedEncodingException;
import javax.ws.rs.core.Response;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.exceptions.HttpStatusException;

public enum ResponseBuilderWrapper {
    INSTANCE;
    private static final Logger LOGGER = Logger.getLogger(ResponseBuilderWrapper.class.getName());

    public Response.ResponseBuilder getResponseBuilder( String json ) throws HttpStatusException {
        return getResponseBuilder(jsonStringToBytes( json ));

    }

    private Response.ResponseBuilder getResponseBuilder(byte[] json) {
        return Response.status(HttpStatus.SC_OK).entity(json)
                .header("Content-Type", "application/json; charset=utf-8")
                .header("X-XSS-Protection", "1; mode = block")
                .header("X-DNS-Prefetch-Control", "off")
                .header("X-Content-Type-Options", "nosniff")
                .header("X-Frame-Options", "sameorigin")
                .header("Strict-Transport-Security", "max-age=15768000; includeSubDomains")
                .header("Cache-Control", "no-cache; no-store; must-revalidate")
                .header("Pragma", "no-cache")
                .header("Expires", "0")
                .header("Access-Control-Allow-Origin", "*");
    }

    private byte[] jsonStringToBytes( String jsonString ) throws HttpStatusException {
        try {
            return jsonString.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error in process grid", e);
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    public Response buildOKResponse(String jsonString) throws HttpStatusException {
        return getResponseBuilder( jsonStringToBytes( jsonString)).build();
    }


    public Response buildOKResponse(String jsonString, String sessionToken) throws HttpStatusException  {
        return getResponseBuilder(jsonStringToBytes(jsonString)).header(Globals.SESSION_TOKEN_TAG, sessionToken).build();
    }

}
