/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.api.*;
import org.eclipse.openk.common.mapper.GridMeasureMapper;
import org.eclipse.openk.common.mapper.generic.GenericApiToDbMapper;
import org.eclipse.openk.common.util.DateUtils;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.ProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmGrid;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.db.dao.AutoCloseEntityManager;
import org.eclipse.openk.db.dao.EntityHelper;
import org.eclipse.openk.db.dao.HTblGridMeasureDao;
import org.eclipse.openk.db.dao.RefVersionDao;
import org.eclipse.openk.db.dao.TblDocumentsDao;
import org.eclipse.openk.db.dao.TblGridMeasureDao;
import org.eclipse.openk.db.dao.TblLockDao;
import org.eclipse.openk.db.dao.TblMeasureDocumentsDao;
import org.eclipse.openk.db.dao.TblSingleGridmeasureDao;
import org.eclipse.openk.db.dao.TblStepsDao;
import org.eclipse.openk.db.model.*;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GridMeasureBackendController {
    protected static final Logger logger = Logger.getLogger(GridMeasureBackendController.class);
    private static PlgmGrid processGrid = new PlgmGrid();

    public VersionInfo getVersionInfo(String version ) {
        return getVersionInfoImpl(new RefVersionDao(), version);
    }

    public GridMeasure storeGridMeasure( String token, String changeuser, GridMeasure newMeasure ) throws HttpStatusException {
        try {
            ProcessState stateInDb = getDbProcessState( newMeasure.getId() );
            PlgmProcessSubject subject = PlgmProcessSubject.fromGridMeasure(newMeasure, changeuser);

            assertIsUserAllowedToStore(token, newMeasure, changeuser);

            subject.setStateInDb( stateInDb );
            subject.setJwt(token);

            processGrid.recover( subject ).start(() -> stateInDb);

            return subject.getGridMeasure();

        } catch (HttpStatusException hse) {
            logger.error("Failure in storeGridMeasure, Dataset locked or user is not authorized to store", hse);
            throw hse;
        }
        catch (ProcessException e) {
            if( e.getCause() instanceof HttpStatusException ) {
                throw (HttpStatusException)e.getCause();
            }
            logger.error("Error in process grid", e);
            throw new HttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR_500);
        }
    }

    public List<GridMeasure> getGridMeasures(String username, FilterObject filterObject)  {

        List<GridMeasure> vmList = new java.util.ArrayList<>();

        try (AutoCloseEntityManager em = createEm()) {
            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
            GridMeasureMapper gmMapper = new GridMeasureMapper();

            List<TblGridMeasure> mList = new ArrayList<>();

            if (filterObject != null) {
                mList = filterGridMeasures(username, filterObject, gmDao, mList);
            }
            else {
                mList = gmDao.getGridMeasuresInTx();
            }

            for (TblGridMeasure m : mList) {
                GridMeasure vm = gmMapper.mapToVModel(m);
                vmList.add(vm);
            }
        }
        return vmList;
    }

    private List<TblGridMeasure> filterGridMeasures(String username,FilterObject filterObject, TblGridMeasureDao gmDao, List<TblGridMeasure> mList) {
        //alle GMs
        if(!filterObject.isOnlyUsersGMsDesired()){
            if (filterObject.isCanceledStatusActive() && filterObject.isClosedStatusActive()) {
                mList = gmDao.getGridMeasuresInTx();
            } else if (filterObject.isCanceledStatusActive() && !filterObject.isClosedStatusActive()) {
                mList = gmDao.getGridMeasuresExcludingStatusInTx(PlgmProcessState.CLOSED.getStatusValue());
            } else if (!filterObject.isCanceledStatusActive() && filterObject.isClosedStatusActive()) {
                mList = gmDao.getGridMeasuresExcludingStatusInTx(PlgmProcessState.CANCELED.getStatusValue());
            } else if (!filterObject.isCanceledStatusActive() && !filterObject.isClosedStatusActive()) {
                mList = gmDao.getGridMeasuresExcludingStatusInTx(PlgmProcessState.CLOSED.getStatusValue(), PlgmProcessState.CANCELED.getStatusValue());
            }
        }
        //nur die den User betreffenden GMs
        else {
            if (filterObject.isCanceledStatusActive() && filterObject.isClosedStatusActive()) {
                mList = gmDao.getGridMeasuresByUserName(username);
            } else if (filterObject.isCanceledStatusActive() && !filterObject.isClosedStatusActive()) {
                mList = gmDao.getGridMeasuresByUserNameExcludingStatus(username, PlgmProcessState.CLOSED.getStatusValue());
            } else if (!filterObject.isCanceledStatusActive() && filterObject.isClosedStatusActive()) {
                mList = gmDao.getGridMeasuresByUserNameExcludingStatus(username, PlgmProcessState.CANCELED.getStatusValue());
            } else if (!filterObject.isCanceledStatusActive() && !filterObject.isClosedStatusActive()) {
                mList = gmDao.getGridMeasuresByUserNameExcludingStatus(username, PlgmProcessState.CLOSED.getStatusValue(), PlgmProcessState.CANCELED.getStatusValue());
            }
        }
        return mList;
    }

    public GridMeasure getGridMeasureById(Integer id)  throws HttpStatusException {

        GridMeasure gridMeasure = null;

        try (AutoCloseEntityManager em = createEm()) {
            em.getTransaction().begin();
            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
            TblSingleGridmeasureDao singleGmDao = createTblSingleGridmeasureDao(em);
            TblStepsDao stepsDao = createTblStepsDao(em);
            GridMeasureMapper gmMapper = new GridMeasureMapper();
            TblGridMeasure tblGridMeasure = gmDao.findByIdInTx(TblGridMeasure.class, (id));
            em.getTransaction().commit();

            if(tblGridMeasure != null) {
                List<TblSingleGridmeasure> listTblSingleGridmeasure = singleGmDao.getSingleGridmeasuresByGmIdInTx((id));
                gridMeasure = gmMapper.mapToVModel(tblGridMeasure, listTblSingleGridmeasure, stepsDao);
            }
            else {
             throw new HttpStatusException(HttpStatus.NOT_FOUND_404);
            }
        }
        return gridMeasure;
    }

    public List<HGridMeasure> getHistoricalStatusChangesById(Integer gmId)  {

        List<HGridMeasure> hGridMeasures = new java.util.ArrayList<>();

        try (AutoCloseEntityManager em = createEm()) {
            HTblGridMeasureDao hgmDao = createHistoricalTblGridMeasureDao(em);
            GridMeasureMapper gmMapper = new GridMeasureMapper();

            List<HTblGridMeasure> htblGridMeasures = hgmDao.getHistoricalGridMeasuresByIdInTx(gmId);
            if (htblGridMeasures != null) {
                Integer previousStatus = null;
                for (HTblGridMeasure hgm : htblGridMeasures) {
                    if (previousStatus != hgm.getFkRefGmStatus()) {
                        HGridMeasure vm = gmMapper.mapToVModel(hgm);
                        hGridMeasures.add(vm);
                        previousStatus = hgm.getFkRefGmStatus();
                    }
                }
            }
        }
        return hGridMeasures;
    }

    public List<String> getAffectedResourcesDistinct()  {

        try (AutoCloseEntityManager em = createEm()) {
            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
            return gmDao.getAffectedResourcesDistinct( PlgmProcessState.CANCELED.getStatusValue() );
        }
    }

    public List<String> getMailAddressesFromGridmeasures()  {
        return MailAddressCache.getInstance().getMailAddresses();
    }

    public List<Integer> getCurrentReminders(String token)  throws HttpStatusException {
        List<Integer> returnList = new ArrayList<>();
        List<Integer> relevantStatusForUserList = new ArrayList<>();
        Date now = new Date();

        List<Integer> relevantStatusList = new ArrayList<>();
        relevantStatusList.add(PlgmProcessState.NEW.getStatusValue());
        relevantStatusList.add(PlgmProcessState.APPLIED.getStatusValue());
        relevantStatusList.add(PlgmProcessState.FOR_APPROVAL.getStatusValue());

        List<String> relevantUserRolesList = getRelevantRolesForUser(token);

        if(!relevantUserRolesList.isEmpty()){
            relevantStatusForUserList = getRelevantStatusForRoles(relevantUserRolesList);
        }

        relevantStatusForUserList.retainAll(relevantStatusList);

        try (AutoCloseEntityManager em = createEm()) {
            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
            BackendSettings bSettings = BackendConfig.getInstance().getBackendSettings();
            Integer reminderPeriod = bSettings.getReminderPeriod();

            for(Integer status: relevantStatusForUserList){
                List<TblGridMeasure> gmList = gmDao.getGridMeasuresByStatusId(status);
                for(TblGridMeasure gm: gmList) {
                    //check auf Unterschreitung der reminderPeriod
                    long nowLong = now.getTime();
                    if( gm.getPlannedStarttimeFirstSinglemeasure() != null ) {
                        LocalDateTime dateTimeNow = LocalDateTime.now();
                        LocalDateTime localDateTimeEnd = dateTimeNow.plusHours(reminderPeriod);
                        LocalDateTime dateTimeToCompare = DateUtils.asLocalDateTime(gm.getPlannedStarttimeFirstSinglemeasure());

                        if (dateTimeNow.compareTo(dateTimeToCompare) <= 0 &&
                                localDateTimeEnd.compareTo(dateTimeToCompare) >= 0){
                            returnList.add(gm.getId());
                        }
                    }
                }
            }
        }

        return returnList;
    }

    public List<Integer> getExpiredReminders(String token)  throws HttpStatusException {
        List<Integer> returnList = new ArrayList<>();
        List<Integer> relevantStatusForUserList = new ArrayList<>();
        Date now = new Date();

        List<Integer> relevantStatusList = new ArrayList<>();
        relevantStatusList.add(PlgmProcessState.NEW.getStatusValue());
        relevantStatusList.add(PlgmProcessState.APPLIED.getStatusValue());
        relevantStatusList.add(PlgmProcessState.FOR_APPROVAL.getStatusValue());

        List<String> relevantUserRolesList = getRelevantRolesForUser(token);

        if(!relevantUserRolesList.isEmpty()){
            relevantStatusForUserList = getRelevantStatusForRoles(relevantUserRolesList);
        }

        relevantStatusForUserList.retainAll(relevantStatusList);

        try (AutoCloseEntityManager em = createEm()) {
            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);

            for(Integer status: relevantStatusForUserList){
                List<TblGridMeasure> gmList = gmDao.getGridMeasuresByStatusId(status);
                for(TblGridMeasure gm: gmList) {
                    //check auf Überschreitung der jetzigen Zeit
                    long nowLong = now.getTime();
                    if( gm.getPlannedStarttimeFirstSinglemeasure() != null ) {
                        long starttimeLong = gm.getPlannedStarttimeFirstSinglemeasure().getTime();
                        long diff = starttimeLong - nowLong;
                        if (diff < 0) {
                            returnList.add(gm.getId());
                        }
                    }
                }
            }
        }

        return returnList;
    }

    private List<String> getRelevantRolesForUser(String token) throws HttpStatusException{

        List<String> returnList = new ArrayList<>();

        ArrayList<String> listRelevantRoles = new ArrayList<>();
        listRelevantRoles.add("planned-policies-measureapplicant");
        listRelevantRoles.add("planned-policies-measureplanner");
        listRelevantRoles.add("planned-policies-measureapprover");

        //get roles from Token
        List<String> actUserRoles = TokenManager.getInstance().getUserRoles(token);

        //iterate User Roles and put relevant ones into returnList
        for (String aktUserRole : actUserRoles) {
            if(listRelevantRoles.contains(aktUserRole)){
                returnList.add(aktUserRole);
            }
        }

        return returnList;
    }

    private List<Integer> getRelevantStatusForRoles(List<String> userRolesList) {
        List<Integer> statusList = new ArrayList<>();

        //get Status for userRoles
        for(String aktUserRole: userRolesList) {
            List<Integer> statusForRoleList = getStatusByRole(aktUserRole);
            statusList.addAll(statusForRoleList);
        }

        return statusList;
    }

    public TblDocuments downloadGridMeasureAttachment(int gridmeasuereId) throws HttpStatusException {
        try (AutoCloseEntityManager em = createEm()) {
            TblDocumentsDao tblDocumentsDao = createTblDocumentsDao(em);
            return tblDocumentsDao.findByIdInTx(TblDocuments.class, gridmeasuereId);
        } catch (Exception e) {
            logger.error("Error in downloadGridMeasureAttachment", e);
            throw new HttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR_500);
        }

    }

    public List<Document> getGridMeasureAttachments(int gridmeasuereId) throws HttpStatusException {
        List<Document> documentList = new ArrayList<>();

        try {
            List<TblMeasureDocuments> measureDocumentsList = getAllTblMeasureDocumentsByGridmeasureId(gridmeasuereId);
            GenericApiToDbMapper mapper = new GenericApiToDbMapper();
            for (TblMeasureDocuments tblMeasureDocuments : measureDocumentsList) {
                TblDocuments currentDocument = tblMeasureDocuments.getTblDocuments();
                documentList.add(mapper.mapToViewModel(Document.class, currentDocument));
            }

        } catch (HttpStatusException e) {
            logger.error("Error in getGridMeasureAttachments", e);
            throw new HttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR_500);
        }

        return documentList;
    }


    public Document uploadDocument(Document document, String changeUser,int gridmeasuereId) throws HttpStatusException {
        Document retDocument = new Document();
        List<TblMeasureDocuments> measureDocumentsList = getAllTblMeasureDocumentsByGridmeasureId(gridmeasuereId);

        try (AutoCloseEntityManager em = createEm()) {

            String filenameUtf8 = document.getDocumentName();
            retDocument.setDocumentName(filenameUtf8);
            byte[] fileData = Base64.decodeBase64(document.getData());
            TblDocumentsDao tblDocumentsDao = createTblDocumentsDao(em);

            //check if document already exists
            TblDocuments documentToStore = getAlreadyExistingDocument(measureDocumentsList, filenameUtf8);

            //new and update always the same steps
            documentToStore.setDocument(fileData);
            documentToStore.setModUser(changeUser);
            documentToStore.setModDate(Date.from(Instant.now()));

            if (documentToStore.getId() != null){
                //update current: Optional aks user again to overwrite? wait for permission to update
                storeTblDocument(documentToStore, tblDocumentsDao);
            } else {
                //id == null: create new Document
                documentToStore.setDocumentName(filenameUtf8);
                documentToStore.setCreateUser(changeUser);
                documentToStore.setCreateDate(Date.from(Instant.now()));

                TblDocuments storedDocument = storeTblDocument(documentToStore, tblDocumentsDao);

                TblMeasureDocuments tblMeasureDocuments = createNewTblMeasureDocument(changeUser, gridmeasuereId, storedDocument);
                storeTblMeasureDocument(em, tblMeasureDocuments);
            }

            retDocument.setId(documentToStore.getId());

        } catch (Exception e) {
            logger.error("Error in uploadGridMeasureAttachments", e);
            throw new HttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR_500);
        }

        return retDocument;
    }

    private TblDocuments getAlreadyExistingDocument(List<TblMeasureDocuments> measureDocumentsList, String filenameUtf8) {
        TblDocuments documentToStore = new TblDocuments();
        for (TblMeasureDocuments tblMeasureDocuments : measureDocumentsList) {
            TblDocuments currentDocument = tblMeasureDocuments.getTblDocuments();

            if (currentDocument.getDocumentName().equals(filenameUtf8)) {
                documentToStore = currentDocument;
                break;
            }
        }
        return documentToStore;
    }

    public int deleteDocument(String moduser, int documentId) throws HttpStatusException {
        try (AutoCloseEntityManager em = createEm()) {
            TblMeasureDocumentsDao tblMeasureDocumentsDao = createTblMeasureDocumentsDao(em);
            TblDocumentsDao tblDocumentsDao = createTblDocumentsDao(em);

            tblMeasureDocumentsDao.getEM().getTransaction().begin();

            List<TblMeasureDocuments> mdList = tblMeasureDocumentsDao.getMeasureDocumentByDocumentId(documentId);
            for( TblMeasureDocuments mMDocuments : mdList) {
                mMDocuments.setModDate(Date.from(Instant.now()));
                mMDocuments.setModUser(moduser);
                tblMeasureDocumentsDao.mergeInTx(mMDocuments);
                tblMeasureDocumentsDao.removeInTx(mMDocuments, mMDocuments.getId());
            }

            TblDocuments mDocuments = tblDocumentsDao.findByIdInTx(TblDocuments.class,documentId);
            mDocuments.setModDate(Date.from(Instant.now()));
            mDocuments.setModUser(moduser);
            tblDocumentsDao.mergeInTx(mDocuments);
            tblDocumentsDao.removeInTx(mDocuments, documentId);

            tblMeasureDocumentsDao.getEM().getTransaction().commit();

            return documentId;
        }
        catch (HttpStatusException se) {
            logger.error("Error in deleteDocument", se);
            throw new HttpStatusException(se.getHttpStatus());
        }
        catch(Exception e) {
            logger.error("Error in deleteDocument", e);
            throw new HttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR_500);
        }

    }

    private List<TblMeasureDocuments> getAllTblMeasureDocumentsByGridmeasureId(int gridmeasuereId) throws HttpStatusException {
        try (AutoCloseEntityManager em = createEm()) {
            TblMeasureDocumentsDao tblMeasureDocumentsDao = createTblMeasureDocumentsDao(em);
            return tblMeasureDocumentsDao.getAllTblMeasureDocumentsById(gridmeasuereId);
        } catch (Exception e) {
            logger.error("Error in getAllTblMeasureDocumentsByGridmeasureId", e);
            throw new HttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR_500);
        }
    }

    private TblDocuments storeTblDocument(TblDocuments documentToStore, TblDocumentsDao tblDocumentsDao) throws Exception {
        tblDocumentsDao.getEM().getTransaction().begin();
        TblDocuments storedDocument = tblDocumentsDao.storeInTx(documentToStore);
        tblDocumentsDao.getEM().getTransaction().commit();
        return storedDocument;
    }

    private void storeTblMeasureDocument(AutoCloseEntityManager em, TblMeasureDocuments tblMeasureDocuments) throws Exception {
        TblMeasureDocumentsDao tblMeasureDocumentsDao = createTblMeasureDocumentsDao(em);
        tblMeasureDocumentsDao.getEM().getTransaction().begin();
        tblMeasureDocumentsDao.storeInTx(tblMeasureDocuments);
        tblMeasureDocumentsDao.getEM().getTransaction().commit();
    }

    private TblMeasureDocuments createNewTblMeasureDocument(String changeUser, int gridmeasuereId, TblDocuments storedDocument) {
        TblMeasureDocuments tblMeasureDocuments = new TblMeasureDocuments();
        tblMeasureDocuments.setTblDocuments(storedDocument);
        tblMeasureDocuments.setFkTblMeasure(gridmeasuereId);
        tblMeasureDocuments.setCreateUser(changeUser);
        tblMeasureDocuments.setCreateDate(Date.from(Instant.now()));
        tblMeasureDocuments.setModUser(changeUser);
        tblMeasureDocuments.setModDate(Date.from(Instant.now()));
        return tblMeasureDocuments;
    }

    public Lock checkLock(Integer key, String info) throws HttpStatusException{
        try (AutoCloseEntityManager em = createEm()) {
            TblLockDao lockDao = createTblLockDao(em);
            //check if lock already exists
            TblLock mLock = lockDao.getLock(key, info);
            if(mLock != null) {
                GenericApiToDbMapper mapper = new GenericApiToDbMapper();
                return mapper.mapToViewModel(Lock.class, mLock);
            }
            else{
                throw new HttpStatusException(HttpStatus.OK_200, "No lock", null);
            }
        }
    }

    private boolean isLocked(Integer key, String info, String userName){
        try (AutoCloseEntityManager em = createEm()) {
            TblLockDao lockDao = createTblLockDao(em);
            //check if lock already exists
            TblLock mLock = lockDao.getLock(key, info);
            return !(mLock == null || mLock.getUsername().equals(userName));
        }
    }

    public Lock createLock(String changeUser, Integer key, String info) throws Exception{
        try (AutoCloseEntityManager em = createEm()) {
            TblLockDao lockDao = createTblLockDao(em);
            GenericApiToDbMapper mapper = new GenericApiToDbMapper();
            //check if lock already exists
            TblLock mLock = lockDao.getLock(key, info);

            if (mLock != null && mLock.getUsername().equals(changeUser)) {
                //existing Lock from current user, update ModUser and ModDate
                mLock.setModUser(changeUser);
                mLock.setModDate(Date.from(Instant.now()));

                lockDao.getEM().getTransaction().begin();
                TblLock updatedLock = lockDao.storeInTx(mLock);
                lockDao.getEM().getTransaction().commit();

                return mapper.mapToViewModel(Lock.class, updatedLock);
            }
            else if (mLock != null && !mLock.getUsername().equals(changeUser)) {
                //existing Lock from another user
                mLock.setModUser(changeUser);
                mLock.setModDate(Date.from(Instant.now()));

                lockDao.getEM().getTransaction().begin();
                TblLock updatedLock = lockDao.storeInTx(mLock);
                lockDao.getEM().getTransaction().commit();

                Lock vmLock = mapper.mapToViewModel(Lock.class, updatedLock);
                throw new HttpStatusException(HttpStatus.CONFLICT_409, "Dataset is locked by another user", vmLock);
            }
            else {
                //mLock== null: no existing Lock for given key and info, create new Lock in DB
                mLock = new TblLock();
                mLock.setKey(key);
                mLock.setUsername(changeUser);
                mLock.setInfo(info);
                mLock.setCreateUser(changeUser);
                mLock.setCreateDate(Date.from(Instant.now()));

                lockDao.getEM().getTransaction().begin();
                TblLock newLock = lockDao.storeInTx(mLock);
                lockDao.getEM().getTransaction().commit();

                return mapper.mapToViewModel(Lock.class, newLock);
            }
        }
    }

    public Integer deleteLock(String changeUser, Integer key, String info, boolean bForce) throws Exception{
        try (AutoCloseEntityManager em = createEm()) {
            TblLockDao lockDao = createTblLockDao(em);
            TblLock mLock = lockDao.getLock(key, info);

            if(mLock != null && (bForce || mLock.getUsername().equals(changeUser))) {
                lockDao.getEM().getTransaction().begin();

                // touch the entity that we know, who unlocked an when->HIS-Table!
                mLock.setModUser(changeUser);
                mLock.setModDate(new Date(System.currentTimeMillis()));
                lockDao.storeInTx(mLock);

                // remove from db
                lockDao.removeInTx(mLock, mLock.getId());
                lockDao.getEM().getTransaction().commit();
                return key;
            }
            else {
                if( mLock == null ) {
                    return key;
                }
                else {
                    throw new HttpStatusException(HttpStatus.FORBIDDEN_403, "Cannot delete a lock of another user");
                }
            }
        }
    }


    public RoleAccessDefinitionApi recalcAndGetRoleAccessDefinition() {
        return RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(new GridConfigButtonManipulator(BackendConfig.getInstance().getBackendSettings().getBpmnGridConfig())::manipulateButtons);
    }

    public BackendSettings.BpmnGridConfig setGridConfig(
            Boolean isSkipForApproval,
            Boolean isEndAfterApproved,
            Boolean isSkipRequesting,
            Boolean isEndAfterReleased,
            Boolean isSkipInWork) {
        BackendSettings.BpmnGridConfig newConfig = BackendConfig.getInstance().getBackendSettings().getBpmnGridConfig();

        if( isSkipForApproval != null ) { newConfig.setSkipForApproval( isSkipForApproval ); }
        if( isEndAfterApproved != null ) { newConfig.setEndAfterApproved( isEndAfterApproved ); }
        if( isSkipRequesting != null ) { newConfig.setSkipRequesting( isSkipRequesting ); }
        if( isEndAfterReleased != null ) { newConfig.setEndAfterReleased( isEndAfterReleased); }
        if( isSkipInWork != null ) { newConfig.setSkipInWork( isSkipInWork ); }
        RoleAccessDefinitions.INSTANCE.setDirty();
        recalcAndGetRoleAccessDefinition();

        return newConfig;
    }


    private HashMap<String, List<Integer>> getUserRolesFromAccessDefinition() {
        HashMap<String, List<Integer>> rolesMap = new HashMap<>();
        RoleAccessDefinitionApi json  = recalcAndGetRoleAccessDefinition();
        for(RoleAccessDefinitionApi.EditRole role : json.getEditRoles()) {
            List<Integer> statuslist = intArrayToList(role.getGridMeasureStatusIds());
            rolesMap.put(role.getName(), statuslist );
        }
        return rolesMap;
    }

    private List<Integer> intArrayToList( int[] arr ) {
        List<Integer> list = new ArrayList<>();
        for( int val : arr ) {
            list.add(val);
        }
        return list;
    }

    private List<String> getRolesByStatus(Integer statusId){
        List <Integer> statusList;
        List <String> rolesList= new ArrayList<>();
        HashMap hmRoles = getUserRolesFromAccessDefinition();
        Iterator it = hmRoles.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry role = (Map.Entry)it.next();
            statusList = (List<Integer>)role.getValue();
            if (statusList.contains(statusId)) {
                rolesList.add(role.getKey().toString());
            }
            it.remove();
        }
        return rolesList;
    }

    private List<Integer> getStatusByRole(String userRole){
        List<Integer> statusList = new ArrayList<>();
        RoleAccessDefinitionApi json  = recalcAndGetRoleAccessDefinition();
        for(RoleAccessDefinitionApi.EditRole role: json.getEditRoles()) {
            if(role.getName().equals(userRole)) {
                int[] jsonStatusList = role.getGridMeasureStatusIds();
                for (int i : jsonStatusList ) {
                    statusList.add(i);
                }
            }
        }

        return statusList;
    }

    private List<String> getStornoRoles(){
        List<String> stornoRolesList = new ArrayList<>();
        RoleAccessDefinitionApi json  = recalcAndGetRoleAccessDefinition();
         String[] stornoRoles = json.getStornoSection().getStornoRoles();
         for(String role: stornoRoles){
             stornoRolesList.add(role); }
        return stornoRolesList;
    }

    private boolean isUserAllowedToCancel( List<String> actUserRoles){

            boolean isAllowedToCancel = false;
            List<String> stornoRoles = getStornoRoles();

            for (String role : actUserRoles) {
                if(stornoRoles.contains(role)) {
                    isAllowedToCancel = true;
                    break;
                }
            }
            return isAllowedToCancel;
    }

    private void assertIsUserAllowedToStore(String token, GridMeasure gridMeasure, String userName) throws HttpStatusException {
        //check GM locked?
        if (!isLocked(gridMeasure.getId(), "gridmeasure", userName)){

            //get DB status from GridMeasure
            Integer statusIdDB;
            if( gridMeasure.getId() != null ) {
                try (AutoCloseEntityManager em = createEm()) {
                    TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
                    TblGridMeasure gm = gmDao.findById(TblGridMeasure.class, gridMeasure.getId());
                    statusIdDB = gm.getFkRefGmStatus();
                }
            }
            else {
                statusIdDB = PlgmProcessState.NEW.getStatusValue();
            }

            //get roles from Token
            List<String> actUserRoles = TokenManager.getInstance().getUserRoles(token);

            //in case of status = 2 (Cancelled), check if user is allowed to cancel
            if(gridMeasure.getStatusId() == PlgmProcessState.CANCELED.getStatusValue()){
                if(isUserAllowedToCancel(actUserRoles)){
                    //user is allowed to cancel, all further checks are unnecessary in this case
                    return;
                }
                else{
                    logger.error("Failure in storeGridMeasure, User is not authorized to cancel a Gridmeasure");
                    throw new HttpStatusException(HttpStatus.FORBIDDEN_403);
                }
            }

            //in case of all other status:
            //get roles from access-definition where DB-status is mentioned
            List<String> listRoles = getRolesByStatus(statusIdDB);

            //check if one of these roles(from access-definition-roles) is in token
            for (String role : listRoles) {
                if(actUserRoles.contains(role)) {
                    return;
                }
            }
            logger.error("Failure in storeGridMeasure, User is not authorized to store");
            throw new HttpStatusException(HttpStatus.FORBIDDEN_403);
        }
        else {
            logger.error("Failure in storeGridMeasure, Dataset locked");
            throw new HttpStatusException(HttpStatus.LOCKED_423);
        }
    }

    private VersionInfo getVersionInfoImpl(RefVersionDao dao, String pomVersion) {
        RefVersion dbVersion = dao.getVersionInTx();
        VersionInfo vi = new VersionInfo();
        vi.setBackendVersion(pomVersion);

        if (dbVersion == null) {
            vi.setDbVersion("NO_DB");
        } else {
            vi.setDbVersion(dbVersion.getVersion());
        }
        return vi;
    }

    private ProcessState getDbProcessState( Integer measureId ) throws HttpStatusException {
        if( measureId == null ) {
            return PlgmProcessState.NEW;
        }

        try (AutoCloseEntityManager em = createEm()) {
            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
            TblGridMeasure gm = gmDao.findById(TblGridMeasure.class, measureId);
            if( gm == null ) {
                logger.error("getDbProcessState(): GridMeasure with Id="+measureId+" could not be found!");
                throw( new HttpStatusException(HttpStatus.NOT_FOUND_404));
            }
            return PlgmProcessState.fromValue(gm.getFkRefGmStatus());
        }
    }

    public List<String> getResponsiblesOnSiteFromSingleGridmeasures(){
        List<String> responsiblesOnSiteList = new ArrayList<>();
        Set<String> responsiblesOnSiteUniqueSet = new HashSet<>();

        try (AutoCloseEntityManager em = createEm()) {
            TblSingleGridmeasureDao gmDao = createTblSingleGridmeasureDao(em);
            responsiblesOnSiteList = gmDao.getResponsiblesOnSite();
        }

        responsiblesOnSiteUniqueSet.addAll(responsiblesOnSiteList);
        responsiblesOnSiteUniqueSet.remove(null);
        return new ArrayList<>(responsiblesOnSiteUniqueSet);
    }

    public List<String> getUserDepartmentsCreated()  {
        List<String> userDepartmentsList = new ArrayList<>();
        Set<String> userDepartmentsUniqueSet = new HashSet<>();

        try (AutoCloseEntityManager em = createEm()) {
            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
            userDepartmentsList = gmDao.getUserDepartmentsCreated();
        }

        userDepartmentsUniqueSet.addAll(userDepartmentsList);
        userDepartmentsUniqueSet.remove(null);
        return new ArrayList<>(userDepartmentsUniqueSet);
    }


    public List<String> getUserDepartmentsModified()  {
        List<String> userDepartmentsList = new ArrayList<>();
        Set<String> userDepartmentsUniqueSet = new HashSet<>();

        try (AutoCloseEntityManager em = createEm()) {
            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
            userDepartmentsList = gmDao.getUserDepartmentsModified();
        }

        userDepartmentsUniqueSet.addAll(userDepartmentsList);
        userDepartmentsUniqueSet.remove(null);
        return new ArrayList<>(userDepartmentsUniqueSet);
    }


    public List<String> getNetworkControlsFromSingleGridmeasures(){
        List<String> networkControlsList = new ArrayList<>();
        Set<String> networkControlsUniqueSet = new HashSet<>();

        try (AutoCloseEntityManager em = createEm()) {
            TblSingleGridmeasureDao sgmDao = createTblSingleGridmeasureDao(em);
            networkControlsList = sgmDao.getNetworkControls();
        }

        networkControlsUniqueSet.addAll(networkControlsList);
        networkControlsUniqueSet.remove(null);
        return new ArrayList<>(networkControlsUniqueSet);
    }

    public List<Calender> getCalender() {

        List<TblGridMeasure> mList;
        List<TblSingleGridmeasure> sgmList;
        List<Calender> calenderList = new ArrayList<>();

        try (AutoCloseEntityManager em = createEm()) {
            TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
            TblSingleGridmeasureDao sgmDao = createTblSingleGridmeasureDao(em);
            mList = gmDao.getGridMeasuresExcludingStatusInTx(PlgmProcessState.CLOSED.getStatusValue(), PlgmProcessState.CANCELED.getStatusValue());

            for (TblGridMeasure m : mList) {
                sgmList = sgmDao.getSingleGridmeasuresByGmIdInTx(m.getId());

                for (TblSingleGridmeasure sgm : sgmList) {

                    Calender calenderItem = new Calender();

                    calenderItem.setGridMeasureId(m.getId());
                    calenderItem.setGridMeasureTitle(m.getTitle());
                    calenderItem.setGridMeasureStatusId(m.getFkRefGmStatus());
                    calenderItem.setSingleGridMeasureId(sgm.getId());
                    calenderItem.setSingleGridMeasureTitle(sgm.getTitle());
                    calenderItem.setPlannedStarttimSinglemeasure(sgm.getPlannedStarttimeSinglemeasure());
                    calenderItem.setPlannedEndtimeSinglemeasure(sgm.getPlannedEndtimeSinglemeasure());

                    calenderList.add(calenderItem);
                }
            }
        }
        return calenderList;
    }

    public List<String> getUserDepartmentsResponsibleOnSite()  {
        List<String> userDepartmentsList = new ArrayList<>();
        Set<String> userDepartmentsUniqueSet = new HashSet<>();


        try (AutoCloseEntityManager em = createEm()) {
            TblSingleGridmeasureDao sgmDao = createTblSingleGridmeasureDao(em);
            userDepartmentsList = sgmDao.getUserDepartmentsResponsibleOnSite();
        }

        userDepartmentsUniqueSet.addAll(userDepartmentsList);
        userDepartmentsUniqueSet.remove(null);
        return new ArrayList<>(userDepartmentsUniqueSet);
    }



    protected TblSingleGridmeasureDao createTblSingleGridmeasureDao(EntityManager em) { return new TblSingleGridmeasureDao(em); }

    protected TblGridMeasureDao createTblGridMeasureDao(EntityManager em) { return new TblGridMeasureDao(em); }

    protected HTblGridMeasureDao createHistoricalTblGridMeasureDao(EntityManager em) { return new HTblGridMeasureDao(em); }

    protected TblStepsDao createTblStepsDao(EntityManager em) { return new TblStepsDao(em); }

    protected TblDocumentsDao createTblDocumentsDao(EntityManager em) { return new TblDocumentsDao(em); }

    protected TblMeasureDocumentsDao createTblMeasureDocumentsDao(EntityManager em) { return new TblMeasureDocumentsDao(em); }

    protected TblLockDao createTblLockDao(EntityManager em) { return new TblLockDao(em); }

    protected AutoCloseEntityManager createEm() {
        return new AutoCloseEntityManager(EntityHelper.getEMF().createEntityManager());
    }





}
