/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.apache.log4j.Logger;
import org.eclipse.openk.api.BackendSettings;
import org.eclipse.openk.api.RoleAccessDefinitionApi;
import org.eclipse.openk.common.util.SmallReflectionHelper;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GridConfigButtonManipulator {
    private static final Logger logger = Logger.getLogger(GridConfigButtonManipulator.class.getName());
    private final BackendSettings.BpmnGridConfig gridConfig;
    public GridConfigButtonManipulator(BackendSettings.BpmnGridConfig gridConfig) {
        this.gridConfig  = gridConfig;
    }

    public void manipulateButtons(RoleAccessDefinitionApi roleAccess) {
        for(RoleAccessDefinitionApi.Control control: roleAccess.getControls()) {
            List<String> newList = new LinkedList<>();

            for( String buttonTag: control.getActiveButtons()) {
                if( evaluationNeeded(buttonTag)) {
                    String newButtonTag = evaluateButtonTag(buttonTag);
                    if(!newButtonTag.isEmpty()) {
                        newList.add(newButtonTag);
                    }
                }
                else {
                    newList.add(buttonTag);
                }
            }
            String[] newArray = new String[newList.size()];
            control.setActiveButtons(newList.toArray(newArray));
        }
    }

    private boolean evaluationNeeded( String buttonTag ) {
        return buttonTag.startsWith("[");
    }

    private String evaluateButtonTag( String buttonTag ) {
        Pattern regexPattern = Pattern.compile("\\[(.*?)\\]");
        Set<String> allExpressions = new HashSet<>();
        Matcher matcher = regexPattern.matcher(buttonTag);
        boolean result = true;
        while( matcher.find()) {
            String expr = matcher.group();
            allExpressions.add(expr);
            result &= evalExpr(expr.replace("[", "").replace("]", ""));
        }
        if( result ) {
            String strResult = buttonTag;
            for( String s : allExpressions ) {
                strResult = strResult.replace( s, "" );
            }
            return strResult;
        }
        return "";
    }

    private boolean evalExpr( String expr ) {
        String[] splitty = expr.split(":");
        try {
            String result = SmallReflectionHelper.checkBoolField(this.gridConfig, splitty[0]) ? "TRUE" : "FALSE";
            return result.equalsIgnoreCase(splitty[1]);
        } catch (Exception e) {
            logger.warn("Error during expression evaluation", e);
        }
        return true;
    }
}
