/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import java.util.ArrayList;
import java.util.List;

public class MailAddressCache {
    private static final MailAddressCache MAILADDRESSCACHE_INSTANCE = new MailAddressCache();
    private final List<String> mailAddresses = new ArrayList<>();

    private MailAddressCache(){
    }

    public static MailAddressCache getInstance() {
        return MAILADDRESSCACHE_INSTANCE;
    }

    public List<String> getMailAddresses() {
        synchronized (mailAddresses){
            return mailAddresses;
        }
    }

    public void setMailAddresses(List<String> mailAddresses) {
        synchronized (this.mailAddresses){
            this.mailAddresses.clear();
            this.mailAddresses.addAll(mailAddresses);
        }
    }
}
