/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao;

import org.eclipse.openk.db.model.RefCostCenter;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class RefCostCenterDao extends GenericDaoJpa<RefCostCenter, Integer>{
    public RefCostCenterDao() {
        super();
    }

    public RefCostCenterDao(EntityManager em) {
        super(em);
    }

    public List<RefCostCenter> getCostCentersInTx() {
        try {
            String selectString = "from RefCostCenter t";
            Query q = getEM().createQuery(selectString);
            return (List<RefCostCenter>) q.getResultList();
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }
}
