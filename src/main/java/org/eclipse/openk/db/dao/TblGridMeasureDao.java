/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao;

import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.db.model.TblGridMeasure;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TblGridMeasureDao extends GenericDaoJpa<TblGridMeasure, Integer>{
    public static final String FK_REF_GM_STATUS = "fkRefGmStatus";

    public TblGridMeasureDao() {
        super();
    }

    public TblGridMeasureDao(EntityManager em) {
        super(em);
    }

    public List<TblGridMeasure> getGridMeasuresInTx() {
        try {
            String selectString = "from TblGridMeasure t";
            Query q = getEM().createQuery(selectString);

            return refreshModelCollection(q.getResultList());
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }

    public List<String> getAffectedResourcesDistinct( Integer stornoStatus ) {
        List<String> retList = new LinkedList<>();
        try {
            String selectString = "select t.affectedResource from TblGridMeasure t WHERE t.fkRefGmStatus <> :fkRefGmStatus group by t.affectedResource";
            Query q = getEM().createQuery(selectString);
            q.setParameter(FK_REF_GM_STATUS, stornoStatus );

            return (List<String>)q.getResultList();

        } catch (Exception t) {
            LOGGER.error(t.getMessage());
        }
        return retList;
    }

    public List<String> getMailAddressesFromGridmeasures() {
        List<String> retList = new LinkedList<>();
        try {
            String selectString = "select t.emailAddresses from TblGridMeasure t";
            Query q = getEM().createQuery(selectString);

            return (List<String>)q.getResultList();

        } catch (Exception t) {
            LOGGER.error(t.getMessage());
        }
        return retList;
    }

    public List<TblGridMeasure> getGridMeasuresByStatusId(Integer statusId) {
        try {
            String selectString = "from TblGridMeasure t WHERE t.fkRefGmStatus = :fkRefGmStatus";
            Query q = getEM().createQuery(selectString);
            q.setParameter(FK_REF_GM_STATUS, statusId);

            return refreshModelCollection(q.getResultList());
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }

    public List<TblGridMeasure> getGridMeasuresExcludingStatusInTx(Integer statusId) {
        try {
            String selectString = "from TblGridMeasure t WHERE t.fkRefGmStatus != :fkRefGmStatus";
            Query q = getEM().createQuery(selectString);
            q.setParameter(FK_REF_GM_STATUS, statusId);

            return refreshModelCollection(q.getResultList());
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }

    public List<TblGridMeasure> getGridMeasuresExcludingStatusInTx(Integer statusId1, Integer statusId2) {
        try {
            String selectString = "from TblGridMeasure t WHERE t.fkRefGmStatus NOT IN (:fkRefGmStatus1, :fkRefGmStatus2)";
            Query q = getEM().createQuery(selectString);
            q.setParameter("fkRefGmStatus1", statusId1);
            q.setParameter("fkRefGmStatus2", statusId2);

            return refreshModelCollection(q.getResultList());
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }


    public List<TblGridMeasure> getGridMeasuresByUserName(String username) {
        try {
            String selectString = "from TblGridMeasure t where t.id IN (select distinct t.id from TblGridMeasure t INNER JOIN HTblGridMeasure h ON t.id = h.id WHERE h.hUser = :UserName)";
            Query q = getEM().createQuery(selectString);
            q.setParameter("UserName", username);

            return refreshModelCollection(q.getResultList());
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }

    public List<TblGridMeasure> getGridMeasuresByUserNameExcludingStatus(String username, Integer statusId) {
        try {
            String selectString = "from TblGridMeasure t where t.id IN (select distinct t.id from TblGridMeasure t INNER JOIN HTblGridMeasure h ON t.id = h.id WHERE h.hUser = :UserName) AND t.fkRefGmStatus <> :fkRefGmStatus ";
            Query q = getEM().createQuery(selectString);
            q.setParameter("UserName", username);
            q.setParameter(FK_REF_GM_STATUS, statusId);

            return refreshModelCollection(q.getResultList());
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }

    public List<TblGridMeasure> getGridMeasuresByUserNameExcludingStatus(String username, Integer statusId1, Integer statusId2) {
        try {
            String selectString = "from TblGridMeasure t where t.id IN (select distinct t.id from TblGridMeasure t INNER JOIN HTblGridMeasure h ON t.id = h.id WHERE h.hUser = :UserName) AND t.fkRefGmStatus NOT IN (:fkRefGmStatus1, :fkRefGmStatus2)";
            Query q = getEM().createQuery(selectString);
            q.setParameter("UserName", username);
            q.setParameter("fkRefGmStatus1", statusId1);
            q.setParameter("fkRefGmStatus2", statusId2);

            return refreshModelCollection(q.getResultList());
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }

    public List<String> getUserDepartmentsCreated() {
        List<String> retList = new ArrayList<>();
        try {
            String selectString = "select t.createUserDepartment from TblGridMeasure t";
            Query q = getEM().createQuery(selectString);

            return (List<String>)q.getResultList();

        } catch (Exception t) {
            LOGGER.error(t.getMessage());
        }
        return retList;
    }

    public List<String> getUserDepartmentsModified() {
        List<String> retList = new ArrayList<>();
        try {
            String selectString = "select t.modUserDepartment from TblGridMeasure t";
            Query q = getEM().createQuery(selectString);

            return (List<String>)q.getResultList();

        } catch (Exception t) {
            LOGGER.error(t.getMessage());
        }
        return retList;
    }





}
