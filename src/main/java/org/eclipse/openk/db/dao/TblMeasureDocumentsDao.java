/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao;

import org.apache.http.HttpStatus;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.db.model.TblMeasureDocuments;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;


public class TblMeasureDocumentsDao extends GenericDaoJpa<TblMeasureDocuments, Integer>{
    public TblMeasureDocumentsDao() {
        super();
    }

    public TblMeasureDocumentsDao(EntityManager em) {
        super(em);
    }

    public List<TblMeasureDocuments> getAllTblMeasureDocumentsById(int gridmeasuereId) throws HttpStatusException {
        try {
            String selectString = "from TblMeasureDocuments t WHERE t.fkTblMeasure = :fkTblMeasure";
            Query q = getEM().createQuery(selectString);
            q.setParameter("fkTblMeasure",gridmeasuereId);
            return (List<TblMeasureDocuments>) q.getResultList();
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR, t.getMessage());
        }

    }

    public List<TblMeasureDocuments> getMeasureDocumentByDocumentId(int documentId) throws HttpStatusException {
        try {
            String selectString = "from TblMeasureDocuments t WHERE t.tblDocuments.id  = :tblDocuments";
            Query q = getEM().createQuery(selectString);
            q.setParameter("tblDocuments",documentId);

            if(!q.getResultList().isEmpty()) {
                return (List<TblMeasureDocuments>) q.getResultList();
            }
            else {
                String errText = "No documentMeasure Result available for the given documentId";
                LOGGER.info(errText);
                throw new HttpStatusException(HttpStatus.SC_NOT_FOUND);
            }

        }
        catch (HttpStatusException se) {
            LOGGER.error("getMeasureDocumentByDocumentId", se);
            throw new HttpStatusException(se.getHttpStatus());
        }
        catch (Exception t) {
            LOGGER.error(t.getMessage());
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR, t.getMessage());
        }
    }
}
