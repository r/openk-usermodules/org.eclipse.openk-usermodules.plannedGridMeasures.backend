/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.eclipse.openk.db.model.RefVersion;


public class RefVersionDao extends GenericDaoJpa<RefVersion, Integer> {
    public RefVersionDao() {
        super();
    }

    public RefVersionDao(EntityManager em) {
        super(em);
    }

    public RefVersion getVersionInTx() {
        try {
            String selectString = "from RefVersion t";
            Query q = getEM().createQuery(selectString);
            return (RefVersion) q.getSingleResult();
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null;
        }
    }
}
