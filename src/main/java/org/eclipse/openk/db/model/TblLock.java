/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the "TBL_LOCK database table.
 */
@Entity
@Table(name = "tbl_lock", schema = "public")
@NamedQuery(name = "TblLock.findAll", query = "SELECT t FROM TblLock t")
public class TblLock implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_LOCK_ID_SEQ")
    @SequenceGenerator(name = "TBL_LOCK_ID_SEQ", sequenceName = "TBL_LOCK_ID_SEQ", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "key")
    private Integer key;

    @Column(name = "username")
    private String username;

    @Column(name = "info")
    private String info;

    @Column(name = "create_user")
    private String createUser;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "mod_user")
    private String modUser;

    @Column(name = "mod_date")
    private Date modDate;

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public Integer getKey() { return key; }

    public void setKey(Integer key) { this.key = key; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getInfo() { return info; }

    public void setInfo(String info) { this.info = info; }

    public String getCreateUser() { return createUser; }

    public void setCreateUser(String createUser) { this.createUser = createUser; }

    public Date getCreateDate() { return createDate; }

    public void setCreateDate(Date createDate) { this.createDate = createDate; }

    public String getModUser() { return modUser; }

    public void setModUser(String modUser) { this.modUser = modUser; }

    public Date getModDate() { return modDate; }

    public void setModDate(Date modDate) { this.modDate = modDate; }
}
