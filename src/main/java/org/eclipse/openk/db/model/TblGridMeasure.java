/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the "TBL_GRIDMEASURE" database table.
 */
@Entity
@Table(name = "TBL_GRIDMEASURE", schema = "public")
@NamedQuery(name = "TblGridMeasure.findAll", query = "SELECT t FROM TblGridMeasure t")
public class TblGridMeasure implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_GRIDMEASURE_ID_SEQ")
    @SequenceGenerator(name = "TBL_GRIDMEASURE_ID_SEQ", sequenceName = "TBL_GRIDMEASURE_ID_SEQ", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "id_descriptive")
    private String idDescriptive;

    @Column(name = "title")
    private String title;

    @Column(name = "affected_resource")
    private String affectedResource;

    @Column(name = "remark")
    private String remark;

    @Column(name = "email_addresses")
    private String emailAddresses;

    @Column(name = "fk_ref_gm_status")
    private Integer fkRefGmStatus;

    @Column (name= "switching_object")
    private String switchingObject;

    @Column (name= "cost_center")
    private String costCenter;

    @Column (name= "approval_by")
    private String approvalBy;

    @Column (name= "area_of_switching")
    private String areaOfSwitching;

    @Column (name= "appointment_repetition")
    private String appointmentRepetition;

    @Column (name= "appointment_startdate")
    private Date appointmentStartdate;

    @Column (name= "appointment_numberof")
    private Integer appointmentNumberOf;

    @Column (name= "planned_starttime_first_singlemeasure")
    private Date plannedStarttimeFirstSinglemeasure;

    @Column (name= "endtime_gridmeasure")
    private Date endtimeGridmeasure;

    @Column (name= "time_of_reallocation")
    private String timeOfReallocation;

    @Column (name= "description")
    private String description;

    @Column (name= "fk_ref_branch")
    private Integer fkRefBranch;

    @Column (name= "fk_ref_branch_level")
    private Integer fkRefBranchLevel;

    @Column(name = "create_user")
    private String createUser;

    @Column(name = "create_user_department")
    private String createUserDepartment;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "mod_user")
    private String modUser;

    @Column(name = "mod_user_department")
    private String modUserDepartment;

    @Column(name = "mod_date")
    private Date modDate;


    public Integer getId() { return this.id; }

    public void setId(Integer id) { this.id = id; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getAffectedResource() { return affectedResource; }

    public void setAffectedResource(String affectedResource) { this.affectedResource = affectedResource; }

    public String getRemark() { return remark; }

    public void setRemark(String remark) { this.remark = remark; }

    public String getEmailAddresses() { return emailAddresses; }

    public void setEmailAddresses(String emailAddresses) { this.emailAddresses = emailAddresses; }

    public Integer getFkRefGmStatus() { return fkRefGmStatus; }

    public void setFkRefGmStatus(Integer fkRefGmStatus) { this.fkRefGmStatus = fkRefGmStatus; }

    public String getIdDescriptive() { return idDescriptive; }

    public void setIdDescriptive(String idDescriptive) { this.idDescriptive = idDescriptive; }

    public String getSwitchingObject() { return switchingObject; }

    public void setSwitchingObject(String switchingObject) { this.switchingObject = switchingObject; }

    public String getCostCenter() { return costCenter; }

    public void setCostCenter(String costCenter) { this.costCenter = costCenter; }

    public String getApprovalBy() { return approvalBy; }

    public void setApprovalBy(String approvalBy) { this.approvalBy = approvalBy; }

    public String getAreaOfSwitching() { return areaOfSwitching; }

    public void setAreaOfSwitching(String areaOfSwitching) { this.areaOfSwitching = areaOfSwitching; }

    public String getAppointmentRepetition() { return appointmentRepetition; }

    public void setAppointmentRepetition(String appointmentRepetition) { this.appointmentRepetition = appointmentRepetition; }

    public Date getAppointmentStartdate() { return appointmentStartdate; }

    public void setAppointmentStartdate(Date appointmentStartdate) { this.appointmentStartdate = appointmentStartdate; }

    public Integer getAppointmentNumberOf() { return appointmentNumberOf; }

    public void setAppointmentNumberOf(Integer appointmentNumberOf) { this.appointmentNumberOf = appointmentNumberOf; }

    public Date getPlannedStarttimeFirstSinglemeasure() { return plannedStarttimeFirstSinglemeasure; }

    public void setPlannedStarttimeFirstSinglemeasure(Date plannedStarttimeFirstSinglemeasure) { this.plannedStarttimeFirstSinglemeasure = plannedStarttimeFirstSinglemeasure; }

    public Date getEndtimeGridmeasure() { return endtimeGridmeasure; }

    public void setEndtimeGridmeasure(Date endtimeGridmeasure) { this.endtimeGridmeasure = endtimeGridmeasure; }

    public String getTimeOfReallocation() { return timeOfReallocation; }

    public void setTimeOfReallocation(String timeOfReallocation) { this.timeOfReallocation = timeOfReallocation; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public Integer getFkRefBranch() { return fkRefBranch; }

    public void setFkRefBranch(Integer fkRefBranch) { this.fkRefBranch = fkRefBranch; }

    public Integer getFkRefBranchLevel() { return fkRefBranchLevel; }

    public void setFkRefBranchLevel(Integer fkRefBranchLevel) { this.fkRefBranchLevel = fkRefBranchLevel; }

    public String getCreateUser() { return createUser; }

    public void setCreateUser(String createUser) { this.createUser = createUser; }

    public String getCreateUserDepartment() { return createUserDepartment; }

    public void setCreateUserDepartment(String createUserDepartment) { this.createUserDepartment = createUserDepartment; }

    public Date getCreateDate() { return createDate; }

    public void setCreateDate(Date createDate) { this.createDate = createDate; }

    public String getModUser() { return modUser; }

    public void setModUser(String modUser) { this.modUser = modUser; }

    public String getModUserDepartment() { return modUserDepartment; }

    public void setModUserDepartment(String modUserDepartment) { this.modUserDepartment = modUserDepartment; }

    public Date getModDate() { return modDate; }

    public void setModDate(Date modDate) { this.modDate = modDate; }

}
