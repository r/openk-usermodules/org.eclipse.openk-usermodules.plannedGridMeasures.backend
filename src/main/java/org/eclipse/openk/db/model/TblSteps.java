/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the "TBL_STEPS" database table.
 */
@Entity
@Table(name = "TBL_STEPS", schema = "public")
@NamedQuery(name = "TblSteps.findAll", query = "SELECT t FROM TblSteps t")
public class TblSteps implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_STEPS_ID_SEQ")
    @SequenceGenerator(name = "TBL_STEPS_ID_SEQ", sequenceName = "TBL_STEPS_ID_SEQ", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "sortorder")
    private Integer sortorder;

    @Column(name = "switching_object")
    private String switchingObject;

    @Column(name = "type")
    private String type;

    @Column(name = "present_time")
    private String presentTime;

    @Column(name = "present_state")
    private String presentState;

    @Column(name = "target_state")
    private String targetState;

    @Column(name = "operator")
    private String operator;

    @Column (name= "fk_tbl_single_gridmeasure")
    private Integer fkTblSingleGridmeasure;

    @Column (name= "create_user")
    private String createUser;

    @Column (name= "create_date")
    private Date createDate;

    @Column (name= "mod_user")
    private String modUser;

    @Column (name= "mod_date")
    private Date modDate;



    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public Integer getSortorder() { return sortorder; }

    public void setSortorder(Integer sortorder) { this.sortorder = sortorder; }

    public String getSwitchingObject() {
        return switchingObject;
    }

    public void setSwitchingObject(String switchingObject) {
        this.switchingObject = switchingObject;
    }

    public static long getSerialVersionUID() { return serialVersionUID; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getPresentTime() { return presentTime; }

    public void setPresentTime(String presentTime) { this.presentTime = presentTime; }

    public String getPresentState() { return presentState; }

    public void setPresentState(String presentState) { this.presentState = presentState; }

    public String getOperator() { return operator; }

    public void setOperator(String operator) { this.operator = operator; }

    public String getTargetState() {
        return targetState;
    }

    public void setTargetState(String targetState) {
        this.targetState = targetState;
    }

    public Integer getFkTblSingleGridmeasure() {
        return fkTblSingleGridmeasure;
    }

    public void setFkTblSingleGridmeasure(Integer fkTblSingleGridmeasure) { this.fkTblSingleGridmeasure = fkTblSingleGridmeasure; }

    public String getCreateUser() { return createUser; }

    public void setCreateUser(String createUser) { this.createUser = createUser; }

    public Date getCreateDate() { return createDate; }

    public void setCreateDate(Date createDate) { this.createDate = createDate; }

    public String getModUser() { return modUser; }

    public void setModUser(String modUser) { this.modUser = modUser; }

    public Date getModDate() { return modDate; }

    public void setModDate(Date modDate) { this.modDate = modDate; }
}

