/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

public class RoleAccessDefinitionApi {

    public static class EditRole {
        private String name;
        private int[] gridMeasureStatusIds;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int[] getGridMeasureStatusIds() {
            return gridMeasureStatusIds;
        }

        public void setGridMeasureStatusIds(int[] gridMeasureStatusIds) {
            this.gridMeasureStatusIds = gridMeasureStatusIds;
        }
    }

    public static class Control {
        private int gridMeasureStatusId;
        private String[] activeButtons;
        private String[] inactiveFields;

        public int getGridMeasureStatusId() {
            return gridMeasureStatusId;
        }

        public void setGridMeasureStatusId(int gridMeasureStatusId) {
            this.gridMeasureStatusId = gridMeasureStatusId;
        }

        public String[] getActiveButtons() {
            return activeButtons;
        }

        public void setActiveButtons(String[] activeButtons) {
            this.activeButtons = activeButtons;
        }

        public String[] getInactiveFields() {
            return inactiveFields;
        }

        public void setInactiveFields(String[] inactiveFields) {
            this.inactiveFields = inactiveFields;
        }
    }

    public static class StornoSection {
        private String[] stornoRoles;

        public String[] getStornoRoles() { return stornoRoles; }

        public void setStornoRoles(String[] stornoRoles) { this.stornoRoles = stornoRoles; }
    }

    public static class DuplicateSection {
        private String[] duplicateRoles;

        public String[] getDuplicateRoles() {
            return duplicateRoles;
        }

        public void setDuplicateRoles(String[] duplicateRoles) {
            this.duplicateRoles = duplicateRoles;
        }
    }

    private EditRole[] editRoles;
    private Control[] controls;
    private StornoSection stornoSection;
    private DuplicateSection duplicateSection;

    public EditRole[] getEditRoles() {
        return editRoles;
    }

    public void setEditRoles(EditRole[] editRoles) {
        this.editRoles = editRoles;
    }

    public Control[] getControls() {
        return controls;
    }

    public void setControls(Control[] controls) { this.controls = controls; }

    public StornoSection getStornoSection() { return stornoSection; }

    public void setStornoSection (StornoSection stornoSection) { this.stornoSection = stornoSection; }

    public DuplicateSection getDuplicateSection() {
        return duplicateSection;
    }

    public void setDuplicateSection(DuplicateSection duplicateSection) {
        this.duplicateSection = duplicateSection;
    }
}
