/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

public class FilterObject {

    private boolean onlyUsersGMsDesired;

    private boolean isClosedStatusActive;

    private boolean isCanceledStatusActive;

    public boolean isClosedStatusActive() {
        return isClosedStatusActive;
    }

    public void setClosedStatusActive(boolean closedStatusActive) {
        isClosedStatusActive = closedStatusActive;
    }

    public boolean isCanceledStatusActive() {
        return isCanceledStatusActive;
    }

    public void setCanceledStatusActive(boolean canceledStatusActive) {
        isCanceledStatusActive = canceledStatusActive;
    }

    public boolean isOnlyUsersGMsDesired() {return onlyUsersGMsDesired; }

    public void setOnlyUsersGMsDesired(boolean onlyUsersGMsDesired) {this.onlyUsersGMsDesired = onlyUsersGMsDesired; }
}
