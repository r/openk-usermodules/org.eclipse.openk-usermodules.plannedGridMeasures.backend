/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelClass;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelField;
import org.eclipse.openk.db.model.RefGmStatus;

@MapDbModelClass(classType = RefGmStatus.class)
public class GmStatus {

    @JsonProperty
    @MapDbModelField
    private Integer id;

    @JsonProperty
    @MapDbModelField
    private String name;

    @JsonProperty
    @MapDbModelField
    private String colorCode;


    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getColorCode() { return colorCode; }

    public void setColorCode(String colorCode) { this.colorCode = colorCode; }
}
