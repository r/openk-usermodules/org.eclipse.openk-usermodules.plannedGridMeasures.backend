/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BackendSettings {

  public static class BpmnGridConfig {
    public static final String SKIP_FOR_APPROVAL = "skipForApproval";
    public static final String END_AFTER_APPROVED = "endAfterApproved";
    public static final String SKIP_REQUESTING = "skipRequesting";
    public static final String END_AFTER_RELEASED = "endAfterReleased";
    public static final String SKIP_IN_WORK = "skipInWork";

    @JsonProperty
    private boolean skipForApproval;

    @JsonProperty
    private boolean endAfterApproved;

    @JsonProperty
    private boolean skipRequesting;

    @JsonProperty
    private boolean endAfterReleased;

    @JsonProperty
    private boolean skipInWork;

    public boolean isSkipForApproval() {
      return skipForApproval;
    }

    public void setSkipForApproval(boolean skipForApproval) {
      this.skipForApproval = skipForApproval;
    }

    public boolean isEndAfterApproved() {
      return endAfterApproved;
    }

    public void setEndAfterApproved(boolean endAfterApproved) {
      this.endAfterApproved = endAfterApproved;
    }

    public boolean isSkipRequesting() {
      return skipRequesting;
    }

    public void setSkipRequesting(boolean skipRequesting) {
      this.skipRequesting = skipRequesting;
    }

    public boolean isEndAfterReleased() {
      return endAfterReleased;
    }

    public void setEndAfterReleased(boolean endAfterReleased) {
      this.endAfterReleased = endAfterReleased;
    }

    public boolean isSkipInWork() {
      return skipInWork;
    }

    public void setSkipInWork(boolean skipInWork) {
      this.skipInWork = skipInWork;
    }
  }

  public static class AppointmentRepetition {

    @JsonProperty
    private Integer id;

    @JsonProperty
    private String name;

    public Integer getId() {
      return id;
    }

    public void setId(Integer id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }
  }

  @JsonProperty
  private Integer reminderPeriod;

  @JsonProperty
  private String emailTemplateAddressesForNotification;

  @JsonProperty
  private Integer reloadMailAddressesInMin;

  private BpmnGridConfig bpmnGridConfig;

  @JsonProperty
  private AppointmentRepetition[] appointmentRepetition;

  public Integer getReminderPeriod() {
    return reminderPeriod;
  }

  public void setReminderPeriod(Integer reminderPeriod) {
    this.reminderPeriod = reminderPeriod;
  }

  public Integer getReloadMailAddressesInMin() { return reloadMailAddressesInMin; }

  public void setReloadMailAddressesInMin(Integer reloadMailAddressesInMin) { this.reloadMailAddressesInMin = reloadMailAddressesInMin; }

  public BpmnGridConfig getBpmnGridConfig() {
    return bpmnGridConfig;
  }

  public void setBpmnGridConfig(BpmnGridConfig bpmnGridConfig) {
    this.bpmnGridConfig = bpmnGridConfig;
  }

  public String getEmailTemplateAddressesForNotification() { return emailTemplateAddressesForNotification; }

  public void setEmailTemplateAddressesForNotification(String emailTemplateAddressesForNotification) { this.emailTemplateAddressesForNotification = emailTemplateAddressesForNotification; }

  public AppointmentRepetition[] getAppointmentRepetition() {
    return appointmentRepetition;
  }

  public void setAppointmentRepetition(AppointmentRepetition[] appointmentRepetition) {
    this.appointmentRepetition = appointmentRepetition;
  }
}
