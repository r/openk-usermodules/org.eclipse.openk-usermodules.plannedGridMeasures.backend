/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.api.mail;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

public class Email {

  private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

  private static Pattern pattern = Pattern.compile(EMAIL_PATTERN);

  private static final String MAIL_ENCODING = "UTF-8";

  private static final Logger LOGGER = Logger.getLogger(Email.class.getName());

  private final Properties props = new Properties();
  private final MimeMultipart multipart = new MimeMultipart();
  private final MimeMessage message;
  private final Session session;

  public Email(String smtpHost, String port) {
    props.put("mail.smtp.auth", "false");
    props.put("mail.smtp.host", smtpHost);
    props.put("mail.smtp.port", port);
    session = Session.getInstance(props);
    message = new MimeMessage(session);
  }

  public void addText(String text) throws MessagingException {
    MimeBodyPart textBody = new MimeBodyPart();
    textBody.setText(text, MAIL_ENCODING);
    textBody.setDisposition(MimeBodyPart.INLINE);
    multipart.addBodyPart(textBody);
  }

  public void setFrom(String emailSender) throws MessagingException {
    try {
      boolean validRecipient = validateEmailAddress(emailSender);
      if (!validRecipient) {
        LOGGER.error("Invalid email-addresse for sender: " + emailSender);
        throw new MessagingException();
      }
      message.setFrom(new InternetAddress(emailSender));
    } catch (MessagingException e) {
      LOGGER.error("setFrom MessagingException caught()", e);
      throw e;
    }
  }

  public void addRecipient(String recipients) throws MessagingException {
    addRecipient(recipients, Message.RecipientType.TO);
  }

  public void addCC(String recipients) throws MessagingException {
    addRecipient(recipients, Message.RecipientType.CC);
  }

  public void addRecipient(String recipients, RecipientType recipientType) throws MessagingException {
    try {
      String checkedRecipients = "";
      if(recipients != null) {
        recipients = recipients.trim();
        recipients = recipients.replace(";", ",");
        String[] recipientsSplit = recipients.split(",");

        for (String recipient : recipientsSplit) {
          if (!validateEmailAddress(recipient.trim())) {
            LOGGER.error("Invalid email-addresse: " + recipients);
            throw new MessagingException();
          }
        }
        checkedRecipients = String.join(",", recipientsSplit);

        //Array of recipients must include only unique recipients => use of a Set
        InternetAddress[] mailAddresses = InternetAddress.parse(checkedRecipients);
        Set<InternetAddress> mailAddressesUniqueSet = new HashSet<>();
        mailAddressesUniqueSet.addAll(Arrays.asList(mailAddresses));
        InternetAddress[] mailAddressesFiltered = mailAddressesUniqueSet.toArray(new InternetAddress[mailAddressesUniqueSet.size()]);

        message.addRecipients(recipientType, mailAddressesFiltered);
        LOGGER.debug("Email-addresse successfully set: " + checkedRecipients);
      }
    } catch (MessagingException e) {
      LOGGER.error("addRecipient MessagingException caught()", e);
      throw e;
    }
  }

  public void setSubject(String subjectText) {
    try {
      message.setSubject(subjectText, MAIL_ENCODING);
    } catch (MessagingException e) {
      LOGGER.error("MessagingException caught()", e);
    }
  }

  public void setContent() {
    try {
      message.setContent(multipart);
    } catch (MessagingException e) {
      LOGGER.error("MessagingException caught()", e);
    }
  }

  public boolean send() {
    LOGGER.debug("send() is called");
    boolean ret = false;

    try {
      Transport.send(message);
      LOGGER.debug("send() finished");
      ret = true;
    } catch (MessagingException e) {
      LOGGER.error("send MessagingException caught()", e);
    }
    return ret;
  }

  public MimeMessage getMessage() {
    return message;
  }

  public static boolean validateEmailAddress(final String hex) {

    Matcher matcher = pattern.matcher(hex);
    return matcher.matches();

  }
}
