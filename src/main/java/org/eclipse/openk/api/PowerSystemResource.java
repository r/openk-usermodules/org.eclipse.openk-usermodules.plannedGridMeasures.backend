/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PowerSystemResource {

  @JsonProperty
  private  String cimId;

  @JsonProperty
  private  String cimName;

  @JsonProperty
  private  String cimDescription;

  public PowerSystemResource() {
    // constructor
  }

  public String getCimId() {
    return cimId;
  }

  public void setCimId(String cimId) {
    this.cimId = cimId;
  }

  public String getCimName() {
    return cimName;
  }

  public void setCimName(String cimName) {
    this.cimName = cimName;
  }

  public String getCimDescription() {
    return cimDescription;
  }

  public void setCimDescription(String cimDescription) {
    this.cimDescription = cimDescription;
  }
}
