/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.resources;

import org.apache.log4j.Logger;
import org.eclipse.openk.auth2.util.JwtHelper;
import org.eclipse.openk.core.controller.BaseWebService;
import org.eclipse.openk.core.controller.TokenManager;
import org.eclipse.openk.core.exceptions.HttpStatusException;

public class BaseResource extends BaseWebService {

    private final boolean developMode;
    private static final String LET_ME_IN = "LET_ME_IN";

    public BaseResource(Logger logger) {
        super(logger);

        String versionString = this.getVersionString();
        developMode = versionString.contains("DEVELOP") || versionString.contains("SNAPSHOT");
    }


    @Override
    protected void assertAndRefreshToken(String token, SecureType secureType) throws HttpStatusException {
        if (isBackdoor(token)) {
            return;
        }
        TokenManager.getInstance().checkAut(token);
        TokenManager.getInstance().checkAutLevel(token, secureType);
    }

    @Override
    public String getUserFromToken(String token) throws HttpStatusException {
        if( isBackdoor(token)) {
            return "default_backdoor_user";
        }
        return JwtHelper.getJwtPayload(token).getPreferredUsername();
    }

    protected boolean isDevelopMode() {
        return developMode;
    }

    private boolean isBackdoor(String token) {
        // backdoor is only available when the version(POM) contains "DEVELOP" or "SNAPSHOT"
        return developMode && LET_ME_IN.equals(token);
    }

}
