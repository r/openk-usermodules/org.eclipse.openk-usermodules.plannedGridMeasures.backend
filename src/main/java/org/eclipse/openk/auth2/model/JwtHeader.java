/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.auth2.model;

public class JwtHeader {
    private String alg;
    private String typ;
    private String kid;

    public String getAlg() { return alg; }
    public void setAlg(String alg) { this.alg = alg; }
    public String getTyp() { return typ; }
    public void setTyp(String typ) { this.typ = typ; }
    public String getKid() { return kid; }
    public void setKid(String kid) { this.kid = kid; }
}
