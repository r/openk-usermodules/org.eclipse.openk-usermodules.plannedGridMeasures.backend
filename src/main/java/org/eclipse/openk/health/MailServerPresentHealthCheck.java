/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.health;
import com.codahale.metrics.health.HealthCheck;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.PlannedGridMeasuresConfiguration.EmailConfiguration;
import java.net.Socket;

public class MailServerPresentHealthCheck extends HealthCheck {

    @Override
    protected Result check() throws Exception {

        EmailConfiguration emailConfiguration = BackendConfig.getInstance().getEmailConfiguration();

        try (Socket testSocket = new Socket(emailConfiguration.getSmtpHost(), Integer.parseInt(emailConfiguration.getPort()))){

            if (testSocket.isConnected()){
                return Result.healthy();
            }
        else{
                return Result.unhealthy("Mailserver not ready");
            }
        }
    }
}
