/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.health;

import com.codahale.metrics.health.HealthCheck;
import org.eclipse.openk.api.VersionInfo;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.resources.PlannedGridMeasuresResource;

public class DBIsPresentHealthCheck extends HealthCheck {
    @Override
    protected Result check() throws Exception {
        VersionInfo vi = getVersionInfoFromResource();

        if( vi != null && !vi.getBackendVersion().isEmpty() && !vi.getDbVersion().isEmpty() &&
                !vi.getDbVersion().contains("NO_DB") ) {
            return Result.healthy();
        }
        else {
            return Result.unhealthy( vi == null || vi.getDbVersion().isEmpty() ||
                    vi.getDbVersion().contains("NO_DB") ? "DB not ready" : "service not ready");
        }
    }

    protected VersionInfo getVersionInfoFromResource(){
        return JsonGeneratorBase.getGson().fromJson(
                new PlannedGridMeasuresResource().getVersionInfo().getEntity().toString(),
                VersionInfo.class);

    }
}
