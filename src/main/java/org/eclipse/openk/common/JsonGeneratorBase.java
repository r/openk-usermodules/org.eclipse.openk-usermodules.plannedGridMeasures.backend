/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Date;

public class JsonGeneratorBase {
    private JsonGeneratorBase() {}
    public static Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new GsonUTCDateAdapter())
                .disableHtmlEscaping()
                .create();
    }
}
