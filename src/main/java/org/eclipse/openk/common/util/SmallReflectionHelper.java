/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.common.util;

import java.lang.reflect.Field;

public class SmallReflectionHelper {
    private SmallReflectionHelper() {}

    public static boolean checkBoolField(Object objectToCheck, String boolFieldName) throws NoSuchFieldException, IllegalAccessException {
        Class clz = objectToCheck.getClass();
        Field searchedField = clz.getDeclaredField( boolFieldName);
        searchedField.setAccessible(true);
        return searchedField.getBoolean(objectToCheck);
    }
}
