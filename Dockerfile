FROM myserver:withproxy
#linux 16.04
MAINTAINER Dimitris

WORKDIR microservices

RUN git clone http://172.18.22.160:8880/gitblit-1.8.0/r/oK/PlannedGridMeasures/mics-backend.git  && cd mics-backend && git checkout DEVELOP_BE

WORKDIR mics-backend

RUN mvn install -DskipTests

WORKDIR /

# COPY C:/\Users/\Administrator/\Projekt/\my_wrapper_script.sh my_wrapper_script.sh
COPY my_wrapper_script.sh my_wrapper_script.sh
RUN sed -i -e 's/\r$//' my_wrapper_script.sh
CMD ./my_wrapper_script.sh

EXPOSE 9050 9051


