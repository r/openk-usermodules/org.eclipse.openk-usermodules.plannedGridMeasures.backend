-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2018 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------
--INSERT new Foreign-key Constraints into Table TBL_GRIDMEASURES
--(ref_cost_center, ref_branch, ref_branch_level)

-- ---------------------------------------------
-- DROPS
-- ---------------------------------------------
DROP TABLE IF EXISTS public.REF_USER_SETTINGS CASCADE;
DROP SEQUENCE IF EXISTS public.REF_USER_SETTINGS_ID_SEQ;

DROP TABLE IF EXISTS public.REF_USER_DEPARTMENT CASCADE;
DROP SEQUENCE IF EXISTS public.REF_USER_DEPARTMENT_ID_SEQ;

DROP TABLE IF EXISTS public.REF_COST_CENTER CASCADE;
DROP SEQUENCE IF EXISTS public.REF_COST_CENTER_ID_SEQ;

DROP TABLE IF EXISTS public.TBL_GRIDMEASURE CASCADE;
DROP SEQUENCE IF EXISTS public.TBL_GRIDMEASURE_ID_SEQ;

DROP TABLE IF EXISTS public.REF_BRANCH CASCADE;
DROP SEQUENCE IF EXISTS public.REF_BRANCH_ID_SEQ;

DROP TABLE IF EXISTS public.REF_BRANCH_LEVEL CASCADE;
DROP SEQUENCE IF EXISTS public.REF_BRANCH_LEVEL_ID_SEQ;

DROP TABLE IF EXISTS public.REF_GM_STATUS CASCADE;
DROP SEQUENCE IF EXISTS public.REF_GM_STATUS_ID_SEQ;

-- ---------------------------------------------
-- TABLE REF_USER_SETTINGS
-- ---------------------------------------------

CREATE SEQUENCE public.ref_user_settings_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 4
  CACHE 1;
ALTER TABLE public.ref_user_settings_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.ref_user_settings
(
  id integer NOT NULL DEFAULT nextval('ref_user_settings_id_seq'::regclass),
  username character varying(50) NOT NULL,
  setting_type character varying(50),
  value character varying,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone NOT NULL,
  CONSTRAINT ref_user_settings_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.ref_user_settings
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.ref_user_settings TO plgm_service;

-- ---------------------------------------------
-- TABLE REF_USER_DEPARTMENT
-- ---------------------------------------------

CREATE SEQUENCE public.REF_USER_DEPARTMENT_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 3
  CACHE 1;
ALTER TABLE public.REF_USER_DEPARTMENT_ID_SEQ
  OWNER TO plgm_service;


CREATE TABLE public.REF_USER_DEPARTMENT
(
  id integer NOT NULL DEFAULT nextval('REF_USER_DEPARTMENT_ID_SEQ'::regclass),
  name character varying(50) NOT NULL,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone NOT NULL,
  CONSTRAINT ref_user_department_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.REF_USER_DEPARTMENT
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.REF_USER_DEPARTMENT TO plgm_service;


INSERT INTO public.ref_user_department(
            name, create_user, create_date, mod_user, mod_date)
    VALUES ('Schnelle Truppe', 'testuser', '12.04.2018','testuser', '12.04.2018');
INSERT INTO public.ref_user_department(
            name, create_user, create_date, mod_user, mod_date)
    VALUES ('Abt. Aufsicht', 'testuser', '12.04.2018','testuser', '12.04.2018');
INSERT INTO public.ref_user_department(
            name, create_user, create_date, mod_user, mod_date)
    VALUES ('Abt. Wartung', 'testuser', '12.04.2018','testuser', '12.04.2018');

-- ---------------------------------------------
-- TABLE REF_COST_CENTER
-- ---------------------------------------------

CREATE SEQUENCE public.REF_COST_CENTER_ID_SEQ
   INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.REF_COST_CENTER_ID_SEQ
  OWNER TO plgm_service;


CREATE TABLE public.REF_COST_CENTER
(
  id integer NOT NULL DEFAULT nextval('REF_COST_CENTER_id_seq'::regclass),
  name character varying(50) NOT NULL,
  description character varying(255),
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone NOT NULL,
  CONSTRAINT REF_COST_CENTER_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.REF_COST_CENTER
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.REF_COST_CENTER TO plgm_service;

INSERT INTO public.ref_cost_center(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('K-155', 'Kostenstelle der Abteilung abc', 'testuser','12.04.2018','testuser','12.04.2018');
INSERT INTO public.ref_cost_center(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('K-004', 'Kostenstelle der Abteilung xyz', 'testuser','12.04.2018','testuser','12.04.2018');
INSERT INTO public.ref_cost_center(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('K-222', 'Kostenstelle der Abteilung hij', 'testuser','12.04.2018','testuser','12.04.2018');

-- ---------------------------------------------
-- TABLE REF_BRANCH
-- ---------------------------------------------
CREATE SEQUENCE public.REF_BRANCH_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.REF_BRANCH_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.REF_BRANCH
(
  ID integer NOT NULL DEFAULT nextval('REF_BRANCH_ID_SEQ'::regclass),
  NAME character varying(50) NOT NULL,
  DESCRIPTION character varying(255),
  COLOR_CODE character varying(20),
  CREATE_USER character varying(100) NOT NULL,
  CREATE_DATE timestamp without time zone  NOT NULL,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone NOT NULL,
  CONSTRAINT REF_BRANCH_PKEY PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.REF_BRANCH
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_BRANCH TO PLGM_SERVICE;

INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('S', 'Strom', '#fc6042','testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('G', 'Gas', '#fdea64', 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('F', 'Fernwärme', '#2cc990', 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('W', 'Wasser', '#2c82c9', 'testuser', '12.04.2018', 'testuser', '12.04.2018');
-- ---------------------------------------------
-- TABLE REF_BRANCH_LEVEL
-- ---------------------------------------------
CREATE SEQUENCE public.ref_branch_level_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.ref_branch_level_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.ref_branch_level
(
  id integer NOT NULL DEFAULT nextval('ref_branch_level_id_seq'::regclass),
  name character varying(50) NOT NULL,
  description character varying(255),
  fk_ref_branch integer NULL,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT ref_branch_level_pkey PRIMARY KEY (id),
  CONSTRAINT FK_REF_BRANCH_LEVEL__REF_BRANCH FOREIGN KEY (FK_REF_BRANCH)
      REFERENCES public.REF_BRANCH (ID) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.ref_branch_level
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.ref_branch_level TO plgm_service;

INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Level1', 'eine Beschreibung', 2, 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Level2', 'noch eine Beschreibung', 2, 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Level3', 'und noch eine Beschreibung', 3, 'testuser', '12.04.2018', 'testuser', '12.04.2018');

-- ---------------------------------------------
-- TABLE REF_GM_STATUS
-- ---------------------------------------------
CREATE SEQUENCE public.REF_GM_STATUS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.REF_GM_STATUS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.REF_GM_STATUS
(
  ID integer NOT NULL DEFAULT nextval('REF_GM_STATUS_ID_SEQ'::regclass),
  NAME character varying(50) NOT NULL,
  CREATE_USER character varying(100) NOT NULL,
  CREATE_DATE timestamp without time zone NOT NULL,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT REF_GM_STATUS_PKEY PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.REF_GM_STATUS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_GM_STATUS TO PLGM_SERVICE;

INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (0,'NEW','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (1,'APPLIED','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (2,'CANCELED','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (3,'FOR_APPROVAL','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (4,'APPROVED','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (5,'REQUESTED','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (6,'RELEASED','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (7,'ACTIVE','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (8,'IN_WORK','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (9,'FINISHED','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (10,'CLOSED','system','09.04.2018','system','09.04.2018');


-- ---------------------------------------------
-- TABLE TBL_GRIDMEASURE
-- ---------------------------------------------
CREATE SEQUENCE public.TBL_GRIDMEASURE_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.TBL_GRIDMEASURE_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.tbl_gridmeasure
(
  id integer NOT NULL DEFAULT nextval('tbl_gridmeasure_id_seq'::regclass),
  id_descriptive character varying(50),
  title character varying(256),
  affected_resource character varying(256),
  remark character varying(1024),
  fk_ref_gm_status integer,
  requester_name character varying(256),
  switching_object character varying(256),
  fk_ref_cost_center integer,
  responsible_onsite_name character varying(256),
  responsible_onsite_department character varying(256),
  approval_by character varying(256),
  area_of_switching character varying(256),
  appointment_repetition character varying(100),
  appointment_startdate timestamp without time zone,
  appointment_numberof integer,
  planned_starttime_first_sequence timestamp without time zone,
  planned_starttime_first_singlemeasure timestamp without time zone,
  planned_endtime_last_singlemeasure timestamp without time zone,
  planned_endtime_gridmeasure timestamp without time zone,
  starttime_first_sequence timestamp without time zone,
  starttime_first_singlemeasure timestamp without time zone,
  endtime_last_singlemeasure timestamp without time zone,
  endtime_gridmeasure timestamp without time zone,
  time_of_reallocation character varying(100),
  description character varying(1024),
  fk_ref_branch integer,
  fk_ref_branch_level integer,
  create_user character varying(100) NOT NULL,
  create_user_department character varying(100),
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_user_department character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_gridmeasure_pkey PRIMARY KEY (id),
  CONSTRAINT fk_gridmeasure__gm_status FOREIGN KEY (fk_ref_gm_status)
      REFERENCES public.ref_gm_status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_gridmeasure__cost_center FOREIGN KEY (fk_ref_cost_center)
      REFERENCES public.ref_cost_center (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_gridmeasure__branch FOREIGN KEY (fk_ref_branch)
      REFERENCES public.ref_branch (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_gridmeasure__branch_level FOREIGN KEY (fk_ref_branch_level)
      REFERENCES public.ref_branch_level (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_gridmeasure
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_gridmeasure TO plgm_service;

INSERT INTO
  public.TBL_GRIDMEASURE(id_descriptive, title, affected_resource, remark, fk_ref_gm_status, fk_ref_cost_center, create_user, create_user_department, create_date, mod_user, mod_user_department, mod_date, fk_ref_branch, fk_ref_branch_level)
VALUES
  ('20','Kabel erneuern','Leitung xyz','Kabel wurde vom Bagger überfahren und muss auf 150 cm erneuert werden', 1, 1, 'Marko Maienschein','Akute Einsatztruppe','19.03.2018',NULL,NULL,'19.03.2018', 2, 2);
INSERT INTO
  public.TBL_GRIDMEASURE(id_descriptive, title, affected_resource, remark, fk_ref_gm_status, fk_ref_cost_center, create_user, create_user_department, create_date, mod_user, mod_user_department, mod_date, fk_ref_branch, fk_ref_branch_level)
VALUES
  ('21','Kabel abdichten','Kabel zzz','Isolierung erneuern', 4, 2 ,'Tom Tulipan','Akute Einsatztruppe','19.03.2018',NULL,NULL,'19.03.2018', 3, 1);
INSERT INTO
  public.TBL_GRIDMEASURE(id_descriptive, title, affected_resource, remark, fk_ref_gm_status, fk_ref_cost_center, create_user, create_user_department, create_date, mod_user, mod_user_department, mod_date, fk_ref_branch, fk_ref_branch_level)
VALUES
  ('22','Stromkasten reparieren','Kasten srt','Kasten wurde gerammt Frontdeckel muß ausgetauscht werden', 8, 2, 'Wendy Wüstenwind','Akute Einsatztruppe','17.03.2018',NULL,NULL,'17.03.2018', 1,2);
INSERT INTO
  public.TBL_GRIDMEASURE(id_descriptive, title, affected_resource, remark, fk_ref_gm_status, fk_ref_cost_center, create_user, create_user_department, create_date, mod_user, mod_user_department, mod_date, fk_ref_branch, fk_ref_branch_level)
VALUES
  ('23','Stromkasten umtauschen','Kasten srt','Kasten wurde gerammt Frontdeckel muß ausgetauscht werden', 4, 3 ,'Kobe Bryant','Akute Einsatztruppe','17.03.2018',NULL,NULL,'17.03.2018', 1,3);
INSERT INTO
  public.TBL_GRIDMEASURE(id_descriptive, title, affected_resource, remark, fk_ref_gm_status, fk_ref_cost_center, create_user, create_user_department, create_date, mod_user, mod_user_department, mod_date, fk_ref_branch, fk_ref_branch_level)
VALUES
  ('24','Lampe','Kasten srt','Kasten wurde gerammt Frontdeckel muß ausgetauscht werden', 3, 2, 'Mary Mustang','Akute Einsatztruppe','17.03.2018',NULL,NULL,'17.03.2018', 1, 1);
INSERT INTO
  public.TBL_GRIDMEASURE(id_descriptive, title, affected_resource, remark, fk_ref_gm_status, fk_ref_cost_center, create_user, create_user_department, create_date, mod_user, mod_user_department, mod_date, fk_ref_branch, fk_ref_branch_level)
VALUES
  ('25','Stromkasten reparieren','Kasten srt','Kasten wurde gerammt Frontdeckel muß ausgetauscht werden', 2, 1, 'Jim Brown','Akute Einsatztruppe','17.03.2018',NULL,NULL,'17.03.2018', 1, 1);


DROP TABLE IF EXISTS public.REF_VERSION;

CREATE TABLE public.REF_VERSION
(
  id integer NOT NULL,
  version character varying(100) NOT NULL,
  CONSTRAINT ref_version_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.REF_VERSION
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_VERSION TO PLGM_SERVICE;

INSERT INTO REF_VERSION VALUES (1, '0.0.1_PG');

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- HISTORY-TABLES
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- PUBLIC.HREF_USER_SETTINGS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_USER_SETTINGS;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_USER_SETTINGS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_USER_SETTINGS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_USER_SETTINGS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_USER_SETTINGS
(
  HID integer NOT NULL DEFAULT nextval('HREF_USER_SETTINGS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  USERNAME character varying (50),
  SETTING_TYPE character varying (50),
  VALUE character varying,

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_USER_SETTINGS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_USER_SETTINGS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_USER_SETTINGS TO PLGM_SERVICE;


-- PUBLIC.HREF_USER_DEPARTMENT Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_USER_DEPARTMENT;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_USER_DEPARTMENT_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_USER_DEPARTMENT_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_USER_DEPARTMENT_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_USER_DEPARTMENT
(
  HID integer NOT NULL DEFAULT nextval('HREF_USER_DEPARTMENT_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_USER_DEPARTMENT_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_USER_DEPARTMENT
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_USER_DEPARTMENT TO PLGM_SERVICE;


-- PUBLIC.HREF_COST_CENTER Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_COST_CENTER;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_COST_CENTER_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_COST_CENTER_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_COST_CENTER_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_COST_CENTER
(
  HID integer NOT NULL DEFAULT nextval('HREF_COST_CENTER_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),
  DESCRIPTION character varying (255),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_COST_CENTER_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_COST_CENTER
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_COST_CENTER TO PLGM_SERVICE;

-- PUBLIC.HREF_BRANCH Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_BRANCH;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_BRANCH_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_BRANCH_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_BRANCH_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_BRANCH
(
  HID integer NOT NULL DEFAULT nextval('HREF_BRANCH_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),
  DESCRIPTION character varying (255),
  COLOR_CODE character varying (20),
  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_BRANCH_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_BRANCH
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_BRANCH TO PLGM_SERVICE;

-- PUBLIC.HREF_BRANCH_LEVEL Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_BRANCH_LEVEL;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_BRANCH_LEVEL_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_BRANCH_LEVEL_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_BRANCH_LEVEL_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_BRANCH_LEVEL
(
  HID integer NOT NULL DEFAULT nextval('HREF_BRANCH_LEVEL_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),
  DESCRIPTION character varying (255),
  FK_REF_BRANCH integer,

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_BRANCH_LEVEL_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_BRANCH_LEVEL
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_BRANCH_LEVEL TO PLGM_SERVICE;


-- PUBLIC.HREF_GM_STATUS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_GM_STATUS;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_GM_STATUS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_GM_STATUS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_GM_STATUS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_GM_STATUS
(
  HID integer NOT NULL DEFAULT nextval('HREF_GM_STATUS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying(50),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_GM_STATUS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_GM_STATUS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_GM_STATUS TO PLGM_SERVICE;


-- PUBLIC.HTBL_GRIDMEASURE Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_GRIDMEASURE;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_GRIDMEASURE_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_GRIDMEASURE_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_GRIDMEASURE_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_GRIDMEASURE
(
  HID integer NOT NULL DEFAULT nextval('HTBL_GRIDMEASURE_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  ID_DESCRIPTIVE character varying (50),
  TITLE character varying (256),
  AFFECTED_RESOURCE character varying (256),
  REMARK character varying (1024),
  FK_REF_GM_STATUS integer,
  REQUESTER_NAME character varying (256),
  SWITCHING_OBJECT character varying (256),
  FK_REF_COST_CENTER integer,
  RESPONSIBLE_ONSITE_NAME character varying (256),
  RESPONSIBLE_ONSITE_DEPARTMENT character varying (256),
  APPROVAL_BY character varying (256),
  AREA_OF_SWITCHING character varying (256),
  APPOINTMENT_REPETITION character varying (100),
  APPOINTMENT_STARTDATE timestamp without time zone,
  APPOINTMENT_NUMBEROF integer,
  PLANNED_STARTTIME_FIRST_SEQUENCE timestamp without time zone,
  PLANNED_STARTTIME_FIRST_SINGLEMEASURE timestamp without time zone,
  PLANNED_ENDTIME_LAST_SINGLEMEASURE timestamp without time zone,
  PLANNED_ENDTIME_GRIDMEASURE timestamp without time zone,
  STARTTIME_FIRST_SEQUENCE timestamp without time zone,
  STARTTIME_FIRST_SINGLEMEASURE timestamp without time zone,
  ENDTIME_LAST_SINGLEMEASURE timestamp without time zone,
  ENDTIME_GRIDMEASURE timestamp without time zone,
  TIME_OF_REALLOCATION character varying (100),
  DESCRIPTION character varying (1024),
  FK_REF_BRANCH integer,
  FK_REF_BRANCH_LEVEL integer,
  CREATE_USER_DEPARTMENT character varying (100),
  MOD_USER_DEPARTMENT character varying (100),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_GRIDMEASURE_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_GRIDMEASURE
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_GRIDMEASURE TO PLGM_SERVICE;


-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- TRIGGER
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- PUBLIC.REF_USER_SETTINGS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_USER_SETTINGS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_USER_SETTINGS (
						  ID,USERNAME,SETTING_TYPE,VALUE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.USERNAME,NEW.SETTING_TYPE,NEW.VALUE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_USER_SETTINGS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_USER_SETTINGS_INSERT_TRG ON PUBLIC.REF_USER_SETTINGS;

CREATE TRIGGER REF_USER_SETTINGS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_USER_SETTINGS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_USER_SETTINGS_INSERT_TRG();



-- PUBLIC.REF_USER_SETTINGS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_USER_SETTINGS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_USER_SETTINGS (
						  ID,USERNAME,SETTING_TYPE,VALUE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.USERNAME,NEW.SETTING_TYPE,NEW.VALUE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_USER_SETTINGS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_USER_SETTINGS_UPDATE_TRG ON PUBLIC.REF_USER_SETTINGS;

CREATE TRIGGER REF_USER_SETTINGS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_USER_SETTINGS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_USER_SETTINGS_UPDATE_TRG();



-- PUBLIC.REF_USER_SETTINGS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_USER_SETTINGS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_USER_SETTINGS (

						  ID,USERNAME,SETTING_TYPE,VALUE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.USERNAME,OLD.SETTING_TYPE,OLD.VALUE,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_USER_SETTINGS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_USER_SETTINGS_DELETE_TRG ON PUBLIC.REF_USER_SETTINGS;

CREATE TRIGGER REF_USER_SETTINGS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_USER_SETTINGS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_USER_SETTINGS_DELETE_TRG();



-- PUBLIC.REF_USER_DEPARTMENT INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_USER_DEPARTMENT_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_USER_DEPARTMENT (
						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_USER_DEPARTMENT_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_USER_DEPARTMENT_INSERT_TRG ON PUBLIC.REF_USER_DEPARTMENT;

CREATE TRIGGER REF_USER_DEPARTMENT_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_USER_DEPARTMENT
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_USER_DEPARTMENT_INSERT_TRG();



-- PUBLIC.REF_USER_DEPARTMENT UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_USER_DEPARTMENT_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_USER_DEPARTMENT (
						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_USER_DEPARTMENT_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_USER_DEPARTMENT_UPDATE_TRG ON PUBLIC.REF_USER_DEPARTMENT;

CREATE TRIGGER REF_USER_DEPARTMENT_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_USER_DEPARTMENT
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_USER_DEPARTMENT_UPDATE_TRG();



-- PUBLIC.REF_USER_DEPARTMENT DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_USER_DEPARTMENT_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_USER_DEPARTMENT (

						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_USER_DEPARTMENT_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_USER_DEPARTMENT_DELETE_TRG ON PUBLIC.REF_USER_DEPARTMENT;

CREATE TRIGGER REF_USER_DEPARTMENT_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_USER_DEPARTMENT
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_USER_DEPARTMENT_DELETE_TRG();


-- PUBLIC.HREF_COST_CENTER INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.HREF_COST_CENTER_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HHREF_COST_CENTER (
						  HID,HACTION,HDATE,HUSER,ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.HID,NEW.HACTION,NEW.HDATE,NEW.HUSER,NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.HREF_COST_CENTER_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS HREF_COST_CENTER_INSERT_TRG ON PUBLIC.HREF_COST_CENTER;

CREATE TRIGGER HREF_COST_CENTER_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.HREF_COST_CENTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.HREF_COST_CENTER_INSERT_TRG();



-- PUBLIC.HREF_COST_CENTER UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.HREF_COST_CENTER_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HHREF_COST_CENTER (
						  HID,HACTION,HDATE,HUSER,ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.HID,NEW.HACTION,NEW.HDATE,NEW.HUSER,NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.HREF_COST_CENTER_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS HREF_COST_CENTER_UPDATE_TRG ON PUBLIC.HREF_COST_CENTER;

CREATE TRIGGER HREF_COST_CENTER_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.HREF_COST_CENTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.HREF_COST_CENTER_UPDATE_TRG();



-- PUBLIC.HREF_COST_CENTER DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.HREF_COST_CENTER_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HHREF_COST_CENTER (

						  HID,HACTION,HDATE,HUSER,ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.HID,OLD.HACTION,OLD.HDATE,OLD.HUSER,OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.HREF_COST_CENTER_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS HREF_COST_CENTER_DELETE_TRG ON PUBLIC.HREF_COST_CENTER;

CREATE TRIGGER HREF_COST_CENTER_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.HREF_COST_CENTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.HREF_COST_CENTER_DELETE_TRG();



-- PUBLIC.REF_BRANCH INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH (

						  ID,NAME,DESCRIPTION,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.COLOR_CODE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_BRANCH_INSERT_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_INSERT_TRG();



-- PUBLIC.REF_BRANCH UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH (
						  ID,NAME,DESCRIPTION,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.COLOR_CODE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_UPDATE_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_UPDATE_TRG();



-- PUBLIC.REF_BRANCH DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_BRANCH (
						  ID,NAME,DESCRIPTION,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,
						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.COLOR_CODE,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,
                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_DELETE_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_DELETE_TRG();



-- PUBLIC.REF_BRANCH_LEVEL INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_LEVEL_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH_LEVEL (
						  ID,NAME,DESCRIPTION,FK_REF_BRANCH,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_LEVEL_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_BRANCH_LEVEL_INSERT_TRG ON PUBLIC.REF_BRANCH_LEVEL;

CREATE TRIGGER REF_BRANCH_LEVEL_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_BRANCH_LEVEL
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_LEVEL_INSERT_TRG();



-- PUBLIC.REF_BRANCH_LEVEL UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_LEVEL_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH_LEVEL (
						  ID,NAME,DESCRIPTION,FK_REF_BRANCH,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_LEVEL_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_LEVEL_UPDATE_TRG ON PUBLIC.REF_BRANCH_LEVEL;

CREATE TRIGGER REF_BRANCH_LEVEL_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_BRANCH_LEVEL
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_LEVEL_UPDATE_TRG();



-- PUBLIC.REF_BRANCH_LEVEL DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_LEVEL_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_BRANCH_LEVEL (

						  ID,NAME,DESCRIPTION,FK_REF_BRANCH,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.FK_REF_BRANCH,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_LEVEL_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_LEVEL_DELETE_TRG ON PUBLIC.REF_BRANCH_LEVEL;

CREATE TRIGGER REF_BRANCH_LEVEL_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_BRANCH_LEVEL
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_LEVEL_DELETE_TRG();





-- PUBLIC.REF_GM_STATUS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_GM_STATUS (
						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_GM_STATUS_INSERT_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_INSERT_TRG();



-- PUBLIC.REF_GM_STATUS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_GM_STATUS (
						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_GM_STATUS_UPDATE_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_UPDATE_TRG();



-- PUBLIC.REF_GM_STATUS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_GM_STATUS (

						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_GM_STATUS_DELETE_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_DELETE_TRG();


-- PUBLIC.TBL_GRIDMEASURE INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_GRIDMEASURE (
						  ID,ID_DESCRIPTIVE,TITLE,AFFECTED_RESOURCE,REMARK,FK_REF_GM_STATUS,REQUESTER_NAME,SWITCHING_OBJECT,FK_REF_COST_CENTER,RESPONSIBLE_ONSITE_NAME,RESPONSIBLE_ONSITE_DEPARTMENT,APPROVAL_BY,AREA_OF_SWITCHING,APPOINTMENT_REPETITION,APPOINTMENT_STARTDATE,APPOINTMENT_NUMBEROF,PLANNED_STARTTIME_FIRST_SEQUENCE,PLANNED_STARTTIME_FIRST_SINGLEMEASURE,PLANNED_ENDTIME_LAST_SINGLEMEASURE,PLANNED_ENDTIME_GRIDMEASURE,STARTTIME_FIRST_SEQUENCE,STARTTIME_FIRST_SINGLEMEASURE,ENDTIME_LAST_SINGLEMEASURE,ENDTIME_GRIDMEASURE,TIME_OF_REALLOCATION,DESCRIPTION,FK_REF_BRANCH,FK_REF_BRANCH_LEVEL,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.ID_DESCRIPTIVE,NEW.TITLE,NEW.AFFECTED_RESOURCE,NEW.REMARK,NEW.FK_REF_GM_STATUS,NEW.REQUESTER_NAME,NEW.SWITCHING_OBJECT,NEW.FK_REF_COST_CENTER,NEW.RESPONSIBLE_ONSITE_NAME,NEW.RESPONSIBLE_ONSITE_DEPARTMENT,NEW.APPROVAL_BY,NEW.AREA_OF_SWITCHING,NEW.APPOINTMENT_REPETITION,NEW.APPOINTMENT_STARTDATE,NEW.APPOINTMENT_NUMBEROF,NEW.PLANNED_STARTTIME_FIRST_SEQUENCE,NEW.PLANNED_STARTTIME_FIRST_SINGLEMEASURE,NEW.PLANNED_ENDTIME_LAST_SINGLEMEASURE,NEW.PLANNED_ENDTIME_GRIDMEASURE,NEW.STARTTIME_FIRST_SEQUENCE,NEW.STARTTIME_FIRST_SINGLEMEASURE,NEW.ENDTIME_LAST_SINGLEMEASURE,NEW.ENDTIME_GRIDMEASURE,NEW.TIME_OF_REALLOCATION,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.FK_REF_BRANCH_LEVEL,NEW.CREATE_USER,NEW.CREATE_USER_DEPARTMENT,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_USER_DEPARTMENT,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_INSERT_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_INSERT_TRG();



-- PUBLIC.TBL_GRIDMEASURE UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_GRIDMEASURE (
						  ID,ID_DESCRIPTIVE,TITLE,AFFECTED_RESOURCE,REMARK,FK_REF_GM_STATUS,REQUESTER_NAME,SWITCHING_OBJECT,FK_REF_COST_CENTER,RESPONSIBLE_ONSITE_NAME,RESPONSIBLE_ONSITE_DEPARTMENT,APPROVAL_BY,AREA_OF_SWITCHING,APPOINTMENT_REPETITION,APPOINTMENT_STARTDATE,APPOINTMENT_NUMBEROF,PLANNED_STARTTIME_FIRST_SEQUENCE,PLANNED_STARTTIME_FIRST_SINGLEMEASURE,PLANNED_ENDTIME_LAST_SINGLEMEASURE,PLANNED_ENDTIME_GRIDMEASURE,STARTTIME_FIRST_SEQUENCE,STARTTIME_FIRST_SINGLEMEASURE,ENDTIME_LAST_SINGLEMEASURE,ENDTIME_GRIDMEASURE,TIME_OF_REALLOCATION,DESCRIPTION,FK_REF_BRANCH,FK_REF_BRANCH_LEVEL,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.ID_DESCRIPTIVE,NEW.TITLE,NEW.AFFECTED_RESOURCE,NEW.REMARK,NEW.FK_REF_GM_STATUS,NEW.REQUESTER_NAME,NEW.SWITCHING_OBJECT,NEW.FK_REF_COST_CENTER,NEW.RESPONSIBLE_ONSITE_NAME,NEW.RESPONSIBLE_ONSITE_DEPARTMENT,NEW.APPROVAL_BY,NEW.AREA_OF_SWITCHING,NEW.APPOINTMENT_REPETITION,NEW.APPOINTMENT_STARTDATE,NEW.APPOINTMENT_NUMBEROF,NEW.PLANNED_STARTTIME_FIRST_SEQUENCE,NEW.PLANNED_STARTTIME_FIRST_SINGLEMEASURE,NEW.PLANNED_ENDTIME_LAST_SINGLEMEASURE,NEW.PLANNED_ENDTIME_GRIDMEASURE,NEW.STARTTIME_FIRST_SEQUENCE,NEW.STARTTIME_FIRST_SINGLEMEASURE,NEW.ENDTIME_LAST_SINGLEMEASURE,NEW.ENDTIME_GRIDMEASURE,NEW.TIME_OF_REALLOCATION,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.FK_REF_BRANCH_LEVEL,NEW.CREATE_USER,NEW.CREATE_USER_DEPARTMENT,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_USER_DEPARTMENT,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_UPDATE_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG();



-- PUBLIC.TBL_GRIDMEASURE DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_GRIDMEASURE (

						  ID,ID_DESCRIPTIVE,TITLE,AFFECTED_RESOURCE,REMARK,FK_REF_GM_STATUS,REQUESTER_NAME,SWITCHING_OBJECT,FK_REF_COST_CENTER,RESPONSIBLE_ONSITE_NAME,RESPONSIBLE_ONSITE_DEPARTMENT,APPROVAL_BY,AREA_OF_SWITCHING,APPOINTMENT_REPETITION,APPOINTMENT_STARTDATE,APPOINTMENT_NUMBEROF,PLANNED_STARTTIME_FIRST_SEQUENCE,PLANNED_STARTTIME_FIRST_SINGLEMEASURE,PLANNED_ENDTIME_LAST_SINGLEMEASURE,PLANNED_ENDTIME_GRIDMEASURE,STARTTIME_FIRST_SEQUENCE,STARTTIME_FIRST_SINGLEMEASURE,ENDTIME_LAST_SINGLEMEASURE,ENDTIME_GRIDMEASURE,TIME_OF_REALLOCATION,DESCRIPTION,FK_REF_BRANCH,FK_REF_BRANCH_LEVEL,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.ID_DESCRIPTIVE,OLD.TITLE,OLD.AFFECTED_RESOURCE,OLD.REMARK,OLD.FK_REF_GM_STATUS,OLD.REQUESTER_NAME,OLD.SWITCHING_OBJECT,OLD.FK_REF_COST_CENTER,OLD.RESPONSIBLE_ONSITE_NAME,OLD.RESPONSIBLE_ONSITE_DEPARTMENT,OLD.APPROVAL_BY,OLD.AREA_OF_SWITCHING,OLD.APPOINTMENT_REPETITION,OLD.APPOINTMENT_STARTDATE,OLD.APPOINTMENT_NUMBEROF,OLD.PLANNED_STARTTIME_FIRST_SEQUENCE,OLD.PLANNED_STARTTIME_FIRST_SINGLEMEASURE,OLD.PLANNED_ENDTIME_LAST_SINGLEMEASURE,OLD.PLANNED_ENDTIME_GRIDMEASURE,OLD.STARTTIME_FIRST_SEQUENCE,OLD.STARTTIME_FIRST_SINGLEMEASURE,OLD.ENDTIME_LAST_SINGLEMEASURE,OLD.ENDTIME_GRIDMEASURE,OLD.TIME_OF_REALLOCATION,OLD.DESCRIPTION,OLD.FK_REF_BRANCH,OLD.FK_REF_BRANCH_LEVEL,OLD.CREATE_USER,OLD.CREATE_USER_DEPARTMENT,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_USER_DEPARTMENT,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_DELETE_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_DELETE_TRG();


