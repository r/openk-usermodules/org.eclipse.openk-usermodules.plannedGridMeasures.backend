-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2018 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------
--Field COLOR_CODE added in Table REF_GM_STATUS, HREF_GM_STATUS, Insert-, Delete-, and Update-Triggers

-- ---------------------------------------------
-- DROPS
-- ---------------------------------------------
DROP TABLE IF EXISTS public.TBL_STEPS;
DROP SEQUENCE IF EXISTS public.TBL_STEPS_ID_SEQ;

DROP TABLE IF EXISTS public.TBL_SINGLE_GRIDMEASURE;
DROP SEQUENCE IF EXISTS public.TBL_SINGLE_GRIDMEASURE_ID_SEQ;

DROP TABLE IF EXISTS public.TBL_LOCK CASCADE;
DROP SEQUENCE IF EXISTS public.TBL_LOCK_ID_SEQ;

DROP TABLE IF EXISTS public.REF_USER_SETTINGS CASCADE;
DROP SEQUENCE IF EXISTS public.REF_USER_SETTINGS_ID_SEQ;

DROP TABLE IF EXISTS public.TBL_USER_SETTINGS CASCADE;
DROP SEQUENCE IF EXISTS public.TBL_USER_SETTINGS_ID_SEQ;

DROP TABLE IF EXISTS public.REF_COST_CENTER CASCADE;
DROP SEQUENCE IF EXISTS public.REF_COST_CENTER_ID_SEQ;

DROP TABLE IF EXISTS public.TBL_GRIDMEASURE CASCADE;
DROP SEQUENCE IF EXISTS public.TBL_GRIDMEASURE_ID_SEQ;

DROP TABLE IF EXISTS public.REF_BRANCH CASCADE;
DROP SEQUENCE IF EXISTS public.REF_BRANCH_ID_SEQ;

DROP TABLE IF EXISTS public.REF_BRANCH_LEVEL CASCADE;
DROP SEQUENCE IF EXISTS public.REF_BRANCH_LEVEL_ID_SEQ;

DROP TABLE IF EXISTS public.REF_GM_STATUS CASCADE;
DROP SEQUENCE IF EXISTS public.REF_GM_STATUS_ID_SEQ;

DROP TABLE IF EXISTS public.tbl_documents CASCADE;
DROP SEQUENCE IF EXISTS public.tbl_documents_id_seq;

DROP TABLE IF EXISTS public.tbl_measure_documents CASCADE;
DROP SEQUENCE IF EXISTS public.tbl_measure_documents_id_seq;

DROP TABLE IF EXISTS public.tbl_id_counter CASCADE;
DROP SEQUENCE IF EXISTS public.tbl_id_counter_id_seq;

DROP TABLE IF EXISTS public.REF_TERRITORY CASCADE;
DROP SEQUENCE IF EXISTS public.REF_TERRITORY_ID_SEQ;


DROP INDEX IF EXISTS public.huser_index;

-- ---------------------------------------------
-- TABLE REF_TERRITORY
-- ---------------------------------------------

CREATE SEQUENCE public.REF_TERRITORY_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.REF_TERRITORY_ID_SEQ
  OWNER TO plgm_service;


CREATE TABLE public.REF_TERRITORY
(
  id integer NOT NULL DEFAULT nextval('REF_TERRITORY_ID_SEQ'::regclass),
  name character varying(50) NOT NULL,
  description character varying(255),
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT REF_TERRITORY_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.REF_TERRITORY
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.REF_TERRITORY TO plgm_service;

INSERT INTO public.REF_TERRITORY(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('Nord', 'Region Nord', 'testuser','12.04.2018','testuser','12.04.2018');
INSERT INTO public.REF_TERRITORY(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('Ost', 'Region Ost', 'testuser','12.04.2018','testuser','12.04.2018');
INSERT INTO public.REF_TERRITORY(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('SUED', 'Region SUED', 'testuser','12.04.2018','testuser','12.04.2018');



-- ---------------------------------------------
-- TABLE TBL_LOCK
-- ---------------------------------------------

CREATE SEQUENCE public.TBL_LOCK_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.TBL_LOCK_ID_SEQ
  OWNER TO plgm_service;

CREATE TABLE public.tbl_lock
(
  id integer NOT NULL DEFAULT nextval('TBL_LOCK_ID_SEQ'::regclass),
  key integer NOT NULL, -- Id from tbl_gridmeasure
  username character varying(50) NOT NULL,
  info character varying(256),
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_lock_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_lock
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_lock TO plgm_service;

CREATE UNIQUE INDEX tbl_lock_unique_key ON public.tbl_lock (key ASC, info ASC );

-- ---------------------------------------------
-- TABLE TBL_USER_SETTINGS
-- ---------------------------------------------

CREATE SEQUENCE public.tbl_user_settings_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.tbl_user_settings_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.tbl_user_settings
(
  id integer NOT NULL DEFAULT nextval('tbl_user_settings_id_seq'::regclass),
  username character varying(50) NOT NULL,
  setting_type character varying(50),
  value character varying(4096),
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_user_settings_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_user_settings
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_user_settings TO plgm_service;

CREATE UNIQUE INDEX tbl_user_set_unique_key ON public.tbl_user_settings (username ASC, setting_type ASC );



-- ---------------------------------------------
-- TABLE TBL_ID_COUNTER
-- ---------------------------------------------

CREATE SEQUENCE public.tbl_id_counter_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.tbl_id_counter_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.tbl_id_counter
(
  id integer NOT NULL DEFAULT nextval('tbl_id_counter_id_seq'::regclass),
  counter integer NOT NULL,
  counter_type character varying(256),
  info character varying(256),
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_id_counter_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_id_counter
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_id_counter TO plgm_service;


-- ---------------------------------------------
-- TABLE REF_COST_CENTER
-- ---------------------------------------------

CREATE SEQUENCE public.REF_COST_CENTER_ID_SEQ
   INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.REF_COST_CENTER_ID_SEQ
  OWNER TO plgm_service;


CREATE TABLE public.REF_COST_CENTER
(
  id integer NOT NULL DEFAULT nextval('REF_COST_CENTER_id_seq'::regclass),
  name character varying(50) NOT NULL,
  description character varying(255),
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT REF_COST_CENTER_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.REF_COST_CENTER
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.REF_COST_CENTER TO plgm_service;

INSERT INTO public.ref_cost_center(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('K-155', 'Kostenstelle der Abteilung abc', 'testuser','12.04.2018','testuser','12.04.2018');
INSERT INTO public.ref_cost_center(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('K-004', 'Kostenstelle der Abteilung xyz', 'testuser','12.04.2018','testuser','12.04.2018');
INSERT INTO public.ref_cost_center(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('K-222', 'Kostenstelle der Abteilung hij', 'testuser','12.04.2018','testuser','12.04.2018');

-- ---------------------------------------------
-- TABLE REF_BRANCH
-- ---------------------------------------------
CREATE SEQUENCE public.REF_BRANCH_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.REF_BRANCH_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.REF_BRANCH
(
  ID integer NOT NULL DEFAULT nextval('REF_BRANCH_ID_SEQ'::regclass),
  NAME character varying(50) NOT NULL,
  DESCRIPTION character varying(255),
  COLOR_CODE character varying(20),
  CREATE_USER character varying(100) NOT NULL,
  CREATE_DATE timestamp without time zone  NOT NULL,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT REF_BRANCH_PKEY PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.REF_BRANCH
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_BRANCH TO PLGM_SERVICE;

INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('S', 'Strom', '#fc6042','testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('G', 'Gas', '#fdea64', 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('F', 'Fernwärme', '#2cc990', 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('W', 'Wasser', '#2c82c9', 'testuser', '12.04.2018', 'testuser', '12.04.2018');
-- ---------------------------------------------
-- TABLE REF_BRANCH_LEVEL
-- ---------------------------------------------
CREATE SEQUENCE public.ref_branch_level_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.ref_branch_level_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.ref_branch_level
(
  id integer NOT NULL DEFAULT nextval('ref_branch_level_id_seq'::regclass),
  name character varying(50) NOT NULL,
  description character varying(255),
  fk_ref_branch integer NULL,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT ref_branch_level_pkey PRIMARY KEY (id),
  CONSTRAINT FK_REF_BRANCH_LEVEL__REF_BRANCH FOREIGN KEY (FK_REF_BRANCH)
      REFERENCES public.REF_BRANCH (ID) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.ref_branch_level
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.ref_branch_level TO plgm_service;

INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Strom Level1', 'eine Beschreibung', 1, 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Strom Level2', 'eine Beschreibung', 1, 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Strom Level3', 'eine Beschreibung', 1, 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Gas Level1', 'eine Beschreibung', 2, 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Gas Level2', 'noch eine Beschreibung', 2, 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Fernwärme Level3', 'und noch eine Beschreibung', 3, 'testuser', '12.04.2018', 'testuser', '12.04.2018');

-- ---------------------------------------------
-- TABLE REF_GM_STATUS
-- ---------------------------------------------
CREATE SEQUENCE public.REF_GM_STATUS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.REF_GM_STATUS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.REF_GM_STATUS
(
  ID integer NOT NULL DEFAULT nextval('REF_GM_STATUS_ID_SEQ'::regclass),
  NAME character varying(50) NOT NULL,
  COLOR_CODE character varying(20),
  CREATE_USER character varying(100) NOT NULL,
  CREATE_DATE timestamp without time zone NOT NULL,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT REF_GM_STATUS_PKEY PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.REF_GM_STATUS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_GM_STATUS TO PLGM_SERVICE;

INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (0,'Neu','system', 'rgba(121,182,28,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (1,'Beantragt','system', 'rgba(109,177,45,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (2,'Storniert','system', 'rgba(153,0,0,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (3,'Zur Genehmigung','system', 'rgba(96,171,63,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (4,'Genehmigt','system', 'rgba(84,166,80,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (5,'Angefordert','system', 'rgba(69,159,101,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (6,'Freigegeben','system', 'rgba(55,153,121,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (7,'Schalten aktiv','system', 'rgba(41,146,141,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (8,'In Arbeit','system', 'rgba(28,141,159,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (9,'Arbeit beendet','system', 'rgba(12,133,182,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (10,'Maßnahme beendet','system', 'rgba(2,129,196,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (11,'Geschlossen','system', 'rgba(2,129,196,1)', '09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, color_code, create_date, mod_user, mod_date)
VALUES
  (12,'Abgelehnt','system', 'rgba(153,0,0,1)', '17.06.2018','system','17.06.2018');



-- ---------------------------------------------
-- TABLE TBL_GRIDMEASURE
-- ---------------------------------------------
-- REQUESTER_NAME COLUMN DELETED--
CREATE SEQUENCE public.TBL_GRIDMEASURE_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.TBL_GRIDMEASURE_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.tbl_gridmeasure
(
  id integer NOT NULL DEFAULT nextval('tbl_gridmeasure_id_seq'::regclass),
  id_descriptive character varying(50),
  title character varying(256),
  affected_resource character varying(256),
  remark character varying(1024),
  email_addresses character varying(1024),
  fk_ref_gm_status integer,
  switching_object character varying(256),
  cost_center character varying(50),
  approval_by character varying(256),
  area_of_switching character varying(256),
  appointment_repetition character varying(100),
  appointment_startdate timestamp without time zone,
  appointment_numberof integer,
  planned_starttime_first_singlemeasure timestamp without time zone,
  endtime_gridmeasure timestamp without time zone,
  time_of_reallocation character varying(100),
  description character varying(1024),
  fk_ref_branch integer,
  fk_ref_branch_level integer,
  create_user character varying(100) NOT NULL,
  create_user_department character varying(100),
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_user_department character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_gridmeasure_pkey PRIMARY KEY (id),
  CONSTRAINT fk_gridmeasure__gm_status FOREIGN KEY (fk_ref_gm_status)
      REFERENCES public.ref_gm_status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_gridmeasure__branch FOREIGN KEY (fk_ref_branch)
      REFERENCES public.ref_branch (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_gridmeasure__branch_level FOREIGN KEY (fk_ref_branch_level)
      REFERENCES public.ref_branch_level (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_gridmeasure
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_gridmeasure TO plgm_service;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_1', 'Kabel erneuern', 'Kabel', 'Kabel defekt', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 0,
            'Kabel', 'K-155',
            'otto', 'Frankfurt', 'einmalig', '2018-06-14 15:15:00',
            7, '2018-06-10 16:30:00',
            '2018-06-18 16:00:00', 'in zwei Tagen', 'Das Kabel austauschen', 1,
            1, 'otto', 'Abteilung abc', '2018-06-09 15:00:00',
            'otto', 'Abteilung abc', '2018-06-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_2', 'Lampe erneuern', 'Lampe', 'Lampe defekt', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 1,
            'Lampe', 'K-004',
            'jasper', 'Mannheim', 'einmalig', '2018-06-16 15:15:00',
            3, '2018-06-12 16:30:00',
            '2018-06-20 16:00:00', 'in zwei Tagen', 'Die Lampe austauschen', 2,
            1, 'jasper', 'Abteilung cde', '2018-06-11 15:00:00',
            'jasper', 'Abteilung cde', '2018-06-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_3', 'Transformator erneuern', 'Transformator', 'Transformator defekt', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 2,
            'Transformator', 'K-222',
            'hugo', 'Murr', 'täglich', '2018-06-17 15:15:00',
            6, '2018-06-13 16:30:00',
            '2018-06-21 16:00:00', 'in 8 Tagen', 'Den Transformator austauschen', 4,
            1, 'hugo', 'Abteilung xyz', '2018-06-12 15:00:00',
            'otto', 'Abteilung abc', '2018-06-17 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_4', 'Transformator ersetzen', 'Transformator', 'Transformator kaputt', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 3,
            'Transformator', 'K-155',
            'otto', 'Stuttgart', 'einmalig', '2018-06-18 15:15:00',
            3, '2018-06-14 16:30:00',
            '2018-06-22 16:00:00', 'in 9 Tagen', 'Den Transformator austauschen', 3,
            1, 'otto', 'Abteilung abc', '2018-06-13 15:00:00',
            'otto', 'Abteilung syx', '2018-06-18 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_5', 'Gas Station', 'Station', 'Station defekt', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 4,
            'Station', 'K-004',
            'otto', 'Ludwigsburg', 'wöchentlich', '2018-07-14 15:15:00',
            1, '2018-07-10 16:30:00',
            '2018-07-18 16:00:00', 'in zwei Wochen', 'Die Gas Station untersuchen', 2,
            1, 'otto', 'Abteilung lmn', '2018-07-09 15:00:00',
            'otto', 'Abteilung lmn', '2018-07-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_6', 'Stuhl mit drei Beinen', 'Stuhl', 'Stuhl kaputt', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 5,
            'Stuhl', 'K-155',
            'otto', 'Hamburg', 'einmalig', '2018-06-12 15:15:00',
            3, '2018-06-08 16:30:00',
            '2018-06-16 16:00:00', 'in zwei Tagen', 'Den Stuhl austauschen', 4,
            1, 'claudio', 'Abteilung lmn', '2018-06-07 15:00:00',
            'claudio', 'Abteilung lmn', '2018-06-12 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_7', 'Kabel vergrößern', 'Kabel', 'Kabel zu kurz', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 6,
            'Kabel', 'K-222',
            'dagmar', 'Steinheim', 'einmalig', '2018-08-14 15:15:00',
            27, '2018-08-10 16:30:00',
            '2018-08-18 16:00:00', 'in zwei Wochen', 'Das Kabel vergrößern', 3,
            1, 'claudio', 'Abteilung lmn', '2018-08-09 15:00:00',
            'bruno', 'Abteilung lmn', '2018-08-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_8', 'Transformator austauschen', 'Transformator', 'Transformator 160 Grads', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 7,
            'Transformator', 'K-155',
            'dagmar', 'Murr', 'täglich', '2018-06-14 15:15:00',
            6, '2018-06-10 16:30:00',
            '2018-06-18 16:00:00', 'in zwei Tagen', 'Den Transformator austauschen', 1,
            1, 'dagmar', 'Abteilung opq', '2018-06-09 15:00:00',
            'dagmar', 'Abteilung opq', '2018-06-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_9', 'Alles kaputt', 'Alles', 'Alles defekt', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 8,
            'Alles', 'K-222',
            'otto', 'Berlin', 'einmalig', '2018-06-15 15:15:00',
            2, '2018-06-11 16:30:00',
            '2018-06-19 16:00:00', 'in zwei Jahren', 'Alles wegwerfen', 2,
            3, 'bruno', 'Abteilung opq', '2018-06-10 15:00:00',
            'claudio', 'Abteilung opq', '2018-06-15 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_10', 'Schalter erneuern', 'Schalter', 'Schalter defekt', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 9,
            'Schalter', 'K-004',
            'hugo', 'Kassel', 'einmalig', '2018-08-14 15:15:00',
            9, '2018-08-10 16:30:00',
            '2018-08-18 16:00:00', 'in 6 Tagen', 'Den Schalter austauschen', 1,
            1, 'dagmar', 'Abteilung opq', '2018-08-09 15:00:00',
            'otto', 'Abteilung opq', '2018-08-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_11', 'Rechner austauschen', 'Rechner', 'Rechner defekt', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 10,
            'Rechner', 'K-155',
            'claudio', 'Murr', 'einmalig', '2018-06-12 15:15:00',
            3, '2018-06-08 16:30:00',
            '2018-06-16 16:00:00', 'in zwei Tagen', 'Den Rechner austauschen', 4,
            1, 'hugo', 'Abteilung opq', '2018-06-07 15:00:00',
            'otto', 'Abteilung opq', '2018-06-12 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, email_addresses, fk_ref_gm_status,
            switching_object, cost_center,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_12', 'Maschine untersuchen', 'Maschine', 'Maschine defekt', 'mailingListRecipient@test.de; mailingList2Recipient@test.de', 11,
            'Maschine', 'K-004',
            'otto', 'Ludwigsburg', 'einmalig', '2018-06-20 15:15:00',
            5, '2018-06-16 16:30:00',
            '2018-06-24 16:00:00', 'in 8 Tagen', 'Die Maschine austauschen', 1,
            1, 'bruno', 'Abteilung opq', '2018-06-15 15:00:00',
            'bruno', 'Abteilung opq', '2018-06-20 15:15:00');




-- ---------------------------------------------
-- TABLE TBL_SINGLE_GRIDMEASURE
-- ---------------------------------------------

CREATE SEQUENCE public.tbl_single_gridmeasure_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.tbl_single_gridmeasure_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.tbl_single_gridmeasure
(
  id integer NOT NULL DEFAULT nextval('tbl_single_gridmeasure_id_seq'::regclass),
  sortorder integer NOT NULL,
  title character varying(256),
  switching_object character varying(256),
  cim_id character varying(256),
  cim_name character varying(256),
  cim_description character varying(1024),
  planned_starttime_singlemeasure timestamp without time zone,
  planned_endtime_singlemeasure timestamp without time zone,
  description character varying(1024),
  responsible_onsite_name character varying(256),
  responsible_onsite_department character varying(256),
  network_control character varying(256),
  fk_tbl_gridmeasure integer NOT NULL,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_single_gridmeasure_pkey PRIMARY KEY (id),
  CONSTRAINT fk_single_gridmeasure__gridmeasure FOREIGN KEY (fk_tbl_gridmeasure)
      REFERENCES public.tbl_gridmeasure (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_single_gridmeasure
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_single_gridmeasure TO plgm_service;


INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Lampe ausschalten', 'Lampe', '2', 'cim', 'cim Beschreibung',
            '2018-06-10 16:30:00', '2018-06-11 14:00:00',
            'Beschreibung','Otto','Abteilung abc', 1, 'Otto', '01.07.2018', 'Otto',
            '01.07.2018');
INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (2, 'Kabel kürzen', 'Kabel', '2', 'cim', 'cim Beschreibung',
            '2018-06-10 16:30:00', '2018-06-11 14:00:00',
            'Beschreibung', 'Otto','Abteilung def',1, 'Otto', '01.07.2018', 'Otto',
            '01.07.2018');
INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (3, 'Transformator tauschen', 'Transformator', '2', 'cim', 'cim Beschreibung',
            '2018-06-10 16:30:00', '2018-06-11 14:00:00',
            'Beschreibung', 'Otto','Abteilung ghi',1, 'Otto', '01.07.2018', 'Otto',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Lampe erneuern', 'Lampe', '2', 'cim', 'cim Beschreibung',
            '2018-06-12 16:30:00', '2018-06-13 14:00:00',
            'Beschreibung', 'Otto','Abteilung jkl',2, 'Hugo', '01.07.2018', 'Hugo',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Transformator', 'Transformator', '2', 'cim', 'cim Beschreibung',
            '2018-06-13 16:30:00', '2018-06-14 14:00:00',
            'Beschreibung', 'Otto','Abteilung mno',3, 'Hugo', '01.07.2018', 'Hugo',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Transformator', 'Transformator', '2', 'cim', 'cim Beschreibung',
            '2018-06-14 16:30:00', '2018-06-15 14:00:00',
            'Beschreibung', 'Otto','Abteilung pqr',4, 'Hugo', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Gas', 'Gas', '2', 'cim', 'cim Beschreibung',
            '2018-07-10 16:30:00', '2018-07-11 14:00:00',
            'Beschreibung', 'Otto','Abteilung stu',5, 'Jasper', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (2, 'Gas 2', 'Gas', '2', 'cim', 'cim Beschreibung',
            '2018-07-10 16:30:00', '2018-07-11 14:00:00',
            'Beschreibung', 'Otto','Abteilung vwx',5, 'Jasper', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Stuhl', 'Stuhl', '2', 'cim', 'cim Beschreibung',
            '2018-06-08 16:30:00', '2018-06-09 14:00:00',
            'Beschreibung', 'Otto','Abteilung yz',6, 'Otto', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Kabel', 'Kabel', '2', 'cim', 'cim Beschreibung',
            '2018-08-10 16:30:00', '2018-08-11 14:00:00',
            'Beschreibung', 'Otto','Abteilung abc',7, 'Otto', '01.07.2018', 'Hugo',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Transformator', 'Transformator', '2', 'cim', 'cim Beschreibung',
            '2018-06-10 16:30:00', '2018-06-11 14:00:00',
            'Beschreibung', 'Otto','Abteilung abc',8, 'Hugo', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (2, 'Transformator 2', 'Transformator', '2', 'cim', 'cim Beschreibung',
            '2018-06-10 16:30:00', '2018-06-11 14:00:00',
            'Beschreibung', 'Otto','Abteilung abc',8, 'Hugo', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Alles', 'Alles', '2', 'cim', 'cim Beschreibung',
            '2018-06-11 16:30:00', '2018-06-12 14:00:00',
            'Beschreibung', 'Otto','Abteilung abc',9, 'Otto', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Schalter', 'Schalter', '2', 'cim', 'cim Beschreibung',
            '2018-08-10 16:30:00', '2018-08-11 14:00:00',
            'Beschreibung', 'Otto','Abteilung abc',10, 'Otto', '01.07.2018', 'Hugo',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (2, 'Schalter 2', 'Schalter', '2', 'cim', 'cim Beschreibung',
            '2018-08-10 16:30:00', '2018-08-11 14:00:00',
            'Beschreibung', 'Otto','Abteilung abc',10, 'Otto', '01.07.2018', 'Hugo',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Rechner', 'Rechner', '2', 'cim', 'cim Beschreibung',
            '2018-06-08 16:30:00', '2018-06-09 14:00:00',
            'Beschreibung', 'Otto','Abteilung abc',11, 'Jasper', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, responsible_onsite_name, responsible_onsite_department,fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Maschine', 'Maschine', '2', 'cim', 'cim Beschreibung',
            '2018-06-16 16:30:00', '2018-06-17 14:00:00',
            'Beschreibung', 'Otto','Abteilung abc',12, 'Hugo', '01.07.2018', 'Jasper',
            '01.07.2018');


-- ---------------------------------------------
-- TABLE TBL_STEPS
-- ---------------------------------------------

CREATE SEQUENCE public.tbl_steps_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.tbl_steps_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.tbl_steps
(
  id integer NOT NULL DEFAULT nextval('tbl_steps_id_seq'::regclass),
  sortorder integer NOT NULL,
  switching_object character varying(256),
  type character varying(256),
  present_time character varying(256),
  present_state character varying(256),
  target_state character varying(256),
  operator character varying(256),
  fk_tbl_single_gridmeasure integer NOT NULL,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_steps_pkey PRIMARY KEY (id),
  CONSTRAINT fk_steps__single_gridmeasure FOREIGN KEY (fk_tbl_single_gridmeasure)
      REFERENCES public.tbl_single_gridmeasure (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_steps
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_steps TO plgm_service;


-- ---------------------------------------------
-- TABLE REF_VERSION
-- ---------------------------------------------

DROP TABLE IF EXISTS public.REF_VERSION;

CREATE TABLE public.REF_VERSION
(
  id integer NOT NULL,
  version character varying(100) NOT NULL,
  CONSTRAINT ref_version_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.REF_VERSION
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_VERSION TO PLGM_SERVICE;

INSERT INTO REF_VERSION VALUES (1, '0.0.1_PG');

-- ---------------------------------------------
-- TABLE TBL_DOCUMENTS
-- ---------------------------------------------

CREATE SEQUENCE public.tbl_documents_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.tbl_documents_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.tbl_documents
(
  id integer NOT NULL DEFAULT nextval('tbl_documents_id_seq'::regclass),
  document_name character varying(260),
  document bytea,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_documents_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_documents
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_documents TO plgm_service;


-- ---------------------------------------------
-- TABLE TBL_MEASURE_DOCUMENTS
-- ---------------------------------------------

CREATE SEQUENCE public.tbl_measure_documents_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.tbl_measure_documents_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.tbl_measure_documents
(
  id integer NOT NULL DEFAULT nextval('tbl_measure_documents_id_seq'::regclass),
  fk_tbl_measure integer,
  fk_tbl_documents integer,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_measure_documents_pkey PRIMARY KEY (id),
  CONSTRAINT fk_tbl_measure_documents_measure FOREIGN KEY (fk_tbl_measure)
      REFERENCES public.tbl_gridmeasure (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fk_tbl_measure_documents_documents FOREIGN KEY (fk_tbl_documents)
      REFERENCES public.tbl_documents (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_measure_documents
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_measure_documents TO plgm_service;



-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- HISTORY-TABLES
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- PUBLIC.HREF_TERRITORY Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_TERRITORY;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_TERRITORY_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_TERRITORY_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_TERRITORY_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_TERRITORY
(
  HID integer NOT NULL DEFAULT nextval('HREF_TERRITORY_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),
  DESCRIPTION character varying (255),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_TERRITORY_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_TERRITORY
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_TERRITORY TO PLGM_SERVICE;



-- PUBLIC.HTBL_LOCK Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_LOCK;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_LOCK_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_LOCK_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_LOCK_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_LOCK
(
  HID integer NOT NULL DEFAULT nextval('HTBL_LOCK_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  KEY integer,
  USERNAME character varying (50),
  INFO character varying (256),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_LOCK_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_LOCK
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_LOCK TO PLGM_SERVICE;




-- PUBLIC.HTBL_USER_SETTINGS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_USER_SETTINGS;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_USER_SETTINGS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_USER_SETTINGS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_USER_SETTINGS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_USER_SETTINGS
(
  HID integer NOT NULL DEFAULT nextval('HTBL_USER_SETTINGS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  USERNAME character varying (50),
  SETTING_TYPE character varying (50),
  VALUE character varying(4096),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_USER_SETTINGS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_USER_SETTINGS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_USER_SETTINGS TO PLGM_SERVICE;


-- PUBLIC.HREF_COST_CENTER Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_COST_CENTER;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_COST_CENTER_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_COST_CENTER_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_COST_CENTER_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_COST_CENTER
(
  HID integer NOT NULL DEFAULT nextval('HREF_COST_CENTER_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),
  DESCRIPTION character varying (255),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_COST_CENTER_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_COST_CENTER
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_COST_CENTER TO PLGM_SERVICE;

-- PUBLIC.HREF_BRANCH Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_BRANCH;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_BRANCH_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_BRANCH_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_BRANCH_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_BRANCH
(
  HID integer NOT NULL DEFAULT nextval('HREF_BRANCH_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),
  DESCRIPTION character varying (255),
  COLOR_CODE character varying (20),
  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_BRANCH_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_BRANCH
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_BRANCH TO PLGM_SERVICE;

-- PUBLIC.HREF_BRANCH_LEVEL Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_BRANCH_LEVEL;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_BRANCH_LEVEL_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_BRANCH_LEVEL_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_BRANCH_LEVEL_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_BRANCH_LEVEL
(
  HID integer NOT NULL DEFAULT nextval('HREF_BRANCH_LEVEL_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),
  DESCRIPTION character varying (255),
  FK_REF_BRANCH integer,

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_BRANCH_LEVEL_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_BRANCH_LEVEL
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_BRANCH_LEVEL TO PLGM_SERVICE;


-- PUBLIC.HREF_GM_STATUS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_GM_STATUS;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_GM_STATUS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_GM_STATUS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_GM_STATUS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_GM_STATUS
(
  HID integer NOT NULL DEFAULT nextval('HREF_GM_STATUS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying(50),
  COLOR_CODE character varying(20),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_GM_STATUS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_GM_STATUS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_GM_STATUS TO PLGM_SERVICE;


-- PUBLIC.HTBL_GRIDMEASURE Automatic generanted History Table DDL --
-- <GENERATED CODE!>

-- REQUESTER_NAME COLUMN DELETED--

DROP TABLE IF EXISTS PUBLIC.HTBL_GRIDMEASURE;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_GRIDMEASURE_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_GRIDMEASURE_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_GRIDMEASURE_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_GRIDMEASURE
(
  HID integer NOT NULL DEFAULT nextval('HTBL_GRIDMEASURE_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  ID_DESCRIPTIVE character varying (50),
  TITLE character varying (256),
  AFFECTED_RESOURCE character varying (256),
  REMARK character varying (1024),
  EMAIL_ADDRESSES character varying(1024),
  FK_REF_GM_STATUS integer,
  SWITCHING_OBJECT character varying (256),
  COST_CENTER character varying (50),
  APPROVAL_BY character varying (256),
  AREA_OF_SWITCHING character varying (256),
  APPOINTMENT_REPETITION character varying (100),
  APPOINTMENT_STARTDATE timestamp without time zone,
  APPOINTMENT_NUMBEROF integer,
  PLANNED_STARTTIME_FIRST_SINGLEMEASURE timestamp without time zone,
  ENDTIME_GRIDMEASURE timestamp without time zone,
  TIME_OF_REALLOCATION character varying (100),
  DESCRIPTION character varying (1024),
  FK_REF_BRANCH integer,
  FK_REF_BRANCH_LEVEL integer,
  CREATE_USER_DEPARTMENT character varying (100),
  MOD_USER_DEPARTMENT character varying (100),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_GRIDMEASURE_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_GRIDMEASURE
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_GRIDMEASURE TO PLGM_SERVICE;

CREATE INDEX HUSER_INDEX ON PUBLIC.HTBL_GRIDMEASURE (HUSER);


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:00:00', 'otto',12,0, 'admin', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'otto',12,1, 'admin', '2018-06-11 14:01:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:03:00', 'otto',12,3, 'admin', '2018-06-11 14:03:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:04:00', 'otto',12,4, 'admin', '2018-06-11 14:04:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:05:00', 'otto',12,5, 'admin', '2018-06-11 14:05:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:06:00', 'otto',12,6, 'admin', '2018-06-11 14:06:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:07:00', 'otto',12,7, 'admin', '2018-06-11 14:07:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:08:00', 'otto',12,8, 'admin', '2018-06-11 14:08:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:09:00', 'otto',12,9, 'admin', '2018-06-11 14:09:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:10:00', 'otto',12,10, 'admin', '2018-06-11 14:10:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:11:00', 'otto',12,11, 'admin', '2018-06-11 14:11:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-07-01 14:55:00', 'admin',13,0, 'admin', '2018-07-01 14:55:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-07-01 14:56:00', 'admin',13,1, 'admin', '2018-07-01 14:56:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-07-01 14:57:00', 'admin',13,3, 'admin', '2018-07-01 14:57:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-07-01 14:57:00', 'admin',13,12, 'admin', '2018-07-01 14:57:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (1, '2018-06-11 14:00:00', 'admin',11,0, 'admin', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (1, '2018-06-11 14:01:00', 'admin',11,1, 'admin', '2018-06-11 14:01:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (1, '2018-06-11 14:02:00', 'admin',11,3, 'admin', '2018-06-11 14:02:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (1, '2018-06-11 14:03:00', 'admin',11,4, 'admin', '2018-06-11 14:03:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (1, '2018-06-11 14:04:00', 'admin',11,5, 'admin', '2018-06-11 14:04:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (1, '2018-06-11 14:05:00', 'admin',11,6, 'admin', '2018-06-11 14:05:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (1, '2018-06-11 14:06:00', 'admin',11,7, 'admin', '2018-06-11 14:06:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (1, '2018-06-11 14:07:00', 'admin',11,8, 'admin', '2018-06-11 14:07:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (1, '2018-06-11 14:08:00', 'admin',11,9, 'admin', '2018-06-11 14:08:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (1, '2018-06-11 14:09:00', 'admin',11,10, 'admin', '2018-06-11 14:09:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:00:00', 'admin',10,0, 'admin', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'admin',10,1, 'admin', '2018-06-11 14:01:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:02:00', 'admin',10,3, 'admin', '2018-06-11 14:02:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:03:00', 'admin',10,4, 'admin', '2018-06-11 14:03:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:04:00', 'admin',10,5, 'admin', '2018-06-11 14:04:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:05:00', 'admin',10,6, 'admin', '2018-06-11 14:05:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:06:00', 'admin',10,7, 'admin', '2018-06-11 14:06:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:07:00', 'admin',10,8, 'admin', '2018-06-11 14:07:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:08:00', 'admin',10,9, 'admin', '2018-06-11 14:08:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:00:00', 'admin',9,0, 'admin', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'admin',9,1, 'admin', '2018-06-11 14:01:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:02:00', 'admin',9,3, 'admin', '2018-06-11 14:02:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:03:00', 'admin',9,4, 'admin', '2018-06-11 14:03:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:04:00', 'admin',9,5, 'admin', '2018-06-11 14:04:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:05:00', 'admin',9,6, 'admin', '2018-06-11 14:05:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:06:00', 'admin',9,7, 'admin', '2018-06-11 14:06:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:07:00', 'admin',9,8, 'admin', '2018-06-11 14:07:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:00:00', 'admin',8,0, 'admin', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'admin',8,1, 'admin', '2018-06-11 14:01:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:02:00', 'admin',8,3, 'admin', '2018-06-11 14:02:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:03:00', 'admin',8,4, 'admin', '2018-06-11 14:03:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:04:00', 'admin',8,5, 'admin', '2018-06-11 14:04:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:05:00', 'admin',8,6, 'admin', '2018-06-11 14:05:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:06:00', 'admin',8,7, 'admin', '2018-06-11 14:06:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:00:00', 'admin',7,0, 'admin', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'admin',7,1, 'admin', '2018-06-11 14:01:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:02:00', 'admin',7,3, 'admin', '2018-06-11 14:02:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:03:00', 'admin',7,4, 'admin', '2018-06-11 14:03:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:04:00', 'admin',7,5, 'admin', '2018-06-11 14:04:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:05:00', 'admin',7,6, 'admin', '2018-06-11 14:05:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:00:00', 'admin',6,0, 'admin', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'admin',6,1, 'admin', '2018-06-11 14:01:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:02:00', 'admin',6,3, 'admin', '2018-06-11 14:02:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:03:00', 'admin',6,4, 'admin', '2018-06-11 14:03:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:04:00', 'admin',6,5, 'admin', '2018-06-11 14:04:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:00:00', 'admin',5,0, 'admin', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'admin',5,1, 'admin', '2018-06-11 14:01:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:02:00', 'admin',5,3, 'admin', '2018-06-11 14:02:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:03:00', 'admin',5,4, 'admin', '2018-06-11 14:03:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:00:00', 'admin',4,0, 'admin', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'admin',4,1, 'admin', '2018-06-11 14:01:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:02:00', 'admin',4,3, 'admin', '2018-06-11 14:02:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:00:00', 'admin',3,0, 'admin', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'admin',3,1, 'admin', '2018-06-11 14:01:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:02:00', 'admin',3,3, 'admin', '2018-06-11 14:02:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:03:00', 'admin',3,2, 'admin', '2018-06-11 14:03:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:00:00', 'admin',2,0, 'jasper', '2018-06-11 14:00:00');
INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'admin',2,1, 'jasper', '2018-06-11 14:01:00');


INSERT INTO public.htbl_gridmeasure(haction, hdate, huser, id, fk_ref_gm_status, mod_user, mod_date)
VALUES (2, '2018-06-11 14:01:00', 'admin',1,0, 'otto', '2018-06-11 14:01:00');

-- PUBLIC.HTBL_ID_COUNTER Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_ID_COUNTER;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_ID_COUNTER_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_ID_COUNTER_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_ID_COUNTER_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_ID_COUNTER
(
  HID integer NOT NULL DEFAULT nextval('HTBL_ID_COUNTER_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  COUNTER integer,
  COUNTER_TYPE character varying (256),
  INFO character varying (256),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_ID_COUNTER_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_ID_COUNTER
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_ID_COUNTER TO PLGM_SERVICE;



-- PUBLIC.HTBL_SINGLE_GRIDMEASURE Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_SINGLE_GRIDMEASURE;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_SINGLE_GRIDMEASURE_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_SINGLE_GRIDMEASURE_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_SINGLE_GRIDMEASURE_ID_SEQ
  OWNER TO PLGM_SERVICE;



CREATE TABLE PUBLIC.HTBL_SINGLE_GRIDMEASURE
(
  HID integer NOT NULL DEFAULT nextval('HTBL_SINGLE_GRIDMEASURE_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  SORTORDER integer,
  TITLE character varying (256),
  SWITCHING_OBJECT character varying (256),
  CIM_ID character varying (256),
  CIM_NAME character varying (256),
  CIM_DESCRIPTION character varying (1024),
  RESPONSIBLE_ONSITE_NAME character varying(256),
  RESPONSIBLE_ONSITE_DEPARTMENT character varying(256),
  NETWORK_CONTROL character varying (256),
  PLANNED_STARTTIME_SINGLEMEASURE timestamp without time zone,
  PLANNED_ENDTIME_SINGLEMEASURE timestamp without time zone,
  DESCRIPTION character varying (1024),
  FK_TBL_GRIDMEASURE integer,

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_SINGLE_GRIDMEASURE_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_SINGLE_GRIDMEASURE
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_SINGLE_GRIDMEASURE TO PLGM_SERVICE;




-- PUBLIC.HTBL_STEPS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_STEPS;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_STEPS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_STEPS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_STEPS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_STEPS
(
  HID integer NOT NULL DEFAULT nextval('HTBL_STEPS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  SORTORDER integer,
  SWITCHING_OBJECT character varying (256),
  TYPE character varying(256),
  PRESENT_TIME character varying(256),
  PRESENT_STATE character varying(256),
  TARGET_STATE character varying (256),
  OPERATOR character varying(256),
  FK_TBL_SINGLE_GRIDMEASURE integer,
  CREATE_USER character varying (100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying (100),
  MOD_DATE timestamp without time zone,

  CONSTRAINT HTBL_STEPS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_STEPS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_STEPS TO PLGM_SERVICE;


-- PUBLIC.HTBL_DOCUMENTS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_DOCUMENTS;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_DOCUMENTS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_DOCUMENTS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_DOCUMENTS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_DOCUMENTS
(
  HID integer NOT NULL DEFAULT nextval('HTBL_DOCUMENTS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  DOCUMENT_NAME character varying (260),
  DOCUMENT bytea,

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_DOCUMENTS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_DOCUMENTS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_DOCUMENTS TO PLGM_SERVICE;

-- PUBLIC.HTBL_MEASURE_DOCUMENTS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_MEASURE_DOCUMENTS;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_MEASURE_DOCUMENTS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_MEASURE_DOCUMENTS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_MEASURE_DOCUMENTS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_MEASURE_DOCUMENTS
(
  HID integer NOT NULL DEFAULT nextval('HTBL_MEASURE_DOCUMENTS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  FK_TBL_MEASURE integer,
  FK_TBL_DOCUMENTS integer,

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_MEASURE_DOCUMENTS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_MEASURE_DOCUMENTS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_MEASURE_DOCUMENTS TO PLGM_SERVICE;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- TRIGGER
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- ---------------------------------------------
-- TRIGGER REF_TERRITORY
-- ---------------------------------------------


-- PUBLIC.REF_TERRITORY INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_TERRITORY_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_TERRITORY (
						  ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_TERRITORY_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_TERRITORY_INSERT_TRG ON PUBLIC.REF_TERRITORY;

CREATE TRIGGER REF_TERRITORY_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_TERRITORY
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_TERRITORY_INSERT_TRG();



-- PUBLIC.REF_TERRITORY UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_TERRITORY_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_TERRITORY (
						  ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_TERRITORY_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_TERRITORY_UPDATE_TRG ON PUBLIC.REF_TERRITORY;

CREATE TRIGGER REF_TERRITORY_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_TERRITORY
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_TERRITORY_UPDATE_TRG();



-- PUBLIC.REF_TERRITORY DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_TERRITORY_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_TERRITORY (

						  ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_TERRITORY_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_TERRITORY_DELETE_TRG ON PUBLIC.REF_TERRITORY;

CREATE TRIGGER REF_TERRITORY_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_TERRITORY
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_TERRITORY_DELETE_TRG();



-- PUBLIC.TBL_ID_COUNTER INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_ID_COUNTER_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_ID_COUNTER (
						  ID,COUNTER,COUNTER_TYPE,INFO,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.COUNTER,NEW.COUNTER_TYPE,NEW.INFO,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_ID_COUNTER_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_ID_COUNTER_INSERT_TRG ON PUBLIC.TBL_ID_COUNTER;

CREATE TRIGGER TBL_ID_COUNTER_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_ID_COUNTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_ID_COUNTER_INSERT_TRG();



-- PUBLIC.TBL_ID_COUNTER UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_ID_COUNTER_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_ID_COUNTER (
						  ID,COUNTER,COUNTER_TYPE,INFO,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.COUNTER,NEW.COUNTER_TYPE,NEW.INFO,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_ID_COUNTER_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_ID_COUNTER_UPDATE_TRG ON PUBLIC.TBL_ID_COUNTER;

CREATE TRIGGER TBL_ID_COUNTER_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_ID_COUNTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_ID_COUNTER_UPDATE_TRG();



-- PUBLIC.TBL_ID_COUNTER DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_ID_COUNTER_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_ID_COUNTER (

						  ID,COUNTER,COUNTER_TYPE,INFO,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.COUNTER,OLD.COUNTER_TYPE,OLD.INFO,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_ID_COUNTER_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_ID_COUNTER_DELETE_TRG ON PUBLIC.TBL_ID_COUNTER;

CREATE TRIGGER TBL_ID_COUNTER_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_ID_COUNTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_ID_COUNTER_DELETE_TRG();



-- PUBLIC.TBL_SINGLE_GRIDMEASURE INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_SINGLE_GRIDMEASURE (
						  ID,SORTORDER,TITLE,SWITCHING_OBJECT,CIM_ID,CIM_NAME,CIM_DESCRIPTION,PLANNED_STARTTIME_SINGLEMEASURE,PLANNED_ENDTIME_SINGLEMEASURE,DESCRIPTION, RESPONSIBLE_ONSITE_NAME, RESPONSIBLE_ONSITE_DEPARTMENT,NETWORK_CONTROL,FK_TBL_GRIDMEASURE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.SORTORDER,NEW.TITLE,NEW.SWITCHING_OBJECT,NEW.CIM_ID,NEW.CIM_NAME,NEW.CIM_DESCRIPTION,NEW.PLANNED_STARTTIME_SINGLEMEASURE,NEW.PLANNED_ENDTIME_SINGLEMEASURE,NEW.DESCRIPTION, NEW.RESPONSIBLE_ONSITE_NAME, NEW.RESPONSIBLE_ONSITE_DEPARTMENT,NEW.NETWORK_CONTROL,NEW.FK_TBL_GRIDMEASURE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_SINGLE_GRIDMEASURE_INSERT_TRG ON PUBLIC.TBL_SINGLE_GRIDMEASURE;

CREATE TRIGGER TBL_SINGLE_GRIDMEASURE_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_SINGLE_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_SINGLE_GRIDMEASURE_INSERT_TRG();



-- PUBLIC.TBL_SINGLE_GRIDMEASURE UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_SINGLE_GRIDMEASURE (
						  ID,SORTORDER,TITLE,SWITCHING_OBJECT,CIM_ID,CIM_NAME,CIM_DESCRIPTION,PLANNED_STARTTIME_SINGLEMEASURE,PLANNED_ENDTIME_SINGLEMEASURE,DESCRIPTION,RESPONSIBLE_ONSITE_NAME, RESPONSIBLE_ONSITE_DEPARTMENT,NETWORK_CONTROL,FK_TBL_GRIDMEASURE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.SORTORDER,NEW.TITLE,NEW.SWITCHING_OBJECT,NEW.CIM_ID,NEW.CIM_NAME,NEW.CIM_DESCRIPTION,NEW.PLANNED_STARTTIME_SINGLEMEASURE,NEW.PLANNED_ENDTIME_SINGLEMEASURE,NEW.DESCRIPTION,NEW.RESPONSIBLE_ONSITE_NAME, NEW.RESPONSIBLE_ONSITE_DEPARTMENT,NEW.NETWORK_CONTROL,NEW.FK_TBL_GRIDMEASURE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_SINGLE_GRIDMEASURE_UPDATE_TRG ON PUBLIC.TBL_SINGLE_GRIDMEASURE;

CREATE TRIGGER TBL_SINGLE_GRIDMEASURE_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_SINGLE_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_SINGLE_GRIDMEASURE_UPDATE_TRG();



-- PUBLIC.TBL_SINGLE_GRIDMEASURE DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_SINGLE_GRIDMEASURE (

						  ID,SORTORDER,TITLE,SWITCHING_OBJECT,CIM_ID,CIM_NAME,CIM_DESCRIPTION,PLANNED_STARTTIME_SINGLEMEASURE,PLANNED_ENDTIME_SINGLEMEASURE,DESCRIPTION,RESPONSIBLE_ONSITE_NAME, RESPONSIBLE_ONSITE_DEPARTMENT, NETWORK_CONTROL,FK_TBL_GRIDMEASURE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.SORTORDER,OLD.TITLE,OLD.SWITCHING_OBJECT,OLD.CIM_ID,OLD.CIM_NAME,OLD.CIM_DESCRIPTION,OLD.PLANNED_STARTTIME_SINGLEMEASURE,OLD.PLANNED_ENDTIME_SINGLEMEASURE,OLD.DESCRIPTION,OLD.RESPONSIBLE_ONSITE_NAME, OLD.RESPONSIBLE_ONSITE_DEPARTMENT,OLD.NETWORK_CONTROL,OLD.FK_TBL_GRIDMEASURE,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_SINGLE_GRIDMEASURE_DELETE_TRG ON PUBLIC.TBL_SINGLE_GRIDMEASURE;

CREATE TRIGGER TBL_SINGLE_GRIDMEASURE_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_SINGLE_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_SINGLE_GRIDMEASURE_DELETE_TRG();





-- PUBLIC.TBL_STEPS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_STEPS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_STEPS (
						  ID,SORTORDER,SWITCHING_OBJECT,TYPE,PRESENT_TIME, PRESENT_STATE, TARGET_STATE, OPERATOR, FK_TBL_SINGLE_GRIDMEASURE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.SORTORDER,NEW.SWITCHING_OBJECT,NEW.TYPE, NEW.PRESENT_TIME, NEW.PRESENT_STATE, NEW.TARGET_STATE, NEW.OPERATOR, NEW.FK_TBL_SINGLE_GRIDMEASURE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_STEPS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_STEPS_INSERT_TRG ON PUBLIC.TBL_STEPS;

CREATE TRIGGER TBL_STEPS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_STEPS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_STEPS_INSERT_TRG();



-- PUBLIC.TBL_STEPS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_STEPS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_STEPS (
						  ID,SORTORDER,SWITCHING_OBJECT,TYPE,PRESENT_TIME, PRESENT_STATE, TARGET_STATE, OPERATOR,FK_TBL_SINGLE_GRIDMEASURE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.SORTORDER,NEW.SWITCHING_OBJECT,NEW.TYPE,NEW.PRESENT_TIME, NEW.PRESENT_STATE, NEW.TARGET_STATE, NEW.OPERATOR,NEW.FK_TBL_SINGLE_GRIDMEASURE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_STEPS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_STEPS_UPDATE_TRG ON PUBLIC.TBL_STEPS;

CREATE TRIGGER TBL_STEPS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_STEPS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_STEPS_UPDATE_TRG();



-- PUBLIC.TBL_STEPS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_STEPS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_STEPS (

						  ID,SORTORDER,SWITCHING_OBJECT,TYPE,PRESENT_TIME, PRESENT_STATE, TARGET_STATE, OPERATOR,FK_TBL_SINGLE_GRIDMEASURE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.SORTORDER,OLD.SWITCHING_OBJECT,OLD.TYPE,OLD.PRESENT_TIME, OLD.PRESENT_STATE, OLD.TARGET_STATE, OLD.OPERATOR,OLD.FK_TBL_SINGLE_GRIDMEASURE,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_STEPS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_STEPS_DELETE_TRG ON PUBLIC.TBL_STEPS;

CREATE TRIGGER TBL_STEPS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_STEPS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_STEPS_DELETE_TRG();


-- PUBLIC.TBL_LOCK INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_LOCK_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_LOCK (
						  ID,KEY,USERNAME,INFO,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.KEY,NEW.USERNAME,NEW.INFO,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_LOCK_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_LOCK_INSERT_TRG ON PUBLIC.TBL_LOCK;

CREATE TRIGGER TBL_LOCK_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_LOCK
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_LOCK_INSERT_TRG();



-- PUBLIC.TBL_LOCK UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_LOCK_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_LOCK (
						  ID,KEY,USERNAME,INFO,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.KEY,NEW.USERNAME,NEW.INFO,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_LOCK_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_LOCK_UPDATE_TRG ON PUBLIC.TBL_LOCK;

CREATE TRIGGER TBL_LOCK_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_LOCK
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_LOCK_UPDATE_TRG();



-- PUBLIC.TBL_LOCK DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_LOCK_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_LOCK (

						  ID,KEY,USERNAME,INFO,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.KEY,OLD.USERNAME,OLD.INFO,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_LOCK_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_LOCK_DELETE_TRG ON PUBLIC.TBL_LOCK;

CREATE TRIGGER TBL_LOCK_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_LOCK
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_LOCK_DELETE_TRG();




-- PUBLIC.TBL_USER_SETTINGS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_USER_SETTINGS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_USER_SETTINGS (
						  ID,USERNAME,SETTING_TYPE,VALUE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.USERNAME,NEW.SETTING_TYPE,NEW.VALUE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_USER_SETTINGS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_USER_SETTINGS_INSERT_TRG ON PUBLIC.TBL_USER_SETTINGS;

CREATE TRIGGER TBL_USER_SETTINGS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_USER_SETTINGS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_USER_SETTINGS_INSERT_TRG();



-- PUBLIC.TBL_USER_SETTINGS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_USER_SETTINGS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_USER_SETTINGS (
						  ID,USERNAME,SETTING_TYPE,VALUE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.USERNAME,NEW.SETTING_TYPE,NEW.VALUE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_USER_SETTINGS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_USER_SETTINGS_UPDATE_TRG ON PUBLIC.TBL_USER_SETTINGS;

CREATE TRIGGER TBL_USER_SETTINGS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_USER_SETTINGS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_USER_SETTINGS_UPDATE_TRG();



-- PUBLIC.TBL_USER_SETTINGS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_USER_SETTINGS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_USER_SETTINGS (

						  ID,USERNAME,SETTING_TYPE,VALUE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.USERNAME,OLD.SETTING_TYPE,OLD.VALUE,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_USER_SETTINGS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_USER_SETTINGS_DELETE_TRG ON PUBLIC.TBL_USER_SETTINGS;

CREATE TRIGGER TBL_USER_SETTINGS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_USER_SETTINGS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_USER_SETTINGS_DELETE_TRG();


-- PUBLIC.HREF_COST_CENTER INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.HREF_COST_CENTER_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HHREF_COST_CENTER (
						  HID,HACTION,HDATE,HUSER,ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.HID,NEW.HACTION,NEW.HDATE,NEW.HUSER,NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.HREF_COST_CENTER_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS HREF_COST_CENTER_INSERT_TRG ON PUBLIC.HREF_COST_CENTER;

CREATE TRIGGER HREF_COST_CENTER_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.HREF_COST_CENTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.HREF_COST_CENTER_INSERT_TRG();



-- PUBLIC.HREF_COST_CENTER UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.HREF_COST_CENTER_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HHREF_COST_CENTER (
						  HID,HACTION,HDATE,HUSER,ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.HID,NEW.HACTION,NEW.HDATE,NEW.HUSER,NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.HREF_COST_CENTER_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS HREF_COST_CENTER_UPDATE_TRG ON PUBLIC.HREF_COST_CENTER;

CREATE TRIGGER HREF_COST_CENTER_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.HREF_COST_CENTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.HREF_COST_CENTER_UPDATE_TRG();



-- PUBLIC.HREF_COST_CENTER DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.HREF_COST_CENTER_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HHREF_COST_CENTER (

						  HID,HACTION,HDATE,HUSER,ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.HID,OLD.HACTION,OLD.HDATE,OLD.HUSER,OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.HREF_COST_CENTER_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS HREF_COST_CENTER_DELETE_TRG ON PUBLIC.HREF_COST_CENTER;

CREATE TRIGGER HREF_COST_CENTER_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.HREF_COST_CENTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.HREF_COST_CENTER_DELETE_TRG();



-- PUBLIC.REF_BRANCH INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH (

						  ID,NAME,DESCRIPTION,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.COLOR_CODE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_BRANCH_INSERT_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_INSERT_TRG();



-- PUBLIC.REF_BRANCH UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH (
						  ID,NAME,DESCRIPTION,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.COLOR_CODE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_UPDATE_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_UPDATE_TRG();



-- PUBLIC.REF_BRANCH DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_BRANCH (
						  ID,NAME,DESCRIPTION,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,
						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.COLOR_CODE,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,
                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_DELETE_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_DELETE_TRG();



-- PUBLIC.REF_BRANCH_LEVEL INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_LEVEL_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH_LEVEL (
						  ID,NAME,DESCRIPTION,FK_REF_BRANCH,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_LEVEL_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_BRANCH_LEVEL_INSERT_TRG ON PUBLIC.REF_BRANCH_LEVEL;

CREATE TRIGGER REF_BRANCH_LEVEL_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_BRANCH_LEVEL
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_LEVEL_INSERT_TRG();



-- PUBLIC.REF_BRANCH_LEVEL UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_LEVEL_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH_LEVEL (
						  ID,NAME,DESCRIPTION,FK_REF_BRANCH,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_LEVEL_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_LEVEL_UPDATE_TRG ON PUBLIC.REF_BRANCH_LEVEL;

CREATE TRIGGER REF_BRANCH_LEVEL_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_BRANCH_LEVEL
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_LEVEL_UPDATE_TRG();



-- PUBLIC.REF_BRANCH_LEVEL DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_LEVEL_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_BRANCH_LEVEL (

						  ID,NAME,DESCRIPTION,FK_REF_BRANCH,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.FK_REF_BRANCH,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_LEVEL_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_LEVEL_DELETE_TRG ON PUBLIC.REF_BRANCH_LEVEL;

CREATE TRIGGER REF_BRANCH_LEVEL_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_BRANCH_LEVEL
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_LEVEL_DELETE_TRG();





-- PUBLIC.REF_GM_STATUS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_GM_STATUS (
						  ID,NAME,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.COLOR_CODE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_GM_STATUS_INSERT_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_INSERT_TRG();



-- PUBLIC.REF_GM_STATUS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_GM_STATUS (
						  ID,NAME,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.COLOR_CODE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_GM_STATUS_UPDATE_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_UPDATE_TRG();



-- PUBLIC.REF_GM_STATUS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_GM_STATUS (

						  ID,NAME,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.COLOR_CODE,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_GM_STATUS_DELETE_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_DELETE_TRG();



-- PUBLIC.TBL_GRIDMEASURE INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_GRIDMEASURE (
						  ID,ID_DESCRIPTIVE,TITLE,AFFECTED_RESOURCE,REMARK,EMAIL_ADDRESSES,FK_REF_GM_STATUS,SWITCHING_OBJECT,COST_CENTER,APPROVAL_BY,AREA_OF_SWITCHING,APPOINTMENT_REPETITION,APPOINTMENT_STARTDATE,APPOINTMENT_NUMBEROF,PLANNED_STARTTIME_FIRST_SINGLEMEASURE,ENDTIME_GRIDMEASURE,TIME_OF_REALLOCATION,DESCRIPTION,FK_REF_BRANCH,FK_REF_BRANCH_LEVEL,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.ID_DESCRIPTIVE,NEW.TITLE,NEW.AFFECTED_RESOURCE,NEW.REMARK,NEW.EMAIL_ADDRESSES,NEW.FK_REF_GM_STATUS,NEW.SWITCHING_OBJECT,NEW.COST_CENTER,NEW.APPROVAL_BY,NEW.AREA_OF_SWITCHING,NEW.APPOINTMENT_REPETITION,NEW.APPOINTMENT_STARTDATE,NEW.APPOINTMENT_NUMBEROF,NEW.PLANNED_STARTTIME_FIRST_SINGLEMEASURE,NEW.ENDTIME_GRIDMEASURE,NEW.TIME_OF_REALLOCATION,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.FK_REF_BRANCH_LEVEL,NEW.CREATE_USER,NEW.CREATE_USER_DEPARTMENT,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_USER_DEPARTMENT,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_INSERT_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_INSERT_TRG();



-- PUBLIC.TBL_GRIDMEASURE UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_GRIDMEASURE (
						  ID,ID_DESCRIPTIVE,TITLE,AFFECTED_RESOURCE,REMARK,EMAIL_ADDRESSES,FK_REF_GM_STATUS,SWITCHING_OBJECT,COST_CENTER,APPROVAL_BY,AREA_OF_SWITCHING,APPOINTMENT_REPETITION,APPOINTMENT_STARTDATE,APPOINTMENT_NUMBEROF,PLANNED_STARTTIME_FIRST_SINGLEMEASURE,ENDTIME_GRIDMEASURE,TIME_OF_REALLOCATION,DESCRIPTION,FK_REF_BRANCH,FK_REF_BRANCH_LEVEL,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.ID_DESCRIPTIVE,NEW.TITLE,NEW.AFFECTED_RESOURCE,NEW.REMARK,NEW.EMAIL_ADDRESSES,NEW.FK_REF_GM_STATUS,NEW.SWITCHING_OBJECT,NEW.COST_CENTER,NEW.APPROVAL_BY,NEW.AREA_OF_SWITCHING,NEW.APPOINTMENT_REPETITION,NEW.APPOINTMENT_STARTDATE,NEW.APPOINTMENT_NUMBEROF,NEW.PLANNED_STARTTIME_FIRST_SINGLEMEASURE,NEW.ENDTIME_GRIDMEASURE,NEW.TIME_OF_REALLOCATION,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.FK_REF_BRANCH_LEVEL,NEW.CREATE_USER,NEW.CREATE_USER_DEPARTMENT,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_USER_DEPARTMENT,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_UPDATE_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG();



-- PUBLIC.TBL_GRIDMEASURE DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_GRIDMEASURE (

						  ID,ID_DESCRIPTIVE,TITLE,AFFECTED_RESOURCE,REMARK,EMAIL_ADDRESSES,FK_REF_GM_STATUS,SWITCHING_OBJECT,COST_CENTER,APPROVAL_BY,AREA_OF_SWITCHING,APPOINTMENT_REPETITION,APPOINTMENT_STARTDATE,APPOINTMENT_NUMBEROF,PLANNED_STARTTIME_FIRST_SINGLEMEASURE,ENDTIME_GRIDMEASURE,TIME_OF_REALLOCATION,DESCRIPTION,FK_REF_BRANCH,FK_REF_BRANCH_LEVEL,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.ID_DESCRIPTIVE,OLD.TITLE,OLD.AFFECTED_RESOURCE,OLD.REMARK,OLD.EMAIL_ADDRESSES,OLD.FK_REF_GM_STATUS,OLD.SWITCHING_OBJECT,OLD.COST_CENTER,OLD.APPROVAL_BY,OLD.AREA_OF_SWITCHING,OLD.APPOINTMENT_REPETITION,OLD.APPOINTMENT_STARTDATE,OLD.APPOINTMENT_NUMBEROF,OLD.PLANNED_STARTTIME_FIRST_SINGLEMEASURE,OLD.ENDTIME_GRIDMEASURE,OLD.TIME_OF_REALLOCATION,OLD.DESCRIPTION,OLD.FK_REF_BRANCH,OLD.FK_REF_BRANCH_LEVEL,OLD.CREATE_USER,OLD.CREATE_USER_DEPARTMENT,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_USER_DEPARTMENT,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_DELETE_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_DELETE_TRG();


-- PUBLIC.TBL_DOCUMENTS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_DOCUMENTS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_DOCUMENTS (
						  ID,DOCUMENT_NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.DOCUMENT_NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_DOCUMENTS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_DOCUMENTS_INSERT_TRG ON PUBLIC.TBL_DOCUMENTS;

CREATE TRIGGER TBL_DOCUMENTS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_DOCUMENTS_INSERT_TRG();



-- PUBLIC.TBL_DOCUMENTS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_DOCUMENTS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_DOCUMENTS (
						  ID,DOCUMENT_NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.DOCUMENT_NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_DOCUMENTS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_DOCUMENTS_UPDATE_TRG ON PUBLIC.TBL_DOCUMENTS;

CREATE TRIGGER TBL_DOCUMENTS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_DOCUMENTS_UPDATE_TRG();



-- PUBLIC.TBL_DOCUMENTS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_DOCUMENTS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_DOCUMENTS (

						  ID,DOCUMENT_NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.DOCUMENT_NAME,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_DOCUMENTS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_DOCUMENTS_DELETE_TRG ON PUBLIC.TBL_DOCUMENTS;

CREATE TRIGGER TBL_DOCUMENTS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_DOCUMENTS_DELETE_TRG();






-- PUBLIC.TBL_MEASURE_DOCUMENTS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_MEASURE_DOCUMENTS (
						  ID,FK_TBL_MEASURE,FK_TBL_DOCUMENTS,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.FK_TBL_MEASURE,NEW.FK_TBL_DOCUMENTS,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_MEASURE_DOCUMENTS_INSERT_TRG ON PUBLIC.TBL_MEASURE_DOCUMENTS;

CREATE TRIGGER TBL_MEASURE_DOCUMENTS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_MEASURE_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_MEASURE_DOCUMENTS_INSERT_TRG();



-- PUBLIC.TBL_MEASURE_DOCUMENTS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_MEASURE_DOCUMENTS (
						  ID,FK_TBL_MEASURE,FK_TBL_DOCUMENTS,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.FK_TBL_MEASURE,NEW.FK_TBL_DOCUMENTS,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_MEASURE_DOCUMENTS_UPDATE_TRG ON PUBLIC.TBL_MEASURE_DOCUMENTS;

CREATE TRIGGER TBL_MEASURE_DOCUMENTS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_MEASURE_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_MEASURE_DOCUMENTS_UPDATE_TRG();



-- PUBLIC.TBL_MEASURE_DOCUMENTS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_MEASURE_DOCUMENTS (

						  ID,FK_TBL_MEASURE,FK_TBL_DOCUMENTS,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.FK_TBL_MEASURE,OLD.FK_TBL_DOCUMENTS,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_MEASURE_DOCUMENTS_DELETE_TRG ON PUBLIC.TBL_MEASURE_DOCUMENTS;

CREATE TRIGGER TBL_MEASURE_DOCUMENTS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_MEASURE_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_MEASURE_DOCUMENTS_DELETE_TRG();
