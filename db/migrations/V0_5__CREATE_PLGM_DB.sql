-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2018 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------
--ADD DATA TO TABLE TBL_GRIDMEASURE;
--ADD DATA TO TABLE REF_GM_STATUS;

-- ---------------------------------------------
-- DROPS
-- ---------------------------------------------
DROP TABLE IF EXISTS public.TBL_GRIDMEASURE;
DROP SEQUENCE IF EXISTS public.TBL_GRIDMEASURE_ID_SEQ;

DROP TABLE IF EXISTS public.REF_BRANCH;
DROP SEQUENCE IF EXISTS public.REF_BRANCH_ID_SEQ;

DROP TABLE IF EXISTS public.REF_GM_STATUS;
DROP SEQUENCE IF EXISTS public.REF_GM_STATUS_ID_SEQ;

-- ---------------------------------------------
-- TABLE REF_BRANCH
-- ---------------------------------------------
CREATE SEQUENCE public.REF_BRANCH_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.REF_BRANCH_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.REF_BRANCH
(
  ID integer NOT NULL DEFAULT nextval('REF_BRANCH_ID_SEQ'::regclass),
  NAME character varying(50) NOT NULL,
  DESCRIPTION character varying(255),
  CREATE_USER character varying(100) NOT NULL,
  CREATE_DATE timestamp without time zone  NOT NULL,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone NOT NULL,
  CONSTRAINT REF_BRANCH_PKEY PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.REF_BRANCH
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_BRANCH TO PLGM_SERVICE;

-- ---------------------------------------------
-- TABLE REF_GM_STATUS
-- ---------------------------------------------
CREATE SEQUENCE public.REF_GM_STATUS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.REF_GM_STATUS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.REF_GM_STATUS
(
  ID integer NOT NULL DEFAULT nextval('REF_GM_STATUS_ID_SEQ'::regclass),
  NAME character varying(50) NOT NULL,
  CREATE_USER character varying(100) NOT NULL,
  CREATE_DATE timestamp without time zone NOT NULL,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT REF_GM_STATUS_PKEY PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.REF_GM_STATUS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_GM_STATUS TO PLGM_SERVICE;

INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (1,'angelegt','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (2,'beantragt','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (3,'zurückgewiesen','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (4,'zur Genehmigung','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (5,'storniert','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (6,'genehmigt','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (7,'storniert','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (8,'freigegeben','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (9,'Schalten aktiv','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (10,'in Arbeit','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (11,'Arbeit beendet','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (12,'beendet','Suse Sommerwind','20.03.2018',NULL,'20.03.2018');


-- ---------------------------------------------
-- TABLE TBL_GRIDMEASURE
-- ---------------------------------------------
CREATE SEQUENCE public.TBL_GRIDMEASURE_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.TBL_GRIDMEASURE_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.TBL_GRIDMEASURE
(
  ID integer NOT NULL DEFAULT nextval('TBL_GRIDMEASURE_ID_SEQ'::regclass),
  ID_EXT character varying(50) NULL,
  TITLE character varying(256) NULL,
  AFFECTED_RESOURCE character varying(256) NULL,
  REMARK character varying(1024) NULL,
  FK_REF_GM_STATUS integer NULL,

  CREATE_USER character varying(100) NOT NULL,
  CREATE_USER_DEPARTMENT character varying(100) NULL,
  CREATE_DATE timestamp without time zone NOT NULL,
  MOD_USER character varying(100),
  MOD_USER_DEPARTMENT character varying(100) NULL,
  MOD_DATE timestamp without time zone,


  CONSTRAINT TBL_GRIDMEASURE_PKEY PRIMARY KEY (ID),
  CONSTRAINT FK_GRIDMEASURE__GM_STATUS FOREIGN KEY (FK_REF_GM_STATUS)
      REFERENCES public.REF_GM_STATUS (ID) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.TBL_GRIDMEASURE
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.TBL_GRIDMEASURE TO PLGM_SERVICE;

INSERT INTO
  public.TBL_GRIDMEASURE(id_ext, title, affected_resource, remark, fk_ref_gm_status, create_user, create_user_department, create_date, mod_user, mod_user_department, mod_date)
VALUES
  ('20','Kabel erneuern','Leitung xyz','Kabel wurde vom Bagger überfahren und muss auf 150 cm erneuert werden',1,'Marko Maienschein','Akute Einsatztruppe','19.03.2018',NULL,NULL,'19.03.2018');
INSERT INTO
  public.TBL_GRIDMEASURE(id_ext, title, affected_resource, remark, fk_ref_gm_status, create_user, create_user_department, create_date, mod_user, mod_user_department, mod_date)
VALUES
  ('21','Kabel abdichten','Kabel zzz','Isolierung erneuern',4,'Tom Tulipan','Akute Einsatztruppe','19.03.2018',NULL,NULL,'19.03.2018');
INSERT INTO
  public.TBL_GRIDMEASURE(id_ext, title, affected_resource, remark, fk_ref_gm_status, create_user, create_user_department, create_date, mod_user, mod_user_department, mod_date)
VALUES
  ('22','Stromkasten reparieren','Kasten srt','Kasten wurde gerammt Frontdeckel muß ausgetauscht werden',8,'Wendy Wüstenwind','Akute Einsatztruppe','17.03.2018',NULL,NULL,'17.03.2018');


DROP TABLE IF EXISTS public.REF_VERSION;

CREATE TABLE public.REF_VERSION
(
  id integer NOT NULL,
  version character varying(100) NOT NULL,
  CONSTRAINT ref_version_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.REF_VERSION
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_VERSION TO PLGM_SERVICE;

INSERT INTO REF_VERSION VALUES (1, '0.0.1_PG');

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- HISTORY-TABLES
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- PUBLIC.HREF_BRANCH Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_BRANCH;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_BRANCH_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_BRANCH_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_BRANCH_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_BRANCH
(
  HID integer NOT NULL DEFAULT nextval('HREF_BRANCH_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying(50),
  DESCRIPTION character varying(255),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_BRANCH_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_BRANCH
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_BRANCH TO PLGM_SERVICE;

-- PUBLIC.HREF_GM_STATUS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_GM_STATUS;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_GM_STATUS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_GM_STATUS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_GM_STATUS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_GM_STATUS
(
  HID integer NOT NULL DEFAULT nextval('HREF_GM_STATUS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying(50),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_GM_STATUS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_GM_STATUS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_GM_STATUS TO PLGM_SERVICE;





-- PUBLIC.HTBL_GRIDMEASURE Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_GRIDMEASURE;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_GRIDMEASURE_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_GRIDMEASURE_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_GRIDMEASURE_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_GRIDMEASURE
(
  HID integer NOT NULL DEFAULT nextval('HTBL_GRIDMEASURE_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  ID_EXT character varying (50),
  TITLE character varying (256),
  AFFECTED_RESOURCE character varying (256),
  REMARK character varying (1024),
  FK_REF_GM_STATUS integer,
  CREATE_USER_DEPARTMENT character varying (100),
  MOD_USER_DEPARTMENT character varying (100),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_GRIDMEASURE_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_GRIDMEASURE
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_GRIDMEASURE TO PLGM_SERVICE;



-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- TRIGGER
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------


-- PUBLIC.REF_BRANCH INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH (
						  ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_BRANCH_INSERT_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_INSERT_TRG();



-- PUBLIC.REF_BRANCH UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH (
						  ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_UPDATE_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_UPDATE_TRG();



-- PUBLIC.REF_BRANCH DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_BRANCH (

						  ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_DELETE_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_DELETE_TRG();






-- PUBLIC.REF_GM_STATUS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_GM_STATUS (
						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_GM_STATUS_INSERT_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_INSERT_TRG();



-- PUBLIC.REF_GM_STATUS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_GM_STATUS (
						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_GM_STATUS_UPDATE_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_UPDATE_TRG();



-- PUBLIC.REF_GM_STATUS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_GM_STATUS (

						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_GM_STATUS_DELETE_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_DELETE_TRG();




-- PUBLIC.TBL_GRIDMEASURE INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_GRIDMEASURE (
						  ID,ID_EXT,TITLE,AFFECTED_RESOURCE,REMARK,FK_REF_GM_STATUS,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.ID_EXT,NEW.TITLE,NEW.AFFECTED_RESOURCE,NEW.REMARK,NEW.FK_REF_GM_STATUS,NEW.CREATE_USER,NEW.CREATE_USER_DEPARTMENT,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_USER_DEPARTMENT,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_INSERT_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_INSERT_TRG();



-- PUBLIC.TBL_GRIDMEASURE UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_GRIDMEASURE (
						  ID,ID_EXT,TITLE,AFFECTED_RESOURCE,REMARK,FK_REF_GM_STATUS,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.ID_EXT,NEW.TITLE,NEW.AFFECTED_RESOURCE,NEW.REMARK,NEW.FK_REF_GM_STATUS,NEW.CREATE_USER,NEW.CREATE_USER_DEPARTMENT,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_USER_DEPARTMENT,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_UPDATE_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG();



-- PUBLIC.TBL_GRIDMEASURE DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_GRIDMEASURE (

						  ID,ID_EXT,TITLE,AFFECTED_RESOURCE,REMARK,FK_REF_GM_STATUS,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.ID_EXT,OLD.TITLE,OLD.AFFECTED_RESOURCE,OLD.REMARK,OLD.FK_REF_GM_STATUS,OLD.CREATE_USER,OLD.CREATE_USER_DEPARTMENT,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_USER_DEPARTMENT,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_DELETE_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_DELETE_TRG();







