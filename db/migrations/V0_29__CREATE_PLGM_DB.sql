-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2018 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------
--Table TBL_LOCK added, table HTBL_LOCK added incl. Triggers

-- ---------------------------------------------
-- DROPS
-- ---------------------------------------------
DROP TABLE IF EXISTS public.TBL_SINGLE_GRIDMEASURE;
DROP SEQUENCE IF EXISTS public.TBL_SINGLE_GRIDMEASURE_ID_SEQ;

DROP TABLE IF EXISTS public.TBL_LOCK CASCADE;
DROP SEQUENCE IF EXISTS public.TBL_LOCK_ID_SEQ;

DROP TABLE IF EXISTS public.REF_USER_SETTINGS CASCADE;
DROP SEQUENCE IF EXISTS public.REF_USER_SETTINGS_ID_SEQ;

DROP TABLE IF EXISTS public.TBL_USER_SETTINGS CASCADE;
DROP SEQUENCE IF EXISTS public.TBL_USER_SETTINGS_ID_SEQ;

DROP TABLE IF EXISTS public.REF_USER_DEPARTMENT CASCADE;
DROP SEQUENCE IF EXISTS public.REF_USER_DEPARTMENT_ID_SEQ;

DROP TABLE IF EXISTS public.REF_COST_CENTER CASCADE;
DROP SEQUENCE IF EXISTS public.REF_COST_CENTER_ID_SEQ;

DROP TABLE IF EXISTS public.TBL_GRIDMEASURE CASCADE;
DROP SEQUENCE IF EXISTS public.TBL_GRIDMEASURE_ID_SEQ;

DROP TABLE IF EXISTS public.REF_BRANCH CASCADE;
DROP SEQUENCE IF EXISTS public.REF_BRANCH_ID_SEQ;

DROP TABLE IF EXISTS public.REF_BRANCH_LEVEL CASCADE;
DROP SEQUENCE IF EXISTS public.REF_BRANCH_LEVEL_ID_SEQ;

DROP TABLE IF EXISTS public.REF_GM_STATUS CASCADE;
DROP SEQUENCE IF EXISTS public.REF_GM_STATUS_ID_SEQ;

DROP TABLE IF EXISTS public.tbl_documents CASCADE;
DROP SEQUENCE IF EXISTS public.tbl_documents_id_seq;

DROP TABLE IF EXISTS public.tbl_measure_documents CASCADE;
DROP SEQUENCE IF EXISTS public.tbl_measure_documents_id_seq;



-- ---------------------------------------------
-- TABLE TBL_LOCK
-- ---------------------------------------------

CREATE SEQUENCE public.TBL_LOCK_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.TBL_LOCK_ID_SEQ
  OWNER TO plgm_service;

CREATE TABLE public.tbl_lock
(
  id integer NOT NULL DEFAULT nextval('TBL_LOCK_ID_SEQ'::regclass),
  key integer NOT NULL, -- Id from tbl_gridmeasure
  username character varying(50) NOT NULL,
  info character varying(256),
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_lock_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_lock
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_lock TO plgm_service;

CREATE UNIQUE INDEX tbl_lock_unique_key ON public.tbl_lock (key ASC, info ASC );

-- ---------------------------------------------
-- TABLE TBL_USER_SETTINGS
-- ---------------------------------------------

CREATE SEQUENCE public.tbl_user_settings_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.tbl_user_settings_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.tbl_user_settings
(
  id integer NOT NULL DEFAULT nextval('tbl_user_settings_id_seq'::regclass),
  username character varying(50) NOT NULL,
  setting_type character varying(50),
  value character varying(4096),
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_user_settings_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_user_settings
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_user_settings TO plgm_service;

CREATE UNIQUE INDEX tbl_user_set_unique_key ON public.tbl_user_settings (username ASC, setting_type ASC );

-- ---------------------------------------------
-- TABLE REF_USER_DEPARTMENT
-- ---------------------------------------------

CREATE SEQUENCE public.REF_USER_DEPARTMENT_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 3
  CACHE 1;
ALTER TABLE public.REF_USER_DEPARTMENT_ID_SEQ
  OWNER TO plgm_service;


CREATE TABLE public.REF_USER_DEPARTMENT
(
  id integer NOT NULL DEFAULT nextval('REF_USER_DEPARTMENT_ID_SEQ'::regclass),
  name character varying(50) NOT NULL,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT ref_user_department_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.REF_USER_DEPARTMENT
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.REF_USER_DEPARTMENT TO plgm_service;


INSERT INTO public.ref_user_department(
            name, create_user, create_date, mod_user, mod_date)
    VALUES ('Schnelle Truppe', 'testuser', '12.04.2018','testuser', '12.04.2018');
INSERT INTO public.ref_user_department(
            name, create_user, create_date, mod_user, mod_date)
    VALUES ('Abt. Aufsicht', 'testuser', '12.04.2018','testuser', '12.04.2018');
INSERT INTO public.ref_user_department(
            name, create_user, create_date, mod_user, mod_date)
    VALUES ('Abt. Wartung', 'testuser', '12.04.2018','testuser', '12.04.2018');

-- ---------------------------------------------
-- TABLE REF_COST_CENTER
-- ---------------------------------------------

CREATE SEQUENCE public.REF_COST_CENTER_ID_SEQ
   INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.REF_COST_CENTER_ID_SEQ
  OWNER TO plgm_service;


CREATE TABLE public.REF_COST_CENTER
(
  id integer NOT NULL DEFAULT nextval('REF_COST_CENTER_id_seq'::regclass),
  name character varying(50) NOT NULL,
  description character varying(255),
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT REF_COST_CENTER_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.REF_COST_CENTER
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.REF_COST_CENTER TO plgm_service;

INSERT INTO public.ref_cost_center(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('K-155', 'Kostenstelle der Abteilung abc', 'testuser','12.04.2018','testuser','12.04.2018');
INSERT INTO public.ref_cost_center(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('K-004', 'Kostenstelle der Abteilung xyz', 'testuser','12.04.2018','testuser','12.04.2018');
INSERT INTO public.ref_cost_center(
            name, description, create_user, create_date, mod_user, mod_date)
    VALUES ('K-222', 'Kostenstelle der Abteilung hij', 'testuser','12.04.2018','testuser','12.04.2018');

-- ---------------------------------------------
-- TABLE REF_BRANCH
-- ---------------------------------------------
CREATE SEQUENCE public.REF_BRANCH_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.REF_BRANCH_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.REF_BRANCH
(
  ID integer NOT NULL DEFAULT nextval('REF_BRANCH_ID_SEQ'::regclass),
  NAME character varying(50) NOT NULL,
  DESCRIPTION character varying(255),
  COLOR_CODE character varying(20),
  CREATE_USER character varying(100) NOT NULL,
  CREATE_DATE timestamp without time zone  NOT NULL,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT REF_BRANCH_PKEY PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.REF_BRANCH
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_BRANCH TO PLGM_SERVICE;

INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('S', 'Strom', '#fc6042','testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('G', 'Gas', '#fdea64', 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('F', 'Fernwärme', '#2cc990', 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.ref_branch(name, description, color_code, create_user, create_date, mod_user, mod_date)
    VALUES ('W', 'Wasser', '#2c82c9', 'testuser', '12.04.2018', 'testuser', '12.04.2018');
-- ---------------------------------------------
-- TABLE REF_BRANCH_LEVEL
-- ---------------------------------------------
CREATE SEQUENCE public.ref_branch_level_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.ref_branch_level_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.ref_branch_level
(
  id integer NOT NULL DEFAULT nextval('ref_branch_level_id_seq'::regclass),
  name character varying(50) NOT NULL,
  description character varying(255),
  fk_ref_branch integer NULL,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT ref_branch_level_pkey PRIMARY KEY (id),
  CONSTRAINT FK_REF_BRANCH_LEVEL__REF_BRANCH FOREIGN KEY (FK_REF_BRANCH)
      REFERENCES public.REF_BRANCH (ID) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.ref_branch_level
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.ref_branch_level TO plgm_service;

INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Level1', 'eine Beschreibung', 2, 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Level2', 'noch eine Beschreibung', 2, 'testuser', '12.04.2018', 'testuser', '12.04.2018');
INSERT INTO public.REF_BRANCH_LEVEL(
            name, description, fk_ref_branch, create_user, create_date, mod_user, mod_date)
    VALUES ('Level3', 'und noch eine Beschreibung', 3, 'testuser', '12.04.2018', 'testuser', '12.04.2018');

-- ---------------------------------------------
-- TABLE REF_GM_STATUS
-- ---------------------------------------------
CREATE SEQUENCE public.REF_GM_STATUS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.REF_GM_STATUS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.REF_GM_STATUS
(
  ID integer NOT NULL DEFAULT nextval('REF_GM_STATUS_ID_SEQ'::regclass),
  NAME character varying(50) NOT NULL,
  CREATE_USER character varying(100) NOT NULL,
  CREATE_DATE timestamp without time zone NOT NULL,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT REF_GM_STATUS_PKEY PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE public.REF_GM_STATUS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_GM_STATUS TO PLGM_SERVICE;

INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (0,'Neu','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (1,'Beantragt','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (2,'Storniert','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (3,'Zur Genehmigung','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (4,'Genehmigt','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (5,'Angefordert','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (6,'Freigegeben','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (7,'Schalten aktiv','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (8,'In Arbeit','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (9,'Arbeit beendet','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (10,'Maßnahme beendet','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (11,'Geschlossen','system','09.04.2018','system','09.04.2018');
INSERT INTO
  public.REF_GM_STATUS(id, name, create_user, create_date, mod_user, mod_date)
VALUES
  (12,'Abgelehnt','system','17.06.2018','system','17.06.2018');



-- ---------------------------------------------
-- TABLE TBL_GRIDMEASURE
-- ---------------------------------------------
-- REQUESTER_NAME COLUMN DELETED--
CREATE SEQUENCE public.TBL_GRIDMEASURE_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE public.TBL_GRIDMEASURE_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE public.tbl_gridmeasure
(
  id integer NOT NULL DEFAULT nextval('tbl_gridmeasure_id_seq'::regclass),
  id_descriptive character varying(50),
  title character varying(256),
  affected_resource character varying(256),
  remark character varying(1024),
  fk_ref_gm_status integer,
  switching_object character varying(256),
  cost_center character varying(50),
  responsible_onsite_name character varying(256),
  responsible_onsite_department character varying(256),
  approval_by character varying(256),
  area_of_switching character varying(256),
  appointment_repetition character varying(100),
  appointment_startdate timestamp without time zone,
  appointment_numberof integer,
  planned_starttime_first_sequence timestamp without time zone,
  planned_starttime_first_singlemeasure timestamp without time zone,
  planned_endtime_last_singlemeasure timestamp without time zone,
  planned_endtime_gridmeasure timestamp without time zone,
  starttime_first_sequence timestamp without time zone,
  starttime_first_singlemeasure timestamp without time zone,
  endtime_last_singlemeasure timestamp without time zone,
  endtime_gridmeasure timestamp without time zone,
  time_of_reallocation character varying(100),
  description character varying(1024),
  fk_ref_branch integer,
  fk_ref_branch_level integer,
  create_user character varying(100) NOT NULL,
  create_user_department character varying(100),
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_user_department character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_gridmeasure_pkey PRIMARY KEY (id),
  CONSTRAINT fk_gridmeasure__gm_status FOREIGN KEY (fk_ref_gm_status)
      REFERENCES public.ref_gm_status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_gridmeasure__branch FOREIGN KEY (fk_ref_branch)
      REFERENCES public.ref_branch (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_gridmeasure__branch_level FOREIGN KEY (fk_ref_branch_level)
      REFERENCES public.ref_branch_level (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_gridmeasure
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_gridmeasure TO plgm_service;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_0', 'Kabel erneuern', 'Kabel', 'Kabel defekt', 0,
            'Kabel', 'K-155', 'hugo', 'Abteilung 1',
            'otto', 'Frankfurt', 'einmalig', '2018-06-14 15:15:00',
            7, '2018-06-09 15:00:00', '2018-06-10 16:30:00',
            '2018-06-11 14:00:00', '2018-06-17 20:15:00',
            '2018-06-14 15:15:00', '2018-06-15 15:45:00', '2018-06-16 13:00:00',
            '2018-06-18 16:00:00', 'in zwei Tagen', 'Das Kabel austauschen', 1,
            1, 'otto', 4, '2018-06-09 15:00:00',
            'otto', 3, '2018-06-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_1', 'Lampe erneuern', 'Lampe', 'Lampe defekt', 1,
            'Lampe', 'K-004', 'otto', 'Abteilung 3',
            'jasper', 'Mannheim', 'einmalig', '2018-06-16 15:15:00',
            3, '2018-06-11 15:00:00', '2018-06-12 16:30:00',
            '2018-06-13 14:00:00', '2018-06-19 20:15:00',
            '2018-06-16 15:15:00', '2018-06-17 15:45:00', '2018-06-18 13:00:00',
            '2018-06-20 16:00:00', 'in zwei Tagen', 'Die Lampe austauschen', 2,
            1, 'jasper', 3, '2018-06-11 15:00:00',
            'jasper', 4, '2018-06-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_2', 'Transformator erneuern', 'Transformator', 'Transformator defekt', 2,
            'Transformator', 'K-222', 'otto', 'Abteilung 4',
            'hugo', 'Murr', 'täglich', '2018-06-17 15:15:00',
            6, '2018-06-12 15:00:00', '2018-06-13 16:30:00',
            '2018-06-14 14:00:00', '2018-06-20 20:15:00',
            '2018-06-17 15:15:00', '2018-06-18 15:45:00', '2018-06-19 13:00:00',
            '2018-06-21 16:00:00', 'in 8 Tagen', 'Den Transformator austauschen', 4,
            1, 'hugo', 4, '2018-06-12 15:00:00',
            'otto', 4, '2018-06-17 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_3', 'Transformator ersetzen', 'Transformator', 'Transformator kaputt', 3,
            'Transformator', 'K-155', 'hugo', 'Abteilung 7',
            'otto', 'Stuttgart', 'einmalig', '2018-06-18 15:15:00',
            3, '2018-06-13 15:00:00', '2018-06-14 16:30:00',
            '2018-06-15 14:00:00', '2018-06-21 20:15:00',
            '2018-06-18 15:15:00', '2018-06-19 15:45:00', '2018-06-20 13:00:00',
            '2018-06-22 16:00:00', 'in 9 Tagen', 'Den Transformator austauschen', 3,
            1, 'otto', 4, '2018-06-13 15:00:00',
            'otto', 5, '2018-06-18 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_4', 'Gas Station', 'Station', 'Station defekt', 4,
            'Station', 'K-004', 'hugo', 'Abteilung 5',
            'otto', 'Ludwigsburg', 'wöchentlich', '2018-07-14 15:15:00',
            1, '2018-07-09 15:00:00', '2018-07-10 16:30:00',
            '2018-07-11 14:00:00', '2018-07-17 20:15:00',
            '2018-07-14 15:15:00', '2018-07-15 15:45:00', '2018-07-16 13:00:00',
            '2018-07-18 16:00:00', 'in zwei Wochen', 'Die Gas Station untersuchen', 2,
            1, 'otto', 5, '2018-07-09 15:00:00',
            'otto', 3, '2018-07-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_5', 'Stuhl mit drei Beinen', 'Stuhl', 'Stuhl kaputt', 5,
            'Stuhl', 'K-155', 'bruno', 'Abteilung 0',
            'otto', 'Hamburg', 'einmalig', '2018-06-12 15:15:00',
            3, '2018-06-07 15:00:00', '2018-06-08 16:30:00',
            '2018-06-09 14:00:00', '2018-06-15 20:15:00',
            '2018-06-12 15:15:00', '2018-06-13 15:45:00', '2018-06-14 13:00:00',
            '2018-06-16 16:00:00', 'in zwei Tagen', 'Den Stuhl austauschen', 4,
            1, 'claudio', 4, '2018-06-07 15:00:00',
            'claudio', 3, '2018-06-12 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_6', 'Kabel vergrößern', 'Kabel', 'Kabel zu kurz', 6,
            'Kabel', 'K-222', 'otto', 'Abteilung 76',
            'dagmar', 'Steinheim', 'einmalig', '2018-08-14 15:15:00',
            27, '2018-08-09 15:00:00', '2018-08-10 16:30:00',
            '2018-08-11 14:00:00', '2018-08-17 20:15:00',
            '2018-08-14 15:15:00', '2018-08-15 15:45:00', '2018-06-16 13:00:00',
            '2018-08-18 16:00:00', 'in zwei Wochen', 'Das Kabel vergrößern', 3,
            1, 'claudio', 3, '2018-08-09 15:00:00',
            'bruno', 5, '2018-08-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_7', 'Transformator austauschen', 'Transformator', 'Transformator 160 Grads', 7,
            'Transformator', 'K-155', 'hugo', 'Abteilung 1',
            'dagmar', 'Murr', 'täglich', '2018-06-14 15:15:00',
            6, '2018-06-09 15:00:00', '2018-06-10 16:30:00',
            '2018-06-11 14:00:00', '2018-06-17 20:15:00',
            '2018-06-14 15:15:00', '2018-06-15 15:45:00', '2018-06-16 13:00:00',
            '2018-06-18 16:00:00', 'in zwei Tagen', 'Den Transformator austauschen', 1,
            1, 'dagmar', 4, '2018-06-09 15:00:00',
            'dagmar', 3, '2018-06-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_8', 'Alles kaputt', 'Alles', 'Alles defekt', 8,
            'Alles', 'K-222', 'hugo', 'Abteilung 100',
            'otto', 'Berlin', 'einmalig', '2018-06-15 15:15:00',
            2, '2018-06-10 15:00:00', '2018-06-11 16:30:00',
            '2018-06-12 14:00:00', '2018-06-18 20:15:00',
            '2018-06-15 15:15:00', '2018-06-16 15:45:00', '2018-06-17 13:00:00',
            '2018-06-19 16:00:00', 'in zwei Jahren', 'Alles wegwerfen', 2,
            3, 'bruno', 5, '2018-06-10 15:00:00',
            'claudio', 5, '2018-06-15 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_9', 'Schalter erneuern', 'Schalter', 'Schalter defekt', 9,
            'Schalter', 'K-004', 'otto', 'Abteilung 15',
            'hugo', 'Kassel', 'einmalig', '2018-08-14 15:15:00',
            9, '2018-08-09 15:00:00', '2018-08-10 16:30:00',
            '2018-08-11 14:00:00', '2018-08-17 20:15:00',
            '2018-08-14 15:15:00', '2018-08-15 15:45:00', '2018-08-16 13:00:00',
            '2018-08-18 16:00:00', 'in 6 Tagen', 'Den Schalter austauschen', 1,
            1, 'dagmar', 3, '2018-08-09 15:00:00',
            'otto', 3, '2018-08-14 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_10', 'Rechner austauschen', 'Rechner', 'Rechner defekt', 10,
            'Rechner', 'K-155', 'bruno', 'Abteilung 14',
            'claudio', 'Murr', 'einmalig', '2018-06-12 15:15:00',
            3, '2018-06-07 15:00:00', '2018-06-08 16:30:00',
            '2018-06-09 14:00:00', '2018-06-15 20:15:00',
            '2018-06-12 15:15:00', '2018-06-13 15:45:00', '2018-06-14 13:00:00',
            '2018-06-16 16:00:00', 'in zwei Tagen', 'Den Rechner austauschen', 4,
            1, 'hugo', 5, '2018-06-07 15:00:00',
            'otto', 3, '2018-06-12 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_11', 'Maschine untersuchen', 'Maschine', 'Maschine defekt', 11,
            'Maschine', 'K-004', 'bruno', 'Abteilung 1',
            'otto', 'Ludwigsburg', 'einmalig', '2018-06-20 15:15:00',
            5, '2018-06-15 15:00:00', '2018-06-16 16:30:00',
            '2018-06-17 14:00:00', '2018-06-23 20:15:00',
            '2018-06-20 15:15:00', '2018-06-21 15:45:00', '2018-06-22 13:00:00',
            '2018-06-24 16:00:00', 'in 8 Tagen', 'Die Maschine austauschen', 1,
            1, 'bruno', 4, '2018-06-15 15:00:00',
            'bruno', 3, '2018-06-20 15:15:00');

INSERT INTO public.tbl_gridmeasure(
            id_descriptive, title, affected_resource, remark, fk_ref_gm_status,
            switching_object, cost_center, responsible_onsite_name, responsible_onsite_department,
            approval_by, area_of_switching, appointment_repetition, appointment_startdate,
            appointment_numberof, planned_starttime_first_sequence, planned_starttime_first_singlemeasure,
            planned_endtime_last_singlemeasure, planned_endtime_gridmeasure,
            starttime_first_sequence, starttime_first_singlemeasure, endtime_last_singlemeasure,
            endtime_gridmeasure, time_of_reallocation, description, fk_ref_branch,
            fk_ref_branch_level, create_user, create_user_department, create_date,
            mod_user, mod_user_department, mod_date)
    VALUES ('2018_12', 'Monitor kaputt', 'Monitor', 'Monitor defekt', 12,
            'Monitor', 'K-222', 'jasper', 'Abteilung 167',
            'hugo', 'Asperg', 'wöchentlich', '2018-10-20 15:15:00',
            5, '2018-10-15 15:00:00', '2018-10-16 16:30:00',
            '2018-10-17 14:00:00', '2018-10-23 20:15:00',
            '2018-10-20 15:15:00', '2018-10-21 15:45:00', '2018-10-22 13:00:00',
            '2018-10-24 16:00:00', 'in 8 Tagen', 'Den Monitor austauschen', 4,
            1, 'jasper', 4, '2018-10-15 15:00:00',
            'bruno', 3, '2018-10-20 15:15:00');




-- ---------------------------------------------
-- TABLE TBL_SINGLE_GRIDMEASURE
-- ---------------------------------------------

CREATE SEQUENCE public.tbl_single_gridmeasure_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.tbl_single_gridmeasure_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.tbl_single_gridmeasure
(
  id integer NOT NULL DEFAULT nextval('tbl_single_gridmeasure_id_seq'::regclass),
  sortorder integer NOT NULL,
  title character varying(256),
  switching_object character varying(256),
  cim_id character varying(256),
  cim_name character varying(256),
  cim_description character varying(1024),
  planned_starttime_singlemeasure timestamp without time zone,
  planned_endtime_singlemeasure timestamp without time zone,
  description character varying(1024),
  fk_tbl_gridmeasure integer NOT NULL,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_single_gridmeasure_pkey PRIMARY KEY (id),
  CONSTRAINT fk_single_gridmeasure__gridmeasure FOREIGN KEY (fk_tbl_gridmeasure)
      REFERENCES public.tbl_gridmeasure (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_single_gridmeasure
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_single_gridmeasure TO plgm_service;


INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Lampe ausschalten', 'Lampe', '2', 'cim', 'cim Beschreibung',
            '2018-06-10 16:30:00', '2018-06-11 14:00:00',
            'Beschreibung', 1, 'Otto', '01.07.2018', 'Otto',
            '01.07.2018');
INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (2, 'Kabel kürzen', 'Kabel', '2', 'cim', 'cim Beschreibung',
            '2018-06-10 16:30:00', '2018-06-11 14:00:00',
            'Beschreibung', 1, 'Otto', '01.07.2018', 'Otto',
            '01.07.2018');
INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (3, 'Transformator tauschen', 'Transformator', '2', 'cim', 'cim Beschreibung',
            '2018-06-10 16:30:00', '2018-06-11 14:00:00',
            'Beschreibung', 1, 'Otto', '01.07.2018', 'Otto',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Lampe erneuern', 'Lampe', '2', 'cim', 'cim Beschreibung',
            '2018-06-12 16:30:00', '2018-06-13 14:00:00',
            'Beschreibung', 2, 'Hugo', '01.07.2018', 'Hugo',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Transformator', 'Transformator', '2', 'cim', 'cim Beschreibung',
            '2018-06-13 16:30:00', '2018-06-14 14:00:00',
            'Beschreibung', 3, 'Hugo', '01.07.2018', 'Hugo',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Transformator', 'Transformator', '2', 'cim', 'cim Beschreibung',
            '2018-06-14 16:30:00', '2018-06-15 14:00:00',
            'Beschreibung', 4, 'Hugo', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Gas', 'Gas', '2', 'cim', 'cim Beschreibung',
            '2018-07-10 16:30:00', '2018-07-11 14:00:00',
            'Beschreibung', 5, 'Jasper', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (2, 'Gas 2', 'Gas', '2', 'cim', 'cim Beschreibung',
            '2018-07-10 16:30:00', '2018-07-11 14:00:00',
            'Beschreibung', 5, 'Jasper', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Stuhl', 'Stuhl', '2', 'cim', 'cim Beschreibung',
            '2018-06-08 16:30:00', '2018-06-09 14:00:00',
            'Beschreibung', 6, 'Otto', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Kabel', 'Kabel', '2', 'cim', 'cim Beschreibung',
            '2018-08-10 16:30:00', '2018-08-11 14:00:00',
            'Beschreibung', 7, 'Otto', '01.07.2018', 'Hugo',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Transformator', 'Transformator', '2', 'cim', 'cim Beschreibung',
            '2018-06-10 16:30:00', '2018-06-11 14:00:00',
            'Beschreibung', 8, 'Hugo', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (2, 'Transformator 2', 'Transformator', '2', 'cim', 'cim Beschreibung',
            '2018-06-10 16:30:00', '2018-06-11 14:00:00',
            'Beschreibung', 8, 'Hugo', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Alles', 'Alles', '2', 'cim', 'cim Beschreibung',
            '2018-06-11 16:30:00', '2018-06-12 14:00:00',
            'Beschreibung', 9, 'Otto', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Schalter', 'Schalter', '2', 'cim', 'cim Beschreibung',
            '2018-08-10 16:30:00', '2018-08-11 14:00:00',
            'Beschreibung', 10, 'Otto', '01.07.2018', 'Hugo',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (2, 'Schalter 2', 'Schalter', '2', 'cim', 'cim Beschreibung',
            '2018-08-10 16:30:00', '2018-08-11 14:00:00',
            'Beschreibung', 10, 'Otto', '01.07.2018', 'Hugo',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Rechner', 'Rechner', '2', 'cim', 'cim Beschreibung',
            '2018-06-08 16:30:00', '2018-06-09 14:00:00',
            'Beschreibung', 11, 'Jasper', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Maschine', 'Maschine', '2', 'cim', 'cim Beschreibung',
            '2018-06-16 16:30:00', '2018-06-17 14:00:00',
            'Beschreibung', 12, 'Hugo', '01.07.2018', 'Jasper',
            '01.07.2018');

INSERT INTO public.tbl_single_gridmeasure(
            sortorder, title, switching_object, cim_id, cim_name, cim_description,
            planned_starttime_singlemeasure, planned_endtime_singlemeasure,
            description, fk_tbl_gridmeasure, create_user, create_date, mod_user,
            mod_date)
    VALUES (1, 'Monitor', 'Monitor', '2', 'cim', 'cim Beschreibung',
            '2018-10-16 16:30:00', '2018-10-17 14:00:00',
            'Beschreibung', 13, 'Hugo', '01.07.2018', 'Otto',
            '01.07.2018');



DROP TABLE IF EXISTS public.REF_VERSION;

CREATE TABLE public.REF_VERSION
(
  id integer NOT NULL,
  version character varying(100) NOT NULL,
  CONSTRAINT ref_version_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.REF_VERSION
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE public.REF_VERSION TO PLGM_SERVICE;

INSERT INTO REF_VERSION VALUES (1, '0.0.1_PG');

-- ---------------------------------------------
-- TABLE TBL_DOCUMENTS
-- ---------------------------------------------

CREATE SEQUENCE public.tbl_documents_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.tbl_documents_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.tbl_documents
(
  id integer NOT NULL DEFAULT nextval('tbl_documents_id_seq'::regclass),
  document_name character varying(260),
  document bytea,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_documents_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_documents
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_documents TO plgm_service;


-- ---------------------------------------------
-- TABLE TBL_MEASURE_DOCUMENTS
-- ---------------------------------------------

CREATE SEQUENCE public.tbl_measure_documents_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.tbl_measure_documents_id_seq
  OWNER TO plgm_service;

CREATE TABLE public.tbl_measure_documents
(
  id integer NOT NULL DEFAULT nextval('tbl_measure_documents_id_seq'::regclass),
  fk_tbl_measure integer,
  fk_tbl_documents integer,
  create_user character varying(100) NOT NULL,
  create_date timestamp without time zone NOT NULL,
  mod_user character varying(100),
  mod_date timestamp without time zone,
  CONSTRAINT tbl_measure_documents_pkey PRIMARY KEY (id),
  CONSTRAINT fk_tbl_measure_documents_measure FOREIGN KEY (fk_tbl_measure)
      REFERENCES public.tbl_gridmeasure (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fk_tbl_measure_documents_documents FOREIGN KEY (fk_tbl_documents)
      REFERENCES public.tbl_documents (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tbl_measure_documents
  OWNER TO plgm_service;
GRANT ALL ON TABLE public.tbl_measure_documents TO plgm_service;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- HISTORY-TABLES
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- PUBLIC.HTBL_LOCK Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_LOCK;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_LOCK_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_LOCK_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_LOCK_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_LOCK
(
  HID integer NOT NULL DEFAULT nextval('HTBL_LOCK_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  KEY integer,
  USERNAME character varying (50),
  INFO character varying (256),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_LOCK_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_LOCK
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_LOCK TO PLGM_SERVICE;



-- PUBLIC.HTBL_USER_SETTINGS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_USER_SETTINGS;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_USER_SETTINGS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_USER_SETTINGS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_USER_SETTINGS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_USER_SETTINGS
(
  HID integer NOT NULL DEFAULT nextval('HTBL_USER_SETTINGS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  USERNAME character varying (50),
  SETTING_TYPE character varying (50),
  VALUE character varying(4096),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_USER_SETTINGS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_USER_SETTINGS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_USER_SETTINGS TO PLGM_SERVICE;


-- PUBLIC.HREF_USER_DEPARTMENT Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_USER_DEPARTMENT;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_USER_DEPARTMENT_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_USER_DEPARTMENT_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_USER_DEPARTMENT_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_USER_DEPARTMENT
(
  HID integer NOT NULL DEFAULT nextval('HREF_USER_DEPARTMENT_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_USER_DEPARTMENT_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_USER_DEPARTMENT
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_USER_DEPARTMENT TO PLGM_SERVICE;


-- PUBLIC.HREF_COST_CENTER Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_COST_CENTER;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_COST_CENTER_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_COST_CENTER_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_COST_CENTER_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_COST_CENTER
(
  HID integer NOT NULL DEFAULT nextval('HREF_COST_CENTER_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),
  DESCRIPTION character varying (255),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_COST_CENTER_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_COST_CENTER
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_COST_CENTER TO PLGM_SERVICE;

-- PUBLIC.HREF_BRANCH Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_BRANCH;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_BRANCH_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_BRANCH_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_BRANCH_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_BRANCH
(
  HID integer NOT NULL DEFAULT nextval('HREF_BRANCH_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),
  DESCRIPTION character varying (255),
  COLOR_CODE character varying (20),
  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_BRANCH_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_BRANCH
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_BRANCH TO PLGM_SERVICE;

-- PUBLIC.HREF_BRANCH_LEVEL Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_BRANCH_LEVEL;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_BRANCH_LEVEL_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_BRANCH_LEVEL_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_BRANCH_LEVEL_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_BRANCH_LEVEL
(
  HID integer NOT NULL DEFAULT nextval('HREF_BRANCH_LEVEL_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying (50),
  DESCRIPTION character varying (255),
  FK_REF_BRANCH integer,

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_BRANCH_LEVEL_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_BRANCH_LEVEL
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_BRANCH_LEVEL TO PLGM_SERVICE;


-- PUBLIC.HREF_GM_STATUS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HREF_GM_STATUS;
DROP SEQUENCE IF EXISTS PUBLIC.HREF_GM_STATUS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HREF_GM_STATUS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HREF_GM_STATUS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HREF_GM_STATUS
(
  HID integer NOT NULL DEFAULT nextval('HREF_GM_STATUS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  NAME character varying(50),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HREF_GM_STATUS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HREF_GM_STATUS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HREF_GM_STATUS TO PLGM_SERVICE;


-- PUBLIC.HTBL_GRIDMEASURE Automatic generanted History Table DDL --
-- <GENERATED CODE!>

-- REQUESTER_NAME COLUMN DELETED--

DROP TABLE IF EXISTS PUBLIC.HTBL_GRIDMEASURE;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_GRIDMEASURE_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_GRIDMEASURE_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_GRIDMEASURE_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_GRIDMEASURE
(
  HID integer NOT NULL DEFAULT nextval('HTBL_GRIDMEASURE_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  ID_DESCRIPTIVE character varying (50),
  TITLE character varying (256),
  AFFECTED_RESOURCE character varying (256),
  REMARK character varying (1024),
  FK_REF_GM_STATUS integer,
  SWITCHING_OBJECT character varying (256),
  COST_CENTER character varying (50),
  RESPONSIBLE_ONSITE_NAME character varying (256),
  RESPONSIBLE_ONSITE_DEPARTMENT character varying (256),
  APPROVAL_BY character varying (256),
  AREA_OF_SWITCHING character varying (256),
  APPOINTMENT_REPETITION character varying (100),
  APPOINTMENT_STARTDATE timestamp without time zone,
  APPOINTMENT_NUMBEROF integer,
  PLANNED_STARTTIME_FIRST_SEQUENCE timestamp without time zone,
  PLANNED_STARTTIME_FIRST_SINGLEMEASURE timestamp without time zone,
  PLANNED_ENDTIME_LAST_SINGLEMEASURE timestamp without time zone,
  PLANNED_ENDTIME_GRIDMEASURE timestamp without time zone,
  STARTTIME_FIRST_SEQUENCE timestamp without time zone,
  STARTTIME_FIRST_SINGLEMEASURE timestamp without time zone,
  ENDTIME_LAST_SINGLEMEASURE timestamp without time zone,
  ENDTIME_GRIDMEASURE timestamp without time zone,
  TIME_OF_REALLOCATION character varying (100),
  DESCRIPTION character varying (1024),
  FK_REF_BRANCH integer,
  FK_REF_BRANCH_LEVEL integer,
  CREATE_USER_DEPARTMENT character varying (100),
  MOD_USER_DEPARTMENT character varying (100),

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_GRIDMEASURE_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_GRIDMEASURE
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_GRIDMEASURE TO PLGM_SERVICE;


-- PUBLIC.HTBL_SINGLE_GRIDMEASURE Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_SINGLE_GRIDMEASURE;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_SINGLE_GRIDMEASURE_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_SINGLE_GRIDMEASURE_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_SINGLE_GRIDMEASURE_ID_SEQ
  OWNER TO PLGM_SERVICE;



CREATE TABLE PUBLIC.HTBL_SINGLE_GRIDMEASURE
(
  HID integer NOT NULL DEFAULT nextval('HTBL_SINGLE_GRIDMEASURE_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  SORTORDER integer,
  TITLE character varying (256),
  SWITCHING_OBJECT character varying (256),
  CIM_ID character varying (256),
  CIM_NAME character varying (256),
  CIM_DESCRIPTION character varying (1024),
  PLANNED_STARTTIME_SINGLEMEASURE timestamp without time zone,
  PLANNED_ENDTIME_SINGLEMEASURE timestamp without time zone,
  DESCRIPTION character varying (1024),
  FK_TBL_GRIDMEASURE integer,

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_SINGLE_GRIDMEASURE_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_SINGLE_GRIDMEASURE
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_SINGLE_GRIDMEASURE TO PLGM_SERVICE;



-- PUBLIC.HTBL_DOCUMENTS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_DOCUMENTS;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_DOCUMENTS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_DOCUMENTS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_DOCUMENTS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_DOCUMENTS
(
  HID integer NOT NULL DEFAULT nextval('HTBL_DOCUMENTS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  DOCUMENT_NAME character varying (260),
  DOCUMENT bytea,

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_DOCUMENTS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_DOCUMENTS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_DOCUMENTS TO PLGM_SERVICE;

-- PUBLIC.HTBL_MEASURE_DOCUMENTS Automatic generanted History Table DDL --
-- <GENERATED CODE!>

DROP TABLE IF EXISTS PUBLIC.HTBL_MEASURE_DOCUMENTS;
DROP SEQUENCE IF EXISTS PUBLIC.HTBL_MEASURE_DOCUMENTS_ID_SEQ;

CREATE SEQUENCE PUBLIC.HTBL_MEASURE_DOCUMENTS_ID_SEQ
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE PUBLIC.HTBL_MEASURE_DOCUMENTS_ID_SEQ
  OWNER TO PLGM_SERVICE;

CREATE TABLE PUBLIC.HTBL_MEASURE_DOCUMENTS
(
  HID integer NOT NULL DEFAULT nextval('HTBL_MEASURE_DOCUMENTS_ID_SEQ'::regclass),
  HACTION integer NOT NULL,
  HDATE timestamp without time zone NOT NULL,
  HUSER character varying(100),

  ID integer,
  FK_TBL_MEASURE integer,
  FK_TBL_DOCUMENTS integer,

  CREATE_USER character varying(100),
  CREATE_DATE timestamp without time zone,
  MOD_USER character varying(100),
  MOD_DATE timestamp without time zone,
  CONSTRAINT HTBL_MEASURE_DOCUMENTS_PKEY PRIMARY KEY (HID)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE PUBLIC.HTBL_MEASURE_DOCUMENTS
  OWNER TO PLGM_SERVICE;
GRANT ALL ON TABLE PUBLIC.HTBL_MEASURE_DOCUMENTS TO PLGM_SERVICE;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- TRIGGER
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- PUBLIC.TBL_SINGLE_GRIDMEASURE INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_SINGLE_GRIDMEASURE (
						  ID,SORTORDER,TITLE,SWITCHING_OBJECT,CIM_ID,CIM_NAME,CIM_DESCRIPTION,PLANNED_STARTTIME_SINGLEMEASURE,PLANNED_ENDTIME_SINGLEMEASURE,DESCRIPTION,FK_TBL_GRIDMEASURE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.SORTORDER,NEW.TITLE,NEW.SWITCHING_OBJECT,NEW.CIM_ID,NEW.CIM_NAME,NEW.CIM_DESCRIPTION,NEW.PLANNED_STARTTIME_SINGLEMEASURE,NEW.PLANNED_ENDTIME_SINGLEMEASURE,NEW.DESCRIPTION,NEW.FK_TBL_GRIDMEASURE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_SINGLE_GRIDMEASURE_INSERT_TRG ON PUBLIC.TBL_SINGLE_GRIDMEASURE;

CREATE TRIGGER TBL_SINGLE_GRIDMEASURE_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_SINGLE_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_SINGLE_GRIDMEASURE_INSERT_TRG();



-- PUBLIC.TBL_SINGLE_GRIDMEASURE UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_SINGLE_GRIDMEASURE (
						  ID,SORTORDER,TITLE,SWITCHING_OBJECT,CIM_ID,CIM_NAME,CIM_DESCRIPTION,PLANNED_STARTTIME_SINGLEMEASURE,PLANNED_ENDTIME_SINGLEMEASURE,DESCRIPTION,FK_TBL_GRIDMEASURE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.SORTORDER,NEW.TITLE,NEW.SWITCHING_OBJECT,NEW.CIM_ID,NEW.CIM_NAME,NEW.CIM_DESCRIPTION,NEW.PLANNED_STARTTIME_SINGLEMEASURE,NEW.PLANNED_ENDTIME_SINGLEMEASURE,NEW.DESCRIPTION,NEW.FK_TBL_GRIDMEASURE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_SINGLE_GRIDMEASURE_UPDATE_TRG ON PUBLIC.TBL_SINGLE_GRIDMEASURE;

CREATE TRIGGER TBL_SINGLE_GRIDMEASURE_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_SINGLE_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_SINGLE_GRIDMEASURE_UPDATE_TRG();



-- PUBLIC.TBL_SINGLE_GRIDMEASURE DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_SINGLE_GRIDMEASURE (

						  ID,SORTORDER,TITLE,SWITCHING_OBJECT,CIM_ID,CIM_NAME,CIM_DESCRIPTION,PLANNED_STARTTIME_SINGLEMEASURE,PLANNED_ENDTIME_SINGLEMEASURE,DESCRIPTION,FK_TBL_GRIDMEASURE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.SORTORDER,OLD.TITLE,OLD.SWITCHING_OBJECT,OLD.CIM_ID,OLD.CIM_NAME,OLD.CIM_DESCRIPTION,OLD.PLANNED_STARTTIME_SINGLEMEASURE,OLD.PLANNED_ENDTIME_SINGLEMEASURE,OLD.DESCRIPTION,OLD.FK_TBL_GRIDMEASURE,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_SINGLE_GRIDMEASURE_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_SINGLE_GRIDMEASURE_DELETE_TRG ON PUBLIC.TBL_SINGLE_GRIDMEASURE;

CREATE TRIGGER TBL_SINGLE_GRIDMEASURE_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_SINGLE_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_SINGLE_GRIDMEASURE_DELETE_TRG();





-- PUBLIC.TBL_LOCK INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_LOCK_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_LOCK (
						  ID,KEY,USERNAME,INFO,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.KEY,NEW.USERNAME,NEW.INFO,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_LOCK_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_LOCK_INSERT_TRG ON PUBLIC.TBL_LOCK;

CREATE TRIGGER TBL_LOCK_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_LOCK
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_LOCK_INSERT_TRG();



-- PUBLIC.TBL_LOCK UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_LOCK_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_LOCK (
						  ID,KEY,USERNAME,INFO,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.KEY,NEW.USERNAME,NEW.INFO,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_LOCK_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_LOCK_UPDATE_TRG ON PUBLIC.TBL_LOCK;

CREATE TRIGGER TBL_LOCK_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_LOCK
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_LOCK_UPDATE_TRG();



-- PUBLIC.TBL_LOCK DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_LOCK_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_LOCK (

						  ID,KEY,USERNAME,INFO,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.KEY,OLD.USERNAME,OLD.INFO,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_LOCK_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_LOCK_DELETE_TRG ON PUBLIC.TBL_LOCK;

CREATE TRIGGER TBL_LOCK_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_LOCK
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_LOCK_DELETE_TRG();




-- PUBLIC.TBL_USER_SETTINGS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_USER_SETTINGS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_USER_SETTINGS (
						  ID,USERNAME,SETTING_TYPE,VALUE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.USERNAME,NEW.SETTING_TYPE,NEW.VALUE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_USER_SETTINGS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_USER_SETTINGS_INSERT_TRG ON PUBLIC.TBL_USER_SETTINGS;

CREATE TRIGGER TBL_USER_SETTINGS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_USER_SETTINGS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_USER_SETTINGS_INSERT_TRG();



-- PUBLIC.TBL_USER_SETTINGS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_USER_SETTINGS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_USER_SETTINGS (
						  ID,USERNAME,SETTING_TYPE,VALUE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.USERNAME,NEW.SETTING_TYPE,NEW.VALUE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_USER_SETTINGS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_USER_SETTINGS_UPDATE_TRG ON PUBLIC.TBL_USER_SETTINGS;

CREATE TRIGGER TBL_USER_SETTINGS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_USER_SETTINGS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_USER_SETTINGS_UPDATE_TRG();



-- PUBLIC.TBL_USER_SETTINGS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_USER_SETTINGS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_USER_SETTINGS (

						  ID,USERNAME,SETTING_TYPE,VALUE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.USERNAME,OLD.SETTING_TYPE,OLD.VALUE,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_USER_SETTINGS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_USER_SETTINGS_DELETE_TRG ON PUBLIC.TBL_USER_SETTINGS;

CREATE TRIGGER TBL_USER_SETTINGS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_USER_SETTINGS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_USER_SETTINGS_DELETE_TRG();



-- PUBLIC.REF_USER_DEPARTMENT INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_USER_DEPARTMENT_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_USER_DEPARTMENT (
						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_USER_DEPARTMENT_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_USER_DEPARTMENT_INSERT_TRG ON PUBLIC.REF_USER_DEPARTMENT;

CREATE TRIGGER REF_USER_DEPARTMENT_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_USER_DEPARTMENT
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_USER_DEPARTMENT_INSERT_TRG();



-- PUBLIC.REF_USER_DEPARTMENT UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_USER_DEPARTMENT_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_USER_DEPARTMENT (
						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_USER_DEPARTMENT_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_USER_DEPARTMENT_UPDATE_TRG ON PUBLIC.REF_USER_DEPARTMENT;

CREATE TRIGGER REF_USER_DEPARTMENT_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_USER_DEPARTMENT
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_USER_DEPARTMENT_UPDATE_TRG();



-- PUBLIC.REF_USER_DEPARTMENT DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_USER_DEPARTMENT_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_USER_DEPARTMENT (

						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_USER_DEPARTMENT_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_USER_DEPARTMENT_DELETE_TRG ON PUBLIC.REF_USER_DEPARTMENT;

CREATE TRIGGER REF_USER_DEPARTMENT_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_USER_DEPARTMENT
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_USER_DEPARTMENT_DELETE_TRG();


-- PUBLIC.HREF_COST_CENTER INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.HREF_COST_CENTER_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HHREF_COST_CENTER (
						  HID,HACTION,HDATE,HUSER,ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.HID,NEW.HACTION,NEW.HDATE,NEW.HUSER,NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.HREF_COST_CENTER_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS HREF_COST_CENTER_INSERT_TRG ON PUBLIC.HREF_COST_CENTER;

CREATE TRIGGER HREF_COST_CENTER_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.HREF_COST_CENTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.HREF_COST_CENTER_INSERT_TRG();



-- PUBLIC.HREF_COST_CENTER UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.HREF_COST_CENTER_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HHREF_COST_CENTER (
						  HID,HACTION,HDATE,HUSER,ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.HID,NEW.HACTION,NEW.HDATE,NEW.HUSER,NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.HREF_COST_CENTER_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS HREF_COST_CENTER_UPDATE_TRG ON PUBLIC.HREF_COST_CENTER;

CREATE TRIGGER HREF_COST_CENTER_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.HREF_COST_CENTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.HREF_COST_CENTER_UPDATE_TRG();



-- PUBLIC.HREF_COST_CENTER DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.HREF_COST_CENTER_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HHREF_COST_CENTER (

						  HID,HACTION,HDATE,HUSER,ID,NAME,DESCRIPTION,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.HID,OLD.HACTION,OLD.HDATE,OLD.HUSER,OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.HREF_COST_CENTER_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS HREF_COST_CENTER_DELETE_TRG ON PUBLIC.HREF_COST_CENTER;

CREATE TRIGGER HREF_COST_CENTER_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.HREF_COST_CENTER
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.HREF_COST_CENTER_DELETE_TRG();



-- PUBLIC.REF_BRANCH INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH (

						  ID,NAME,DESCRIPTION,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.COLOR_CODE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_BRANCH_INSERT_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_INSERT_TRG();



-- PUBLIC.REF_BRANCH UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH (
						  ID,NAME,DESCRIPTION,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.COLOR_CODE,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_UPDATE_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_UPDATE_TRG();



-- PUBLIC.REF_BRANCH DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_BRANCH (
						  ID,NAME,DESCRIPTION,COLOR_CODE,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,
						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.COLOR_CODE,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,
                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_DELETE_TRG ON PUBLIC.REF_BRANCH;

CREATE TRIGGER REF_BRANCH_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_BRANCH
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_DELETE_TRG();



-- PUBLIC.REF_BRANCH_LEVEL INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_LEVEL_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH_LEVEL (
						  ID,NAME,DESCRIPTION,FK_REF_BRANCH,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_LEVEL_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_BRANCH_LEVEL_INSERT_TRG ON PUBLIC.REF_BRANCH_LEVEL;

CREATE TRIGGER REF_BRANCH_LEVEL_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_BRANCH_LEVEL
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_LEVEL_INSERT_TRG();



-- PUBLIC.REF_BRANCH_LEVEL UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_LEVEL_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_BRANCH_LEVEL (
						  ID,NAME,DESCRIPTION,FK_REF_BRANCH,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_LEVEL_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_LEVEL_UPDATE_TRG ON PUBLIC.REF_BRANCH_LEVEL;

CREATE TRIGGER REF_BRANCH_LEVEL_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_BRANCH_LEVEL
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_LEVEL_UPDATE_TRG();



-- PUBLIC.REF_BRANCH_LEVEL DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_BRANCH_LEVEL_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_BRANCH_LEVEL (

						  ID,NAME,DESCRIPTION,FK_REF_BRANCH,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.DESCRIPTION,OLD.FK_REF_BRANCH,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_BRANCH_LEVEL_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_BRANCH_LEVEL_DELETE_TRG ON PUBLIC.REF_BRANCH_LEVEL;

CREATE TRIGGER REF_BRANCH_LEVEL_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_BRANCH_LEVEL
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_BRANCH_LEVEL_DELETE_TRG();





-- PUBLIC.REF_GM_STATUS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_GM_STATUS (
						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS REF_GM_STATUS_INSERT_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_INSERT_TRG();



-- PUBLIC.REF_GM_STATUS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HREF_GM_STATUS (
						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_GM_STATUS_UPDATE_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_UPDATE_TRG();



-- PUBLIC.REF_GM_STATUS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.REF_GM_STATUS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HREF_GM_STATUS (

						  ID,NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.NAME,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.REF_GM_STATUS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS REF_GM_STATUS_DELETE_TRG ON PUBLIC.REF_GM_STATUS;

CREATE TRIGGER REF_GM_STATUS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.REF_GM_STATUS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.REF_GM_STATUS_DELETE_TRG();





-- PUBLIC.TBL_GRIDMEASURE INSERT TRIGGER --
-- <GENERATED CODE!>
-- REQUESTER_NAME COLUMN DELETED--
CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_GRIDMEASURE (
						  ID,ID_DESCRIPTIVE,TITLE,AFFECTED_RESOURCE,REMARK,FK_REF_GM_STATUS,SWITCHING_OBJECT, COST_CENTER,RESPONSIBLE_ONSITE_NAME,RESPONSIBLE_ONSITE_DEPARTMENT,APPROVAL_BY,AREA_OF_SWITCHING,APPOINTMENT_REPETITION,APPOINTMENT_STARTDATE,APPOINTMENT_NUMBEROF,PLANNED_STARTTIME_FIRST_SEQUENCE,PLANNED_STARTTIME_FIRST_SINGLEMEASURE,PLANNED_ENDTIME_LAST_SINGLEMEASURE,PLANNED_ENDTIME_GRIDMEASURE,STARTTIME_FIRST_SEQUENCE,STARTTIME_FIRST_SINGLEMEASURE,ENDTIME_LAST_SINGLEMEASURE,ENDTIME_GRIDMEASURE,TIME_OF_REALLOCATION,DESCRIPTION,FK_REF_BRANCH,FK_REF_BRANCH_LEVEL,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.ID_DESCRIPTIVE,NEW.TITLE,NEW.AFFECTED_RESOURCE,NEW.REMARK,NEW.FK_REF_GM_STATUS,NEW.SWITCHING_OBJECT,NEW.COST_CENTER,NEW.RESPONSIBLE_ONSITE_NAME,NEW.RESPONSIBLE_ONSITE_DEPARTMENT,NEW.APPROVAL_BY,NEW.AREA_OF_SWITCHING,NEW.APPOINTMENT_REPETITION,NEW.APPOINTMENT_STARTDATE,NEW.APPOINTMENT_NUMBEROF,NEW.PLANNED_STARTTIME_FIRST_SEQUENCE,NEW.PLANNED_STARTTIME_FIRST_SINGLEMEASURE,NEW.PLANNED_ENDTIME_LAST_SINGLEMEASURE,NEW.PLANNED_ENDTIME_GRIDMEASURE,NEW.STARTTIME_FIRST_SEQUENCE,NEW.STARTTIME_FIRST_SINGLEMEASURE,NEW.ENDTIME_LAST_SINGLEMEASURE,NEW.ENDTIME_GRIDMEASURE,NEW.TIME_OF_REALLOCATION,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.FK_REF_BRANCH_LEVEL,NEW.CREATE_USER,NEW.CREATE_USER_DEPARTMENT,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_USER_DEPARTMENT,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_INSERT_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_INSERT_TRG();



-- PUBLIC.TBL_GRIDMEASURE UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_GRIDMEASURE (
						  ID,ID_DESCRIPTIVE,TITLE,AFFECTED_RESOURCE,REMARK,FK_REF_GM_STATUS,SWITCHING_OBJECT,COST_CENTER,RESPONSIBLE_ONSITE_NAME,RESPONSIBLE_ONSITE_DEPARTMENT,APPROVAL_BY,AREA_OF_SWITCHING,APPOINTMENT_REPETITION,APPOINTMENT_STARTDATE,APPOINTMENT_NUMBEROF,PLANNED_STARTTIME_FIRST_SEQUENCE,PLANNED_STARTTIME_FIRST_SINGLEMEASURE,PLANNED_ENDTIME_LAST_SINGLEMEASURE,PLANNED_ENDTIME_GRIDMEASURE,STARTTIME_FIRST_SEQUENCE,STARTTIME_FIRST_SINGLEMEASURE,ENDTIME_LAST_SINGLEMEASURE,ENDTIME_GRIDMEASURE,TIME_OF_REALLOCATION,DESCRIPTION,FK_REF_BRANCH,FK_REF_BRANCH_LEVEL,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.ID_DESCRIPTIVE,NEW.TITLE,NEW.AFFECTED_RESOURCE,NEW.REMARK,NEW.FK_REF_GM_STATUS,NEW.SWITCHING_OBJECT,NEW.COST_CENTER,NEW.RESPONSIBLE_ONSITE_NAME,NEW.RESPONSIBLE_ONSITE_DEPARTMENT,NEW.APPROVAL_BY,NEW.AREA_OF_SWITCHING,NEW.APPOINTMENT_REPETITION,NEW.APPOINTMENT_STARTDATE,NEW.APPOINTMENT_NUMBEROF,NEW.PLANNED_STARTTIME_FIRST_SEQUENCE,NEW.PLANNED_STARTTIME_FIRST_SINGLEMEASURE,NEW.PLANNED_ENDTIME_LAST_SINGLEMEASURE,NEW.PLANNED_ENDTIME_GRIDMEASURE,NEW.STARTTIME_FIRST_SEQUENCE,NEW.STARTTIME_FIRST_SINGLEMEASURE,NEW.ENDTIME_LAST_SINGLEMEASURE,NEW.ENDTIME_GRIDMEASURE,NEW.TIME_OF_REALLOCATION,NEW.DESCRIPTION,NEW.FK_REF_BRANCH,NEW.FK_REF_BRANCH_LEVEL,NEW.CREATE_USER,NEW.CREATE_USER_DEPARTMENT,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_USER_DEPARTMENT,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_UPDATE_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_UPDATE_TRG();



-- PUBLIC.TBL_GRIDMEASURE DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_GRIDMEASURE_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_GRIDMEASURE (

						  ID,ID_DESCRIPTIVE,TITLE,AFFECTED_RESOURCE,REMARK,FK_REF_GM_STATUS,SWITCHING_OBJECT,COST_CENTER,RESPONSIBLE_ONSITE_NAME,RESPONSIBLE_ONSITE_DEPARTMENT,APPROVAL_BY,AREA_OF_SWITCHING,APPOINTMENT_REPETITION,APPOINTMENT_STARTDATE,APPOINTMENT_NUMBEROF,PLANNED_STARTTIME_FIRST_SEQUENCE,PLANNED_STARTTIME_FIRST_SINGLEMEASURE,PLANNED_ENDTIME_LAST_SINGLEMEASURE,PLANNED_ENDTIME_GRIDMEASURE,STARTTIME_FIRST_SEQUENCE,STARTTIME_FIRST_SINGLEMEASURE,ENDTIME_LAST_SINGLEMEASURE,ENDTIME_GRIDMEASURE,TIME_OF_REALLOCATION,DESCRIPTION,FK_REF_BRANCH,FK_REF_BRANCH_LEVEL,CREATE_USER,CREATE_USER_DEPARTMENT,CREATE_DATE,MOD_USER,MOD_USER_DEPARTMENT,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.ID_DESCRIPTIVE,OLD.TITLE,OLD.AFFECTED_RESOURCE,OLD.REMARK,OLD.FK_REF_GM_STATUS,OLD.SWITCHING_OBJECT,OLD.COST_CENTER,OLD.RESPONSIBLE_ONSITE_NAME,OLD.RESPONSIBLE_ONSITE_DEPARTMENT,OLD.APPROVAL_BY,OLD.AREA_OF_SWITCHING,OLD.APPOINTMENT_REPETITION,OLD.APPOINTMENT_STARTDATE,OLD.APPOINTMENT_NUMBEROF,OLD.PLANNED_STARTTIME_FIRST_SEQUENCE,OLD.PLANNED_STARTTIME_FIRST_SINGLEMEASURE,OLD.PLANNED_ENDTIME_LAST_SINGLEMEASURE,OLD.PLANNED_ENDTIME_GRIDMEASURE,OLD.STARTTIME_FIRST_SEQUENCE,OLD.STARTTIME_FIRST_SINGLEMEASURE,OLD.ENDTIME_LAST_SINGLEMEASURE,OLD.ENDTIME_GRIDMEASURE,OLD.TIME_OF_REALLOCATION,OLD.DESCRIPTION,OLD.FK_REF_BRANCH,OLD.FK_REF_BRANCH_LEVEL,OLD.CREATE_USER,OLD.CREATE_USER_DEPARTMENT,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_USER_DEPARTMENT,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_GRIDMEASURE_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_GRIDMEASURE_DELETE_TRG ON PUBLIC.TBL_GRIDMEASURE;

CREATE TRIGGER TBL_GRIDMEASURE_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_GRIDMEASURE
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_GRIDMEASURE_DELETE_TRG();



-- PUBLIC.TBL_DOCUMENTS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_DOCUMENTS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_DOCUMENTS (
						  ID,DOCUMENT_NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.DOCUMENT_NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_DOCUMENTS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_DOCUMENTS_INSERT_TRG ON PUBLIC.TBL_DOCUMENTS;

CREATE TRIGGER TBL_DOCUMENTS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_DOCUMENTS_INSERT_TRG();



-- PUBLIC.TBL_DOCUMENTS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_DOCUMENTS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_DOCUMENTS (
						  ID,DOCUMENT_NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.DOCUMENT_NAME,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_DOCUMENTS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_DOCUMENTS_UPDATE_TRG ON PUBLIC.TBL_DOCUMENTS;

CREATE TRIGGER TBL_DOCUMENTS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_DOCUMENTS_UPDATE_TRG();



-- PUBLIC.TBL_DOCUMENTS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_DOCUMENTS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_DOCUMENTS (

						  ID,DOCUMENT_NAME,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.DOCUMENT_NAME,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_DOCUMENTS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_DOCUMENTS_DELETE_TRG ON PUBLIC.TBL_DOCUMENTS;

CREATE TRIGGER TBL_DOCUMENTS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_DOCUMENTS_DELETE_TRG();






-- PUBLIC.TBL_MEASURE_DOCUMENTS INSERT TRIGGER --
-- <GENERATED CODE!>
CREATE OR REPLACE FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_INSERT_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_MEASURE_DOCUMENTS (
						  ID,FK_TBL_MEASURE,FK_TBL_DOCUMENTS,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				 (

						  NEW.ID,NEW.FK_TBL_MEASURE,NEW.FK_TBL_DOCUMENTS,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,

                          1,
						  current_timestamp,
					      NEW.CREATE_USER );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_INSERT_TRG()
  OWNER TO PLGM_SERVICE;


DROP TRIGGER IF EXISTS TBL_MEASURE_DOCUMENTS_INSERT_TRG ON PUBLIC.TBL_MEASURE_DOCUMENTS;

CREATE TRIGGER TBL_MEASURE_DOCUMENTS_INSERT_TRG
  BEFORE INSERT
  ON PUBLIC.TBL_MEASURE_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_MEASURE_DOCUMENTS_INSERT_TRG();



-- PUBLIC.TBL_MEASURE_DOCUMENTS UPDATE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_UPDATE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
	INSERT INTO HTBL_MEASURE_DOCUMENTS (
						  ID,FK_TBL_MEASURE,FK_TBL_DOCUMENTS,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 			    (
	                      NEW.ID,NEW.FK_TBL_MEASURE,NEW.FK_TBL_DOCUMENTS,NEW.CREATE_USER,NEW.CREATE_DATE,NEW.MOD_USER,NEW.MOD_DATE,
                          2,
						  current_timestamp,
					      NEW.MOD_USER
					    );

        RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_UPDATE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_MEASURE_DOCUMENTS_UPDATE_TRG ON PUBLIC.TBL_MEASURE_DOCUMENTS;

CREATE TRIGGER TBL_MEASURE_DOCUMENTS_UPDATE_TRG
  BEFORE UPDATE
  ON PUBLIC.TBL_MEASURE_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_MEASURE_DOCUMENTS_UPDATE_TRG();



-- PUBLIC.TBL_MEASURE_DOCUMENTS DELETE TRIGGER --
-- <GENERATED CODE!>

CREATE OR REPLACE FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_DELETE_TRG()
  RETURNS trigger AS
$BODY$
    BEGIN
    IF TG_OP = 'DELETE' THEN
	INSERT INTO HTBL_MEASURE_DOCUMENTS (

						  ID,FK_TBL_MEASURE,FK_TBL_DOCUMENTS,CREATE_USER,CREATE_DATE,MOD_USER,MOD_DATE,

						  HACTION,
						  HDATE,
						  HUSER
						 )
	VALUES 				(
						  OLD.ID,OLD.FK_TBL_MEASURE,OLD.FK_TBL_DOCUMENTS,OLD.CREATE_USER,OLD.CREATE_DATE,OLD.MOD_USER,OLD.MOD_DATE,

                          3,
						  current_timestamp,
					      OLD.MOD_USER );
	END IF;

        RETURN OLD;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION PUBLIC.TBL_MEASURE_DOCUMENTS_DELETE_TRG()
  OWNER TO PLGM_SERVICE;

DROP TRIGGER IF EXISTS TBL_MEASURE_DOCUMENTS_DELETE_TRG ON PUBLIC.TBL_MEASURE_DOCUMENTS;

CREATE TRIGGER TBL_MEASURE_DOCUMENTS_DELETE_TRG
  BEFORE DELETE
  ON PUBLIC.TBL_MEASURE_DOCUMENTS
  FOR EACH ROW
  EXECUTE PROCEDURE PUBLIC.TBL_MEASURE_DOCUMENTS_DELETE_TRG();
